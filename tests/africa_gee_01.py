# import modules
import os
import glob
import time
import numpy as np
import subprocess
import pandas as pd
import tensorflow as tf
import a1902alos.gee_app as ga
import a1902alos.grid_models as ggm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from concurrent import futures
import learn2map.raster_tools as rt

# %% produce csv for gee

# os.chdir("E:\data00\\a1902alostexture\data3")
os.chdir("Y:\\Projects\\a1902alostexture\\input")

# mch_list = glob.glob(
#     "F:\Data\MyBookDuo\Archived_Data\DRC_data\\001_lidar_rasters\Lidar_2meter\CHM\*.tif"
# )
# mch_train, mch_test = train_test_split(mch_list, test_size=0.2, random_state=77)
#
# df_lst = []
# for mch_file in mch_train:
#     start_time = time.time()
#     mch_newres, mch_count_newres = ga.raster_resample(mch_file, "africa_aeac_250m.tif")
#     df2 = ga.raster_to_vector(f"{mch_newres}", f"{mch_count_newres}")
#     os.remove(mch_newres)
#     os.remove(mch_count_newres)
#     df_lst.append(df2)
#     print(f"--- {time.time() - start_time} seconds ---")
# df = pd.concat(df_lst, axis=0)
# df["geometry"] = "Point"
# df.to_csv("output/mch_250m_training.csv", index=False)
#
# df_lst = []
# for mch_file in mch_test:
#     start_time = time.time()
#     mch_newres, mch_count_newres = ga.raster_resample(mch_file, "africa_aeac_250m.tif")
#     df2 = ga.raster_to_vector(f"{mch_newres}", f"{mch_count_newres}")
#     os.remove(mch_newres)
#     os.remove(mch_count_newres)
#     df_lst.append(df2)
#     print(f"--- {time.time() - start_time} seconds ---")
# df = pd.concat(df_lst, axis=0)
# df["geometry"] = "Point"
# df.to_csv("output/mch_250m_test.csv", index=False)


# df_lst = []
# for mch_file in mch_list:
#     start_time = time.time()
#     mch_newres, mch_count_newres = ga.raster_resample(mch_file, "mask_250m_all.tif")
#     df2 = ga.raster_to_vector(f"{mch_newres}", f"{mch_count_newres}", "tmp")
#     os.remove(mch_newres)
#     os.remove(mch_count_newres)
#     df_lst.append(df2)
#     print(f"--- {time.time() - start_time} seconds ---")
# df = pd.concat(df_lst, axis=0)
# df["geometry"] = "Point"


# df = pd.read_csv("csvs/mch_250m_v0.csv").dropna()
# df_train, df_test = train_test_split(df, test_size=0.2, random_state=77)
# df_train.to_csv("csvs/mch_250m_training_v1.csv", index=False)
# df_test.to_csv("csvs/mch_250m_test_v1.csv", index=False)


ref_file = "mask_250m_all.tif"
mch_100 = "lidar_data/drc/lidar_plots_mch.tif"
mch_newres = "lidar_data/drc/lidar_plots_mch_250m.tif"
mch_count_newres = "lidar_data/drc/lidar_plots_mch_tf_250m.tif"
mch_newres, mch_count_newres = ga.raster_resample(mch_100, ref_file)
df2 = ga.raster_to_vector(f"{mch_newres}", f"{mch_count_newres}", "tmp")
df2.compute().to_csv("csvs/mch_250m_geo_v2.csv", index=False)
df = pd.read_csv("csvs/mch_250m_geo_v2.csv").dropna()
df_train, df_test = train_test_split(df, test_size=0.2, random_state=77)
df_train.to_csv("csvs/mch_250m_training_v2.csv", index=False)
df_test.to_csv("csvs/mch_250m_test_v2.csv", index=False)


#     mch_newres, mch_count_newres = ga.raster_resample(mch_file, "mask_250m_all.tif")

print(0)

# %% merge tiff from gee to vrt
os.chdir("Y:\\Projects\\a1902alostexture\\input")
file_list = glob.glob("xlyrs_250m/*.tif")
in_file = "xlyrs_250m_files.txt"
with open(in_file, "w") as f:
    for file in file_list:
        print(f"{file}", file=f)

out_file = "xlyrs_250m_median_1317_v0.vrt"
gdal_expression_01 = f'gdalbuildvrt -overwrite -input_file_list "{in_file}" "{out_file}" --config GDAL_CACHEMAX 2000'
subprocess.check_output(gdal_expression_01, shell=True)


os.chdir("Y:\\Projects\\a1902alostexture\\input")
file_list = glob.glob("xlyrs_lc8_25m/*.tif")
in_file = "xlyrs_lc8_25m_files.txt"
with open(in_file, "w") as f:
    for file in file_list:
        print(f"{file}", file=f)

out_file = "xlyrs_lc8_25m_1317_v0.vrt"
gdal_expression_01 = f'gdalbuildvrt -overwrite -input_file_list "{in_file}" "{out_file}" --config GDAL_CACHEMAX 2000'
subprocess.check_output(gdal_expression_01, shell=True)


out_mask1 = "mask_250m_l8_b5.tif"
# cmmd = (
#     f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --type=Byte --co="COMPRESS=LZW" '
#     f'--co="PREDICTOR=2" -A "{out_file}" --A_band=1 --outfile="{out_mask1}" --calc="(A>0)*(A<1)"'
# )
# print(cmmd)
# subprocess.check_output(cmmd, shell=True)
out_mask2 = "mask_250m_srtm.tif"
# cmmd = (
#     f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --type=Byte --co="COMPRESS=LZW" '
#     f'--co="PREDICTOR=2" -A "{out_file}" --A_band=7 --outfile="{out_mask2}" --calc="(A>0)*(A<1000000)"'
# )
# print(cmmd)
# subprocess.check_output(cmmd, shell=True)

out_mask = "mask_250m_all.tif"
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --type=Byte --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=2" -A "{out_mask1}" -B "{out_mask2}" --outfile="{out_mask}" --calc="(A+B)>0"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)

# %% save training as tfrecords
os.chdir("Y:\\Projects\\a1902alostexture\\input")
path_tif = "xlyrs_250m_median_1317_v0.vrt"

# df = pd.read_csv("csvs/mch_250m_training_v2.csv")
# path_tfrecord = "tfrecords/training_250m_from_xlyrs_250m_1317_v0v2.tfrecord"
df = pd.read_csv("csvs/mch_250m_test_v2.csv")
path_tfrecord = "tfrecords/test_250m_from_xlyrs_250m_1317_v0v2.tfrecord"
ga.regression_tfrecord_from_df(
    df,
    path_tif,
    path_tfrecord,
    x_cols="X",
    # response="mch",    # for v1
    response="MCH",  # for v0, v2
    # loc=("latitude", "longitude"),  # for v0,v1
    loc=("y", "x"),  # for v2
    offset=(-0.0005, 0.0005),
    blocksize=(1, 1),
)

# %% save predictions
os.chdir("Y:\\Projects\\a1902alostexture\\input")
path_tif = "xlyrs_250m_median_1317_v0.vrt"
x_cols = ["B4", "B5", "B6", "B7", "HH", "HV", "elevation"]
path_mask = "mask_250m_all.tif"
out_name = "csvs/pred_xlyrs_250m_1317_v0"
ga.raster_to_vector(path_tif, path_mask, out_name, band_list=x_cols)

# %% training of tfrecords
print(0)
# os.chdir("Y:\\Projects\\a1902alostexture\\input")
os.chdir("/Volumes/wd4tb/a1902alostexture/input")
path_training = "tfrecords/training_250m_from_xlyrs_250m_1317_v0v2.tfrecord"
path_test = "tfrecords/test_250m_from_xlyrs_250m_1317_v0v2.tfrecord"


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    return X, Y


batch_size = 700
ds = ga.load_tf(
    path_training,
    x_features="X",
    # y_feature="mch",    # for v1
    y_feature="MCH",  # for v0, v2
    x_width=(1, 1, 7),
    y_width=(1,),
    compression="",
)
ds = ds.map(sample_normalize)
ds1 = ds.shuffle(100000).batch(batch_size).repeat()
print(iter(ds1.take(1)).next())

dst = ga.load_tf(
    path_test,
    x_features="X",
    # y_feature="mch",    # for v1
    y_feature="MCH",  # for v0, v2
    x_width=(1, 1, 7),
    y_width=(1,),
    compression="",
)
dst = dst.map(sample_normalize)
ds_test = dst.shuffle(100000).batch(batch_size).repeat()
# print(iter(ds_test.take(1)).next())

m = ga.DenseNet(
    num_layers_in_each_block=[12,],
    growth_rate=12,
    dropout_rate=0.2,
    weight_decay=1e-4,
    compression=1,
    bottleneck=True,
)
# m = ga.toy_resnet()
m.compile(
    optimizer=tf.keras.optimizers.Adam(0.01),
    loss=tf.keras.losses.get("MeanSquaredError"),
    metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
)

history = m.fit(
    x=ds1,
    epochs=50,
    steps_per_epoch=int(45875 / batch_size),
    validation_data=ds_test,
    validation_steps=int(11469 / batch_size),
)

plt.plot(history.history["root_mean_squared_error"], label="Training")
plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
plt.xlabel("Epoch")
plt.ylabel("RMSE")
plt.legend(loc="upper right")
plt.savefig("cnn1_output_v2_history.png", dpi=150)
plt.show()

y_list = []
X_list = []
for X, Y in ds_test.take(int(11469 / batch_size)):  # only take first element of dataset
    X_list.append(X.numpy())
    y_list.append(Y.numpy())
y_test = np.concatenate(y_list)
X_test = np.concatenate(X_list)
predictions = m.predict(X_test, verbose=1)
r2, rmse, beta0 = ggm.density_scatter_plot(
    y_test[~np.isnan(y_test)],
    predictions[~np.isnan(y_test)],
    file_name="{}_val.png".format("cnn1_output_v2"),
)


# %% training of tfrecords - cnn2 - using last feature of CNN for BC correction
print(0)
os.chdir("Y:\\Projects\\a1902alostexture\\input")
pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([0, 0.5, 1, 1.5, 2.5, 3.5, 5.5, 9.5])
width = np.array([1, 2, 3, 4, 6, 8, 12, 20])
k = 4
path_tif = "xlyrs_250m_median_1317_v0.vrt"

# df = pd.read_csv(f"csvs/mch_1500m_training_v2.csv")
# path_tfrecord = f"tfrecords/training_1500m_from_xlyrs_250m_1317_v0v2.tfrecord"
# ga.regression_tfrecord_from_df(
#     df,
#     path_tif,
#     path_tfrecord,
#     x_cols="X",
#     # response="mch",    # for v1
#     response="MCH",  # for v0, v2
#     # loc=("latitude", "longitude"),  # for v0,v1
#     loc=("y", "x"),  # for v2
#     offset=(-offsets[k], offsets[k]),
#     blocksize=(width[k], width[k]),
# )
# df = pd.read_csv(f"csvs/mch_1500m_test_v2.csv")
# path_tfrecord = f"tfrecords/test_1500m_from_xlyrs_250m_1317_v0v2.tfrecord"
# ga.regression_tfrecord_from_df(
#     df,
#     path_tif,
#     path_tfrecord,
#     x_cols="X",
#     # response="mch",    # for v1
#     response="MCH",  # for v0, v2
#     # loc=("latitude", "longitude"),  # for v0,v1
#     loc=("y", "x"),  # for v2
#     offset=(-offsets[k], offsets[k]),
#     blocksize=(width[k], width[k]),
# )

path_training = f"tfrecords/training_1500m_from_xlyrs_250m_1317_v0v2.tfrecord"
path_test = f"tfrecords/test_1500m_from_xlyrs_250m_1317_v0v2.tfrecord"


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    return X, Y


batch_size = 100
ds, train_n = ga.load_tf(
    path_training,
    x_features="X",
    # y_feature="mch",    # for v1
    y_feature="MCH",  # for v0, v2
    x_width=(6, 6, 7),
    y_width=(1,),
    compression="",
)
ds = ds.map(sample_normalize)
ds1 = ds.shuffle(100000).batch(batch_size).repeat()
# print(iter(ds1.take(1)).next())

dst, test_n = ga.load_tf(
    path_test,
    x_features="X",
    # y_feature="mch",    # for v1
    y_feature="MCH",  # for v0, v2
    x_width=(6, 6, 7),
    y_width=(1,),
    compression="",
)
dst = dst.map(sample_normalize)
ds_test = dst.shuffle(100000).batch(batch_size).repeat()
# print(iter(ds_test.take(1)).next())

# m = ga.toy_resnet()
m = ga.DenseNet(
    num_layers_in_each_block=[12, 24],
    growth_rate=24,
    dropout_rate=0.2,
    weight_decay=1e-4,
    compression=0.5,
    bottleneck=True,
    include_top=False,
)
global_average_layer = tf.keras.layers.GlobalAveragePooling2D(name="globalave")
dense_layer = tf.keras.layers.Dense(1024, activation="relu")
prediction_layer = tf.keras.layers.Dense(1)

m2 = tf.keras.Sequential([m, global_average_layer, dense_layer, prediction_layer])

m2.build(input_shape=(20, 6, 6, 7))
m2.summary()
feature_batch = m(iter(ds_test.take(1)).next()[0])
print(feature_batch.shape)
feature_batch_average = global_average_layer(feature_batch)
dense_batch = dense_layer(feature_batch_average)
prediction_batch = prediction_layer(dense_batch)
print(prediction_batch.shape)

m2.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.get("MeanSquaredError"),
    metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
)

history = m2.fit(
    x=ds1,
    epochs=100,
    steps_per_epoch=int(train_n / batch_size),
    validation_data=ds_test,
    validation_steps=int(test_n / batch_size),
)

plt.plot(history.history["root_mean_squared_error"], label="Training")
plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
plt.xlabel("Epoch")
plt.ylabel("RMSE")
plt.legend(loc="upper right")
plt.savefig("cnn2_output_v2_history.png", dpi=150)
plt.show()

y_list = []
X_list = []
for X, Y in ds_test.take(int(test_n / batch_size)):  # only take first element of dataset
    X_list.append(X.numpy())
    y_list.append(Y.numpy())
y_test = np.concatenate(y_list)
X_test = np.concatenate(X_list)
predictions = m2.predict(X_test, verbose=1)
r2, rmse, beta0 = ggm.density_scatter_plot(
    y_test[~np.isnan(y_test)],
    predictions[~np.isnan(y_test)],
    file_name="{}_val.png".format("cnn2_output_v2"),
)


#%% training using sklearn
print(0)
# os.chdir("Y:\\Projects\\a1902alostexture\\input")
os.chdir("/Volumes/wd4tb/a1902alostexture/input")
path_training = "tfrecords/training_250m_from_xlyrs_250m_1317_v0v2.tfrecord"
path_test = "tfrecords/test_250m_from_xlyrs_250m_1317_v0v2.tfrecord"

batch_size = 7000
ds = ga.load_tf(
    path_training,
    x_features="X",
    # y_feature="mch",    # for v1
    y_feature="MCH",  # for v0, v2
    x_width=(1, 1, 7),
    y_width=(1,),
    compression="",
)

dst = ga.load_tf(
    path_test,
    x_features="X",
    # y_feature="mch",    # for v1
    y_feature="MCH",  # for v0, v2
    x_width=(1, 1, 7),
    y_width=(1,),
    compression="",
)
ds_X = ds.batch(45875)
ds_Y = dst.batch(11469)
train_x, train_y = iter(ds_X.take(1)).next()
test_x, test_y = iter(ds_Y.take(1)).next()
train_x = train_x.numpy().squeeze()
train_y = train_y.numpy().squeeze()
test_x = test_x.numpy().squeeze()
test_y = test_y.numpy().squeeze()

# model_regress = ggm.StackRegressor(test_x, test_y)
# model_pipe, params_grid = model_regress.setup_xgb_model(
#     param_grid={
#         "pca": [None],
#         "learn__n_estimators": [250, 500, 750],
#         "learn__max_depth": [6, 9, 12],
#         "learn__booster": ["gbtree"],
#         "learn__colsample_bytree": [0.6, 1],
#     }
# )
# model_regress.grid_opts(model_pipe, params_grid, k=5)
#
# model_regress.meta_model_biascorr(X=train_x, y=train_y, k=3)
# model_y_pred = model_regress.meta_model_predict_biascorr(test_x, test_y)
# model_regress.save_models(outname="xgboost_bc_output_v2.pkl")
# # model_regress.meta_model(X=train_x, y=train_y, k=3)
# # model_y_pred = model_regress.meta_model_predict(test_x)
# # model_regress.save_models(outname="xgboost_output_v2.pkl")
#
# # model_y_pred[model_y_pred < 0] = 0
#
# if isinstance(test_y, pd.core.series.Series):
#     test_y = test_y.values
#
# r2, rmse, beta0 = ggm.density_scatter_plot(
#     test_y[~np.isnan(test_y)],
#     model_y_pred[~np.isnan(test_y)],
#     file_name="{}_val.png".format("xgboost_bc_output_v2"),
#     # file_name="{}_val.png".format("xgboost_output_v2"),
# )


model_regress = ggm.StackRegressor(test_x, test_y)
model_pipe, params_grid = model_regress.setup_rf_model(
    param_grid={
        "pca": [None],
        "learn__n_estimators": [500],
        # "learn__max_depth": [12],
        # "learn__min_samples_split": [2],
        "learn__max_depth": [6, 9, 12],
        "learn__min_samples_split": [2, 7, 12],
    }
)
model_regress.grid_opts(model_pipe, params_grid, k=5)

model_regress.meta_model_biascorr(X=train_x, y=train_y, k=3)
model_y_pred = model_regress.meta_model_predict_biascorr(test_x, test_y)
model_regress.save_models(outname="rf_bc_output_v2.pkl")
# model_regress.meta_model(X=train_x, y=train_y, k=3)
# model_y_pred = model_regress.meta_model_predict(test_x)
# model_regress.save_models(outname="rf_output_v2.pkl")

# model_y_pred[model_y_pred < 0] = 0

if isinstance(test_y, pd.core.series.Series):
    test_y = test_y.values

r2, rmse, beta0 = ggm.density_scatter_plot(
    test_y[~np.isnan(test_y)],
    model_y_pred[~np.isnan(test_y)],
    file_name="{}_val.png".format("rf_bc_output_v2"),
    # file_name="{}_val.png".format("rf_output_v2"),
)


#%% predict using xg-boost
print(0)
# os.chdir("Y:\\Projects\\a1902alostexture\\input")
os.chdir("/Volumes/wd4tb/a1902alostexture/input")

dst = ga.load_tf(
    path_test,
    x_features="X",
    # y_feature="mch",    # for v1
    y_feature="MCH",  # for v0, v2
    x_width=(1, 1, 7),
    y_width=(1,),
    compression="",
)
ds_Y = dst.batch(11469)
test_x, test_y = iter(ds_Y.take(1)).next()
test_x = test_x.numpy().squeeze()
test_y = test_y.numpy().squeeze()
model_regress = ggm.StackRegressor(test_x, test_y)
model_regress.load_models("xgboost_bc_output_v2.pkl")

path_tif = "xlyrs_250m_median_1317_v0.vrt"
path_mask = "mask_250m_all.tif"
out_name = "../output/MCH_est_from_xlyrs250mmedian1317v0_trainv2_xgboostv0"
ga.raster_predict(path_tif, path_mask, out_name, model_regress, depth=0, valid_min=0.8)


# %% training of tfrecords (different resolutions: 250m, 500, 750, 1k, 1.5k, 2k, 3k, 5k)
start = time.time()
os.chdir("Y:\\Projects\\a1902alostexture\\input")
# os.chdir('/Volumes/wd4tb/a1902alostexture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    return X, Y


res = [250, 500, 750, 1000, 1500, 2000, 3000, 5000]
pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([0, 0.5, 1, 1.5, 2.5, 3.5, 5.5, 9.5])
width = np.array([1, 2, 3, 4, 6, 8, 12, 20])
dn_structure = [
    (12,),
    (12,),
    (12, 18),
    (12, 18),
    (12, 18, 24),
    (12, 18, 24),
    (12, 18, 24),
    (12, 18, 24, 16),
]
epochs = [50, 100, 150, 200, 250, 300, 400, 500]

mch_100 = "lidar_data/drc/lidar_plots_mch.tif"
for k in range(3, 4):
    # for k in range(5, len(res)):
    #     ref_file = f"mask_{res[k]}m_all.tif"
    #     mch_newres = f"lidar_data/drc/lidar_plots_mch_{res[k]}m.tif"
    #     mch_count_newres = f"lidar_data/drc/lidar_plots_mch_tf_{res[k]}m.tif"
    #     mch_newres, mch_count_newres = ga.raster_resample(mch_100, ref_file, mch_newres, mch_count_newres)
    #     df2 = ga.raster_to_vector(f"{mch_newres}", f"{mch_count_newres}", "tmp", valid_min=0.5)
    #     df2.compute().to_csv(f"csvs/mch_{res[k]}m_geo_v2.csv", index=False)
    #     df = pd.read_csv(f"csvs/mch_{res[k]}m_geo_v2.csv").dropna()
    #     df_train, df_test = train_test_split(df, test_size=0.2, random_state=77)
    #     df_train.to_csv(f"csvs/mch_{res[k]}m_training_v2.csv", index=False)
    #     df_test.to_csv(f"csvs/mch_{res[k]}m_test_v2.csv", index=False)

    # path_tif = "xlyrs_250m_median_1317_v0.vrt"
    # df = pd.read_csv(f"csvs/mch_{res[k]}m_training_v2.csv")
    # path_tfrecord = f"tfrecords/training_{res[k]}m_from_xlyrs_250m_1317_v0v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="X",
    #     # response="mch",    # for v1
    #     response="MCH",  # for v0, v2
    #     # loc=("latitude", "longitude"),  # for v0,v1
    #     loc=("y", "x"),  # for v2
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )
    # df = pd.read_csv(f"csvs/mch_{res[k]}m_test_v2.csv")
    # path_tfrecord = f"tfrecords/test_{res[k]}m_from_xlyrs_250m_1317_v0v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="X",
    #     # response="mch",    # for v1
    #     response="MCH",  # for v0, v2
    #     # loc=("latitude", "longitude"),  # for v0,v1
    #     loc=("y", "x"),  # for v2
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )

    path_training = f"tfrecords/training_{res[k]}m_from_xlyrs_250m_1317_v0v2.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_from_xlyrs_250m_1317_v0v2.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="X",
        # y_feature="mch",    # for v1
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], 7),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="X",
        # y_feature="mch",    # for v1
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], 7),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds_test.take(1)).next())

    m = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=12,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.6,
        bottleneck=True,
    )
    # m = ga.toy_resnet()
    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    history = m.fit(
        x=ds1,
        epochs=epochs[k],
        steps_per_epoch=int(train_n / batch_size),
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    plt.savefig(f"cnn1_output_v2_{res[k]}m_history.png", dpi=300)
    plt.show()

    X_list = []
    y_list = []
    for X, Y in ds_test.take(
        int(test_n / batch_size)
    ):  # only take first element of dataset
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        file_name=f"cnn1_output_v2_{res[k]}m_val.png",
    )

    # X_list = []
    # y_list = []
    # for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_train = np.concatenate(X_list)
    # y_train = np.concatenate(y_list)
    # X_train = np.nanmean(X_train, axis=(1, 2))
    # y_train = y_train.squeeze()
    # X_test = np.nanmean(X_test, axis=(1, 2))
    # y_test = y_test.squeeze()
    #
    # model_regress = ggm.StackRegressor(X_test, y_test)
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [500],
    #         # "learn__max_depth": [12],
    #         # "learn__min_samples_split": [2],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__min_samples_split": [2, 7, 12],
    #     }
    # )
    # # model_pipe, params_grid = model_regress.setup_xgb_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [250, 500, 750],
    # #         "learn__max_depth": [6, 9, 12],
    # #         "learn__booster": ["gbtree"],
    # #         "learn__colsample_bytree": [0.6, 1],
    # #     }
    # # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)
    #
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # # model_regress.save_models(outname=f"xgboost_bc_output_v2_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"rf_bc_output_v2_{res[k]}m.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost_bc_output_v2_{res[k]}m"),
    #     file_name="{}_val.png".format(f"rf_bc_output_v2_{res[k]}m"),
    # )
    #
    # model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict(X_test)
    # # model_regress.save_models(outname=f"xgboost_output_v2_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"rf_output_v2_{res[k]}m.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost_output_v2_{res[k]}m"),
    #     file_name="{}_val.png".format(f"rf_output_v2_{res[k]}m"),
    # )

    print(time.time() - start)
