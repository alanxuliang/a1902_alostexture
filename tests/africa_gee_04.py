# import modules (using ALOS/SRTM/L8 layers @ 50m)
import os
import subprocess
import time
import glob
import gdal
import h5py
import a1902alos.gee_app as ga
import a1902alos.grid_models as ggm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import xarray as xr
from scipy import spatial
import scipy.ndimage as ndimage
import dask
import dask.array as dskda
import dask.dataframe as dd
import seaborn as sns
import learn2map.raster_tools as rt
from scipy.interpolate import UnivariateSpline
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import (
    train_test_split,
)
from sklearn.impute import SimpleImputer
from dask.diagnostics import ProgressBar
import zarr
from zarr import blosc

AUTOTUNE = tf.data.experimental.AUTOTUNE
compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)

# from dask.distributed import Client, progress
# client = Client(threads_per_worker=4, n_workers=1)
# client
print(1)


#%% reproject rasters
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")

ref_file = "xlyrs_50m_proba_2015_v2_vza90.vrt"
in_file = "xlyrs_250m_median_1317_v0_lc8bands.vrt"
out_file = "xlyrs_50m_2015_v0_lc8nbar/xlyrs_50m_median_1317_v0_lc8bands.tif"
rt.raster_clip(ref_file, in_file, out_file, resampling_method="near")


# %% training data generation (@ 250m)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

res = [250, ]

# lidar_path = [
#     "../input/lidar_data/drc/CHM",
#     "../input/lidar_data/gabon_tanzania",
# ]
# for k in range(len(res)):
#     ref_file = f"../input/mask_{res[k]}m_all.tif"
#     for path0 in lidar_path:
#         files = glob.glob(f"{path0}/*.tif")
#         for mch_file in files:
#             filename = os.path.basename(mch_file)
#             mch_newres = filename.replace(".tif", f"_{res[k]}m.tif")
#             mch_newres = f"../input/lidar_data/tmp/{mch_newres}"
#             mch_count_newres = filename.replace(".tif", f"_tf_{res[k]}m.tif")
#             mch_count_newres = f"../input/lidar_data/tmp/{mch_count_newres}"
#             mch_newres, mch_count_newres = ga.raster_resample(
#                 mch_file, ref_file, mch_newres, mch_count_newres, v_min=0, v_max=80
#             )
#
#             mch_newres_out = mch_newres.replace("/tmp/", "/all_africa_lidar_processed/valid_")
#             ga.raster_mask(mch_newres, mch_count_newres, mch_newres_out, valid_min=0.8)

# mch_csv_out = f"../input/csvs/gee04_mch_{res[0]}m_geo_v2"
# files = glob.glob(f"../input/lidar_data/all_africa_lidar_processed/*.tif")
# for file0 in files:
#     ga.raster_to_csv(file0, mch_csv_out, band_list=["MCH"], valid_min=0.0, write_mode="a+")

# mch_csv_out = f"../input/csvs/gee04_mch_{res[0]}m_training_v2"
# files = glob.glob(f"../input/lidar_data/all_africa_lidar_processed/*.tif")
# ga.raster_to_csv(files, mch_csv_out, band_list=["MCH"], valid_min=0.0)
#
# mch_csv_out = f"../input/csvs/gee04_mch_{res[0]}m_test_v2"
# files = glob.glob(f"../input/lidar_data/all_africa_lidar_processed/test/*.tif")
# ga.raster_to_csv(files, mch_csv_out, band_list=["MCH"], valid_min=0.0)

mch_csv_out = f"../input/csvs/mch_{res[0]}m_training_v1"
files = glob.glob(f"../input/lidar_data/all_africa_lidar_processed_v1/*.tif")
ga.raster_to_csv(files, mch_csv_out, band_list=["MCH"], valid_min=0.0)




#%% GLAS processing (glas-als overlap)

start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

# glas_file = "../input/lidar_data/glas_lidar/yan_106_global5_metrics_table.csv"
# df1 = pd.read_csv(glas_file)
#
# lidar_path = "../input/csvs"
# df2 = dd.read_csv(f"{lidar_path}/gee04_mch_250m_training_v2_*.csv").compute()
#
# tree = spatial.cKDTree(df1[["Lon", "Lat"]].values)
# mindist, minid = tree.query(df2[["X", "Y"]].values, k=10, distance_upper_bound=0.0025)
#
# x_lst = []
# y_lst = []
# for i in range(minid.shape[0]):
#     if np.sum(minid[i, :] < 6919604) > 0:
#         y_lst.append(df2.iloc[i, 1:].values)
#         idx = minid[i, minid[i, :] < 6919604]
#         x0 = df1.iloc[idx, 8:].values.mean(axis=0)
#         x_lst.append(x0)
# xy = np.concatenate([np.array(y_lst), np.array(x_lst)], axis=1)
# xy_col = df2.columns[1:].to_list() + df1.columns[8:].to_list()
# df3 = pd.DataFrame(xy, columns=xy_col)
# df3.to_csv("../input/lidar_data/glas_als_overlap.csv")
#

print(time.time()-start)

#%% GLAS processing (save all points on the map)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

glas_file = "../input/lidar_data/glas_lidar/yan_106_global5_metrics_table.csv"
df1 = pd.read_csv(glas_file)

daX = xr.open_rasterio("../input/mask_250m_all.tif")
id0 = np.argwhere(daX.data[0, :, :] > 0)
X1 = daX.x.values[id0[:, 1]][:, None]
Y1 = daX.y.values[id0[:, 0]][:, None]
del daX
del id0
tree = spatial.cKDTree(df1[["Lon", "Lat"]].values)
x_lst = []
y_lst = []
kspace = np.linspace(0, X1.shape[0], num=51).astype(int)
for k in range(kspace.shape[0]-1):
    print(k)
    X2 = X1[kspace[k]:kspace[k+1], :]
    Y2 = Y1[kspace[k]:kspace[k+1], :]
    _, minid = tree.query(np.concatenate([X2, Y2], axis=1), k=10, distance_upper_bound=0.0025)

    for i in range(minid.shape[0]):
        if np.sum(minid[i, :] < df1.shape[0]) > 2:
            idx = minid[i, minid[i, :] < df1.shape[0]]
            x0 = df1.iloc[idx, 8:].values.mean(axis=0)
            xy = np.concatenate([Y2[i:i+1, 0], X2[i:i+1, 0], x0])
            x_lst.append(xy)

xy = np.array(x_lst)
xy_col = ["Y", "X"] + df1.columns[8:].to_list()
df3 = pd.DataFrame(xy, columns=xy_col)
df3.to_csv("../input/lidar_data/glas_africa_all_3pts.csv")


print(time.time()-start)

#%% GLAS processing (filter points by GFC gain/loss)

start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

ref_file = "../input/mask_250m_all.tif"
in_file = "../input/gfc_gain_loss.vrt"
out_file = "../input/gfc_gain_loss_250m.tif"
# rt.raster_clip(ref_file, in_file, out_file, resampling_method="average")

df3 = pd.read_csv("../input/lidar_data/glas_als_overlap.csv", index_col=0)
da0 = xr.open_rasterio(out_file)
da1 = da0.mean("band")
ind1 = da1.sel(y=xr.DataArray(df3.Y.values, dims='z'),
               x=xr.DataArray(df3.X.values, dims='z'),
               method="nearest", tolerance=0.00225)

y = df3.MCH.values[ind1==0]
X = df3.iloc[:, 3:].values[ind1==0, :]
xgb = ggm.data_train_ensemble(X, y, outname="../input/lidar_data/glas_als_model_v2_stable")
# xgb = ggm.data_train_ridge(X, y, outname="../input/lidar_data/glas_als_model_v2_ridge_stable")


r2, rmse, beta0 = ggm.density_scatter_plot(
    y,
    xgb[0].meta_model_predict_biascorr(X),
    # xgb[0].meta_model_predict(X),
    # xgb[0].best_models[0].predict(X),
    file_name="../input/lidar_data/glas_als_model_v2_stable_xyscatter.png",
)

print(time.time()-start)


#%% GLAS shots predicted to MCH
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")


def save_geotiff_0(da1, out_name="20022017"):
    out_file1 = f"{out_name}.nc"
    ds1 = xr.Dataset({"da": da1})
    ds1.to_netcdf(out_file1)
    out_file2 = out_file1.replace(".nc", ".tif")
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)


# ref_file = "../input/mask_250m_all.tif"
# in_file = "../input/lc/CGLS_barevcf_250m_2015.vrt"
# out_file = "../input/lc/CGLS_barevcf_250m_2015_geo.tif"
# rt.raster_clip(ref_file, in_file, out_file, resampling_method="average")

# out_file = "../input/gfc_gain_loss_250m.tif"
# da0 = xr.open_rasterio(out_file)
# da_lc = xr.open_rasterio("../input/lc/CGLS_barevcf_250m_2015_geo.tif")
# da_tf = xr.where(da_lc > 50, 1, 0)
# da_all = xr.concat([da0, da_tf], dim="band")
# da1 = da_all.mean("band")
# save_geotiff_0(da1, "../input/lc/CGLS_GFC_meanvcf")

da1 = xr.open_rasterio("../input/lc/CGLS_GFC_meanvcf.tif")[0,:,:]
df3 = pd.read_csv("../input/lidar_data/glas_africa_all_3pts.csv", index_col=0)
ind1 = da1.sel(y=xr.DataArray(df3.Y.values, dims='z'),
               x=xr.DataArray(df3.X.values, dims='z'),
               method="nearest", tolerance=0.00225)
df1 = df3.iloc[ind1.values==0,:]
X = df3.iloc[ind1.values==0, 2:].values

df0 = pd.read_csv("../input/lidar_data/glas_als_overlap.csv", index_col=0)
y0 = df0.MCH.values
X0 = df0.iloc[:, 3:].values
model_regress = ggm.StackRegressor(X0, y0)
model_regress.load_models("../input/lidar_data/glas_als_model_v2_stable_training.pkl")
# mch_pred = model_regress.meta_model_predict(X)
mch_pred = model_regress.meta_model_predict_biascorr(X)
mch_pred[mch_pred < 0] = 0
# mch_pred = model_regress.best_models[0].predict(X)
# model_regress.load_models("../input/lidar_data/glas_als_model_v2_ridge_stable_training.pkl")
# mch_pred = model_regress.best_models[0].predict(X)
df1["MCH"] = mch_pred
df1.to_csv("../input/lidar_data/glas_africa_all_3pts_v2_stable_mch.csv")

print(time.time()-start)




#%% ATLAS preprocessing
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\atlas_lidar")

j = 0
files = glob.glob("atl08/*.h5")
for k in range(len(files)):
    print(files[k])
    h_lst = []
    gtx = ['gt1l', 'gt1r', 'gt2l', 'gt2r', 'gt3l', 'gt3r']
    for i in range(6):
        print(i)
        gti = gtx[i]
        with h5py.File(files[k], 'r') as f:
            try:
                lati = f[f'/{gti}/land_segments/latitude'].value[:, None]
                loni = f[f'/{gti}/land_segments/longitude'].value[:, None]
                hmetric1 = f[f'/{gti}/land_segments/canopy/canopy_h_metrics'].value
                hmetric2 = f[f'/{gti}/land_segments/canopy/h_canopy'].value[:, None]
                hmetric3 = f[f'/{gti}/land_segments/canopy/h_max_canopy'].value[:, None]
                hmetric4 = f[f'/{gti}/land_segments/canopy/h_mean_canopy'].value[:, None]
                hmetric5 = f[f'/{gti}/land_segments/canopy/canopy_openness'].value[:, None]
                hmetric6 = f[f'/{gti}/land_segments/canopy/toc_roughness'].value[:, None]
                hmetric7 = f[f'/{gti}/land_segments/terrain/terrain_slope'].value[:, None]
                h_all = np.concatenate([lati, loni, hmetric1, hmetric2, hmetric3,
                                        hmetric4, hmetric5, hmetric6, hmetric7], axis=1)
                try:
                    cloudflag = f[f'/{gti}/land_segments/cloud_flag_asr'].value
                except:
                    print("atm")
                    cloudflag = f[f'/{gti}/land_segments/cloud_flag_atm'].value
                canopyflag = f[f'/{gti}/land_segments/canopy/canopy_rh_conf'].value
                h_lst.append(h_all[(cloudflag == 0) & (canopyflag == 2) &
                                   (hmetric4.ravel() < 100) &
                                   (hmetric1[:, -2].ravel() < 100), :])
            except:
                print("segment corrupted")
    h_arr = np.concatenate(h_lst, axis=0)
    if h_arr.shape[0] > 0:
        df0 = pd.DataFrame(h_arr, columns=['lat', 'lon', 'rh25', 'rh50', 'rh60', 'rh70',
                                           'rh75', 'rh80', 'rh85', 'rh90', 'rh95', 'rh98',
                                           'rh100', 'hmean', 'hstd', 'hrough', 'tslope'])
        df0.to_csv(f"atl08_lidar_1819_{j}.csv", index=False)
        j = j+1


df = dd.read_csv("atl08_lidar_1819_*.csv").compute()


#%% ATLAS processing (atlas-als overlap)

start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\atlas_lidar")

glas_file = "atl08_all_1819_v1.csv"
df1 = pd.read_csv(glas_file)

lidar_path = "../../csvs"
df2 = pd.read_csv(f"{lidar_path}/mch_250m_training_v0v1.csv")

tree = spatial.cKDTree(df1[["lon", "lat"]].values)
mindist, minid = tree.query(df2[["longitude", "latitude"]].values, k=10, distance_upper_bound=0.00225)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i, :] < df1.shape[0]) > 2:
        y_lst.append(df2.iloc[i, 1:].values)
        idx = minid[i, minid[i, :] < df1.shape[0]]
        x0 = df1.iloc[idx, 3:].values.mean(axis=0)
        x_lst.append(x0)
xy = np.concatenate([np.array(y_lst), np.array(x_lst)], axis=1)
xy_col = df2.columns[1:].to_list() + df1.columns[3:].to_list()
df3 = pd.DataFrame(xy, columns=xy_col)
df3.to_csv("../atlas_als_overlap.csv")

print(time.time()-start)



#%% ATLAS processing (save all points on the map)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

glas_file = "../input/lidar_data/atlas_lidar/atl08_all_1819_v1.csv"
df1 = pd.read_csv(glas_file)

daX = xr.open_rasterio("../input/mask_250m_all.tif")
id0 = np.argwhere(daX.data[0, :, :] > 0)
X1 = daX.x.values[id0[:, 1]][:, None]
Y1 = daX.y.values[id0[:, 0]][:, None]
del daX
del id0
tree = spatial.cKDTree(df1[["lon", "lat"]].values)
x_lst = []
y_lst = []
kspace = np.linspace(0, X1.shape[0], num=51).astype(int)
for k in range(kspace.shape[0]-1):
    print(k)
    X2 = X1[kspace[k]:kspace[k+1], :]
    Y2 = Y1[kspace[k]:kspace[k+1], :]
    _, minid = tree.query(np.concatenate([X2, Y2], axis=1), k=10, distance_upper_bound=0.00225)

    for i in range(minid.shape[0]):
        if np.sum(minid[i, :] < df1.shape[0]) > 2:
            idx = minid[i, minid[i, :] < df1.shape[0]]
            x0 = df1.iloc[idx, 3:].values.mean(axis=0)
            xy = np.concatenate([Y2[i:i+1, 0], X2[i:i+1, 0], x0])
            x_lst.append(xy)

xy = np.array(x_lst)
xy_col = ["Y", "X"] + df1.columns[3:].to_list()
df3 = pd.DataFrame(xy, columns=xy_col)
df3.to_csv("../input/lidar_data/atlas_africa_all_3pts.csv")

print(time.time()-start)


#%% ATLAS processing (filter points by GFC gain/loss)

start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

ref_file = "../input/mask_250m_all.tif"
in_file = "../input/gfc_gain_loss.vrt"
out_file = "../input/gfc_gain_loss_250m.tif"
# rt.raster_clip(ref_file, in_file, out_file, resampling_method="average")

df3 = pd.read_csv("../input/lidar_data/atlas_als_overlap.csv", index_col=0)
da0 = xr.open_rasterio(out_file)
da1 = da0.mean("band")
ind1 = da1.sel(y=xr.DataArray(df3.latitude.values, dims='z'),
               x=xr.DataArray(df3.longitude.values, dims='z'),
               method="nearest", tolerance=0.00225)

y = df3.MCH.values[ind1==0]
X = df3.iloc[:, 3:].values[ind1==0, :]
xgb = ggm.data_train_ensemble(X, y, outname="../input/lidar_data/atlas_als_model_v2_stable")
# xgb = ggm.data_train_ridge(X, y, outname="../input/lidar_data/atlas_als_model_v2_ridge_stable")


r2, rmse, beta0 = ggm.density_scatter_plot(
    y,
    xgb[0].meta_model_predict_biascorr(X),
    # xgb[0].meta_model_predict(X),
    # xgb[0].best_models[0].predict(X),
    file_name="../input/lidar_data/atlas_als_model_v2_stable_xyscatter.png",
)

print(time.time()-start)


#%% ATLAS shots predicted to MCH
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")


def save_geotiff_0(da1, out_name="20022017"):
    out_file1 = f"{out_name}.nc"
    ds1 = xr.Dataset({"da": da1})
    ds1.to_netcdf(out_file1)
    out_file2 = out_file1.replace(".nc", ".tif")
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)


# ref_file = "../input/mask_250m_all.tif"
# in_file = "../input/lc/CGLS_barevcf_250m_2015.vrt"
# out_file = "../input/lc/CGLS_barevcf_250m_2015_geo.tif"
# rt.raster_clip(ref_file, in_file, out_file, resampling_method="average")

# out_file = "../input/gfc_gain_loss_250m.tif"
# da0 = xr.open_rasterio(out_file)
# da_lc = xr.open_rasterio("../input/lc/CGLS_barevcf_250m_2015_geo.tif")
# da_tf = xr.where(da_lc > 50, 1, 0)
# da_all = xr.concat([da0, da_tf], dim="band")
# da1 = da_all.mean("band")
# save_geotiff_0(da1, "../input/lc/CGLS_GFC_meanvcf")

da1 = xr.open_rasterio("../input/lc/CGLS_GFC_meanvcf.tif")[0,:,:]
df3 = pd.read_csv("../input/lidar_data/atlas_africa_all_3pts.csv", index_col=0)
ind1 = da1.sel(y=xr.DataArray(df3.Y.values, dims='z'),
               x=xr.DataArray(df3.X.values, dims='z'),
               method="nearest", tolerance=0.00225)
df1 = df3.iloc[ind1.values==0,:]
X = df3.iloc[ind1.values==0, 2:].values

df0 = pd.read_csv("../input/lidar_data/atlas_als_overlap.csv", index_col=0)
y0 = df0.MCH.values
X0 = df0.iloc[:, 3:].values
model_regress = ggm.StackRegressor(X0, y0)
model_regress.load_models("../input/lidar_data/atlas_als_model_v2_stable_training.pkl")
# mch_pred = model_regress.meta_model_predict(X)
mch_pred = model_regress.meta_model_predict_biascorr(X)
mch_pred[mch_pred < 0] = 0
# model_regress.load_models("../input/lidar_data/atlas_als_model_v2_ridge_stable_training.pkl")
# mch_pred = model_regress.best_models[0].predict(X)
df1["MCH"] = mch_pred
df1.to_csv("../input/lidar_data/atlas_africa_all_3pts_v2_stable_mch.csv")

print(time.time()-start)





#%% ATLAS - GLAS comparison

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
glas_file = "glas_africa_all_3pts_v2_stable_mch.csv"
df1 = pd.read_csv(glas_file)

df2 = pd.read_csv(f"atlas_africa_all_3pts_v2_stable_mch.csv")

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0011)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i] < df1.shape[0]) > 0:
        y_lst.append(df2.iloc[i, -1])
        idx = minid[i]
        x0 = df1.iloc[idx, -1]
        x_lst.append(x0)
xy = np.stack([np.array(y_lst), np.array(x_lst)], axis=1)

r2, rmse, beta0 = ggm.density_scatter_plot(
    xy[:, 0],
    xy[:, 1],
    x_label="ATLAS MCH (m)",
    y_label="GLAS MCH (m)",
    x_limit=[0, xy.max() * 1.1],
    file_name="atlas_glas_mch_stable_xyscatter.png",
)


#%% combine ATLAS-GLAS mch files

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
glas_file = "glas_africa_all_3pts_v2_stable_mch.csv"
df1 = pd.read_csv(glas_file)

df2 = pd.read_csv(f"atlas_africa_all_3pts_v2_stable_mch.csv")

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0011)

idx_tf = minid < df1.shape[0]
df2a = df2[~idx_tf]
# df2b = df2a[df2a.MCH < 15]
# n = df1[df1.MCH < 15].shape[0]
# df2c = df2b.sample(n)
n = df1.shape[0]
df2c = df2a.sample(n * 2 // 3)

df3 = pd.concat([df1, df2c], ignore_index=True)
df3.to_csv("glasxatlas_balanced_samples_v2_stable.csv")


#%% GEDI preprocessing

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")

j = 0
files = glob.glob("L2A/*.h5")
for k in range(len(files)):
    try:
        print(files[k])
        with h5py.File(files[k], 'r') as f:
            h_lst = []
            gtx = ['BEAM0000', 'BEAM0001', 'BEAM0010', 'BEAM0011', 'BEAM0101', 'BEAM0110', 'BEAM1000', 'BEAM1011']
            for i in range(len(gtx)):
                print(i)
                gti = gtx[i]
                try:
                    lati = f[f'/{gti}/lat_lowestmode'].value[:, None]
                    loni = f[f'/{gti}/lon_lowestmode'].value[:, None]
                    hmetric1 = f[f'/{gti}/rh'].value[:, np.arange(5, 101, 5).astype(int)]
                    hmetric2 = f[f'/{gti}/elev_lowestmode'].value[:, None]
                    hmetric3 = f[f'/{gti}/elevation_bin0_error'].value[:, None]
                    modeflag = f[f'/{gti}/num_detectedmodes'].value[:, None]
                    sensiflag = f[f'/{gti}/sensitivity'].value[:, None]
                    h_all = np.concatenate([lati, loni, hmetric1, hmetric2, hmetric3,
                                            modeflag, sensiflag], axis=1)
                    qualityflag = f[f'/{gti}/       '].value
                    lcflag = f[f'/{gti}/land_cover_data/landsat_treecover'].value
                    h_lst.append(h_all[(qualityflag == 1) & (lcflag > 0), :])
                except:
                    print("segment corrupted")
        h_arr = np.concatenate(h_lst, axis=0)
        if h_arr.shape[0] > 0:
            df0 = pd.DataFrame(h_arr, columns=['lat', 'lon'] + [f'rh{i}' for i in range(5, 101, 5)] +
                                               ['elev', 'elev_err', 'num_modes', 'sensitivity'])
            df0.to_csv(f"GEDI02A_lidar_2019_{j}.csv", index=False)
            j = j+1
    except:
        print("h5 file corrupted")


#%% GEDI processing (gedi-als overlap)

start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")

glas_file = "GEDI02A_lidar_2019_*.csv"
df1 = dd.read_csv(glas_file).compute()
df1.to_csv("gedi01a_lidar_part1.csv")
dim0 = df1.shape[0]

lidar_path = "../../csvs"
df2 = pd.read_csv(f"{lidar_path}/mch_250m_training_v0v1.csv")


tree = spatial.cKDTree(df1[["lon", "lat"]].values)
mindist, minid = tree.query(df2[["longitude", "latitude"]].values, k=50, distance_upper_bound=0.0025)
idsum = np.sum(minid < dim0, axis=1)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if idsum[i] > 4:
        y_lst.append(df2.iloc[i, 1:].values)
        idx = minid[i, minid[i, :] < dim0]
        x0 = df1.iloc[idx, 2:].values.mean(axis=0)
        x_lst.append(x0)
xy = np.concatenate([np.array(y_lst), np.array(x_lst)], axis=1)
xy_col = df2.columns[1:].to_list() + df1.columns[2:].to_list()
df3 = pd.DataFrame(xy, columns=xy_col)
df3.to_csv("../gedi_als_overlap.csv")

print(time.time()-start)





#%% GEDI processing (filter points by GFC gain/loss)

start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

ref_file = "../input/mask_250m_all.tif"
in_file = "../input/gfc_gain_loss.vrt"
out_file = "../input/gfc_gain_loss_250m.tif"
# rt.raster_clip(ref_file, in_file, out_file, resampling_method="average")

df3 = pd.read_csv("../input/lidar_data/gedi_als_overlap.csv", index_col=0)
df3 = df3[df3['elev_err'] < 1]
df3 = df3.drop(columns={'elev'})

da0 = xr.open_rasterio(out_file)
da1 = da0.mean("band")
ind1 = da1.sel(y=xr.DataArray(df3.latitude.values, dims='z'),
               x=xr.DataArray(df3.longitude.values, dims='z'),
               method="nearest", tolerance=0.00225)

y = df3.MCH.values[ind1==0]
X = df3.iloc[:, 3:].values[ind1==0, :]
xgb = ggm.data_train_ensemble(X, y, outname="../input/lidar_data/gedi_als_model_v2_stable")
# xgb = ggm.data_train_ridge(X, y, outname="../input/lidar_data/gedi_als_model_v2_ridge_stable")


r2, rmse, beta0 = ggm.density_scatter_plot(
    y,
    # xgb[0].meta_model_predict_biascorr(X),
    xgb[0].meta_model_predict(X),
    # xgb[0].best_models[0].predict(X),
    file_name="../input/lidar_data/gedi_als_model_v2_stable_xyscatter.png",
)

print(time.time()-start)








#%% GEDI processing (save all points on the map)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")

glas_file = "../input/lidar_data/gedi_lidar/gedi01a_lidar_part1.csv"
df1 = pd.read_csv(glas_file)
print(df1.columns)


df3 = df1[(df1.lat > -35) & (df1.lat < 38) & (df1.lon > -20) & (df1.lon < 52)]
print(df3.shape[0])
daX = xr.open_rasterio("../input/mask_250m_all.tif")
ind1 = daX.sel(y=xr.DataArray(df3.lat.values, dims='z'),
               x=xr.DataArray(df3.lon.values, dims='z'),
               method="nearest", tolerance=0.0016)
print(ind1.sum())


daX = xr.open_rasterio("../input/mask_250m_all.tif")
id0 = np.argwhere(daX.data[0, :, :] > 0)
X1 = daX.x.values[id0[:, 1]][:, None]
Y1 = daX.y.values[id0[:, 0]][:, None]
del daX
del id0
tree = spatial.cKDTree(df1[["lon", "lat"]].values)
x_lst = []
y_lst = []
kspace = np.linspace(0, X1.shape[0], num=51).astype(int)
for k in range(kspace.shape[0]-1):
    print(k)
    X2 = X1[kspace[k]:kspace[k+1], :]
    Y2 = Y1[kspace[k]:kspace[k+1], :]
    _, minid = tree.query(np.concatenate([X2, Y2], axis=1), k=10, distance_upper_bound=0.00225)

    for i in range(minid.shape[0]):
        count = np.sum(minid[i, :] < df1.shape[0])
        if np.sum(minid[i, :] < df1.shape[0]) > 2:
            idx = minid[i, minid[i, :] < df1.shape[0]]
            x0 = df1.iloc[idx, 3:].values.mean(axis=0)
            xy = np.concatenate([Y2[i:i+1, 0], X2[i:i+1, 0], x0])
            x_lst.append(xy)

xy = np.array(x_lst)
xy_col = ["Y", "X"] + df1.columns[3:].to_list()
df3 = pd.DataFrame(xy, columns=xy_col)
df3.to_csv("../input/lidar_data/gedi_africa_all_3pts.csv")

print(time.time()-start)





#%% GEDI shots predicted to MCH
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")


def save_geotiff_0(da1, out_name="20022017"):
    out_file1 = f"{out_name}.nc"
    ds1 = xr.Dataset({"da": da1})
    ds1.to_netcdf(out_file1)
    out_file2 = out_file1.replace(".nc", ".tif")
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)


# ref_file = "../input/mask_250m_all.tif"
# in_file = "../input/lc/CGLS_barevcf_250m_2015.vrt"
# out_file = "../input/lc/CGLS_barevcf_250m_2015_geo.tif"
# rt.raster_clip(ref_file, in_file, out_file, resampling_method="average")

# out_file = "../input/gfc_gain_loss_250m.tif"
# da0 = xr.open_rasterio(out_file)
# da_lc = xr.open_rasterio("../input/lc/CGLS_barevcf_250m_2015_geo.tif")
# da_tf = xr.where(da_lc > 50, 1, 0)
# da_all = xr.concat([da0, da_tf], dim="band")
# da1 = da_all.mean("band")
# save_geotiff_0(da1, "../input/lc/CGLS_GFC_meanvcf")

da1 = xr.open_rasterio("../input/lc/CGLS_GFC_meanvcf.tif")[0,:,:]
df3 = pd.read_csv("../input/lidar_data/gedi_africa_all_5pts.csv", index_col=0)
df3 = df3[df3['elev_err'] < 1]
df3 = df3.drop(columns={'elev'})
ind1 = da1.sel(y=xr.DataArray(df3.Y.values, dims='z'),
               x=xr.DataArray(df3.X.values, dims='z'),
               method="nearest", tolerance=0.00225)
df1 = df3.iloc[ind1.values==0,:]
X = df3.iloc[ind1.values==0, 2:].values

df0 = pd.read_csv("../input/lidar_data/gedi_als_overlap.csv", index_col=0)
y0 = df0.MCH.values
X0 = df0.iloc[:, 3:].values
model_regress = ggm.StackRegressor(X0, y0)
model_regress.load_models("../input/lidar_data/gedi_als_model_v2_stable_training.pkl")
mch_pred = model_regress.meta_model_predict(X)
# mch_pred = model_regress.meta_model_predict_biascorr(X)
mch_pred[mch_pred < 0] = 0
# model_regress.load_models("../input/lidar_data/gedi_als_model_v2_ridge_stable_training.pkl")
# mch_pred = model_regress.best_models[0].predict(X)
df1["MCH"] = mch_pred
df1.to_csv("../input/lidar_data/gedi_africa_all_5pts_v2_stable_mch.csv")

print(time.time()-start)






#%% GEDI - GLAS comparison

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
glas_file = "glas_africa_all_3pts_v2_stable_mch.csv"
df1 = pd.read_csv(glas_file)

df2 = pd.read_csv(f"gedi_africa_all_5pts_v2_stable_mch.csv")

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0011)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i] < df1.shape[0]) > 0:
        y_lst.append(df2.iloc[i, -1])
        idx = minid[i]
        x0 = df1.iloc[idx, -1]
        x_lst.append(x0)
xy = np.stack([np.array(y_lst), np.array(x_lst)], axis=1)

r2, rmse, beta0 = ggm.density_scatter_plot(
    xy[:, 0],
    xy[:, 1],
    x_label="ATLAS MCH (m)",
    y_label="GLAS MCH (m)",
    x_limit=[0, xy.max() * 1.1],
    file_name="gedi_glas_mch_stable_xyscatter.png",
)



#%% GEDI - GLAS new MCH model

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
glas_file = "glas_africa_all_3pts_v2_stable_mch.csv"
df1 = pd.read_csv(glas_file, index_col=0)

df2 = pd.read_csv(f"gedi_africa_all_5pts_v2_stable_mch.csv", index_col=0)

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0011)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i] < df1.shape[0]) > 0:
        y_lst.append(df2.iloc[i, -1])
        idx = minid[i]
        x0 = df1.iloc[idx, 0:-1].values
        x_lst.append(x0)
y = np.array(y_lst)
X = np.stack(x_lst, axis=0)

xgb = ggm.data_train_ensemble(X, y, outname="glas_gedi_model_v2_stable")
mch_pred = xgb[0].meta_model_predict(df1.iloc[:, 0:-1].values)
df1["MCH_from_gedi"] = mch_pred
df1.to_csv("glas_africa_all_3pts_v2_stable_mch_gedi.csv")



#%% GEDI-dervied GLAS MCH vs ALS

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
glas_file = "glas_africa_all_3pts_v2_stable_mch_gedi.csv"
df1 = pd.read_csv(glas_file, index_col=0)

df2 = pd.read_csv(f"glas_als_overlap.csv", index_col=0)

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0025)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i] < df1.shape[0]) > 0:
        y_lst.append(df2.iloc[i, 2])
        idx = minid[i]
        x0 = df1.iloc[idx, -1]
        x_lst.append(x0)
xy = np.stack([np.array(y_lst), np.array(x_lst)], axis=1)

r2, rmse, beta0 = ggm.density_scatter_plot(
    xy[:, 0],
    xy[:, 1],
    x_label="ALS MCH (m)",
    y_label="GLAS MCH (m)",
    x_limit=[0, xy.max() * 1.1],
    file_name="gediglas_als_mch_stable_xyscatter.png",
)



#%% GEDI - atlas comparison

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
atlas_file = "atlas_africa_all_3pts_v2_stable_mch.csv"
df1 = pd.read_csv(atlas_file)

df2 = pd.read_csv(f"gedi_africa_all_5pts_v2_stable_mch.csv")

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0011)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i] < df1.shape[0]) > 0:
        y_lst.append(df2.iloc[i, -1])
        idx = minid[i]
        x0 = df1.iloc[idx, -1]
        x_lst.append(x0)
xy = np.stack([np.array(y_lst), np.array(x_lst)], axis=1)

r2, rmse, beta0 = ggm.density_scatter_plot(
    xy[:, 0],
    xy[:, 1],
    x_label="ATLAS MCH (m)",
    y_label="atlas MCH (m)",
    x_limit=[0, xy.max() * 1.1],
    file_name="gedi_atlas_mch_stable_xyscatter.png",
)



#%% GEDI - atlas new MCH model

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
atlas_file = "atlas_africa_all_3pts_v2_stable_mch.csv"
df1 = pd.read_csv(atlas_file, index_col=0)

df2 = pd.read_csv(f"gedi_africa_all_5pts_v2_stable_mch.csv", index_col=0)

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0011)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i] < df1.shape[0]) > 0:
        y_lst.append(df2.iloc[i, -1])
        idx = minid[i]
        x0 = df1.iloc[idx, 0:-1].values
        x_lst.append(x0)
y = np.array(y_lst)
X = np.stack(x_lst, axis=0)

xgb = ggm.data_train_ensemble(X, y, outname="atlas_gedi_model_v2_stable")
mch_pred = xgb[0].meta_model_predict(df1.iloc[:, 0:-1].values)
df1["MCH_from_gedi"] = mch_pred
df1.to_csv("atlas_africa_all_3pts_v2_stable_mch_gedi.csv")



#%% GEDI-dervied atlas MCH vs ALS

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
atlas_file = "atlas_africa_all_3pts_v2_stable_mch_gedi.csv"
df1 = pd.read_csv(atlas_file, index_col=0)

df2 = pd.read_csv(f"atlas_als_overlap.csv", index_col=0)

tree = spatial.cKDTree(df1[["X", "Y"]].values)
mindist, minid = tree.query(df2[["longitude", "latitude"]].values, k=1, distance_upper_bound=0.0025)

x_lst = []
y_lst = []
for i in range(minid.shape[0]):
    if np.sum(minid[i] < df1.shape[0]) > 0:
        y_lst.append(df2.iloc[i, 2])
        idx = minid[i]
        x0 = df1.iloc[idx, -1]
        x_lst.append(x0)
xy = np.stack([np.array(y_lst), np.array(x_lst)], axis=1)

r2, rmse, beta0 = ggm.density_scatter_plot(
    xy[:, 0],
    xy[:, 1],
    x_label="ALS MCH (m)",
    y_label="atlas MCH (m)",
    x_limit=[0, xy.max() * 1.1],
    file_name="gediatlas_als_mch_stable_xyscatter.png",
)



#%% combine GEDI-ATLAS-GLAS mch files (not done)

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
df0 = pd.read_csv("gedi_africa_all_5pts_v2_stable_mch.csv")
df1 = pd.read_csv("glas_africa_all_3pts_v2_stable_mch.csv")
df2 = pd.read_csv(f"atlas_africa_all_3pts_v2_stable_mch.csv")

n = df0.shape[0]
tree = spatial.cKDTree(df0[["X", "Y"]].values)
mindist, minid = tree.query(df1[["X", "Y"]].values, k=1, distance_upper_bound=0.0022)
idx_tf = minid < n
df1a = df1[~idx_tf]
mindist, minid = tree.query(df2[["X", "Y"]].values, k=1, distance_upper_bound=0.0022)
idx_tf = minid < n
df2a = df2[~idx_tf]

k = df1a.shape[0] * 3

df3 = pd.concat([df0.sample(k), df1a, df2a.sample(k//3)], ignore_index=True)
df3.to_csv("gedixglasxatlas_balanced_samples_v2_stable.csv")



#%% combine GEDI-GLAS mch files

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
df0 = pd.read_csv("gedi_africa_all_5pts_v2_stable_mch.csv")
# df1 = pd.read_csv("glas_africa_all_3pts_v2_stable_mch_gedi.csv")
df1 = pd.read_csv("glas_africa_all_3pts_v2_stable_mch.csv")

n = df0.shape[0]
tree = spatial.cKDTree(df0[["X", "Y"]].values)
mindist, minid = tree.query(df1[["X", "Y"]].values, k=1, distance_upper_bound=0.0022)
idx_tf = minid < n
df1a = df1[~idx_tf]
# df1a = df1a.drop(columns="MCH").rename(columns={"MCH_from_gedi": "MCH"})

k = df1a.shape[0] * 2

df3 = pd.concat([df0.sample(k), df1a], join="inner", ignore_index=True)
# df3.to_csv("gedixglas_balanced_samples_v2_stable.csv")
df3.to_csv("gedixglas_balanced_samples_v2_stable2.csv")



#%% combine GEDI-GLAS + ALS mch files (filtered by LC)

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
df0 = pd.read_csv("../csvs/mch_250m_training_v0v1.csv", index_col=0)
# df1 = pd.read_csv("gedixglas_balanced_samples_v2_stable.csv", index_col=0)\
#     .rename(columns={"X": "longitude", "Y": "latitude"})
df1 = pd.read_csv("gedixglas_balanced_samples_v2_stable2.csv", index_col=0)\
    .rename(columns={"X": "longitude", "Y": "latitude"})

n = df0.shape[0]
tree = spatial.cKDTree(df0[["longitude", "latitude"]].values)
mindist, minid = tree.query(df1[["longitude", "latitude"]].values, k=1, distance_upper_bound=0.0022)
idx_tf = minid < n
df1a = df1[~idx_tf]

df_all = pd.concat([df0, df1a.sample(df0.shape[0])], join="inner", ignore_index=True)

da1 = xr.open_rasterio("../lc/CGLS_GFC_meanvcf.tif")[0,:,:]
ind1 = da1.sel(y=xr.DataArray(df_all.latitude.values, dims='z'),
               x=xr.DataArray(df_all.longitude.values, dims='z'),
               method="nearest", tolerance=0.00225)
df_final = df_all.iloc[ind1.values==0,:]
df_final.to_csv("../csvs/mch_250m_training_v0v1_gedixglas_v2.csv")





#%% combine GLAS + ALS mch files (filtered by LC)

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data")
df0 = pd.read_csv("../csvs/mch_250m_training_v0v1.csv", index_col=0)
# df1 = pd.read_csv("gedixglas_balanced_samples_v2_stable.csv", index_col=0)\
#     .rename(columns={"X": "longitude", "Y": "latitude"})
# df1 = pd.read_csv("gedixglas_balanced_samples_v2_stable2.csv", index_col=0)\
#     .rename(columns={"X": "longitude", "Y": "latitude"})
df1 = pd.read_csv("glas_africa_all_3pts_v2_stable_mch.csv", index_col=0)\
    .rename(columns={"X": "longitude", "Y": "latitude"})

n = df0.shape[0]
tree = spatial.cKDTree(df0[["longitude", "latitude"]].values)
mindist, minid = tree.query(df1[["longitude", "latitude"]].values, k=1, distance_upper_bound=0.0022)
idx_tf = minid < n
df1a = df1[~idx_tf]

df_all = pd.concat([df0, df1a.sample(df0.shape[0])], join="inner", ignore_index=True)

da1 = xr.open_rasterio("../lc/CGLS_GFC_meanvcf.tif")[0,:,:]
ind1 = da1.sel(y=xr.DataArray(df_all.latitude.values, dims='z'),
               x=xr.DataArray(df_all.longitude.values, dims='z'),
               method="nearest", tolerance=0.00225)
df_final = df_all.iloc[ind1.values==0,:]
df_final.to_csv("../csvs/mch_250m_training_v0v1_glas_v2.csv")




#%% training models - 7 bands from 50m proba-lyrs (2015)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.6161337e-02,
                    4.7125414e-02,
                    2.9189381e-01,
                    1.5104064e-01,
                    1.8675402e-01,
                    6.4551055e-02,
                    4.1073050e+02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.0501601e-02,
                    1.8563354e-02,
                    4.1548073e-02,
                    4.6346959e-02,
                    8.2421511e-02,
                    3.0078700e-02,
                    2.3043668e+02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.6161337e-02,
            4.7125414e-02,
            2.9189381e-01,
            1.5104064e-01,
            1.8675402e-01,
            6.4551055e-02,
            4.1073050e+02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            1.0501601e-02,
            1.8563354e-02,
            4.1548073e-02,
            4.6346959e-02,
            8.2421511e-02,
            3.0078700e-02,
            2.3043668e+02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    return X



nbands = 7

pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [10, ]
res = [250, ]

for k in range(0, len(width)):

    df = pd.read_csv("csvs/mch_250m_training_v0v1.csv")
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv")

    path_tif = "xlyrs_50m_proba_2015_v2_vza90.vrt"
    # path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2015_v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     response="MCH",
    #     # loc=("Y", "X"),
    #     loc=("latitude", "longitude"),
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )
    # path_tfrecord = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2015_v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df_test,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     response="MCH",
    #     # loc=("Y", "X"),
    #     loc=("latitude", "longitude"),
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )


    path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2015_v2.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_width{width[k]}_from_xlyrs_2015_v2.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()


    # CNN model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        # loss=tf.keras.losses.get("MeanSquaredError"),
        loss=tf.keras.losses.get("MeanAbsoluteError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    history = m.fit(
        x=ds1,
        epochs=epochs[k],
        steps_per_epoch=int(train_n / batch_size),
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    plt.savefig(f"../output/cnn50m_output2015_v2_{res[k]}m_width{width[k]}_history_mae.png", dpi=300)
    plt.show()
    m.save_weights(f"../output/cnn50m_output2015_v2_{res[k]}m_width{width[k]}_trained_weights_mae.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        file_name=f"../output/cnn50m_output2015_v2_{res[k]}m_width{width[k]}_val_mae.png",
    )

    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})

    tf.keras.backend.clear_session()

    # compare with RF model using spatial mean of each band
    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_train = np.nanmean(X_train, axis=(1, 2))
    y_train = y_train.squeeze()
    X_test1 = np.nanmean(X_test, axis=(1, 2))
    y_test = y_test.squeeze()

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test1, y_test)
    # model_regress.save_models(outname=f"xgboost50m_bc_output2015_v2_{res[k]}m.pkl")
    model_regress.save_models(outname=f"../output/rf50m_bc_output2015_v2_{res[k]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost50m_bc_output2015_v2_{res[k]}m"),
        file_name="{}_val.png".format(f"../output/rf50m_bc_output2015_v2_{res[k]}m_width{width[k]}"),
    )

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict(X_test1)
    # model_regress.save_models(outname=f"xgboost50m_output2015_v2_{res[k]}m.pkl")
    model_regress.save_models(outname=f"../output/rf50m_output2015_v2_{res[k]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost50m_output2015_v2_{res[k]}m"),
        file_name="{}_val.png".format(f"../output/rf50m_output2015_v2_{res[k]}m_width{width[k]}"),
    )
    dfy['y_rfbc'] = model_y_pred[~np.isnan(y_test)]

    # compare with CNN+linear model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn50m_output2015_v2_{res[k]}m_width{width[k]}_trained_weights.h5")
    mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)

    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_mlayer_train = mlayer_model.predict(X_train, verbose=1)

    # X_list = []
    # y_list = []
    # for X, Y in ds_test.take(int(test_n / batch_size)):
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_test = np.concatenate(X_list)
    # y_test = np.concatenate(y_list)
    X_mlayer_test = mlayer_model.predict(X_test, verbose=1)

    model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [100, 200, 400, 600],
    #         "learn__max_depth": [3, 6, 9, 12],
    #         # "learn__booster": ["gbtree","gblinear", "dart"],
    #         "learn__booster": ["dart"],
    #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    #     }
    # )
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [300],
    #         # "learn__max_depth": [6, 9, 12],
    #         # "learn__min_samples_split": [2, 7, 12],
    #         "learn__max_depth": [9],
    #         "learn__min_samples_split": [2],
    #     }
    # )
    model_regress.grid_opts(model_pipe, params_grid, k=3)
    # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    model_y_pred = model_regress.meta_model_predict(X_mlayer_test)

    model_regress.save_models(outname=f"../output/ridge50m_mlayer_model_v2_{res[k]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    y_test = y_test.ravel()
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"../output/ridge50m_mlayer_model_v2_{res[k]}m_width{width[k]}"),
    )
    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": model_y_pred[~np.isnan(y_test)]})
    dfy["y_ridgetest"] = y_test[~np.isnan(y_test)]
    dfy.to_csv(f"../output/cnn50m_output2015_v2_{res[k]}m_width{width[k]}_val_ridge.csv")

    # dfy.to_csv(f"../output/cnn250m_output2010_v3_{res[k]}m_val_data.csv")
    tf.keras.backend.clear_session()

    print(time.time() - start)



#%% predict model - 7bands from 50m proba-lyrs (2015)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def func_predict_cnn_0(
        mask_arr,
        # block_id=None,
        # chunksize=600,
        # xdata=None,
        # ydata=None,
        # model=None,
        # out_name=None
):
    # max_x = min((block_id[2] + 1) * chunksize, xdata.shape[0])
    # max_y = min((block_id[1] + 1) * chunksize, ydata.shape[0])
    # idx = np.arange(block_id[2] * chunksize + 2, max_x, 5)
    # idy = np.arange(block_id[1] * chunksize + 2, max_y, 5)
    # x1 = xdata[idx]
    # y1 = ydata[idy]
    # arr_new = np.moveaxis(mask_arr, 0, -1)[None, :, :, :]
    # arr_patch = tf.image.extract_patches(images=arr_new,
    #                                      sizes=[1, 15, 15, 1],
    #                                      strides=[1, 5, 5, 1],
    #                                      rates=[1, 1, 1, 1],
    #                                      padding='VALID')
    # arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
    # pred_X = arr_table[arr_table[:, 790] > -1000, :]
    # pred_X = sample_normalize_1(pred_X.reshape(-1, 7)).reshape(-1, arr_table.shape[1])
    # arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, 7)
    # print(arr_cnn.shape)
    # if pred_X.shape[0] > 0:
    #     model_y_pred = model.predict(arr_cnn)
    #     # model_y_pred = model.meta_model_predict_biascorr(pred_X)
    #
    #     tf.keras.backend.clear_session()
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
    #     pred_arr[arr_table[:, 790] > -1000] = model_y_pred.ravel()
    #     pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
    #     print(f"pred_shape: {pred_arr.shape}")
    # else:
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")
    #
    # da = xr.DataArray(data=pred_arr, coords=[y1[:pred_arr.shape[0]], x1[:pred_arr.shape[1]]], dims=['y', 'x'])
    # out_file1 = f"{out_name}_{block_id[1]}_{block_id[2]}.nc"
    # ds1 = xr.Dataset({"da": da})
    # ds1.to_netcdf(out_file1)
    # # np.save(out_file, pred_arr)
    #
    # del model
    # del ydata
    # del xdata
    # del pred_arr
    # del arr_new
    # del arr_patch
    # del arr_table
    # del arr_cnn
    # del pred_X
    return mask_arr
    # return np.array([[1]])


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn50m_output2015_v2_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        model_cnn,
        chunksize=None,
        out_name=None
):
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, 790] > -1000, :]
        pred_X = sample_normalize_1(pred_X.reshape(-1, 7)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, 7)
        print(arr_cnn.shape)
        if pred_X.shape[0] > 0:
            # model_cnn = load_model((12, 18, 24), 250, 15, 7)
            model_y_pred = model_cnn[0].predict(arr_cnn)
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, 790] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.6161337e-02,
                    4.7125414e-02,
                    2.9189381e-01,
                    1.5104064e-01,
                    1.8675402e-01,
                    6.4551055e-02,
                    4.1073050e+02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.0501601e-02,
                    1.8563354e-02,
                    4.1548073e-02,
                    4.6346959e-02,
                    8.2421511e-02,
                    3.0078700e-02,
                    2.3043668e+02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.6161337e-02,
            4.7125414e-02,
            2.9189381e-01,
            1.5104064e-01,
            1.8675402e-01,
            6.4551055e-02,
            4.1073050e+02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            1.0501601e-02,
            1.8563354e-02,
            4.1548073e-02,
            4.6346959e-02,
            8.2421511e-02,
            3.0078700e-02,
            2.3043668e+02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


nbands = 7
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [10, ]
res = [250, ]
buffer = [5, ]

for k in range(0, len(width)):
    df = pd.read_csv("csvs/mch_250m_training_v0v1.csv")
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv")

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_proba_2015_v2_vza90.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))    # da_b = da_mask.band.values
    # da_x = da_mask.x.values
    # da_y = da_mask.y.values
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        # print(ij)
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        # results.append(func_predict_cnn(ij, da_mask, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
        results.append(func_predict_cnn(ij, da_mask, model, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 0, 2: 0},
    #                           boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'})
    # file_id = zarr.open_array("../output/tmp_dask.zarr", shape=g.shape, mode="w",
    #                           chunks=(nbands, chunk_size+10, chunk_size+10),
    #                           synchronizer=zarr.ThreadSynchronizer())
    # dskda.store(g, file_id)
    # g2 = g.map_blocks(func_predict_cnn, drop_axis=[0, 1, 2])
    # da_pred = dskda.overlap.trim_internal(g2, {0: 0, 1: 5, 2: 5})
    # da_pred = g.map_blocks(func_predict_cnn, dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
    #                             dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             # boundary={0: 'none', 1: 'none', 2: 'none'},
    #                             trim=True)

    # out_file1 = f"../output/mch_prediction_dask.zarr"
    # # file_id = zarr.open_array(out_file1, shape=da_pred.shape, mode="w",
    # #                           chunks=(nbands, chunk_size, chunk_size),
    # #                           # synchronizer=zarr.ThreadSynchronizer(),
    # #                           )
    # # dskda.store(da_pred, file_id)
    # delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True,
    #                             synchronizer=zarr.ThreadSynchronizer(),
    #                             compute=False)
    # # out_file1 = f"../output/mch_prediction_dask.nc"
    # # da1 = xr.DataArray(da_pred, coords=(da_b, da_y, da_x), dims=("band", "y", "x"))
    # # ds2 = xr.Dataset({"da": da1})
    # # delayed_obj = ds2.to_netcdf(out_file1, compute=True)
    # # with ProgressBar():
    # #     results = delayed_obj.compute(num_workers=1)

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_output2015_v2_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_output2015_v2_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)




#%% training models - 11 bands from 50m probaxlc8-lyrs (2015) - glas+airborne - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def random_rotate_image(image):
  image = ndimage.rotate(image, np.random.uniform(-90, 90), reshape=False)
  return image


def augment(image,label):
  # print(image.shape)
  image = tf.image.random_brightness(image, max_delta=0.3)
  image = tf.image.random_contrast(image, 0.7, 1.3)
  image = tf.image.random_flip_up_down(image)
  image = tf.image.random_flip_left_right(image)
  image = tf.image.resize_with_crop_or_pad(image, 20, 20)
  im_shape = image.shape
  [image,] = tf.py_function(random_rotate_image, [image], [tf.float32])
  image.set_shape(im_shape)
  image = tf.image.random_crop(image, size=[15, 15, 11])
  return image,label


nbands = 11

pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [128, ]
res = [250, ]

for k in range(0, len(width)):

    # df_all = pd.read_csv("csvs/mch_250m_training_v0v1_gedixglasxatlas_v2.csv", index_col=0)
    # df_all = pd.read_csv("csvs/mch_250m_training_v0v1_gedixglas_v2.csv", index_col=0)
    df_all = pd.read_csv("csvs/mch_250m_training_v0v1_glas_v2.csv", index_col=0)
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv", index_col=0)

    path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
    # path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2015_v3.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df_all,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     response="MCH",
    #     # loc=("Y", "X"),
    #     loc=("latitude", "longitude"),
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )
    # path_tfrecord = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2015_v3.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df_test,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     response="MCH",
    #     # loc=("Y", "X"),
    #     loc=("latitude", "longitude"),
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )

    path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2015_v3.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_width{width[k]}_from_xlyrs_2015_v3.tfrecord"

    batch_size = 256
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds1 = (
            ds
            .cache()
            .shuffle(220000)
            .repeat()
            .map(sample_normalize)
            .batch(batch_size)
            .prefetch(AUTOTUNE)
    )
    # ds = ds.map(sample_normalize)
    # ds1 = ds.shuffle(80000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds_test = (
            dst
            .map(sample_normalize)
            .batch(batch_size)
    )
    # dst = dst.map(sample_normalize)
    # ds_test = dst.shuffle(80000).batch(batch_size).repeat()


    # CNN model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        # loss=tf.keras.losses.get("MeanAbsoluteError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    earlystop_callback = tf.keras.callbacks.EarlyStopping(
        monitor='val_loss', min_delta=0.01, verbose=1,
        patience=30, restore_best_weights=True)

    history = m.fit(
        x=ds1,
        epochs=epochs[k], callbacks=[earlystop_callback],
        # steps_per_epoch=int(train_n / batch_size),
        steps_per_epoch=76,
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    # plt.savefig(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_history_mae.png", dpi=300)
    plt.savefig(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_history.png", dpi=300)
    plt.show()
    # m.save_weights(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_trained_weights_mae.h5")
    m.save_weights(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        # file_name=f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_val_mae.png",
        file_name=f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_val.png",
    )

    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})

    tf.keras.backend.clear_session()


    # # compare with RF model using spatial mean of each band
    # X_list = []
    # y_list = []
    # for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_train = np.concatenate(X_list)
    # y_train = np.concatenate(y_list)
    # X_train = np.nanmean(X_train, axis=(1, 2))
    # y_train = y_train.squeeze()
    # X_test1 = np.nanmean(X_test, axis=(1, 2))
    # y_test = y_test.squeeze()
    #
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [500],
    #         # "learn__max_depth": [12],
    #         # "learn__min_samples_split": [2],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__min_samples_split": [2, 7, 12],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)
    #
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test1, y_test)
    # # model_regress.save_models(outname=f"xgboost50m_bc_output2015_v3_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"../output/rf50m_bc_output2015_v3_{res[k]}m_width{width[k]}.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost50m_bc_output2015_v3_{res[k]}m"),
    #     file_name="{}_val.png".format(f"../output/rf50m_bc_output2015_v3_{res[k]}m_width{width[k]}"),
    # )
    #
    # model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict(X_test1)
    # # model_regress.save_models(outname=f"xgboost50m_output2015_v3_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"../output/rf50m_output2015_v3_{res[k]}m_width{width[k]}.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost50m_output2015_v3_{res[k]}m"),
    #     file_name="{}_val.png".format(f"../output/rf50m_output2015_v3_{res[k]}m_width{width[k]}"),
    # )
    # dfy['y_rfbc'] = model_y_pred[~np.isnan(y_test)]
    #
    # # compare with CNN+linear model
    # inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    # mlayer = ga.DenseNet(
    #     num_layers_in_each_block=dn_structure[k],
    #     growth_rate=8,
    #     dropout_rate=0.2,
    #     weight_decay=1e-4,
    #     compression=0.5,
    #     bottleneck=True,
    #     include_top=False,
    # )(inputs)
    # m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    # m2 = tf.keras.layers.Dense(64)(m2)
    # outputs = tf.keras.layers.Dense(1)(m2)
    # m = tf.keras.Model(inputs, outputs)
    # m.load_weights(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_trained_weights.h5")
    # mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)
    #
    # X_list = []
    # y_list = []
    # for X, Y in ds1.take(int(train_n / batch_size)):
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_train = np.concatenate(X_list)
    # y_train = np.concatenate(y_list)
    # X_mlayer_train = mlayer_model.predict(X_train, verbose=1)
    #
    # # X_list = []
    # # y_list = []
    # # for X, Y in ds_test.take(int(test_n / batch_size)):
    # #     X_list.append(X.numpy())
    # #     y_list.append(Y.numpy())
    # # X_test = np.concatenate(X_list)
    # # y_test = np.concatenate(y_list)
    # X_mlayer_test = mlayer_model.predict(X_test, verbose=1)
    #
    # model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    # model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # # model_pipe, params_grid = model_regress.setup_xgb_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [100, 200, 400, 600],
    # #         "learn__max_depth": [3, 6, 9, 12],
    # #         # "learn__booster": ["gbtree","gblinear", "dart"],
    # #         "learn__booster": ["dart"],
    # #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    # #     }
    # # )
    # # model_pipe, params_grid = model_regress.setup_rf_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [300],
    # #         # "learn__max_depth": [6, 9, 12],
    # #         # "learn__min_samples_split": [2, 7, 12],
    # #         "learn__max_depth": [9],
    # #         "learn__min_samples_split": [2],
    # #     }
    # # )
    # model_regress.grid_opts(model_pipe, params_grid, k=3)
    # # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    # model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict(X_mlayer_test)
    #
    # model_regress.save_models(outname=f"../output/ridge50m_mlayer_model_v3_{res[k]}m_width{width[k]}.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # y_test = y_test.ravel()
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     file_name="{}_val.png".format(f"../output/ridge50m_mlayer_model_v3_{res[k]}m_width{width[k]}"),
    # )
    # dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": model_y_pred[~np.isnan(y_test)]})
    # dfy["y_ridgetest"] = y_test[~np.isnan(y_test)]
    # dfy.to_csv(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_val_ridge.csv")
    #
    # # dfy.to_csv(f"../output/cnn250m_output2010_v3_{res[k]}m_val_data.csv")
    # tf.keras.backend.clear_session()
    #
    print('---------')

    # Augmented CNN
    batch_size = 256
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds1 = (
            ds
            .cache()
            .shuffle(220000)
            .repeat()
            .map(sample_normalize)
            .map(augment, num_parallel_calls=AUTOTUNE)
            .batch(batch_size)
            .prefetch(AUTOTUNE)
    )
    # ds = ds.map(sample_normalize).map(augment)
    # ds1 = ds.shuffle(80000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds_test = (
            dst
            .map(sample_normalize)
            .batch(batch_size)
    )
    # dst = dst.map(sample_normalize)
    # ds_test = dst.shuffle(80000).batch(batch_size).repeat()

    # CNN model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        # loss=tf.keras.losses.get("MeanAbsoluteError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    earlystop_callback = tf.keras.callbacks.EarlyStopping(
        monitor='val_loss', min_delta=0.01, verbose=1,
        patience=30, restore_best_weights=True)

    history = m.fit(
        x=ds1,
        epochs=epochs[k], callbacks=[earlystop_callback],
        # steps_per_epoch=int(train_n / batch_size),
        steps_per_epoch=76,
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    # plt.savefig(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_history_mae.png", dpi=300)
    plt.savefig(f"../output/augcnn50m_output2015_v3_{res[k]}m_width{width[k]}_history.png", dpi=300)
    plt.show()
    # m.save_weights(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_trained_weights_mae.h5")
    m.save_weights(f"../output/augcnn50m_output2015_v3_{res[k]}m_width{width[k]}_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        # file_name=f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_val_mae.png",
        file_name=f"../output/augcnn50m_output2015_v3_{res[k]}m_width{width[k]}_val.png",
    )
    tf.keras.backend.clear_session()


    # # compare with augCNN+linear model
    # inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    # mlayer = ga.DenseNet(
    #     num_layers_in_each_block=dn_structure[k],
    #     growth_rate=8,
    #     dropout_rate=0.2,
    #     weight_decay=1e-4,
    #     compression=0.5,
    #     bottleneck=True,
    #     include_top=False,
    # )(inputs)
    # m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    # m2 = tf.keras.layers.Dense(64)(m2)
    # outputs = tf.keras.layers.Dense(1)(m2)
    # m = tf.keras.Model(inputs, outputs)
    # m.load_weights(f"../output/augcnn50m_output2015_v3_{res[k]}m_width{width[k]}_trained_weights.h5")
    # mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)
    #
    # X_list = []
    # y_list = []
    # for X, Y in ds1.take(int(train_n / batch_size)):
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_train = np.concatenate(X_list)
    # y_train = np.concatenate(y_list)
    # X_mlayer_train = mlayer_model.predict(X_train, verbose=1)
    #
    # # X_list = []
    # # y_list = []
    # # for X, Y in ds_test.take(int(test_n / batch_size)):
    # #     X_list.append(X.numpy())
    # #     y_list.append(Y.numpy())
    # # X_test = np.concatenate(X_list)
    # # y_test = np.concatenate(y_list)
    # X_mlayer_test = mlayer_model.predict(X_test, verbose=1)
    #
    # model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    # model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # # model_pipe, params_grid = model_regress.setup_xgb_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [100, 200, 400, 600],
    # #         "learn__max_depth": [3, 6, 9, 12],
    # #         # "learn__booster": ["gbtree","gblinear", "dart"],
    # #         "learn__booster": ["dart"],
    # #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    # #     }
    # # )
    # # model_pipe, params_grid = model_regress.setup_rf_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [300],
    # #         # "learn__max_depth": [6, 9, 12],
    # #         # "learn__min_samples_split": [2, 7, 12],
    # #         "learn__max_depth": [9],
    # #         "learn__min_samples_split": [2],
    # #     }
    # # )
    # model_regress.grid_opts(model_pipe, params_grid, k=3)
    # # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    # model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict(X_mlayer_test)
    #
    # model_regress.save_models(outname=f"../output/augridge50m_mlayer_model_v3_{res[k]}m_width{width[k]}.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # y_test = y_test.ravel()
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     file_name="{}_val.png".format(f"../output/augridge50m_mlayer_model_v3_{res[k]}m_width{width[k]}"),
    # )
    # tf.keras.backend.clear_session()


    print(time.time() - start)





#%% predict model - 11bands from 50m proba-lyrs (2015) - glas+airborne - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def func_predict_cnn_0(
        mask_arr,
        # block_id=None,
        # chunksize=600,
        # xdata=None,
        # ydata=None,
        # model=None,
        # out_name=None
):
    # max_x = min((block_id[2] + 1) * chunksize, xdata.shape[0])
    # max_y = min((block_id[1] + 1) * chunksize, ydata.shape[0])
    # idx = np.arange(block_id[2] * chunksize + 2, max_x, 5)
    # idy = np.arange(block_id[1] * chunksize + 2, max_y, 5)
    # x1 = xdata[idx]
    # y1 = ydata[idy]
    # arr_new = np.moveaxis(mask_arr, 0, -1)[None, :, :, :]
    # arr_patch = tf.image.extract_patches(images=arr_new,
    #                                      sizes=[1, 15, 15, 1],
    #                                      strides=[1, 5, 5, 1],
    #                                      rates=[1, 1, 1, 1],
    #                                      padding='VALID')
    # arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
    # pred_X = arr_table[arr_table[:, 790] > -1000, :]
    # pred_X = sample_normalize_1(pred_X.reshape(-1, 7)).reshape(-1, arr_table.shape[1])
    # arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, 7)
    # print(arr_cnn.shape)
    # if pred_X.shape[0] > 0:
    #     model_y_pred = model.predict(arr_cnn)
    #     # model_y_pred = model.meta_model_predict_biascorr(pred_X)
    #
    #     tf.keras.backend.clear_session()
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
    #     pred_arr[arr_table[:, 790] > -1000] = model_y_pred.ravel()
    #     pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
    #     print(f"pred_shape: {pred_arr.shape}")
    # else:
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")
    #
    # da = xr.DataArray(data=pred_arr, coords=[y1[:pred_arr.shape[0]], x1[:pred_arr.shape[1]]], dims=['y', 'x'])
    # out_file1 = f"{out_name}_{block_id[1]}_{block_id[2]}.nc"
    # ds1 = xr.Dataset({"da": da})
    # ds1.to_netcdf(out_file1)
    # # np.save(out_file, pred_arr)
    #
    # del model
    # del ydata
    # del xdata
    # del pred_arr
    # del arr_new
    # del arr_patch
    # del arr_table
    # del arr_cnn
    # del pred_X
    return mask_arr
    # return np.array([[1]])


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn50m_output2015_v3_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        model_cnn,
        nbands=11,
        chunksize=None,
        out_name=None
):
    center_pixel = 7*15*nbands + 7*nbands + 6
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, center_pixel] > -1000, :]
        print(pred_X.shape)
        pred_X = sample_normalize_1(pred_X.reshape(-1, nbands)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, nbands)
        print(arr_cnn.shape)
        if pred_X.shape[0] > 0:
            model_y_pred = model_cnn[0].predict(arr_cnn)
            model_y_pred[model_y_pred < 0] = 0
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, center_pixel] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
            1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
            1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
            9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
            1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


nbands = 11
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [128, ]
res = [250, ]
buffer = [5, ]

for k in range(0, len(width)):

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))    # da_b = da_mask.band.values
    # da_x = da_mask.x.values
    # da_y = da_mask.y.values
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        # print(ij)
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        # results.append(func_predict_cnn(ij, da_mask, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
        results.append(func_predict_cnn(ij, da_mask, model, nbands=nbands, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 0, 2: 0},
    #                           boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'})
    # file_id = zarr.open_array("../output/tmp_dask.zarr", shape=g.shape, mode="w",
    #                           chunks=(nbands, chunk_size+10, chunk_size+10),
    #                           synchronizer=zarr.ThreadSynchronizer())
    # dskda.store(g, file_id)
    # g2 = g.map_blocks(func_predict_cnn, drop_axis=[0, 1, 2])
    # da_pred = dskda.overlap.trim_internal(g2, {0: 0, 1: 5, 2: 5})
    # da_pred = g.map_blocks(func_predict_cnn, dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
    #                             dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             # boundary={0: 'none', 1: 'none', 2: 'none'},
    #                             trim=True)

    # out_file1 = f"../output/mch_prediction_dask.zarr"
    # # file_id = zarr.open_array(out_file1, shape=da_pred.shape, mode="w",
    # #                           chunks=(nbands, chunk_size, chunk_size),
    # #                           # synchronizer=zarr.ThreadSynchronizer(),
    # #                           )
    # # dskda.store(da_pred, file_id)
    # delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True,
    #                             synchronizer=zarr.ThreadSynchronizer(),
    #                             compute=False)
    # # out_file1 = f"../output/mch_prediction_dask.nc"
    # # da1 = xr.DataArray(da_pred, coords=(da_b, da_y, da_x), dims=("band", "y", "x"))
    # # ds2 = xr.Dataset({"da": da1})
    # # delayed_obj = ds2.to_netcdf(out_file1, compute=True)
    # # with ProgressBar():
    # #     results = delayed_obj.compute(num_workers=1)

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_output2015_v3_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_output2015_v3_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)




#%% training models - 7 bands from 50m nbar-lyrs (2010) - glas+gedi+airborne - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')

# with open("combine_bands.txt", "w") as txt0:
#     print("xlyrs_50m_proba_2015_v2_vza90.vrt", file=txt0)
#     print("xlyrs_50m_2015_v0_lc8nbar/xlyrs_50m_median_1317_v0_lc8bands.tif", file=txt0)
# rt.build_stack_vrt("combine_bands.txt", "xlyrs_50m_probaxlc8_2015_v3.vrt")

def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def random_rotate_image(image):
  image = ndimage.rotate(image, np.random.uniform(-90, 90), reshape=False)
  return image


def augment(image,label):
  # print(image.shape)
  image = tf.image.random_brightness(image, max_delta=0.3)
  image = tf.image.random_contrast(image, 0.7, 1.3)
  image = tf.image.random_flip_up_down(image)
  image = tf.image.random_flip_left_right(image)
  image = tf.image.resize_with_crop_or_pad(image, 20, 20)
  im_shape = image.shape
  [image,] = tf.py_function(random_rotate_image, [image], [tf.float32])
  image.set_shape(im_shape)
  image = tf.image.random_crop(image, size=[15, 15, 11])
  return image,label


nbands = 7

pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [128, ]
res = [250, ]

for k in range(0, len(width)):

    # df_all = pd.read_csv("csvs/mch_250m_training_v0v1_gedixglasxatlas_v2.csv", index_col=0)
    # df_all = pd.read_csv("csvs/mch_250m_training_v0v1_gedixglas_v2.csv", index_col=0)
    df_all = pd.read_csv("csvs/mch_250m_training_v0v1_glas_v2.csv", index_col=0)
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv", index_col=0)

    path_tif = "xlyrs_50m_nbar_2010_v2.vrt"
    path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2010_v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_all,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )
    path_tfrecord = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2010_v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_test,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )

    path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2010_v3.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_width{width[k]}_from_xlyrs_2010_v3.tfrecord"

    batch_size = 256
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds1 = (
            ds
            .cache()
            .shuffle(220000)
            .repeat()
            .map(sample_normalize)
            .batch(batch_size)
            .prefetch(AUTOTUNE)
    )
    # ds = ds.map(sample_normalize)
    # ds1 = ds.shuffle(80000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds_test = (
            dst
            .map(sample_normalize)
            .batch(batch_size)
    )
    # dst = dst.map(sample_normalize)
    # ds_test = dst.shuffle(80000).batch(batch_size).repeat()


    # CNN model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        # loss=tf.keras.losses.get("MeanAbsoluteError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    earlystop_callback = tf.keras.callbacks.EarlyStopping(
        monitor='val_loss', min_delta=0.01, verbose=1,
        patience=30, restore_best_weights=True)

    history = m.fit(
        x=ds1,
        epochs=epochs[k], callbacks=[earlystop_callback],
        # steps_per_epoch=int(train_n / batch_size),
        steps_per_epoch=76,
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
        use_multiprocessing=False,
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    # plt.savefig(f"../output/cnn50m_output2010_v3_{res[k]}m_width{width[k]}_history_mae.png", dpi=300)
    plt.savefig(f"../output/cnn50m_output2010_v3_{res[k]}m_width{width[k]}_history.png", dpi=300)
    plt.show()
    # m.save_weights(f"../output/cnn50m_output2010_v3_{res[k]}m_width{width[k]}_trained_weights_mae.h5")
    m.save_weights(f"../output/cnn50m_output2010_v3_{res[k]}m_width{width[k]}_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        # file_name=f"../output/cnn50m_output2010_v3_{res[k]}m_width{width[k]}_val_mae.png",
        file_name=f"../output/cnn50m_output2010_v3_{res[k]}m_width{width[k]}_val.png",
    )

    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})

    # tf.keras.backend.clear_session()



    print(time.time() - start)





#%% predict model - 7bands from 50m proba-lyrs (2010) - glas+airborne - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def func_predict_cnn_0(
        mask_arr,
        # block_id=None,
        # chunksize=600,
        # xdata=None,
        # ydata=None,
        # model=None,
        # out_name=None
):
    # max_x = min((block_id[2] + 1) * chunksize, xdata.shape[0])
    # max_y = min((block_id[1] + 1) * chunksize, ydata.shape[0])
    # idx = np.arange(block_id[2] * chunksize + 2, max_x, 5)
    # idy = np.arange(block_id[1] * chunksize + 2, max_y, 5)
    # x1 = xdata[idx]
    # y1 = ydata[idy]
    # arr_new = np.moveaxis(mask_arr, 0, -1)[None, :, :, :]
    # arr_patch = tf.image.extract_patches(images=arr_new,
    #                                      sizes=[1, 15, 15, 1],
    #                                      strides=[1, 5, 5, 1],
    #                                      rates=[1, 1, 1, 1],
    #                                      padding='VALID')
    # arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
    # pred_X = arr_table[arr_table[:, 790] > -1000, :]
    # pred_X = sample_normalize_1(pred_X.reshape(-1, 7)).reshape(-1, arr_table.shape[1])
    # arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, 7)
    # print(arr_cnn.shape)
    # if pred_X.shape[0] > 0:
    #     model_y_pred = model.predict(arr_cnn)
    #     # model_y_pred = model.meta_model_predict_biascorr(pred_X)
    #
    #     tf.keras.backend.clear_session()
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
    #     pred_arr[arr_table[:, 790] > -1000] = model_y_pred.ravel()
    #     pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
    #     print(f"pred_shape: {pred_arr.shape}")
    # else:
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")
    #
    # da = xr.DataArray(data=pred_arr, coords=[y1[:pred_arr.shape[0]], x1[:pred_arr.shape[1]]], dims=['y', 'x'])
    # out_file1 = f"{out_name}_{block_id[1]}_{block_id[2]}.nc"
    # ds1 = xr.Dataset({"da": da})
    # ds1.to_netcdf(out_file1)
    # # np.save(out_file, pred_arr)
    #
    # del model
    # del ydata
    # del xdata
    # del pred_arr
    # del arr_new
    # del arr_patch
    # del arr_table
    # del arr_cnn
    # del pred_X
    return mask_arr
    # return np.array([[1]])


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn50m_output2010_v3_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        model_cnn,
        nbands=7,
        chunksize=None,
        out_name=None
):
    center_pixel = 7*15*nbands + 7*nbands + 6
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, center_pixel] > -1000, :]
        print(pred_X.shape)
        pred_X = sample_normalize_1(pred_X.reshape(-1, nbands)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, nbands)
        print(arr_cnn.shape)
        if pred_X.shape[0] > 0:
            model_y_pred = model_cnn[0].predict(arr_cnn)
            model_y_pred[model_y_pred < 0] = 0
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, center_pixel] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


nbands = 7
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [128, ]
res = [250, ]
buffer = [5, ]

for k in range(0, len(width)):

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_nbar_2010_v2.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        results.append(func_predict_cnn(ij, da_mask, model, nbands=nbands, chunksize=chunk_size, out_name=f"../output/tmp2/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    ds2 = xr.open_mfdataset(f"../output/tmp2/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_output2010_v3_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_output2010_v3_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)





#%% training models - 7 bands from 50m nbarlc8-lyrs (2015) - glas+airborne - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')

# with open("combine_bands.txt", "w") as txt0:
#     print("xlyrs_50m_proba_2015_v2_vza90.vrt", file=txt0)
#     print("xlyrs_50m_2015_v0_lc8nbar/xlyrs_50m_median_1317_v0_lc8bands.tif", file=txt0)
# rt.build_stack_vrt("combine_bands.txt", "xlyrs_50m_probaxlc8_2015_v3.vrt")

def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def random_rotate_image(image):
  image = ndimage.rotate(image, np.random.uniform(-90, 90), reshape=False)
  return image


def augment(image,label):
  # print(image.shape)
  image = tf.image.random_brightness(image, max_delta=0.3)
  image = tf.image.random_contrast(image, 0.7, 1.3)
  image = tf.image.random_flip_up_down(image)
  image = tf.image.random_flip_left_right(image)
  image = tf.image.resize_with_crop_or_pad(image, 20, 20)
  im_shape = image.shape
  [image,] = tf.py_function(random_rotate_image, [image], [tf.float32])
  image.set_shape(im_shape)
  image = tf.image.random_crop(image, size=[15, 15, 11])
  return image,label


nbands = 7

pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [128, ]
res = [250, ]

for k in range(0, len(width)):

    # df_all = pd.read_csv("csvs/mch_250m_training_v0v1_gedixglasxatlas_v2.csv", index_col=0)
    # df_all = pd.read_csv("csvs/mch_250m_training_v0v1_gedixglas_v2.csv", index_col=0)
    df_all = pd.read_csv("csvs/mch_250m_training_v0v1_glas_v2.csv", index_col=0)
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv", index_col=0)

    path_tif = "xlyrs_50m_lc8nbar_2015_v3.vrt"
    path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_nbarlyrs_2015_v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_all,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )
    path_tfrecord = f"tfrecords/test_{res[0]}m_width{width[k]}_from_nbarlyrs_2015_v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_test,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )

    path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_nbarlyrs_2015_v3.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_width{width[k]}_from_nbarlyrs_2015_v3.tfrecord"

    batch_size = 256
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds1 = (
            ds
            .cache()
            .shuffle(220000)
            .repeat()
            .map(sample_normalize)
            .batch(batch_size)
            .prefetch(AUTOTUNE)
    )
    # ds = ds.map(sample_normalize)
    # ds1 = ds.shuffle(80000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds_test = (
            dst
            .map(sample_normalize)
            .batch(batch_size)
    )
    # dst = dst.map(sample_normalize)
    # ds_test = dst.shuffle(80000).batch(batch_size).repeat()


    # CNN model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        # loss=tf.keras.losses.get("MeanAbsoluteError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    earlystop_callback = tf.keras.callbacks.EarlyStopping(
        monitor='val_loss', min_delta=0.01, verbose=1,
        patience=30, restore_best_weights=True)

    history = m.fit(
        x=ds1,
        epochs=epochs[k], callbacks=[earlystop_callback],
        # steps_per_epoch=int(train_n / batch_size),
        steps_per_epoch=76,
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    # plt.savefig(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_history_mae.png", dpi=300)
    plt.savefig(f"../output/cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}_history.png", dpi=300)
    plt.show()
    # m.save_weights(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_trained_weights_mae.h5")
    m.save_weights(f"../output/cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        # file_name=f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_val_mae.png",
        file_name=f"../output/cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}_val.png",
    )

    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})

    tf.keras.backend.clear_session()


    print(time.time() - start)





#%% predict model - 7 bands from 50m nbarlc8-lyrs (2015) - glas+airborne - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def func_predict_cnn_0(
        mask_arr,
        # block_id=None,
        # chunksize=600,
        # xdata=None,
        # ydata=None,
        # model=None,
        # out_name=None
):
    # max_x = min((block_id[2] + 1) * chunksize, xdata.shape[0])
    # max_y = min((block_id[1] + 1) * chunksize, ydata.shape[0])
    # idx = np.arange(block_id[2] * chunksize + 2, max_x, 5)
    # idy = np.arange(block_id[1] * chunksize + 2, max_y, 5)
    # x1 = xdata[idx]
    # y1 = ydata[idy]
    # arr_new = np.moveaxis(mask_arr, 0, -1)[None, :, :, :]
    # arr_patch = tf.image.extract_patches(images=arr_new,
    #                                      sizes=[1, 15, 15, 1],
    #                                      strides=[1, 5, 5, 1],
    #                                      rates=[1, 1, 1, 1],
    #                                      padding='VALID')
    # arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
    # pred_X = arr_table[arr_table[:, 790] > -1000, :]
    # pred_X = sample_normalize_1(pred_X.reshape(-1, 7)).reshape(-1, arr_table.shape[1])
    # arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, 7)
    # print(arr_cnn.shape)
    # if pred_X.shape[0] > 0:
    #     model_y_pred = model.predict(arr_cnn)
    #     # model_y_pred = model.meta_model_predict_biascorr(pred_X)
    #
    #     tf.keras.backend.clear_session()
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
    #     pred_arr[arr_table[:, 790] > -1000] = model_y_pred.ravel()
    #     pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
    #     print(f"pred_shape: {pred_arr.shape}")
    # else:
    #     pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")
    #
    # da = xr.DataArray(data=pred_arr, coords=[y1[:pred_arr.shape[0]], x1[:pred_arr.shape[1]]], dims=['y', 'x'])
    # out_file1 = f"{out_name}_{block_id[1]}_{block_id[2]}.nc"
    # ds1 = xr.Dataset({"da": da})
    # ds1.to_netcdf(out_file1)
    # # np.save(out_file, pred_arr)
    #
    # del model
    # del ydata
    # del xdata
    # del pred_arr
    # del arr_new
    # del arr_patch
    # del arr_table
    # del arr_cnn
    # del pred_X
    return mask_arr
    # return np.array([[1]])


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn50m_nbar2015_v3_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        model_cnn,
        nbands=11,
        chunksize=None,
        out_name=None
):
    center_pixel = 7*15*nbands + 7*nbands + 6
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, center_pixel] > -1000, :]
        print(pred_X.shape)
        pred_X = sample_normalize_1(pred_X.reshape(-1, nbands)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, nbands)
        print(arr_cnn.shape)
        if pred_X.shape[0] > 0:
            model_y_pred = model_cnn[0].predict(arr_cnn)
            model_y_pred[model_y_pred < 0] = 0
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, center_pixel] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02,
               ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            4.6418205e-02,
            1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
            1.3404459e-01, 4.0531594e-02, 5.4019067e+02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            3.5437502e-02,
            1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
            9.6319959e-02, 2.8609153e-02, 3.7470444e+02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


nbands = 7
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
dn_structure = [
    (12, 18, 24),
]
epochs = [128, ]
res = [250, ]
buffer = [5, ]

for k in range(0, len(width)):

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_lc8nbar_2015_v3.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))    # da_b = da_mask.band.values
    # da_x = da_mask.x.values
    # da_y = da_mask.y.values
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        # print(ij)
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        # results.append(func_predict_cnn(ij, da_mask, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
        results.append(func_predict_cnn(ij, da_mask, model, nbands=nbands, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 0, 2: 0},
    #                           boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'})
    # file_id = zarr.open_array("../output/tmp_dask.zarr", shape=g.shape, mode="w",
    #                           chunks=(nbands, chunk_size+10, chunk_size+10),
    #                           synchronizer=zarr.ThreadSynchronizer())
    # dskda.store(g, file_id)
    # g2 = g.map_blocks(func_predict_cnn, drop_axis=[0, 1, 2])
    # da_pred = dskda.overlap.trim_internal(g2, {0: 0, 1: 5, 2: 5})
    # da_pred = g.map_blocks(func_predict_cnn, dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
    #                             dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             # boundary={0: 'none', 1: 'none', 2: 'none'},
    #                             trim=True)

    # out_file1 = f"../output/mch_prediction_dask.zarr"
    # # file_id = zarr.open_array(out_file1, shape=da_pred.shape, mode="w",
    # #                           chunks=(nbands, chunk_size, chunk_size),
    # #                           # synchronizer=zarr.ThreadSynchronizer(),
    # #                           )
    # # dskda.store(da_pred, file_id)
    # delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True,
    #                             synchronizer=zarr.ThreadSynchronizer(),
    #                             compute=False)
    # # out_file1 = f"../output/mch_prediction_dask.nc"
    # # da1 = xr.DataArray(da_pred, coords=(da_b, da_y, da_x), dims=("band", "y", "x"))
    # # ds2 = xr.Dataset({"da": da1})
    # # delayed_obj = ds2.to_netcdf(out_file1, compute=True)
    # # with ProgressBar():
    # #     results = delayed_obj.compute(num_workers=1)

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)




#%% extract gabon layers
print(0)

# os.chdir("F:\Projects\\a1902alostexture\output")
# files = [
#     "mch_prediction2010_250m_width1_from_xlyrs_250m_v0v3.tif",
#     "mch_prediction2015_250m_width1_from_xlyrs_250m_v0v3.tif",
#     "mch_prediction_cnn50m_output2015_v2_250m_width15.tif",
#     "mch_prediction_cnn50m_output2010_v3_250m_width15.tif",
#     "mch_prediction_cnn50m_nbar2015_v3_250m_width15.tif",
#     "../input/xlyrs_250m_median_1317_v0.vrt",
#     "../input/xlyrs_250m_median_2010_v0_l7.vrt",
# ]
#
# outs = [
#     "../gabon/mch2010_rf250m_l7nbar_250m_v0.tif",
#     "../gabon/mch2015_rf250m_l8nbar_250m_v0.tif",
#     "../gabon/mch2015_cnn50m_l8nbar_250m_v2.tif",
#     "../gabon/mch2010_cnn50m_nbar_250m_v3.tif",
#     "../gabon/mch2015_cnn50m_l8nbar_250m_v3.tif",
#     "../gabon/xlyrs_250m_median_2015_v0.tif",
#     "../gabon/xlyrs_250m_median_2010_v0.tif",
# ]
#
# for i in range(7):
#     rt.raster_clip("../gabon/gabon_reference.tif", files[i], outs[i])


# os.chdir("F:\Projects\\a1902alostexture\\drc")
# files = [
#     "gfc_lastyear_v16_250m_drc_b1.tif",
#     "gfc_lastyear_v16_250m_drc_b2.tif",
#     "gfc_lastyear_v16_250m_drc_b3.tif",
#     "gfc_lastyear_v16_250m_drc_b4.tif",
#     "../alos_raw/hh_2018.vrt",
#     "../alos_raw/hv_2018.vrt",
#     "../input/xlyrs_radar_25m/srtm_africa_1517.tif",
# ]
#
# outs = [
#     "gfc_lastyear_v16_250m_drc_b1_geo.tif",
#     "gfc_lastyear_v16_250m_drc_b2_geo.tif",
#     "gfc_lastyear_v16_250m_drc_b3_geo.tif",
#     "gfc_lastyear_v16_250m_drc_b4_geo.tif",
#     "hh_2018_geo.tif",
#     "hv_2018_geo.tif",
#     "srtm_africa_geo.tif",
# ]
#
# for i in range(4):
#     rt.raster_clip("drc_reference.tif", files[i], outs[i], resampling_method="average")


os.chdir("F:\Projects\\a1902alostexture\\gabon")
files = [
    "xlyrs_2019/gfc_lastyear_v16_250m_gabon_b1.tif",
    "xlyrs_2019/gfc_lastyear_v16_250m_gabon_b2.tif",
    "xlyrs_2019/gfc_lastyear_v16_250m_gabon_b3.tif",
    "xlyrs_2019/gfc_lastyear_v16_250m_gabon_b4.tif",
    "../alos_raw/hh_2018.vrt",
    "../alos_raw/hv_2018.vrt",
    "../input/xlyrs_radar_25m/srtm_africa_1517.tif",
]

outs = [
    "gfc_lastyear_v16_250m_gabon_b1_geo.tif",
    "gfc_lastyear_v16_250m_gabon_b2_geo.tif",
    "gfc_lastyear_v16_250m_gabon_b3_geo.tif",
    "gfc_lastyear_v16_250m_gabon_b4_geo.tif",
    "hh_2018_geo.tif",
    "hv_2018_geo.tif",
    "srtm_africa_geo.tif",
]

for i in range(7):
    rt.raster_clip("gabon_reference.tif", files[i], outs[i], resampling_method="average")


print(1)

#%% setup training data for changes (2010-2015)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_2(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


nbands = 11
nbands2 = 7
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
res = [250, ]
batch_size = 256

k = 0

# 2015
df_all = pd.read_csv("csvs/mch_250m_training_v0v1.csv", index_col=0)
df_test = pd.read_csv("csvs/mch_250m_test_v0.csv", index_col=0)

path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2015_v3v0v1.tfrecord"
ga.regression_tfrecord_from_df(
    df_all,
    path_tif,
    path_tfrecord,
    x_cols="xvar",
    response="MCH",
    # loc=("Y", "X"),
    loc=("latitude", "longitude"),
    offset=(-offsets[k], offsets[k]),
    blocksize=(width[k], width[k]),
)
path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2015_v3v0v1.tfrecord"
path_test = f"tfrecords/test_{res[k]}m_width{width[k]}_from_xlyrs_2015_v3.tfrecord"
ds, train_n = ga.load_tf(
    path_training,
    x_features="xvar",
    y_feature="MCH",
    x_width=(width[k], width[k], nbands),
    y_width=(1,),
    compression="",
)
ds1 = (
        ds
        .cache()
        .map(sample_normalize)
        .batch(batch_size)
        .prefetch(AUTOTUNE)
)
xlst = []
ylst = []
for element in ds1.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x_train = np.concatenate(xlst, axis=0)
y_train = np.concatenate(ylst, axis=0)

dst, test_n = ga.load_tf(
    path_test,
    x_features="xvar",
    y_feature="MCH",  # for v0, v2
    x_width=(width[k], width[k], nbands),
    y_width=(1,),
    compression="",
)
ds_test = (
        dst
        .map(sample_normalize)
        .batch(batch_size)
)
xlst = []
ylst = []
for element in ds_test.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x_test = np.concatenate(xlst, axis=0)
y_test = np.concatenate(ylst, axis=0)

# 2010
nbands2 = 7
path_tif = "xlyrs_50m_nbar_2010_v2.vrt"
path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2010_v3v0v1.tfrecord"
ga.regression_tfrecord_from_df(
    df_all,
    path_tif,
    path_tfrecord,
    x_cols="xvar",
    response="MCH",
    # loc=("Y", "X"),
    loc=("latitude", "longitude"),
    offset=(-offsets[k], offsets[k]),
    blocksize=(width[k], width[k]),
)
path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2010_v3v0v1.tfrecord"
path_test = f"tfrecords/test_{res[k]}m_width{width[k]}_from_xlyrs_2010_v3.tfrecord"
ds, train_n = ga.load_tf(
    path_training,
    x_features="xvar",
    y_feature="MCH",
    x_width=(width[k], width[k], nbands2),
    y_width=(1,),
    compression="",
)
ds1 = (
    ds
    .cache()
    .map(sample_normalize_2)
    .batch(batch_size)
    .prefetch(AUTOTUNE)
)
xlst = []
ylst = []
for element in ds1.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x2_train = np.concatenate(xlst, axis=0)
y2_train = np.concatenate(ylst, axis=0)

dst, test_n = ga.load_tf(
    path_test,
    x_features="xvar",
    y_feature="MCH",  # for v0, v2
    x_width=(width[k], width[k], nbands2),
    y_width=(1,),
    compression="",
)
ds_test = (
    dst
    .map(sample_normalize_2)
    .batch(batch_size)
)
xlst = []
ylst = []
for element in ds_test.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x2_test = np.concatenate(xlst, axis=0)
y2_test = np.concatenate(ylst, axis=0)


# #randomly select changing pixels
# n = 600
# rs1 = np.random.RandomState(111)
# id1 = np.random.choice(x_train.shape[0], n, replace=False)
# rs2 = np.random.RandomState(222)
# id2 = np.random.choice(x2_train.shape[0], n, replace=False)
# x_lst = []
# y_lst = []
# for i in id1:
#     x1 = [
#         # np.repeat(x_train[i:i + 1, :, :, :], n, axis=0),
#         # x2_train[id2, :, :, :],
#         np.repeat(x_train[i:i + 1, :, :, :], n, axis=0).reshape(n, -1),
#         x2_train[id2, :, :, :].reshape(n, -1),
#         np.repeat(y_train[i:i + 1, :], n, axis=0),
#     ]
#     x_lst.append(np.concatenate(x1, axis=-1))
#     y_lst.append(y2_train[id2, :] - y_train[i, 0])
# xdiff_train = np.concatenate(x_lst, axis=0)
# ydiff_train = np.concatenate(y_lst, axis=0)
#
# n = 200
# rs1 = np.random.RandomState(111)
# id1 = np.random.choice(x_test.shape[0], n, replace=False)
# rs2 = np.random.RandomState(222)
# id2 = np.random.choice(x2_test.shape[0], n, replace=False)
# x_lst = []
# y_lst = []
# for i in id1:
#     x1 = [
#         np.repeat(x_test[i:i + 1, :, :, :], n, axis=0).reshape(n, -1),
#         x2_test[id2, :, :, :].reshape(n, -1),
#         np.repeat(y_test[i:i + 1, :], n, axis=0),
#     ]
#     x_lst.append(np.concatenate(x1, axis=-1))
#     y_lst.append(y2_test[id2, :] - y_test[i, 0])
# xdiff_test = np.concatenate(x_lst, axis=0)
# ydiff_test = np.concatenate(y_lst, axis=0)

print(0)
# select set changing pixels
n = 6000
# n = x2_train.shape[0]
rs1 = np.random.RandomState(111)
id1 = np.random.choice(x_train.shape[0], n, replace=False)
rs2 = np.random.RandomState(222)
id2 = np.random.choice(x2_train.shape[0], n, replace=False)
for i in id1:
# for i in range(x_train.shape[0]):
    x1 = [
        np.repeat(x_train[i:i + 1, :, :, :], n, axis=0),
        x2_train[id2, :, :, :]
    ]
    x1a = np.concatenate(x1, axis=-1)
    x1b = np.repeat(y_train[i:i + 1, :], n, axis=0)
    y1diff = y2_train[id2, :] - y_train[i, 0]
    path = f"tfrecords/change/train_2015-2010_als_v3v0v1_i{i}.tfrecord"
    opt1 = tf.io.TFRecordOptions(compression_type="GZIP")
    with tf.io.TFRecordWriter(path, options=opt1) as file_writer:
        for k in range(x1a.shape[0]):
            record_bytes = tf.train.Example(
                features=tf.train.Features(feature={
                    "cnn_input": tf.train.Feature(float_list=tf.train.FloatList(value=x1a[k:k+1,:,:,:].ravel())),
                    "y0_input": tf.train.Feature(float_list=tf.train.FloatList(value=x1b[k:k+1,:].ravel())),
                    "dy_output": tf.train.Feature(float_list=tf.train.FloatList(value=y1diff[k:k+1,:].ravel())),
                })
            ).SerializeToString()
            file_writer.write(record_bytes)


n = 300
rs1 = np.random.RandomState(111)
id1 = np.random.choice(x_test.shape[0], n, replace=False)
rs2 = np.random.RandomState(222)
id2 = np.random.choice(x2_test.shape[0], n, replace=False)
x_lst = []
x2_lst = []
y_lst = []
for i in id1:
    x1 = [
        np.repeat(x_test[i:i + 1, :, :, :], n, axis=0),
        x2_test[id2, :, :, :],
    ]
    x1a = np.concatenate(x1, axis=-1)
    x1b = np.repeat(y_test[i:i + 1, :], n, axis=0)
    y1diff = y2_test[id2, :] - y_test[i, 0]
    x_lst.append(x1a)
    x2_lst.append(x1b)
    y_lst.append(y1diff)
xdiff_test = np.concatenate(x_lst, axis=0)
xdiff2_test = np.concatenate(x2_lst, axis=0)
ydiff_test = np.concatenate(y_lst, axis=0)
np.save("tfrecords/change/test_2015-2010_als_v3_xdiff1.npy", xdiff_test)
np.save("tfrecords/change/test_2015-2010_als_v3_xdiff2.npy", xdiff2_test)
np.save("tfrecords/change/test_2015-2010_als_v3_ydiff.npy", ydiff_test)

print(1)



#%% training model for changes (2015-2010)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


# def sample_normalize_18(X, X2, Y):
#     sample_mean = tf.constant(
#         [
#             [
#                 [
#                     3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
#                     1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
#                     1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
#                     3.2123085e-02,
#                     3.1446800e-01,
#                     1.7229147e-01,
#                     6.1950065e-02,
#                     2.2465163e-01,
#                     7.6607764e-02,
#                     5.6069208e02,
#                 ]
#             ]
#         ]
#     )
#     sample_std = tf.constant(
#         [
#             [
#                 [
#                     1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
#                     9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
#                     1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
#                     7.8484304e-03,
#                     2.8701460e-02,
#                     2.1761550e-02,
#                     1.4474887e-02,
#                     4.6037395e-02,
#                     1.8193731e-02,
#                     2.4210707e02,
#                 ]
#             ]
#         ]
#     )
#     X = (X - sample_mean) / sample_std
#
#     X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
#     # # X1 = X.numpy().reshape([-1, 7])
#     # X1 = X.reshape([-1, 7])
#     # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
#     # imp_0.fit_transform(X1)
#     # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
#     # # X = tf.convert_to_tensor(X2, np.float32)
#
#     # return X[:, :, 4:], Y
#     return X[:, :, :], X2, Y


def sample_normalize_18(input, output):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = input["cnn_input"]
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    input["cnn_input"] = X
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return input, output


nbands = 11
nbands2 = 7
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
res = [250, ]
batch_size = 128
epochs = [10, ]
dn_structure = [
    (12, 18, 24),
]

k = 0
inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands+nbands2), name='cnn_input')
mlayer = ga.DenseNet(
    num_layers_in_each_block=dn_structure[k],
    growth_rate=8,
    dropout_rate=0.2,
    weight_decay=1e-4,
    compression=0.5,
    bottleneck=True,
    include_top=False,
)(inputs)
m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
inputs2 = tf.keras.Input(batch_shape=(None, 1), name='y0_input')
m2 = tf.keras.layers.concatenate([m2, inputs2], axis=-1)
m2 = tf.keras.layers.Dense(512)(m2)
outputs = tf.keras.layers.Dense(1, name='dy_output')(m2)
m = tf.keras.Model(inputs=[inputs, inputs2], outputs=outputs)
# m = tf.keras.Model(inputs=inputs, outputs=outputs)
m.summary()

m.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.get("MeanSquaredError"),
    # loss=tf.keras.losses.get("MeanAbsoluteError"),
    metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
)


path_tfrecord = f"tfrecords/change/train_2015-2010_als_v3v0v1_i*.tfrecord"
glob = tf.io.gfile.glob(path_tfrecord)
dataset = tf.data.TFRecordDataset(glob, compression_type="GZIP")
feature_columns = {
    "cnn_input": tf.io.FixedLenFeature([width[k], width[k], nbands+nbands2], dtype=tf.float32),
    "y0_input": tf.io.FixedLenFeature([1, ], dtype=tf.float32),
    "dy_output": tf.io.FixedLenFeature([1, ], dtype=tf.float32),
}
parsed = dataset.map(
    lambda example: tf.io.parse_single_example(example, feature_columns),
    num_parallel_calls=4,
)
parsed = parsed.map(
    lambda example: ({"cnn_input": example.get("cnn_input"), "y0_input": example.get("y0_input")},
                     {"dy_output": example.get("dy_output")}),
    num_parallel_calls=4,
)
# parsed = parsed.map(
#     lambda example: ({"cnn_input": example.get("cnn_input")},
#                      {"dy_output": example.get("dy_output")}),
#     num_parallel_calls=4,
# )
records_n = sum(1 for _ in dataset)
print("records_n = {}".format(records_n))
ds1 = (
    parsed
    # .cache()
    .map(sample_normalize_18)
    .batch(batch_size)
    .prefetch(AUTOTUNE)
)
xdiff_test = np.load("tfrecords/change/test_2015-2010_als_v3_xdiff1.npy")
xdiff2_test = np.load("tfrecords/change/test_2015-2010_als_v3_xdiff2.npy")
ydiff_test = np.load("tfrecords/change/test_2015-2010_als_v3_ydiff.npy")
ds0 = tf.data.Dataset.from_tensor_slices(
    ({'cnn_input': xdiff_test, 'y0_input': xdiff2_test},
     {'dy_output': ydiff_test}))
# ds0 = tf.data.Dataset.from_tensor_slices(
#     ({'cnn_input': xdiff_test},
#      {'dy_output': ydiff_test}))
ds0 = ds0.map(sample_normalize_18).batch(batch_size)


earlystop_callback = tf.keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0.001, verbose=1,
    patience=2, restore_best_weights=True)
history = m.fit(
    ds1,
    epochs=epochs[0],
    callbacks=[earlystop_callback],
    validation_data=ds0,
    # steps_per_epoch=int(train_n / batch_size),
    # validation_steps=int(test_n / batch_size),
)
plt.figure(figsize=(6, 4))
plt.plot(history.history["root_mean_squared_error"], label="Training")
plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
plt.xlabel("Epoch")
plt.ylabel("RMSE")
plt.legend(loc="upper right")
plt.savefig(f"../output/cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}_history.png", dpi=300)
plt.show()
m.save_weights(f"../output/cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}_trained_weights.h5")

predictions = m.predict(ds0, verbose=1)
r2, rmse, beta0 = ggm.density_scatter_plot(
    ydiff_test[~np.isnan(ydiff_test)],
    predictions[~np.isnan(ydiff_test)],
    file_name=f"../output/cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}_val.png",
)


# model_regress = ggm.StackRegressor(xdiff_test, ydiff_test)
# model_pipe, params_grid = model_regress.setup_rf_model(
#     param_grid={
#         "pca": [None],
#         "learn__n_estimators": [500],
#         # "learn__max_depth": [12],
#         # "learn__min_samples_split": [2],
#         "learn__max_depth": [6, 9, 12],
#         "learn__min_samples_split": [2, 7, 12],
#     }
# )
# model_regress.grid_opts(model_pipe, params_grid, k=5)
#
# model_regress.meta_model_biascorr(X=xdiff_train, y=ydiff_train, k=3)
# model_y_pred = model_regress.meta_model_predict_biascorr(xdiff_test, ydiff_test)
# model_regress.save_models(outname=f"../output/rf50m_bc_diff2010-2015_v3_{res[k]}m_width{width[k]}.pkl")
# if isinstance(y_test, pd.core.series.Series):
#     y_test = y_test.values
# r2, rmse, beta0 = ggm.density_scatter_plot(
#     y_test[~np.isnan(y_test)],
#     model_y_pred[~np.isnan(y_test)],
#     file_name="{}_val.png".format(f"../output/rf50m_bc_diff2010-2015_v3_{res[k]}m_width{width[k]}"),
# )
#
# model_regress.meta_model(X=xdiff_train, y=ydiff_train, k=3)
# model_y_pred = model_regress.meta_model_predict(xdiff_test, ydiff_test)
# model_regress.save_models(outname=f"../output/rf50m_diff2010-2015_v3_{res[k]}m_width{width[k]}.pkl")
# if isinstance(y_test, pd.core.series.Series):
#     y_test = y_test.values
# r2, rmse, beta0 = ggm.density_scatter_plot(
#     y_test[~np.isnan(y_test)],
#     model_y_pred[~np.isnan(y_test)],
#     file_name="{}_val.png".format(f"../output/rf50m_diff2010-2015_v3_{res[k]}m_width{width[k]}"),
# )

print(time.time() - start)



#%% predict change - 18 bands from 2015 to 2010 - from airborne-only - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
            1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
            1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
            9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
            1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands), name='cnn_input')
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    inputs2 = tf.keras.Input(batch_shape=(None, 1), name='y0_input')
    m2 = tf.keras.layers.concatenate([m2, inputs2], axis=-1)
    m2 = tf.keras.layers.Dense(512)(m2)
    outputs = tf.keras.layers.Dense(1, name='dy_output')(m2)
    m = tf.keras.Model(inputs=[inputs, inputs2], outputs=outputs)

    m.load_weights(f"../output/cnn50m_change2010-2015_v3_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        data_mask2,
        model_cnn,
        nbands=11,
        chunksize=None,
        out_name=None
):
    center_pixel = 7*15*nbands + 7*nbands + 6
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        arr_new2 = data_mask2[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]]), arr_new2], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2]), arr_new2], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_new2 = np.moveaxis(arr_new2, 0, -1)[None, :, :, :]
        arr_new = np.concatenate([arr_new, arr_new2], axis=-1)
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, center_pixel] > -1000, :]
        print(pred_X.shape)
        pred_X = sample_normalize_1(pred_X.reshape(-1, nbands)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, nbands)
        print(arr_cnn.shape)
        if pred_X.shape[0] > 0:
            model_y_pred = model_cnn[0].predict(arr_cnn)
            model_y_pred[model_y_pred < 0] = 0
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, center_pixel] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


nbands = 18
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
batch_size = 128
epochs = [10, ]
dn_structure = [
    (12, 18, 24),
]
buffer = [5, ]

for k in range(0, len(width)):

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
    path_tif2 = "xlyrs_50m_nbar_2010_v2.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))    # da_b = da_mask.band.values
    # da_x = da_mask.x.values
    # da_y = da_mask.y.values
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        # print(ij)
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        da_mask2 = dask.delayed(xr.open_rasterio)(path_tif2, chunks=(-1, chunk_size, chunk_size))
        # results.append(func_predict_cnn(ij, da_mask, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
        results.append(func_predict_cnn(ij, da_mask, da_mask2, model,
                                        nbands=nbands, chunksize=chunk_size,
                                        out_name=f"../output/tmp/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 0, 2: 0},
    #                           boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'})
    # file_id = zarr.open_array("../output/tmp_dask.zarr", shape=g.shape, mode="w",
    #                           chunks=(nbands, chunk_size+10, chunk_size+10),
    #                           synchronizer=zarr.ThreadSynchronizer())
    # dskda.store(g, file_id)
    # g2 = g.map_blocks(func_predict_cnn, drop_axis=[0, 1, 2])
    # da_pred = dskda.overlap.trim_internal(g2, {0: 0, 1: 5, 2: 5})
    # da_pred = g.map_blocks(func_predict_cnn, dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
    #                             dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             # boundary={0: 'none', 1: 'none', 2: 'none'},
    #                             trim=True)

    # out_file1 = f"../output/mch_prediction_dask.zarr"
    # # file_id = zarr.open_array(out_file1, shape=da_pred.shape, mode="w",
    # #                           chunks=(nbands, chunk_size, chunk_size),
    # #                           # synchronizer=zarr.ThreadSynchronizer(),
    # #                           )
    # # dskda.store(da_pred, file_id)
    # delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True,
    #                             synchronizer=zarr.ThreadSynchronizer(),
    #                             compute=False)
    # # out_file1 = f"../output/mch_prediction_dask.nc"
    # # da1 = xr.DataArray(da_pred, coords=(da_b, da_y, da_x), dims=("band", "y", "x"))
    # # ds2 = xr.Dataset({"da": da1})
    # # delayed_obj = ds2.to_netcdf(out_file1, compute=True)
    # # with ProgressBar():
    # #     results = delayed_obj.compute(num_workers=1)

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)













#%% predict change - 18 bands + MCH from 2015 to 2010 - from airborne-only - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
            1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
            1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
            9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
            1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands), name='cnn_input')
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    inputs2 = tf.keras.Input(batch_shape=(None, 1), name='y0_input')
    m2 = tf.keras.layers.concatenate([m2, inputs2], axis=-1)
    m2 = tf.keras.layers.Dense(512)(m2)
    outputs = tf.keras.layers.Dense(1, name='dy_output')(m2)
    m = tf.keras.Model(inputs=[inputs, inputs2], outputs=outputs)

    m.load_weights(f"../output/cnn50m_change2010-2015_v3_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        data_mask2,
        data_mask3,
        model_cnn,
        nbands=11,
        chunksize=None,
        out_name=None
):
    center_pixel = 7*15*nbands + 7*nbands + 6
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        arr_new2 = data_mask2[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]]), arr_new2], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2]), arr_new2], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_new2 = np.moveaxis(arr_new2, 0, -1)[None, :, :, :]
        arr_new = np.concatenate([arr_new, arr_new2], axis=-1)
        print(f'input1 dim: {arr_new.shape}')
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, center_pixel] > -1000, :]
        pred_X = sample_normalize_1(pred_X.reshape(-1, nbands)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, nbands)
        print(f'input1 patch dim: {arr_cnn.shape}')
        arr_new3 = data_mask3[:,
                   (block_id[0] + 0) * chunksize//5:min((block_id[0] + 1) * chunksize//5, data_mask3.shape[1]),
                   (block_id[1] + 0) * chunksize//5:min((block_id[1] + 1) * chunksize//5, data_mask3.shape[2])
                   ].values
        arr_new3 = np.moveaxis(arr_new3, 0, -1)[None, :, :, :]
        print(f'input2 dim: {arr_new3.shape}')
        arr_y = tf.image.extract_patches(images=arr_new3,
                                         sizes=[1, 1, 1, 1],
                                         strides=[1, 1, 1, 1],
                                         rates=[1, 1, 1, 1],
                                         padding='VALID').numpy().reshape(-1, 1)
        arr_y = arr_y[arr_table[:, center_pixel] > -1000, :]
        print(f'input2 patch dim: {arr_y.shape}')
        if pred_X.shape[0] > 0:
            model_y_pred = model_cnn[0].predict([arr_cnn, arr_y])
            # model_y_pred[model_y_pred < 0] = 0
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, center_pixel] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


nbands = 18
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
res = [250, ]
batch_size = 128
epochs = [10, ]
dn_structure = [
    (12, 18, 24),
]
buffer = [5, ]

for k in range(0, len(width)):

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
    path_tif2 = "xlyrs_50m_nbar_2010_v2.vrt"
    path_tif3 = f"../output/mch_prediction_cnn50m_output2015_v3_{res[k]}m_width{width[k]}.tif"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))    # da_b = da_mask.band.values
    # da_x = da_mask.x.values
    # da_y = da_mask.y.values
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        # print(ij)
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        da_mask2 = dask.delayed(xr.open_rasterio)(path_tif2, chunks=(-1, chunk_size, chunk_size))
        da_mask3 = dask.delayed(xr.open_rasterio)(path_tif3, chunks=(-1, chunk_size, chunk_size))
        # results.append(func_predict_cnn(ij, da_mask, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
        results.append(func_predict_cnn(ij, da_mask, da_mask2, da_mask3, model,
                                        nbands=nbands, chunksize=chunk_size,
                                        out_name=f"../output/tmp/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 0, 2: 0},
    #                           boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'})
    # file_id = zarr.open_array("../output/tmp_dask.zarr", shape=g.shape, mode="w",
    #                           chunks=(nbands, chunk_size+10, chunk_size+10),
    #                           synchronizer=zarr.ThreadSynchronizer())
    # dskda.store(g, file_id)
    # g2 = g.map_blocks(func_predict_cnn, drop_axis=[0, 1, 2])
    # da_pred = dskda.overlap.trim_internal(g2, {0: 0, 1: 5, 2: 5})
    # da_pred = g.map_blocks(func_predict_cnn, dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
    #                             dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             # boundary={0: 'none', 1: 'none', 2: 'none'},
    #                             trim=True)

    # out_file1 = f"../output/mch_prediction_dask.zarr"
    # # file_id = zarr.open_array(out_file1, shape=da_pred.shape, mode="w",
    # #                           chunks=(nbands, chunk_size, chunk_size),
    # #                           # synchronizer=zarr.ThreadSynchronizer(),
    # #                           )
    # # dskda.store(da_pred, file_id)
    # delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True,
    #                             synchronizer=zarr.ThreadSynchronizer(),
    #                             compute=False)
    # # out_file1 = f"../output/mch_prediction_dask.nc"
    # # da1 = xr.DataArray(da_pred, coords=(da_b, da_y, da_x), dims=("band", "y", "x"))
    # # ds2 = xr.Dataset({"da": da1})
    # # delayed_obj = ds2.to_netcdf(out_file1, compute=True)
    # # with ProgressBar():
    # #     results = delayed_obj.compute(num_workers=1)

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)












#%% setup training data for changes (2010-2015) -- more complete
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_2(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


nbands = 11
nbands2 = 7
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
res = [250, ]
batch_size = 256

k = 0

# 2015 -als
df0 = pd.read_csv("csvs/mch_250m_training_v0v1_glas_v2.csv", index_col=0)
df_all = df0.iloc[:73227, :]

# path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
# path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2015_v3als.tfrecord"
# ga.regression_tfrecord_from_df(
#     df_all,
#     path_tif,
#     path_tfrecord,
#     x_cols="xvar",
#     response="MCH",
#     loc=("latitude", "longitude"),
#     offset=(-offsets[k], offsets[k]),
#     blocksize=(width[k], width[k]),
# )
path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2015_v3als.tfrecord"
ds, train_n = ga.load_tf(
    path_training,
    x_features="xvar",
    y_feature="MCH",
    x_width=(width[k], width[k], nbands),
    y_width=(1,),
    compression="",
)
ds1 = (
        ds
        .cache()
        .map(sample_normalize)
        .batch(batch_size)
        .prefetch(AUTOTUNE)
)
xlst = []
ylst = []
for element in ds1.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x_train = np.concatenate(xlst, axis=0)
y_train = np.concatenate(ylst, axis=0)


# 2010 -als
nbands2 = 7
# path_tif = "xlyrs_50m_nbar_2010_v2.vrt"
# path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2010_v3als.tfrecord"
# ga.regression_tfrecord_from_df(
#     df_all,
#     path_tif,
#     path_tfrecord,
#     x_cols="xvar",
#     response="MCH",
#     loc=("latitude", "longitude"),
#     offset=(-offsets[k], offsets[k]),
#     blocksize=(width[k], width[k]),
# )
path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2010_v3als.tfrecord"
ds, train_n = ga.load_tf(
    path_training,
    x_features="xvar",
    y_feature="MCH",
    x_width=(width[k], width[k], nbands2),
    y_width=(1,),
    compression="",
)
ds1 = (
    ds
    .cache()
    .map(sample_normalize_2)
    .batch(batch_size)
    .prefetch(AUTOTUNE)
)
xlst = []
ylst = []
for element in ds1.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x2_train = np.concatenate(xlst, axis=0)
y2_train = np.concatenate(ylst, axis=0)


print(0)
# select set changing pixels
# n = x2_train.shape[0]
n = 6000
rs1 = np.random.RandomState(666)
id1 = np.random.choice(x_train.shape[0], n, replace=False)
rs2 = np.random.RandomState(777)
id2 = np.random.choice(x2_train.shape[0], n, replace=False)
for i in id1:
# for i in range(x_train.shape[0]):
    x1 = [
        np.repeat(x_train[i:i + 1, :, :, :], n, axis=0),
        x2_train[id2, :, :, :]
    ]
    x1a = np.concatenate(x1, axis=-1)
    x1b = np.repeat(y_train[i:i + 1, :], n, axis=0)
    y1diff = y2_train[id2, :] - y_train[i, 0]
    path = f"tfrecords/change_all/train_2015-2010_als_v3_i{i}.tfrecord"
    opt1 = tf.io.TFRecordOptions(compression_type=None)
    with tf.io.TFRecordWriter(path, options=opt1) as file_writer:
        for k in range(x1a.shape[0]):
            record_bytes = tf.train.Example(
                features=tf.train.Features(feature={
                    "cnn_input": tf.train.Feature(float_list=tf.train.FloatList(value=x1a[k:k+1,:,:,:].ravel())),
                    "y0_input": tf.train.Feature(float_list=tf.train.FloatList(value=x1b[k:k+1,:].ravel())),
                    "dy_output": tf.train.Feature(float_list=tf.train.FloatList(value=y1diff[k:k+1,:].ravel())),
                })
            ).SerializeToString()
            file_writer.write(record_bytes)




# 2015 -glas
df0 = pd.read_csv("csvs/mch_250m_training_v0v1_glas_v2.csv", index_col=0)
df_all = df0.iloc[73227:, :]

path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2015_v3glas.tfrecord"
ga.regression_tfrecord_from_df(
    df_all,
    path_tif,
    path_tfrecord,
    x_cols="xvar",
    response="MCH",
    loc=("latitude", "longitude"),
    offset=(-offsets[k], offsets[k]),
    blocksize=(width[k], width[k]),
)
path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2015_v3glas.tfrecord"
ds, train_n = ga.load_tf(
    path_training,
    x_features="xvar",
    y_feature="MCH",
    x_width=(width[k], width[k], nbands),
    y_width=(1,),
    compression="",
)
ds1 = (
        ds
        .cache()
        .map(sample_normalize)
        .batch(batch_size)
        .prefetch(AUTOTUNE)
)
xlst = []
ylst = []
for element in ds1.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x_train = np.concatenate(xlst, axis=0)
y_train = np.concatenate(ylst, axis=0)


# 2010 -glas
nbands2 = 7
path_tif = "xlyrs_50m_nbar_2010_v2.vrt"
path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2010_v3glas.tfrecord"
ga.regression_tfrecord_from_df(
    df_all,
    path_tif,
    path_tfrecord,
    x_cols="xvar",
    response="MCH",
    loc=("latitude", "longitude"),
    offset=(-offsets[k], offsets[k]),
    blocksize=(width[k], width[k]),
)
path_training = f"tfrecords/training_{res[k]}m_width{width[k]}_from_xlyrs_2010_v3glas.tfrecord"
ds, train_n = ga.load_tf(
    path_training,
    x_features="xvar",
    y_feature="MCH",
    x_width=(width[k], width[k], nbands2),
    y_width=(1,),
    compression="",
)
ds1 = (
    ds
    .cache()
    .map(sample_normalize_2)
    .batch(batch_size)
    .prefetch(AUTOTUNE)
)
xlst = []
ylst = []
for element in ds1.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
x2_train = np.concatenate(xlst, axis=0)
y2_train = np.concatenate(ylst, axis=0)


print(0)
# select set changing pixels
# n = x2_train.shape[0]
n = 6000
rs1 = np.random.RandomState(666)
id1 = np.random.choice(x_train.shape[0], n, replace=False)
rs2 = np.random.RandomState(777)
id2 = np.random.choice(x2_train.shape[0], n, replace=False)
for i in id1:
# for i in range(x_train.shape[0]):
    x1 = [
        np.repeat(x_train[i:i + 1, :, :, :], n, axis=0),
        x2_train[id2, :, :, :]
    ]
    x1a = np.concatenate(x1, axis=-1)
    x1b = np.repeat(y_train[i:i + 1, :], n, axis=0)
    y1diff = y2_train[id2, :] - y_train[i, 0]
    path = f"tfrecords/change_all/train_2015-2010_glas_v3_i{i}.tfrecord"
    opt1 = tf.io.TFRecordOptions(compression_type=None)
    with tf.io.TFRecordWriter(path, options=opt1) as file_writer:
        for k in range(x1a.shape[0]):
            record_bytes = tf.train.Example(
                features=tf.train.Features(feature={
                    "cnn_input": tf.train.Feature(float_list=tf.train.FloatList(value=x1a[k:k+1,:,:,:].ravel())),
                    "y0_input": tf.train.Feature(float_list=tf.train.FloatList(value=x1b[k:k+1,:].ravel())),
                    "dy_output": tf.train.Feature(float_list=tf.train.FloatList(value=y1diff[k:k+1,:].ravel())),
                })
            ).SerializeToString()
            file_writer.write(record_bytes)




#%% training model for changes (2015-2010)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


# def sample_normalize_18(X, X2, Y):
#     sample_mean = tf.constant(
#         [
#             [
#                 [
#                     3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
#                     1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
#                     1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
#                     3.2123085e-02,
#                     3.1446800e-01,
#                     1.7229147e-01,
#                     6.1950065e-02,
#                     2.2465163e-01,
#                     7.6607764e-02,
#                     5.6069208e02,
#                 ]
#             ]
#         ]
#     )
#     sample_std = tf.constant(
#         [
#             [
#                 [
#                     1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
#                     9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
#                     1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
#                     7.8484304e-03,
#                     2.8701460e-02,
#                     2.1761550e-02,
#                     1.4474887e-02,
#                     4.6037395e-02,
#                     1.8193731e-02,
#                     2.4210707e02,
#                 ]
#             ]
#         ]
#     )
#     X = (X - sample_mean) / sample_std
#
#     X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
#     # # X1 = X.numpy().reshape([-1, 7])
#     # X1 = X.reshape([-1, 7])
#     # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
#     # imp_0.fit_transform(X1)
#     # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
#     # # X = tf.convert_to_tensor(X2, np.float32)
#
#     # return X[:, :, 4:], Y
#     return X[:, :, :], X2, Y


def sample_normalize_18(input, output):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = input["cnn_input"]
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    input["cnn_input"] = X
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return input, output


nbands = 11
nbands2 = 7
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
res = [250, ]
batch_size = 128
epochs = [10, ]
dn_structure = [
    (12, 18, 24),
]

k = 0
inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], nbands+nbands2), name='cnn_input')
mlayer = ga.DenseNet(
    num_layers_in_each_block=dn_structure[k],
    growth_rate=8,
    dropout_rate=0.2,
    weight_decay=1e-4,
    compression=0.5,
    bottleneck=True,
    include_top=False,
)(inputs)
m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
inputs2 = tf.keras.Input(batch_shape=(None, 1), name='y0_input')
m2 = tf.keras.layers.concatenate([m2, inputs2], axis=-1)
m2 = tf.keras.layers.Dense(512)(m2)
outputs = tf.keras.layers.Dense(1, name='dy_output')(m2)
m = tf.keras.Model(inputs=[inputs, inputs2], outputs=outputs)
# m = tf.keras.Model(inputs=inputs, outputs=outputs)
m.summary()

m.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.get("MeanSquaredError"),
    # loss=tf.keras.losses.get("MeanAbsoluteError"),
    metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
)


path_tfrecord = f"tfrecords/change/train_2015-2010_als_v3v0v1_i*.tfrecord"
glob = tf.io.gfile.glob(path_tfrecord)
dataset = tf.data.TFRecordDataset(glob, compression_type="GZIP")
feature_columns = {
    "cnn_input": tf.io.FixedLenFeature([width[k], width[k], nbands+nbands2], dtype=tf.float32),
    "y0_input": tf.io.FixedLenFeature([1, ], dtype=tf.float32),
    "dy_output": tf.io.FixedLenFeature([1, ], dtype=tf.float32),
}
parsed = dataset.map(
    lambda example: tf.io.parse_single_example(example, feature_columns),
    num_parallel_calls=4,
)
parsed = parsed.map(
    lambda example: ({"cnn_input": example.get("cnn_input"), "y0_input": example.get("y0_input")},
                     {"dy_output": example.get("dy_output")}),
    num_parallel_calls=4,
)
# parsed = parsed.map(
#     lambda example: ({"cnn_input": example.get("cnn_input")},
#                      {"dy_output": example.get("dy_output")}),
#     num_parallel_calls=4,
# )
records_n = sum(1 for _ in dataset)
print("records_n = {}".format(records_n))
ds1 = (
    parsed
    # .cache()
    .map(sample_normalize_18)
    .batch(batch_size)
    .prefetch(AUTOTUNE)
)
xdiff_test = np.load("tfrecords/change/test_2015-2010_als_v3_xdiff1.npy")
xdiff2_test = np.load("tfrecords/change/test_2015-2010_als_v3_xdiff2.npy")
ydiff_test = np.load("tfrecords/change/test_2015-2010_als_v3_ydiff.npy")
ds0 = tf.data.Dataset.from_tensor_slices(
    ({'cnn_input': xdiff_test, 'y0_input': xdiff2_test},
     {'dy_output': ydiff_test}))
# ds0 = tf.data.Dataset.from_tensor_slices(
#     ({'cnn_input': xdiff_test},
#      {'dy_output': ydiff_test}))
ds0 = ds0.map(sample_normalize_18).batch(batch_size)


earlystop_callback = tf.keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0.001, verbose=1,
    patience=2, restore_best_weights=True)
history = m.fit(
    ds1,
    epochs=epochs[0],
    callbacks=[earlystop_callback],
    validation_data=ds0,
    # steps_per_epoch=int(train_n / batch_size),
    # validation_steps=int(test_n / batch_size),
)
plt.figure(figsize=(6, 4))
plt.plot(history.history["root_mean_squared_error"], label="Training")
plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
plt.xlabel("Epoch")
plt.ylabel("RMSE")
plt.legend(loc="upper right")
plt.savefig(f"../output/cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}_history.png", dpi=300)
plt.show()
m.save_weights(f"../output/cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}_trained_weights.h5")

predictions = m.predict(ds0, verbose=1)
r2, rmse, beta0 = ggm.density_scatter_plot(
    ydiff_test[~np.isnan(ydiff_test)],
    predictions[~np.isnan(ydiff_test)],
    file_name=f"../output/cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}_val.png",
)


# model_regress = ggm.StackRegressor(xdiff_test, ydiff_test)
# model_pipe, params_grid = model_regress.setup_rf_model(
#     param_grid={
#         "pca": [None],
#         "learn__n_estimators": [500],
#         # "learn__max_depth": [12],
#         # "learn__min_samples_split": [2],
#         "learn__max_depth": [6, 9, 12],
#         "learn__min_samples_split": [2, 7, 12],
#     }
# )
# model_regress.grid_opts(model_pipe, params_grid, k=5)
#
# model_regress.meta_model_biascorr(X=xdiff_train, y=ydiff_train, k=3)
# model_y_pred = model_regress.meta_model_predict_biascorr(xdiff_test, ydiff_test)
# model_regress.save_models(outname=f"../output/rf50m_bc_diff2010-2015_v3_{res[k]}m_width{width[k]}.pkl")
# if isinstance(y_test, pd.core.series.Series):
#     y_test = y_test.values
# r2, rmse, beta0 = ggm.density_scatter_plot(
#     y_test[~np.isnan(y_test)],
#     model_y_pred[~np.isnan(y_test)],
#     file_name="{}_val.png".format(f"../output/rf50m_bc_diff2010-2015_v3_{res[k]}m_width{width[k]}"),
# )
#
# model_regress.meta_model(X=xdiff_train, y=ydiff_train, k=3)
# model_y_pred = model_regress.meta_model_predict(xdiff_test, ydiff_test)
# model_regress.save_models(outname=f"../output/rf50m_diff2010-2015_v3_{res[k]}m_width{width[k]}.pkl")
# if isinstance(y_test, pd.core.series.Series):
#     y_test = y_test.values
# r2, rmse, beta0 = ggm.density_scatter_plot(
#     y_test[~np.isnan(y_test)],
#     model_y_pred[~np.isnan(y_test)],
#     file_name="{}_val.png".format(f"../output/rf50m_diff2010-2015_v3_{res[k]}m_width{width[k]}"),
# )

print(time.time() - start)



#%% predict change - 18 bands from 2015 to 2010 - from airborne-only - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
            1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
            1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
            9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
            1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands), name='cnn_input')
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    inputs2 = tf.keras.Input(batch_shape=(None, 1), name='y0_input')
    m2 = tf.keras.layers.concatenate([m2, inputs2], axis=-1)
    m2 = tf.keras.layers.Dense(512)(m2)
    outputs = tf.keras.layers.Dense(1, name='dy_output')(m2)
    m = tf.keras.Model(inputs=[inputs, inputs2], outputs=outputs)

    m.load_weights(f"../output/cnn50m_change2010-2015_v3_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        data_mask2,
        model_cnn,
        nbands=11,
        chunksize=None,
        out_name=None
):
    center_pixel = 7*15*nbands + 7*nbands + 6
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        arr_new2 = data_mask2[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]]), arr_new2], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2]), arr_new2], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_new2 = np.moveaxis(arr_new2, 0, -1)[None, :, :, :]
        arr_new = np.concatenate([arr_new, arr_new2], axis=-1)
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, center_pixel] > -1000, :]
        print(pred_X.shape)
        pred_X = sample_normalize_1(pred_X.reshape(-1, nbands)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, nbands)
        print(arr_cnn.shape)
        if pred_X.shape[0] > 0:
            model_y_pred = model_cnn[0].predict(arr_cnn)
            model_y_pred[model_y_pred < 0] = 0
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, center_pixel] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


nbands = 18
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
batch_size = 128
epochs = [10, ]
dn_structure = [
    (12, 18, 24),
]
buffer = [5, ]

for k in range(0, len(width)):

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
    path_tif2 = "xlyrs_50m_nbar_2010_v2.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))    # da_b = da_mask.band.values
    # da_x = da_mask.x.values
    # da_y = da_mask.y.values
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        # print(ij)
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        da_mask2 = dask.delayed(xr.open_rasterio)(path_tif2, chunks=(-1, chunk_size, chunk_size))
        # results.append(func_predict_cnn(ij, da_mask, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
        results.append(func_predict_cnn(ij, da_mask, da_mask2, model,
                                        nbands=nbands, chunksize=chunk_size,
                                        out_name=f"../output/tmp/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 0, 2: 0},
    #                           boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'})
    # file_id = zarr.open_array("../output/tmp_dask.zarr", shape=g.shape, mode="w",
    #                           chunks=(nbands, chunk_size+10, chunk_size+10),
    #                           synchronizer=zarr.ThreadSynchronizer())
    # dskda.store(g, file_id)
    # g2 = g.map_blocks(func_predict_cnn, drop_axis=[0, 1, 2])
    # da_pred = dskda.overlap.trim_internal(g2, {0: 0, 1: 5, 2: 5})
    # da_pred = g.map_blocks(func_predict_cnn, dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
    #                             dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             # boundary={0: 'none', 1: 'none', 2: 'none'},
    #                             trim=True)

    # out_file1 = f"../output/mch_prediction_dask.zarr"
    # # file_id = zarr.open_array(out_file1, shape=da_pred.shape, mode="w",
    # #                           chunks=(nbands, chunk_size, chunk_size),
    # #                           # synchronizer=zarr.ThreadSynchronizer(),
    # #                           )
    # # dskda.store(da_pred, file_id)
    # delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True,
    #                             synchronizer=zarr.ThreadSynchronizer(),
    #                             compute=False)
    # # out_file1 = f"../output/mch_prediction_dask.nc"
    # # da1 = xr.DataArray(da_pred, coords=(da_b, da_y, da_x), dims=("band", "y", "x"))
    # # ds2 = xr.Dataset({"da": da1})
    # # delayed_obj = ds2.to_netcdf(out_file1, compute=True)
    # # with ProgressBar():
    # #     results = delayed_obj.compute(num_workers=1)

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_nbar2015_v3_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)













#%% predict change - 18 bands + MCH from 2015 to 2010 - from airborne-only - v3
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
            1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
            1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
            9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
            1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    X = np.where(np.isnan(X), np.zeros_like(X), X)

    return X


def load_model(dn_structure, res, width, nbands):
    # model_regress = ggm.StackRegressor(X_test1, y_test)
    # model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands), name='cnn_input')
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    inputs2 = tf.keras.Input(batch_shape=(None, 1), name='y0_input')
    m2 = tf.keras.layers.concatenate([m2, inputs2], axis=-1)
    m2 = tf.keras.layers.Dense(512)(m2)
    outputs = tf.keras.layers.Dense(1, name='dy_output')(m2)
    m = tf.keras.Model(inputs=[inputs, inputs2], outputs=outputs)

    m.load_weights(f"../output/cnn50m_change2010-2015_v3_{res}m_width{width}_trained_weights.h5")
    return m


@dask.delayed
def func_predict_cnn(
        block_id,
        data_mask,
        data_mask2,
        data_mask3,
        model_cnn,
        nbands=11,
        chunksize=None,
        out_name=None
):
    center_pixel = 7*15*nbands + 7*nbands + 6
    out_file1 = f"{out_name}_{block_id[0]}_{block_id[1]}.nc"
    if os.path.isfile(out_file1):
        print(out_file1)
        return 0
    else:
        buffer = 5
        min_x = (block_id[1] + 0) * chunksize - buffer
        min_y = (block_id[0] + 0) * chunksize - buffer
        max_x = min((block_id[1] + 1) * chunksize + buffer, data_mask.shape[2] + buffer)
        max_y = min((block_id[0] + 1) * chunksize + buffer, data_mask.shape[1] + buffer)
        arr_new = data_mask[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        arr_new2 = data_mask2[:, max(min_y, 0):min(max_y, data_mask.shape[1]),
                  max(min_x, 0):min(max_x, data_mask.shape[2])].values
        if min_y < 0:
            dim1 = 0 - min_y
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]]), arr_new], axis=1)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]]), arr_new2], axis=1)
        if min_x < 0:
            dim2 = 0 - min_x
            arr_new = np.concatenate([np.zeros([arr_new.shape[0], arr_new.shape[1], dim2]), arr_new], axis=2)
            arr_new2 = np.concatenate([np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2]), arr_new2], axis=2)
        if max_y > data_mask.shape[1]:
            dim1 = max_y - data_mask.shape[1]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], dim1, arr_new.shape[2]])], axis=1)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], dim1, arr_new2.shape[2]])], axis=1)
        if max_x > data_mask.shape[2]:
            dim2 = max_x - data_mask.shape[2]
            arr_new = np.concatenate([arr_new, np.zeros([arr_new.shape[0], arr_new.shape[1], dim2])], axis=2)
            arr_new2 = np.concatenate([arr_new2, np.zeros([arr_new2.shape[0], arr_new2.shape[1], dim2])], axis=2)

        arr_new = np.moveaxis(arr_new, 0, -1)[None, :, :, :]
        arr_new2 = np.moveaxis(arr_new2, 0, -1)[None, :, :, :]
        arr_new = np.concatenate([arr_new, arr_new2], axis=-1)
        print(f'input1 dim: {arr_new.shape}')
        arr_patch = tf.image.extract_patches(images=arr_new,
                                             sizes=[1, 15, 15, 1],
                                             strides=[1, 5, 5, 1],
                                             rates=[1, 1, 1, 1],
                                             padding='VALID')
        arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
        pred_X = arr_table[arr_table[:, center_pixel] > -1000, :]
        pred_X = sample_normalize_1(pred_X.reshape(-1, nbands)).reshape(-1, arr_table.shape[1])
        arr_cnn = pred_X.reshape(pred_X.shape[0], 15, 15, nbands)
        print(f'input1 patch dim: {arr_cnn.shape}')
        arr_new3 = data_mask3[:,
                   (block_id[0] + 0) * chunksize//5:min((block_id[0] + 1) * chunksize//5, data_mask3.shape[1]),
                   (block_id[1] + 0) * chunksize//5:min((block_id[1] + 1) * chunksize//5, data_mask3.shape[2])
                   ].values
        arr_new3 = np.moveaxis(arr_new3, 0, -1)[None, :, :, :]
        print(f'input2 dim: {arr_new3.shape}')
        arr_y = tf.image.extract_patches(images=arr_new3,
                                         sizes=[1, 1, 1, 1],
                                         strides=[1, 1, 1, 1],
                                         rates=[1, 1, 1, 1],
                                         padding='VALID').numpy().reshape(-1, 1)
        arr_y = arr_y[arr_table[:, center_pixel] > -1000, :]
        print(f'input2 patch dim: {arr_y.shape}')
        if pred_X.shape[0] > 0:
            model_y_pred = model_cnn[0].predict([arr_cnn, arr_y])
            # model_y_pred[model_y_pred < 0] = 0
            # model_y_pred = model.meta_model_predict_biascorr(pred_X)

            # tf.keras.backend.clear_session()
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
            pred_arr[arr_table[:, center_pixel] > -1000] = model_y_pred.ravel()
            pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
            print(f"pred_shape: {pred_arr.shape}")
        else:
            pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

        idx = np.arange(block_id[1] * chunksize + 2, max_x, 5)
        idy = np.arange(block_id[0] * chunksize + 2, max_y, 5)
        x1 = data_mask.x[idx[:pred_arr.shape[1]]]
        y1 = data_mask.y[idy[:pred_arr.shape[0]]]
        da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])

        ds1 = xr.Dataset({"da": da})
        s = ds1.to_netcdf(out_file1)
        return 1


nbands = 18
pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([5.])
width = np.array([15])
res = [250, ]
batch_size = 128
epochs = [10, ]
dn_structure = [
    (12, 18, 24),
]
buffer = [5, ]

for k in range(0, len(width)):

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_50m_probaxlc8_2015_v3.vrt"
    path_tif2 = "xlyrs_50m_nbar_2010_v2.vrt"
    path_tif3 = f"../output/mch_prediction_cnn50m_output2015_v3_{res[k]}m_width{width[k]}.tif"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))    # da_b = da_mask.band.values
    # da_x = da_mask.x.values
    # da_y = da_mask.y.values
    model = load_model(dn_structure[k], res[k], width[k], nbands)
    model = dask.persist(model)

    ij_lst = [(i, j) for i in range(np.ceil(da_mask.shape[1]/chunk_size).astype(int))
              for j in range(np.ceil(da_mask.shape[2]/chunk_size).astype(int))]
    results = []
    for ij in ij_lst:
        # print(ij)
        da_mask = dask.delayed(xr.open_rasterio)(path_tif, chunks=(-1, chunk_size, chunk_size))
        da_mask2 = dask.delayed(xr.open_rasterio)(path_tif2, chunks=(-1, chunk_size, chunk_size))
        da_mask3 = dask.delayed(xr.open_rasterio)(path_tif3, chunks=(-1, chunk_size, chunk_size))
        # results.append(func_predict_cnn(ij, da_mask, chunksize=chunk_size, out_name=f"../output/tmp/mch_tmpblocks"))
        results.append(func_predict_cnn(ij, da_mask, da_mask2, da_mask3, model,
                                        nbands=nbands, chunksize=chunk_size,
                                        out_name=f"../output/tmp/mch_tmpblocks"))
    with ProgressBar():
        s = dask.compute(*results)


    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 0, 2: 0},
    #                           boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'})
    # file_id = zarr.open_array("../output/tmp_dask.zarr", shape=g.shape, mode="w",
    #                           chunks=(nbands, chunk_size+10, chunk_size+10),
    #                           synchronizer=zarr.ThreadSynchronizer())
    # dskda.store(g, file_id)
    # g2 = g.map_blocks(func_predict_cnn, drop_axis=[0, 1, 2])
    # da_pred = dskda.overlap.trim_internal(g2, {0: 0, 1: 5, 2: 5})
    # da_pred = g.map_blocks(func_predict_cnn, dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
    #                             dtype='float32', **kwargs)
    # da_pred = dskda.map_overlap(da_mask.data, func_predict_cnn, depth={0: 0, 1: 5, 2: 5},
    #                             # boundary={0: 'none', 1: 'none', 2: 'none'},
    #                             trim=True)

    # out_file1 = f"../output/mch_prediction_dask.zarr"
    # # file_id = zarr.open_array(out_file1, shape=da_pred.shape, mode="w",
    # #                           chunks=(nbands, chunk_size, chunk_size),
    # #                           # synchronizer=zarr.ThreadSynchronizer(),
    # #                           )
    # # dskda.store(da_pred, file_id)
    # delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True,
    #                             synchronizer=zarr.ThreadSynchronizer(),
    #                             compute=False)
    # # out_file1 = f"../output/mch_prediction_dask.nc"
    # # da1 = xr.DataArray(da_pred, coords=(da_b, da_y, da_x), dims=("band", "y", "x"))
    # # ds2 = xr.Dataset({"da": da1})
    # # delayed_obj = ds2.to_netcdf(out_file1, compute=True)
    # # with ProgressBar():
    # #     results = delayed_obj.compute(num_workers=1)

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_cnn50m_change2010-2015_v3_{res[k]}m_width{width[k]}.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)









