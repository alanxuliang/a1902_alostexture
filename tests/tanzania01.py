#%% setup environment
from pathlib import Path
import subprocess
import numpy as np
import pandas as pd
import xarray as xr
import zarr
from zarr import blosc
import seaborn as sns
import matplotlib.pyplot as plt
import a1902alos.data_load as adl
import a1902alos.grid_models as agm
import learn2map.raster_tools as rt
from sklearn.model_selection import train_test_split

compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)


#%% get registered
p = Path("F:\\Projects\\p0x_tanzania")
ref_file = str(p / "input" / "Tanzania_chm_mosaic_100m_agb_geo.tif")
# in01 = str(p / "input" / "Tanzania_alos_hh_hv_mosaic_25m_resample1.flt")
# out01 = str(p / "input" / "alos_hhhv_100m_geo.tif")
# rt.raster_clip(ref_file, in01, out01, resampling_method="average")

in01 = str(p / "input" / "sentinel_1_tanz_2016.tif")
out01 = str(p / "input" / "sentinel_vvvh_100m_geo.tif")
rt.raster_clip(ref_file, in01, out01, resampling_method="average")

#%% get zarr
p = Path("F:\\Projects\\p0x_tanzania")
in01 = [
    str(p / "input" / "Tanzania_chm_mosaic_100m_agb_geo.tif"),
    str(p / "input" / "alos_hhhv_100m_geo.tif"),
    str(p / "input" / "sentinel_vvvh_100m_geo.tif"),
    ]
da_list = []
for i in in01:
    da = xr.open_rasterio(i)
    da_list.append(da)
    print(da)

da2 = xr.concat(da_list, dim="band")
da2["band"] = ("band", ["agb", "alos_hh", "alos_hv", "sntnl_vv", "sntnl_vh"])

ds0 = xr.Dataset({"da": da2})
ds0.to_zarr(
    str(p / "input" / "tanzania_ds.zarr"),
    mode="w",
    synchronizer=zarr.ThreadSynchronizer(),
    encoding={"da": {"compressor": compressor}},
)


#%% get training samples
da = xr.open_zarr(str(p / "input" / "tanzania_ds.zarr"))["da"]
out_file1 = str(p / "output" / "tanzania01_training_samples")
adl.get_df_from_da_with_xy(da, da[:1, :, :], out_file1, valid_min=0)


#%% explore df
p = Path("F:\\Projects\\p0x_tanzania")
out_file1 = str(p / "output" / "tanzania01_training_samples")
df = pd.read_parquet(f"{out_file1}.pqt")
df = df.dropna()
df["sntnl_vv"] = 10 ** (df["sntnl_vv"] / 10)
df["sntnl_vh"] = 10 ** (df["sntnl_vh"] / 10)
df1 = df.iloc[:, 2:]
sns.clustermap(df1.corr(), figsize=(6, 6), annot=True, center=0)
plt.savefig(f"{out_file1}_corr.png", dpi=300)

bins = np.append(np.arange(0, 201, 20), 9999)
df["agb_cat"] = pd.cut(df.agb, bins)
df["agb_cat_int"] = df.agb_cat.cat.codes.astype(int) * 20 + 10
plt.clf()
sns.set()
sns.lineplot(x="agb_cat_int", y="sntnl_vv", err_style='band', ci='sd', data=df)
sns.lineplot(x="agb_cat_int", y="sntnl_vh", err_style='band', ci='sd', data=df)
sns.lineplot(x="agb_cat_int", y="alos_hh", err_style='band', ci='sd', data=df)
sns.lineplot(x="agb_cat_int", y="alos_hv", err_style='band', ci='sd', data=df)
plt.legend(['Sentinel VV', "Sentinel VH", "ALOS HH", 'ALOS HV'])
plt.xlim([10, 150])
# plt.ylim([0, 0.3])
plt.xlabel("AGB (Mg/ha)")
plt.ylabel("Radar Backscatter")
plt.gcf().set_size_inches(6, 4)
plt.tight_layout()
plt.savefig(f"{out_file1}_agb_vs_radar.png", dpi=300)


#%% learning models
df = df[df.agb<150]
y = df.agb
X1 = df[["alos_hh", "alos_hv"]]
X2 = df[["alos_hh", "alos_hv", "sntnl_vv", "sntnl_vh"]]

X_train, X_test, y_train, y_test1 = train_test_split(
    X1, y, test_size=400, random_state=577
)
model_regress = agm.StackRegressor(X_train, y_train)
model_pipe, params_grid = model_regress.setup_ridgecv_model()
model_regress.grid_opts(model_pipe, params_grid, k=5)
y_pred1 = model_regress.best_models[0].predict(X_test)

X_train, X_test, y_train, y_test2 = train_test_split(
    X2, y, test_size=400, random_state=577
)
model_regress = agm.StackRegressor(X_train, y_train)
model_pipe, params_grid = model_regress.setup_ridgecv_model()
model_regress.grid_opts(model_pipe, params_grid, k=5)
y_pred2 = model_regress.best_models[0].predict(X_test)

fig0 = plt.figure(figsize=(5, 4))
plt.scatter(y_test1, y_pred1, s=30, alpha=0.3)
plt.scatter(y_test2, y_pred2, s=30, alpha=0.3)
plt.xlabel("Measured AGB (Mg/ha)")
plt.ylabel("Predicted AGB (Mg/ha)")
plt.legend({'ALOS Only ($RMSE=20.5$)', 'ALOS + Sentinel ($RMSE=18.6$)'}, loc='lower right')
plt.xlim([0, 100])
plt.ylim([0, 100])
plt.tight_layout()
plt.savefig(f"{out_file1}_ridgemodel_comparison.png", dpi=300)
