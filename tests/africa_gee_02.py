# import modules (using ALOS/SRTM layers only @ 30m)
import os
import subprocess
import time
import gdal
import a1902alos.gee_app as ga
import a1902alos.grid_models as ggm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import xarray as xr
import dask.array as dskda
import seaborn as sns
from scipy.interpolate import UnivariateSpline
from sklearn.linear_model import LinearRegression
from dask.diagnostics import ProgressBar
import zarr
from zarr import blosc

compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)

# %% training data generation (different resolutions: 100, 250, 500, 750, 1k, 1.5k, 2k, 3k)
start = time.time()
os.chdir("Y:\\Projects\\a1902alostexture\\output")


def sample_normalize(X, Y):
    # sample_mean = tf.constant(
    #     [
    #         [
    #             [
    #                 3.2123085e-02,
    #                 3.1446800e-01,
    #                 1.7229147e-01,
    #                 6.1950065e-02,
    #                 2.2465163e-01,
    #                 7.6607764e-02,
    #                 5.6069208e02,
    #             ]
    #         ]
    #     ]
    # )
    # sample_std = tf.constant(
    #     [
    #         [
    #             [
    #                 7.8484304e-03,
    #                 2.8701460e-02,
    #                 2.1761550e-02,
    #                 1.4474887e-02,
    #                 4.6037395e-02,
    #                 1.8193731e-02,
    #                 2.4210707e02,
    #             ]
    #         ]
    #     ]
    # )
    sample_mean = tf.constant([[[6570.302, 3821.262, 563.231]]])
    sample_std = tf.constant([[[1464.3604, 929.6932, 249.8123]]])
    X = (X - sample_mean) / sample_std
    return X, Y


res = [100, 250, 500, 750, 1000, 1500, 2000, 3000]
pix_size = 0.000224578821
offsets = 0.00005 + pix_size * np.array([1.5, 4.5, 9.5, 14.5, 19.5, 29.5, 39.5, 59.5])
width = np.array([1, 2, 3, 4, 6, 8, 12, 20])
width = np.array([4, 10, 20, 30, 40, 60, 80, 120])
dn_structure = [
    (6, 12),
    (6, 12, 12),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 32, 20),
]
epochs = [5, 10, 80, 110, 150, 200, 250, 300]

mch_100 = "../input/lidar_data/drc/lidar_plots_mch.tif"
# for k in range(0, 1):
for k in range(len(res)):
    ref_file = f"../input/mask_{res[k]}m_all.tif"
    mch_newres = f"../input/lidar_data/drc/lidar_plots_mch_{res[k]}m.tif"
    mch_count_newres = f"../input/lidar_data/drc/lidar_plots_mch_tf_{res[k]}m.tif"
    mch_newres, mch_count_newres = ga.raster_resample(
        mch_100, ref_file, mch_newres, mch_count_newres
    )
    mch_newres_out = f"../input/lidar_data/drc/lidar_valid_mch_{res[k]}m.tif"
    ga.raster_mask(mch_newres, mch_count_newres, mch_newres_out, valid_min=0.8)

    mch_csv_out = f"../input/csvs/mch_{res[k]}m_geo_v2"
    ga.raster_to_csv(f"{mch_newres_out}", mch_csv_out, valid_min=0.0)

# %% training model - 7 bands from 250m lyrs (different resolutions: 100, 250, 500, 750, 1k, 1.5k, 2k, 3k)
start = time.time()
# os.chdir("Y:\\Projects\\a1902alostexture\\input")
os.chdir('/Volumes/wd4tb/a1902alostexture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    return X[:, :, 4:], Y


pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([0, 0.5, 1, 1.5, 2.5, 3.5, 5.5, 9.5])
width = np.array([1, 2, 3, 4, 6, 8, 12, 20])
dn_structure = [
    (12,),
    (12,),
    (12, 18),
    (12, 18),
    (12, 18, 24),
    (12, 18, 24),
    (12, 18, 24),
    (12, 18, 24, 16),
]
epochs = [10, 20, 30, 50, 80, 100, 150, 200]
# epochs = [20, 50, 80, 110, 150, 200, 250, 300]

res = [250, 500, 750, 1000, 1500, 2000]

for k in range(len(res)):

    path_training = f"tfrecords/training_{res[k]}m_from_xlyrs_250m_1317_v0v2.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_from_xlyrs_250m_1317_v0v2.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="X",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], 7),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="X",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], 7),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds_test.take(1)).next())

    # inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 3))
    # mlayer = ga.DenseNet(
    #     num_layers_in_each_block=dn_structure[k],
    #     growth_rate=8,
    #     dropout_rate=0.2,
    #     weight_decay=1e-4,
    #     compression=0.5,
    #     bottleneck=True,
    #     include_top=False,
    # )(inputs)
    # m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    # m2 = tf.keras.layers.Dense(32)(m2)
    # # m2 = tf.keras.layers.Dropout(0.2)(m2)
    # outputs = tf.keras.layers.Dense(1)(m2)
    # m = tf.keras.Model(inputs, outputs)
    #
    # m.compile(
    #     optimizer=tf.keras.optimizers.Adam(0.001),
    #     loss=tf.keras.losses.get("MeanSquaredError"),
    #     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    # )
    #
    # history = m.fit(
    #     x=ds1,
    #     epochs=epochs[k],
    #     steps_per_epoch=int(train_n / batch_size),
    #     validation_data=ds_test,
    #     validation_steps=int(test_n / batch_size),
    # )
    # plt.figure(figsize=(6, 4))
    # plt.plot(history.history["root_mean_squared_error"], label="Training")
    # plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    # plt.xlabel("Epoch")
    # plt.ylabel("RMSE")
    # plt.legend(loc="upper right")
    # plt.savefig(f"../output/cnn250m_output_v2_{res[k]}m_history.png", dpi=300)
    # plt.show()
    # m.save_weights(f"../output/cnn250m_output_v2_{res[k]}m_trained_weights.h5")
    #
    # X_list = []
    # y_list = []
    # for X, Y in ds_test.take(int(test_n / batch_size)):
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_test = np.concatenate(X_list)
    # y_test = np.concatenate(y_list)
    # predictions = m.predict(X_test, verbose=1)
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     predictions[~np.isnan(y_test)],
    #     file_name=f"../output/cnn250m_output_v2_{res[k]}m_val.png",
    # )
    #
    # dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})
    #
    # tf.keras.backend.clear_session()
    #
    # # compare with RF model using spatial mean of each band
    # X_list = []
    # y_list = []
    # for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_train = np.concatenate(X_list)
    # y_train = np.concatenate(y_list)
    # X_train = np.nanmean(X_train, axis=(1, 2))
    # y_train = y_train.squeeze()
    # X_test = np.nanmean(X_test, axis=(1, 2))
    # y_test = y_test.squeeze()
    #
    # model_regress = ggm.StackRegressor(X_test, y_test)
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [500],
    #         # "learn__max_depth": [12],
    #         # "learn__min_samples_split": [2],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__min_samples_split": [2, 7, 12],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)
    #
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # # model_regress.save_models(outname=f"xgboost250m_bc_output_v2_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"rf250m_bc_output_v2_{res[k]}m.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost250m_bc_output_v2_{res[k]}m"),
    #     file_name="{}_val.png".format(f"rf250m_bc_output_v2_{res[k]}m"),
    # )
    #
    # model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict(X_test)
    # # model_regress.save_models(outname=f"xgboost250m_output_v2_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"rf250m_output_v2_{res[k]}m.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost250m_output_v2_{res[k]}m"),
    #     file_name="{}_val.png".format(f"rf250m_output_v2_{res[k]}m"),
    # )
    # dfy['y_rfbc'] = model_y_pred[~np.isnan(y_test)]

    # compare with CNN+linear model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 3))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(32)(m2)
    # m2 = tf.keras.layers.Dropout(0.2)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn250m_output_v2_{res[k]}m_trained_weights.h5")
    mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)

    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_mlayer_train = mlayer_model.predict(X_train, verbose=1)

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    X_mlayer_test = mlayer_model.predict(X_test, verbose=1)

    model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [100, 200, 400, 600],
    #         "learn__max_depth": [3, 6, 9, 12],
    #         # "learn__booster": ["gbtree","gblinear", "dart"],
    #         "learn__booster": ["dart"],
    #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    #     }
    # )
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [300],
    #         # "learn__max_depth": [6, 9, 12],
    #         # "learn__min_samples_split": [2, 7, 12],
    #         "learn__max_depth": [9],
    #         "learn__min_samples_split": [2],
    #     }
    # )
    model_regress.grid_opts(model_pipe, params_grid, k=3)
    # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    model_y_pred = model_regress.meta_model_predict(X_mlayer_test)

    # model_regress.save_models(outname=f"../output/ridge250m_mlayer_model_v2_{res[k]}m.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    y_test = y_test.ravel()
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"../output/ridge250m_mlayer_model_v2_{res[k]}m"),
    )
    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": model_y_pred[~np.isnan(y_test)]})
    dfy["y_ridgetest"] = y_test[~np.isnan(y_test)]
    dfy.to_csv(f"../output/cnn250m_output_v2_{res[k]}m_val_ridge.csv")

    # dfy.to_csv(f"../output/cnn250m_output_v2_{res[k]}m_val_data.csv")
    tf.keras.backend.clear_session()

    print(time.time() - start)


def get_xys(df):
    df['y_sum'] = df.y_test.values + df.y_predcnn.values
    df['y_bin'] = pd.cut(df.y_sum, np.arange(0, 70, 6))
    df1 = df.groupby('y_bin').mean()
    x = df1.y_test.values
    y = df1.y_predcnn.values
    spl = UnivariateSpline(x, y)
    spl.set_smoothing_factor(10)
    # reg = LinearRegression().fit(x[:, None], y)
    xs = np.linspace(0, 35, 100)
    ys = spl(xs)
    # ys = reg.predict(xs[:, None])
    return x, y, xs, ys


l1 = []
fig, ax = plt.subplots(figsize=(5, 4))
l1a = plt.plot([-5, 35], [-5, 35], 'k')
l1.append(l1a[0])

# df = pd.read_csv(f"../output/cnn250m_output_v2_250m_val_data.csv")
df = pd.read_csv(f"../output/cnn250m_output_v2_250m_val_ridge.csv")
x, y, xs, ys = get_xys(df)
p1 = plt.plot(x, y, 'o', alpha=0.7)
l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
l1.append(l1a[0])
# ax = sns.regplot(x=df.y_test, y=df.y_predcnn, x_bins=np.arange(0, 35.1, 3), ci=None, ax=ax, scatter_kws={'alpha': 0.7}, order=3, line_kws={'alpha': 0.7})
# l1.append(ax.get_lines()[-1])

# df = pd.read_csv(f"../output/cnn250m_output_v2_500m_val_data.csv")
df = pd.read_csv(f"../output/cnn250m_output_v2_500m_val_ridge.csv")
x, y, xs, ys = get_xys(df)
p1 = plt.plot(x, y, 'o', alpha=0.7)
l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
l1.append(l1a[0])

# df = pd.read_csv(f"../output/cnn250m_output_v2_1000m_val_data.csv")
df = pd.read_csv(f"../output/cnn250m_output_v2_1000m_val_ridge.csv")
x, y, xs, ys = get_xys(df)
p1 = plt.plot(x, y, 'o', alpha=0.7)
l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
l1.append(l1a[0])

# df = pd.read_csv(f"../output/cnn250m_output_v2_2000m_val_data.csv")
df = pd.read_csv(f"../output/cnn250m_output_v2_2000m_val_ridge.csv")
x, y, xs, ys = get_xys(df)
p1 = plt.plot(x, y, 'o', alpha=0.7)
l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
l1.append(l1a[0])

ax.set_xlabel('Measurements')
ax.set_ylabel('Predictions')
ax.set_xlim([-5, 35])
ax.set_ylim([-5, 35])
ax.legend(l1, ['1-to-1 Line', '250m Retrieval', '500m Retrieval', '1km Retrieval', '2km Retrieval'])
# ax.legend(l1, ['1-to-1 Line', '250m Retrieval', '500m Retrieval', '1km Retrieval'])
plt.savefig(f"../output/cnn250m_output_v2_multi-scale_scatter3s.png", dpi=300)
plt.show()



# %% training model - 3 bands (different resolutions: 100, 250, 500, 750, 1k, 1.5k, 2k, 3k)
start = time.time()
os.chdir("Y:\\Projects\\a1902alostexture\\input")


def sample_normalize(X, Y):
    # sample_mean = tf.constant(
    #     [
    #         [
    #             [
    #                 3.2123085e-02,
    #                 3.1446800e-01,
    #                 1.7229147e-01,
    #                 6.1950065e-02,
    #                 2.2465163e-01,
    #                 7.6607764e-02,
    #                 5.6069208e02,
    #             ]
    #         ]
    #     ]
    # )
    # sample_std = tf.constant(
    #     [
    #         [
    #             [
    #                 7.8484304e-03,
    #                 2.8701460e-02,
    #                 2.1761550e-02,
    #                 1.4474887e-02,
    #                 4.6037395e-02,
    #                 1.8193731e-02,
    #                 2.4210707e02,
    #             ]
    #         ]
    #     ]
    # )
    sample_mean = tf.constant([[[6570.302, 3821.262, 563.231]]])
    sample_std = tf.constant([[[1464.3604, 929.6932, 249.8123]]])
    X = (X - sample_mean) / sample_std
    return X, Y


res = [100, 250, 500, 750, 1000, 1500, 2000, 3000]
pix_size = 0.000224578821
offsets = 0.00005 + pix_size * np.array([1.5, 4.5, 9.5, 14.5, 19.5, 29.5, 39.5, 59.5])
width = np.array([4, 10, 20, 30, 40, 60, 80, 120])
dn_structure = [
    (6, 12),
    (6, 12, 12),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 32, 20),
]
epochs = [5, 10, 30, 50, 80, 100, 150, 200]

# for k in range(len(res)):
for k in range(1, 2):
    path_tif = "xlyrs_radar_25m_1517_v0.vrt"

    # df0 = dd.read_csv(f"csvs/mch_{res[k]}m_geo_v2_*.csv").compute()
    # df, df_test = train_test_split(df0, test_size=0.2, random_state=77)
    #
    # path_tfrecord = f"tfrecords/training_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     # response="mch",    # for v1
    #     response="MCH",  # for v0, v2
    #     # loc=("latitude", "longitude"),  # for v0,v1
    #     loc=("Y", "X"),  # for v2
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )
    # path_tfrecord = f"tfrecords/test_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df_test,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     # response="mch",    # for v1
    #     response="MCH",  # for v0, v2
    #     # loc=("latitude", "longitude"),  # for v0,v1
    #     loc=("Y", "X"),  # for v2
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )

    path_training = f"tfrecords/training_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        # y_feature="mch",    # for v1
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], 3),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        # y_feature="mch",    # for v1
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], 3),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds_test.take(1)).next())

    # m = ga.DenseNet(
    #     num_layers_in_each_block=dn_structure[k],
    #     growth_rate=12,
    #     dropout_rate=0.2,
    #     weight_decay=1e-4,
    #     compression=0.6,
    #     bottleneck=True,
    # )
    # m = ga.toy_resnet()
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 3))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(1024)(m2)
    m2 = tf.keras.layers.Dropout(0.2)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    history = m.fit(
        x=ds1,
        epochs=epochs[k],
        steps_per_epoch=int(train_n / batch_size),
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    plt.savefig(f"../output/cnn1_output_v2_{res[k]}m_history.png", dpi=300)
    plt.show()

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        file_name=f"../output/cnn1_output_v2_{res[k]}m_val.png",
    )

    tf.keras.backend.clear_session()

    # compare with RF model using spatial mean of each band
    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_train = np.nanmean(X_train, axis=(1, 2))
    y_train = y_train.squeeze()
    X_test = np.nanmean(X_test, axis=(1, 2))
    y_test = y_test.squeeze()

    model_regress = ggm.StackRegressor(X_test, y_test)
    model_regress = ggm.StackRegressor(X_test, y_test)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [250, 500, 750],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__booster": ["gbtree"],
    #         "learn__colsample_bytree": [0.6, 1],
    #     }
    # )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # model_regress.save_models(outname=f"xgboost_bc_output_v2_{res[k]}m.pkl")
    model_regress.save_models(outname=f"rf_bc_output_v2_{res[k]}m.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost_bc_output_v2_{res[k]}m"),
        file_name="{}_val.png".format(f"rf_bc_output_v2_{res[k]}m"),
    )

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict(X_test)
    # model_regress.save_models(outname=f"xgboost_output_v2_{res[k]}m.pkl")
    model_regress.save_models(outname=f"rf_output_v2_{res[k]}m.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost_output_v2_{res[k]}m"),
        file_name="{}_val.png".format(f"rf_output_v2_{res[k]}m"),
    )

    # compare with CNN+linear model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 3))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=12,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.6,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(16, activation="relu")(m2)
    m2 = tf.keras.layers.Dropout(0.2)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn1_output_v2_{res[k]}m_trained_weights.h5")
    mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)

    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_mlayer_train = mlayer_model.predict(X_train, verbose=1)

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    X_mlayer_test = mlayer_model.predict(X_test, verbose=1)

    model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [100, 200, 400, 600],
    #         "learn__max_depth": [3, 6, 9, 12],
    #         # "learn__booster": ["gbtree","gblinear", "dart"],
    #         "learn__booster": ["dart"],
    #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    #     }
    # )
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [300],
    #         # "learn__max_depth": [6, 9, 12],
    #         # "learn__min_samples_split": [2, 7, 12],
    #         "learn__max_depth": [9],
    #         "learn__min_samples_split": [2],
    #     }
    # )
    model_regress.grid_opts(model_pipe, params_grid, k=3)
    # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    model_y_pred = model_regress.meta_model_predict(X_mlayer_test)

    model_regress.save_models(outname=f"../output/ridge_mlayer_model_v2_{res[k]}m.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    y_test = y_test.ravel()
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"../output/ridge_mlayer_model_v2_{res[k]}m"),
    )

    print(time.time() - start)

# %% training model (resolution: 250m)
start = time.time()
os.chdir("Y:\\Projects\\a1902alostexture\\input")


def sample_normalize(X, Y):
    sample_mean = tf.constant([[[6570.302, 3821.262, 563.231]]])
    sample_std = tf.constant([[[1464.3604, 929.6932, 249.8123]]])
    X = (X - sample_mean) / sample_std
    return X, Y


res = [
    250,
]
pix_size = 0.000224578821
offsets = 0.00005 + pix_size * np.array([4.5, ])
width = np.array([10, ])
dn_structure = [
    (6, 12, 12),
]
epochs = [
    20,
]

for k in range(1):
    path_tif = "xlyrs_radar_25m_1517_v0.vrt"

    # df0 = dd.read_csv(f"csvs/mch_{res[k]}m_geo_v2_*.csv").compute()
    # df, df_test = train_test_split(df0, test_size=0.2, random_state=77)
    #
    # path_tfrecord = f"tfrecords/training_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     # response="mch",    # for v1
    #     response="MCH",  # for v0, v2
    #     # loc=("latitude", "longitude"),  # for v0,v1
    #     loc=("Y", "X"),  # for v2
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )
    # path_tfrecord = f"tfrecords/test_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"
    # ga.regression_tfrecord_from_df(
    #     df_test,
    #     path_tif,
    #     path_tfrecord,
    #     x_cols="xvar",
    #     # response="mch",    # for v1
    #     response="MCH",  # for v0, v2
    #     # loc=("latitude", "longitude"),  # for v0,v1
    #     loc=("Y", "X"),  # for v2
    #     offset=(-offsets[k], offsets[k]),
    #     blocksize=(width[k], width[k]),
    # )

    path_training = f"tfrecords/training_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"
    path_test = f"tfrecords/test_{res[k]}m_from_xlyrs_1517_v0v2.tfrecord"
    band_width = 3

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], band_width),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        # y_feature="mch",    # for v1
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], band_width),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds_test.take(1)).next())

    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 3))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=12,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.6,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(16, activation="relu")(m2)
    m2 = tf.keras.layers.Dropout(0.2)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    history = m.fit(
        x=ds1,
        epochs=epochs[k],
        steps_per_epoch=int(train_n / batch_size),
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    plt.savefig(f"../output/cnn1_output_v2_{res[k]}m_history.png", dpi=300)
    plt.show()
    m.save_weights(f"../output/cnn1_output_v2_{res[k]}m_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        file_name=f"../output/cnn1_output_v2_{res[k]}m_val.png",
    )

    tf.keras.backend.clear_session()

    # # compare with RF model using spatial mean of each band
    # X_list = []
    # y_list = []
    # for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_train = np.concatenate(X_list)
    # y_train = np.concatenate(y_list)
    # X_train = np.nanmean(X_train, axis=(1, 2))
    # y_train = y_train.squeeze()
    # X_test = np.nanmean(X_test, axis=(1, 2))
    # y_test = y_test.squeeze()
    #
    # model_regress = ggm.StackRegressor(X_test, y_test)
    # model_regress = ggm.StackRegressor(X_test, y_test)
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [500],
    #         # "learn__max_depth": [12],
    #         # "learn__min_samples_split": [2],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__min_samples_split": [2, 7, 12],
    #     }
    # )
    # # model_pipe, params_grid = model_regress.setup_xgb_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [250, 500, 750],
    # #         "learn__max_depth": [6, 9, 12],
    # #         "learn__booster": ["gbtree"],
    # #         "learn__colsample_bytree": [0.6, 1],
    # #     }
    # # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)
    #
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # # model_regress.save_models(outname=f"xgboost_bc_output_v2_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"rf_bc_output_v2_{res[k]}m.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost_bc_output_v2_{res[k]}m"),
    #     file_name="{}_val.png".format(f"rf_bc_output_v2_{res[k]}m"),
    # )
    #
    # model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict(X_test)
    # # model_regress.save_models(outname=f"xgboost_output_v2_{res[k]}m.pkl")
    # model_regress.save_models(outname=f"rf_output_v2_{res[k]}m.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost_output_v2_{res[k]}m"),
    #     file_name="{}_val.png".format(f"rf_output_v2_{res[k]}m"),
    # )

    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 3))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=12,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.6,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(16, activation="relu")(m2)
    m2 = tf.keras.layers.Dropout(0.2)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn1_output_v2_{res[k]}m_trained_weights.h5")
    mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)

    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_mlayer_train = mlayer_model.predict(X_train, verbose=1)

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    X_mlayer_test = mlayer_model.predict(X_test, verbose=1)

    model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [100, 200, 400, 600],
    #         "learn__max_depth": [3, 6, 9, 12],
    #         # "learn__booster": ["gbtree","gblinear", "dart"],
    #         "learn__booster": ["dart"],
    #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    #     }
    # )
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [300],
    #         # "learn__max_depth": [6, 9, 12],
    #         # "learn__min_samples_split": [2, 7, 12],
    #         "learn__max_depth": [9],
    #         "learn__min_samples_split": [2],
    #     }
    # )
    model_regress.grid_opts(model_pipe, params_grid, k=3)
    # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    model_y_pred = model_regress.meta_model_predict(X_mlayer_test)

    model_regress.save_models(outname=f"../output/ridge_mlayer_model_v2_{res[k]}m.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    y_test = y_test.ravel()
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"../output/ridge_mlayer_model_v2_{res[k]}m"),
    )

    print(time.time() - start)

# %% get prediction (resolution: 250m)
start = time.time()
os.chdir("Y:\\Projects\\a1902alostexture\\input")


def sample_normalize_0(X):
    # sample_mean = tf.constant(
    #     [
    #         [
    #             [
    #                 3.2123085e-02,
    #                 3.1446800e-01,
    #                 1.7229147e-01,
    #                 6.1950065e-02,
    #                 2.2465163e-01,
    #                 7.6607764e-02,
    #                 5.6069208e02,
    #             ]
    #         ]
    #     ]
    # )
    # sample_std = tf.constant(
    #     [
    #         [
    #             [
    #                 7.8484304e-03,
    #                 2.8701460e-02,
    #                 2.1761550e-02,
    #                 1.4474887e-02,
    #                 4.6037395e-02,
    #                 1.8193731e-02,
    #                 2.4210707e02,
    #             ]
    #         ]
    #     ]
    # )
    sample_mean = tf.constant([[[6570.302, 3821.262, 563.231]]])
    sample_std = tf.constant([[[1464.3604, 929.6932, 249.8123]]])
    X = (X - sample_mean) / sample_std
    return X


def sample_normalize(X, Y):
    # sample_mean = tf.constant(
    #     [
    #         [
    #             [
    #                 3.2123085e-02,
    #                 3.1446800e-01,
    #                 1.7229147e-01,
    #                 6.1950065e-02,
    #                 2.2465163e-01,
    #                 7.6607764e-02,
    #                 5.6069208e02,
    #             ]
    #         ]
    #     ]
    # )
    # sample_std = tf.constant(
    #     [
    #         [
    #             [
    #                 7.8484304e-03,
    #                 2.8701460e-02,
    #                 2.1761550e-02,
    #                 1.4474887e-02,
    #                 4.6037395e-02,
    #                 1.8193731e-02,
    #                 2.4210707e02,
    #             ]
    #         ]
    #     ]
    # )
    sample_mean = tf.constant([[[6570.302, 3821.262, 563.231]]])
    sample_std = tf.constant([[[1464.3604, 929.6932, 249.8123]]])
    X = (X - sample_mean) / sample_std
    return X, Y


def func_predict_cnn_0(
        mask_arr,
        block_id=None,
        chunksize=None,
        xdata=None,
        ydata=None,
        model=None,
        path_tif="",
        path_tfrecord="",
        offset=(0, 0),
        blocksize=(1, 1),
        batch_size=40,
):
    if xdata is None:
        xdata = np.arange(mask_arr.shape[1])
        ydata = np.arange(mask_arr.shape[0])

    id = np.argwhere(mask_arr > 0)
    print(id.shape)
    x1 = xdata[id[:, 1] + block_id[1] * chunksize]
    y1 = ydata[id[:, 0] + block_id[0] * chunksize]

    # bak
    # y_width = blocksize[0]
    # x_width = blocksize[1]
    # y_offset = y1 - offset[0]
    # x_offset = x1 - offset[1]
    # da1 = xr.open_rasterio(path_tif, chunks=(-1, 2000, 2000))
    # nbands = da1.band.shape[0]
    # da1y = da1.y.values
    # da1x = da1.x.values
    # X_test = np.zeros([id.shape[0], y_width, x_width, nbands], "float32")
    # for i in np.arange(id.shape[0]):
    #     id1y = np.abs(da1y - y_offset[i]).argmin()
    #     id1x = np.abs(da1x - x_offset[i]).argmin()
    #     id2y = id1y + y_width
    #     id2x = id1x + x_width
    #     id1y = max(0, id1y)
    #     id1x = max(0, id1x)
    #     id2y = min(da1y.shape[0], id2y)
    #     id2x = min(da1x.shape[0], id2x)
    #     img = da1.isel(y=slice(id1y, id2y), x=slice(id1x, id2x)).transpose(
    #         "y", "x", "band"
    #     )
    #     X_test[i, : img.shape[0], : img.shape[1], :] = img.values

    df1 = pd.DataFrame({"Y": y1, "X": x1})
    file_tfrecord = f"{path_tfrecord}_{block_id[0]}_{block_id[1]}.tfrecord"
    if os.path.isfile(file_tfrecord):
        pass
    else:
        print(file_tfrecord)
        ga.regression_tfrecord_from_df(df1, path_tif, file_tfrecord, offset=offset, blocksize=blocksize)

    in0 = gdal.Open(path_tif)
    n_bands = in0.RasterCount
    in0 = None
    pred_ds, pred_n = ga.load_tf(
        file_tfrecord,
        x_features="xvar",
        y_feature="NA",  # predictions
        x_width=(blocksize[0], blocksize[1], n_bands),
        y_width=(1,),
        compression="",
    )

    pred_ds = pred_ds.map(sample_normalize).batch(batch_size)
    model_y_pred = model.predict(pred_ds, verbose=1)

    pred_arr = np.full_like(mask_arr.ravel(), np.nan, dtype="float32")
    # pred_arr[0, id[:, 0], id[:, 1]] = model_y_pred.ravel()
    pred_arr[mask_arr.ravel() > 0] = model_y_pred.ravel()
    pred_arr = pred_arr.reshape(mask_arr.shape)
    print(f"pred_shape: {pred_arr.shape}")
    return pred_arr


res = [100, 250, 500, 750, 1000, 1500, 2000, 3000]
pix_size = 0.000224578821
offsets = 0.00005 + pix_size * np.array([1.5, 4.5, 9.5, 14.5, 19.5, 29.5, 39.5, 59.5])
width = np.array([4, 10, 20, 30, 40, 60, 80, 120])
dn_structure = [
    (6, 12),
    (6, 12, 12),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 32, 20),
]
epochs = [5, 10, 80, 110, 150, 200, 250, 300]

for k in range(1, 2):
    # mask_file = f"mask_{res[k]}m_all.tif"
    mask_file = f"mask_{res[k]}m_congo.tif"
    path_tif = "xlyrs_radar_25m_1517_v0.vrt"
    mask_zarr = f"mask_{res[k]}m_congo.zarr"
    path_zarr = f"xlyrs_radar_25m_1517_v0.zarr"
    # da_mask = xr.open_rasterio(mask_file, chunks=(1, 1000, 1000))[0, :, :]
    # ds0 = xr.Dataset({"da": da_mask})
    # ds0.to_zarr(
    #     mask_zarr,
    #     mode="w",
    #     synchronizer=zarr.ThreadSynchronizer(),
    #     encoding={"da": {"compressor": compressor}},
    # )
    da_mask = xr.open_zarr(mask_zarr)["da"]
    chunk_size = 1000

    # da0 = xr.open_rasterio(path_tif, chunks=(1, 3000, 3000))
    # ds0 = xr.Dataset({"da": da0})
    # ds0.to_zarr(
    #     path_zarr,
    #     mode="w",
    #     synchronizer=zarr.ThreadSynchronizer(),
    #     encoding={"da": {"compressor": compressor}},
    # )

    da_x = da_mask.x.values
    da_y = da_mask.y.values
    path_tfrecord = f"tfrecords/xlyrs_radar_{res[k]}m_from_25m_1517_v0"

    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 3))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=12,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.6,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(16, activation="relu")(m2)
    m2 = tf.keras.layers.Dropout(0.2)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn1_output_v2_{res[k]}m_trained_weights.h5")

    kwargs = {
        "chunksize": chunk_size,
        "xdata": da_x,
        "ydata": da_y,
        "model": m,
        "path_tif": path_tif,
        "path_tfrecord": path_tfrecord,
        "offset": (-offsets[k], offsets[k]),
        "blocksize": (width[k], width[k]),
        "batch_size": 40,
    }
    da_pred = dskda.map_blocks(func_predict_cnn_0, da_mask.data, dtype='float32', **kwargs)

    # da_pred = xr.apply_ufunc(
    #     func_predict_cnn_0,
    #     da_mask,
    #     kwargs=kwargs,
    #     dask="parallelized",
    #     # dask="allowed",
    #     output_dtypes=[float],
    # )

    da1 = xr.DataArray(da_pred, coords=(da_y, da_x), dims=("y", "x"))
    out_file1 = f"../output/mch_prediction_{res[k]}m_from_radar_v0.nc"
    ds2 = xr.Dataset({"da": da1})
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_{res[k]}m_from_radar_v0.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)
