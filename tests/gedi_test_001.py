# import modules (using ALOS/SRTM/L8 layers @ 50m)
import os
import subprocess
import time
import glob
import gdal
import h5py
import hypertools as hyp
import a1902alos.gee_app as ga
import a1902alos.grid_models as ggm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import xarray as xr
from scipy import spatial
from scipy import interpolate
import scipy.ndimage as ndimage
import dask
import dask.array as dskda
import dask.dataframe as dd
import seaborn as sns
import learn2map.raster_tools as rt
from scipy.interpolate import UnivariateSpline
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import (
    train_test_split,
)
from sklearn.impute import SimpleImputer
from dask.diagnostics import ProgressBar
import zarr
from zarr import blosc

AUTOTUNE = tf.data.experimental.AUTOTUNE
compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)

# from dask.distributed import Client, progress
# client = Client(threads_per_worker=4, n_workers=1)
# client
print(1)


#%% GEDI preprocessing (all RH metrics)

os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")

j = 0
files = glob.glob("L2A/*.h5")
for k in range(len(files)):
    try:
        print(files[k])
        with h5py.File(files[k], 'r') as f:
            h_lst = []
            gtx = ['BEAM0000', 'BEAM0001', 'BEAM0010', 'BEAM0011', 'BEAM0101', 'BEAM0110', 'BEAM1000', 'BEAM1011']
            for i in range(len(gtx)):
                print(i)
                gti = gtx[i]
                try:
                    lati = f[f'/{gti}/lat_lowestmode'].value[:, None]
                    loni = f[f'/{gti}/lon_lowestmode'].value[:, None]
                    hmetric1 = f[f'/{gti}/rh'].value[:, :]
                    hmetric2 = f[f'/{gti}/elev_lowestmode'].value[:, None]
                    hmetric3 = f[f'/{gti}/elevation_bin0_error'].value[:, None]
                    modeflag = f[f'/{gti}/num_detectedmodes'].value[:, None]
                    sensiflag = f[f'/{gti}/sensitivity'].value[:, None]
                    h_all = np.concatenate([lati, loni, hmetric1, hmetric2, hmetric3,
                                            modeflag, sensiflag], axis=1)
                    qualityflag = f[f'/{gti}/quality_flag'].value
                    lcflag = f[f'/{gti}/land_cover_data/landsat_treecover'].value
                    h_lst.append(h_all[(qualityflag == 1) & (lcflag > 0), :])
                except:
                    print("segment corrupted")
        h_arr = np.concatenate(h_lst, axis=0)
        if h_arr.shape[0] > 0:
            df0 = pd.DataFrame(h_arr, columns=['lat', 'lon'] + [f'rh{i}' for i in range(0, 101, 1)] +
                                               ['elev', 'elev_err', 'num_modes', 'sensitivity'])
            df0.to_csv(f"global/GEDI02A_lidar_2019_{j}.csv", index=False)
            j = j+1
    except:
        print("h5 file corrupted")



#%% GEDI processing (gedi-amazon overlap)

start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")

# glas_file = "GEDI02A_lidar_2019_*.csv"
# df1 = dd.read_csv(glas_file)
# df2 = df1[['lat', 'lon', 'rh90']].compute()
# df3 = df2[(df2.lat > -30) & (df2.lat < 30)]
# df3.to_csv("gedi01a_lidar_xyz.csv")

# df1 = pd.read_csv("gedi01a_lidar_xyz.csv", index_col=0)
# df2 = df1.sample(5000000)
# df2.to_csv("gedi01a_lidar_xyz_sample.csv")


df1 = pd.read_csv("gedi01a_lidar_xyz.csv", index_col=0)
df1 = df1[(df1.lat>-30) & (df1.lat<10) & (df1.lon>-90) & (df1.lon<-40)]
files = [
    "../../gfc_gain_loss/gfc_loss_amazon_100m_north.tif",
    "../../gfc_gain_loss/gfc_loss_amazon_100m_south.tif",
]
for ifile in files:
    daX = xr.open_rasterio(ifile)
    id0 = np.argwhere(daX.data[0, :, :] > -1)
    X1 = daX.x.values[id0[:, 1]][:, None]
    Y1 = daX.y.values[id0[:, 0]][:, None]
    del daX
    del id0
    tree = spatial.cKDTree(df1[["lon", "lat"]].values)
    x_lst = []
    y_lst = []
    kspace = np.linspace(0, X1.shape[0], num=51).astype(int)
    for k in range(kspace.shape[0] - 1):
        print(k)
        X2 = X1[kspace[k]:kspace[k + 1], :]
        Y2 = Y1[kspace[k]:kspace[k + 1], :]
        _, minid = tree.query(np.concatenate([X2, Y2], axis=1), k=10, distance_upper_bound=0.00064)

        for i in range(minid.shape[0]):
            count = np.sum(minid[i, :] < df1.shape[0])
            if count > 0:
                # idx = minid[i, minid[i, :] < df1.shape[0]]
                # x0 = df1.iloc[idx, 2:].values.mean(axis=0)
                xy = np.concatenate([Y2[i:i + 1, 0], X2[i:i + 1, 0], np.array([count])])
                x_lst.append(xy)
    xy = np.array(x_lst)
    xy_col = ['lat', 'lon', 'count']
    df3 = pd.DataFrame(xy, columns=xy_col)
    df3.to_csv(ifile.replace('.tif', '_gedi.csv'))


sns.set()
for ifile in files:
    daX = xr.open_rasterio(ifile)
    size_x = daX.shape[1] * daX.shape[2]
    print(size_x)
    df3 = pd.read_csv(ifile.replace('.tif', '_gedi.csv'))
    fig = plt.figure(figsize=[4, 3])
    ax = sns.countplot(x="count", data=df3)
    plt.xlabel('Number of Shots in 1-ha pixels')
    plt.ylabel('Pixel Density per ha')
    plt.ylim([0, 0.007 * size_x])
    plt.yticks([i * size_x for i in np.arange(0, 0.008, 0.001)])
    num_list = np.array(ax.get_yticks()) / size_x * 100
    ax.set_yticklabels([f"{x:.1f}%" for x in num_list.tolist()])
    plt.tight_layout()
    plt.savefig(ifile.replace('.tif', '_gedi_count.png'), dpi=300, bbox_inches='tight')
    print(df3["count"].values.mean())



#%% GEDI amazon waveform plotting
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")


files = [
    # "../../gfc_gain_loss/gfc_loss_amazon_100m_north.tif",
    "../../gfc_gain_loss/gfc_loss_amazon_100m_south.tif",
]
for ifile in files:
    daX = xr.open_rasterio(ifile)
    df0 = dd.read_csv("global/GEDI02A_lidar_2019_*.csv")
    df1 = df0[(df0.lat > daX.y.values.min()) & (df0.lat < daX.y.values.max()) &
              (df0.lon > daX.x.values.min()) & (df0.lon < daX.x.values.max())].compute()
    print(df1.columns)

    id0 = np.argwhere(daX.data[0, :, :] > -1)
    X1 = daX.x.values[id0[:, 1]][:, None]
    Y1 = daX.y.values[id0[:, 0]][:, None]

    tree = spatial.cKDTree(df1[["lon", "lat"]].values)
    x_lst = []
    y_lst = []
    kspace = np.linspace(0, X1.shape[0], num=11).astype(int)
    for k in range(kspace.shape[0] - 1):
        print(k)
        X2 = X1[kspace[k]:kspace[k + 1], :]
        Y2 = Y1[kspace[k]:kspace[k + 1], :]
        _, minid = tree.query(np.concatenate([X2, Y2], axis=1), k=10, distance_upper_bound=0.00064)
        for i in range(minid.shape[0]):
            count = np.sum(minid[i, :] < df1.shape[0])
            if count > 0:
                idx = minid[i, minid[i, :] < df1.shape[0]]
                x0 = df1.iloc[idx, 2:].values.mean(axis=0)
                xy = np.concatenate([Y2[i:i + 1, 0], X2[i:i + 1, 0], np.array([count]), x0])
                x_lst.append(xy)
    xy = np.array(x_lst)
    xy_col = ['lat', 'lon', 'count'] + df1.columns[2:].to_list()
    df3 = pd.DataFrame(xy, columns=xy_col)
    df3.to_csv(ifile.replace('.tif', '_gedi_waveforms.csv'))



#%% GEDI Amazon age-filtering
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")

in0 = "../../gfc_gain_loss/gfc_loss_amazon_100m_south.tif"
in1 = "G:\Projects\Yan\\amazon_sf_age.tif"
out1 = "../../gfc_gain_loss/gfc_loss_amazon_100m_south_sfage.tif"
# rt.raster_clip(in0, in1, out1)


daX = xr.open_rasterio(out1)
df1 = pd.read_csv("../../gfc_gain_loss/gfc_loss_amazon_100m_south_gedi_waveforms.csv")
print(df1.columns)
df_dim0 = df1.shape[0]

id0 = np.argwhere(daX.data[0, :, :] > 0)
X1 = daX.x.values[id0[:, 1]][:, None]
Y1 = daX.y.values[id0[:, 0]][:, None]
z1 = daX[0, :, :].values[id0[:, 0], id0[:, 1]]

tree = spatial.cKDTree(df1[["lon", "lat"]].values)
x_lst = []
y_lst = []
kspace = np.linspace(0, X1.shape[0], num=11).astype(int)
for k in range(kspace.shape[0] - 1):
    print(k)
    X2 = X1[kspace[k]:kspace[k + 1], :]
    Y2 = Y1[kspace[k]:kspace[k + 1], :]
    z2 = z1[kspace[k]:kspace[k + 1], ]
    _, minid = tree.query(np.concatenate([X2, Y2], axis=1), k=5, distance_upper_bound=0.00064)
    for i in range(minid.shape[0]):
        count = np.sum(minid[i, :] < df_dim0)
        if count > 0:
            idx = minid[i, minid[i, :] < df_dim0]
            x0 = df1.iloc[idx, 4:].values.mean(axis=0)
            xy = np.concatenate([Y2[i:i + 1, 0], X2[i:i + 1, 0], z2[i:i + 1, ], np.array([count]), x0])
            x_lst.append(xy)
xy = np.array(x_lst)
xy_col = ['lat', 'lon', 'age', 'count'] + df1.columns[4:].to_list()
df3 = pd.DataFrame(xy, columns=xy_col)
df3.to_csv("../../gfc_gain_loss/gfc_loss_amazon_100m_south_gedi_waveforms_sfage.csv")


#%% GEDI Amazon age-waveform plots
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")
def plot_waveform(rh_arr, outfile, color='tab:blue'):
    x = np.arange(-10, 43, 2)
    xnew = np.arange(-10, x[-1] + 0.1, 0.1)
    y_lst = []
    for i in range(rh_arr.shape[0]):
        rh = rh_arr[i, :]
        zh_lst = []
        for xi in x:
            rhi = rh[(rh > xi - 1.5) & (rh <= xi + 1.5)].shape[0]
            zh_lst.append(rhi)
        zh = np.array(zh_lst)
        # zh_norm = zh / max(zh)
        zh_norm = zh
        s = interpolate.InterpolatedUnivariateSpline(x, zh_norm, ext=1)
        ynew = s(xnew)
        y_lst.append(ynew)
    y_arr = np.stack(y_lst, axis=0)
    y_mean = np.mean(y_arr, axis=0)
    y_std = np.std(y_arr, axis=0)
    plt.figure(figsize=(3, 6))
    h1 = plt.plot(y_mean, xnew, '-', linewidth=3, alpha=1, color=color)
    plt.fill_betweenx(xnew, y_mean - y_std, y_mean + y_std,
                      alpha=0.3, facecolor=color)
    plt.tight_layout()
    plt.savefig(outfile, dpi=300, bbox_inches='tight')
    return h1


def plot_waveform_all(rh_arr, outfile, color='tab:blue'):
    x = np.arange(-10, 43, 2)
    xnew = np.arange(-10, x[-1] + 0.1, 0.1)
    y_lst = []
    for i in range(rh_arr.shape[0]):
        rh = rh_arr[i, :]
        zh_lst = []
        for xi in x:
            rhi = rh[(rh > xi - 1.5) & (rh <= xi + 1.5)].shape[0]
            zh_lst.append(rhi)
        zh = np.array(zh_lst)
        zh_norm = zh / max(zh)
        # zh_norm = zh
        s = interpolate.InterpolatedUnivariateSpline(x, zh_norm, ext=1)
        ynew = s(xnew)
        y_lst.append(ynew)
    y_arr = np.stack(y_lst, axis=0)
    y_mean = np.mean(y_arr, axis=0)
    y_std = np.std(y_arr, axis=0)
    # plt.figure(figsize=(3, 6))
    h1 = plt.plot(y_mean, xnew, '-', linewidth=3, alpha=1, color=color)
    # plt.fill_betweenx(xnew, y_mean - y_std, y_mean + y_std,
    #                  alpha=0.3, facecolor=color)
    # plt.tight_layout()
    # plt.savefig(outfile, dpi=300, bbox_inches='tight')
    return h1


df3 = pd.read_csv("../../gfc_gain_loss/gfc_loss_amazon_100m_south_gedi_waveforms_sfage.csv")
h50_lst = []
h90_lst = []
h98_lst = []
hmax_lst = []
cmap = plt.get_cmap("tab10")
sns.set()
# plt.figure(figsize=(3, 6))
k = 0
h1_lst = []
for i in range(6):
# for i in [0, 1, 2]:
    age0 = i*5
    age1 = (i+1)*5
    df_a = df3[(df3.age > age0) & (df3.age < age1)]
    print(df_a.shape[0])
    h50_lst.append(df_a.rh50.values)
    h90_lst.append(df_a.rh90.values)
    h98_lst.append(df_a.rh98.values)
    hmax_lst.append(df_a.rh100.values)
    # rh_arr = df_a.iloc[:, 5:106].sample(55).values
    rh_arr = df_a.iloc[:, 5:106].values
    # h1 = plot_waveform_all(rh_arr, f"tests/amazon_south_gedi_waveforms_plot_age_{(i+1)*5}.png", color=cmap(k))
    h1 = plot_waveform(rh_arr, f"tests/amazon_south_gedi_waveforms_plot_age_{(i+1)*5}.png", color=cmap(k))
    k = k+1
    h1_lst.append(h1[0])
# plt.legend(h1_lst, ["Age 0-10", "Age 10-20", "Age 20-30"])
# plt.tight_layout()
# plt.savefig(f"tests/amazon_south_gedi_waveforms_plot_ageall", dpi=300, bbox_inches='tight')

age_lst = []
h5_lst = []
h9_lst = []
h8_lst = []
hmx_lst = []
for i in [0, 1, 3, 5]:
# for i in range(6):
    age_lst.append(np.full_like(h50_lst[i], (i+1)*5.0))
    h5_lst.append(h50_lst[i])
    h9_lst.append(h90_lst[i])
    h8_lst.append(h98_lst[i])
    hmx_lst.append(hmax_lst[i])
df_arr = np.stack(
    [
        np.concatenate(age_lst),
        np.concatenate(h5_lst),
        np.concatenate(h9_lst),
        np.concatenate(h8_lst),
        np.concatenate(hmx_lst),
    ],
    axis=1
)
df = pd.DataFrame(df_arr, columns=["Age (yr)", "RH50 (m)", "RH90 (m)", "RH98 (m)", "RH100 (m)"])

for k in range(4):
    fig = plt.figure(figsize=(4, 3))
    ax = sns.boxplot(x="Age (yr)", y=df.columns[k+1], data=df, showfliers=False)
    plt.tight_layout()
    plt.savefig(f"tests/amazon_south_gedi_boxplot_rh{k}", dpi=300, bbox_inches='tight')


#%% GEDI DRC airborne waveform generating
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")


files = [
    "../../../lcluc\\als\\UTM34N_Plot106_CHM_geo.tif",
]
for ifile in files:
    daX = xr.open_rasterio(ifile)
    df0 = dd.read_csv("global/GEDI02A_lidar_2019_*.csv")
    df1 = df0[(df0.lat > daX.y.values.min()) & (df0.lat < daX.y.values.max()) &
              (df0.lon > daX.x.values.min()) & (df0.lon < daX.x.values.max())].compute()
    print(df1.columns)

    df1.to_csv('drc/drc_Plot106_gedi_waveforms.csv')



#%% GEDI DRC airborne waveform plotting
os.chdir("F:\\Projects\\a1902alostexture\\input\\lidar_data\\gedi_lidar")

sns.set()

chm_file = "../../../lcluc\\als\\UTM34N_Plot106_CHM_geo.tif"
rgb_file = "../../../lcluc\\als\\UTM34N_Plot106_RGB_geo.tif"

files = [
    "drc/drc_Plot106_gedi_waveforms_secondary.csv",
    "drc/drc_Plot106_gedi_waveforms_primary.csv",
]
for ifile in files:
    df0 = pd.read_csv(ifile)
    daX = xr.open_rasterio(chm_file)
    id0 = np.argwhere(daX.data[0, :, :] > -1)
    X1 = daX.x.values[id0[:, 1]][:, None]
    Y1 = daX.y.values[id0[:, 0]][:, None]
    z1 = daX[0, :, :].values[id0[:, 0], id0[:, 1]]

    tree = spatial.cKDTree(np.concatenate([X1, Y1], axis=1))
    x_lst = []
    _, minid = tree.query(df0[["lon", "lat"]].values, k=300, distance_upper_bound=0.000135)
    for i in range(minid.shape[0]):
        count = np.sum(minid[i, :] < Y1.shape[0])
        print(count)
        if count > 0:
            idx = minid[i, minid[i, :] < Y1.shape[0]]
            x0 = z1[idx, ]
            x_lst.append(x0)
    z0 = np.concatenate(x_lst)
    # np.save(ifile.replace(".csv", "_chm.npy"), z0)

    plt.figure(figsize=(3, 6))
    x = np.arange(-10, 43, 2)
    xnew = np.arange(-10, x[-1] + 0.1, 0.1)
    ynew_lst = []
    ynew2_lst = []
    for i in range(df0.shape[0]):
        rh = df0.iloc[i, 3:104].values
        zh_lst = []
        z0h_lst = []
        for xi in x:
            rhi = rh[(rh > xi-1.5) & (rh <= xi+1.5)].shape[0]
            zi = z0[(z0 > xi-1.5) & (z0 <= xi+1.5)].shape[0]
            zh_lst.append(rhi)
            z0h_lst.append(zi)
        zh = np.array(zh_lst)
        z0h = np.array(z0h_lst)
        zh_norm = zh / max(zh)
        izh = np.argmax(z0h)
        z0h_norm = z0h / max(z0h) * zh_norm[izh]
        s = interpolate.InterpolatedUnivariateSpline(x, zh_norm, ext=1)
        ynew = s(xnew)
        ynew_lst.append(ynew)
        s = interpolate.InterpolatedUnivariateSpline(x, z0h_norm, ext=1)
        ynew2 = s(xnew)
        ynew2_lst.append(ynew2)
        plt.plot(ynew, xnew, '-', linewidth=3, alpha=0.2)
        # plt.plot(ynew2, xnew, '--', linewidth=3, alpha=0.2)
    ymean = np.stack(ynew_lst, axis=1).mean(axis=1)
    ymean2 = np.stack(ynew2_lst, axis=1).mean(axis=1)
    h1 = plt.plot(ymean, xnew, '-', linewidth=5, color='tab:blue')
    h2 = plt.plot(ymean2, xnew, '-', linewidth=5, color='tab:orange')
    plt.legend([h1[0], h2[0]], ['GEDI Waveform', 'ALS Profile'])
    plt.tight_layout()
    plt.savefig(ifile.replace(".csv", "_plot.png"), dpi=300, bbox_inches='tight')

    for i in range(df0.shape[0]):
        rh = df0.iloc[i, 3:104].values
        zh_lst = []
        z0h_lst = []
        for xi in x:
            rhi = rh[(rh > xi-1.5) & (rh <= xi+1.5)].shape[0]
            zi = z0[(z0 > xi-1.5) & (z0 <= xi+1.5)].shape[0]
            zh_lst.append(rhi)
            z0h_lst.append(zi)
        zh = np.array(zh_lst)
        z0h = np.array(z0h_lst)
        zh_norm = zh / max(zh)
        izh = np.argmax(z0h)
        z0h_norm = z0h / max(z0h)
        s = interpolate.InterpolatedUnivariateSpline(x, zh_norm, ext=1)
        ynew = s(xnew)
        s = interpolate.InterpolatedUnivariateSpline(x, z0h_norm, ext=1)
        ynew2 = s(xnew)
        plt.figure(figsize=(3, 6))
        plt.plot(ynew, xnew, '-', linewidth=3, alpha=0.7)
        plt.plot(ynew2, xnew, '--', linewidth=3, alpha=0.7)
        plt.tight_layout()
        plt.savefig(ifile.replace(".csv", f"_plot_{i}.png"), dpi=300, bbox_inches='tight')
