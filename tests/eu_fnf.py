#%% setup environment
from pathlib import Path
import subprocess
import xarray as xr
import zarr
import numpy as np
import pandas as pd
import dask.dataframe as dd
from zarr import blosc
import matplotlib.pyplot as plt
import a1902alos.data_load as adl
import a1902alos.data_clean as adc
import learn2map.raster_tools as rt
import seaborn as sns

compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)
sns.set_style("darkgrid")

p = Path("F:\Projects\FNF")

#%% combine geotiff to vrt
p = Path("F:\Projects\FNF")

file_list = list(p.glob("alos_fnf/*.tif"))
txt_file = p / "alos_list.txt"
with open(txt_file, "w") as t:
    t.write("\n".join(f"{i}" for i in file_list))
out_file = str(p / "alos_fnf_2007.vrt")
gdal_expression = f'gdalbuildvrt "{out_file}" -input_file_list {txt_file}'
print(gdal_expression)
subprocess.check_output(gdal_expression, shell=True)

ref_file = str(p / "pan_eu_fnf.tif")
out_file3 = str(p / "alos_fnf_2007.tif")
rt.raster_clip(ref_file, out_file, out_file3)


#%% calculation
p = Path("F:\Projects\FNF")
out_file3 = str(p / "alos_fnf_2007.tif")
out_file4 = str(p / "alos_fnf_2007_bool.tif")
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --overwrite --type=Int16 --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=2" -A "{out_file3}" --outfile="{out_file4}" --calc="(A>0)*(A<3)*A"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)


out_file3 = str(p / "pan_eu_fnf.tif")
out_file4 = str(p / "pan_eu_fnf_bool.tif")
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --overwrite --type=Int16 --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=2" -A "{out_file3}" --outfile="{out_file4}" --calc="(A>0)*(A<3)*A"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)


out_file3 = str(p / "alos_fnf_2007_bool.tif")
out_file4 = str(p / "pan_eu_fnf_bool.tif")
out_file5 = str(p / "alos_paneu_diff.tif")
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --overwrite --type=Int16 --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=2" -A "{out_file3}" -B "{out_file4}" --outfile="{out_file5}" --calc="(A>0)*(B>0)*A*3 - (B>0)*B"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)


#%% to zarr
out_file5 = str(p / "alos_paneu_diff.tif")
da = xr.open_rasterio(out_file5, chunks=(1, 5000, 5000))
da["band"] = ("band", ["difference"])
ds0 = xr.Dataset({"da": da})
ds0.to_zarr(
    str(p / "alos_paneu_diff.zarr"),
    mode="w",
    # synchronizer=zarr.ThreadSynchronizer(),
    encoding={"da": {"compressor": compressor}},
)

#%% save df
daX = xr.open_zarr(str(p / "alos_paneu_diff.zarr"))["da"]
da_data = daX.data.reshape(daX.shape[0], -1).transpose()
col_names = daX["band"].values.tolist()
df = da_data.to_dask_dataframe(columns=col_names)
df = df[df.iloc[:, 0] != 0]
df.to_parquet("alos_paneu_diff_valid.pqt", compression="snappy", engine="fastparquet")
df = dd.read_parquet("alos_paneu_diff_valid.pqt")
dc = df.difference.value_counts().compute()
dc.to_frame().to_csv(p / "dc.csv")
#% plot

# df.difference.histogram(bins=[-1, 0, 1, 2, 3])
# plt.savefig(p / f"output/difference_alos-paneu.png", dpi=150)
print(0)

#%% to 1-km

out_file = str(p / "pan_eu_fnf_bool.tif")
out_file2 = str(p / "pan_eu_fnf_bool_ft.tif")
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --overwrite --type=Float32 --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=3" -A "{out_file}" --outfile="{out_file2}" --calc="A==1"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)

out_file = str(p / "pan_eu_fnf_bool_ft.tif")
out_file2 = str(p / "pan_eu_fnf_bool_1km.tif")
gdal_expression = (
    f"gdal_translate -tr 1000 1000 -r average -ot Float32 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=3 "
    f'-co ZLEVEL=1 -co BIGTIFF=YES -co NUM_THREADS=6 "{out_file}" "{out_file2}"'
)
print(gdal_expression)
subprocess.check_output(gdal_expression, shell=True)


out_file = str(p / "alos_fnf_2007_bool.tif")
out_file2 = str(p / "alos_fnf_2007_bool_ft.tif")
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --overwrite --type=Float32 --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=3" -A "{out_file}" --outfile="{out_file2}" --calc="A==1"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)

ref_file = str(p / "pan_eu_fnf_bool_1km.tif")
out_file = str(p / "alos_fnf_2007_bool_ft.tif")
out_file3 = str(p / "alos_fnf_2007_bool_1km.tif")
rt.raster_clip(ref_file, out_file, out_file3, resampling_method="average")


out_file = str(p / "alos_fnf_2007_bool_1km.tif")
out_file2 = str(p / "pan_eu_fnf_bool_1km.tif")
out_file3 = str(p / "alos_fnf_2007_bool2_1km.tif")
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --overwrite --type=Float32 --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=3" -A "{out_file}" -B "{out_file2}" --outfile="{out_file3}" --calc="A*(B>0)"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)


#%% plot values
df = pd.read_csv(p / "dc.csv")
dha = df.difference[:4] / 16
dname = ["Alos_NF -> EU_NF", "Alos_F -> EU_F", "Alos_F -> EU_NF", "Alos_NF -> EU_F"]
x = np.arange(4)
plt.figure(figsize=(6, 4.5))
plt.bar(x, dha)
plt.xticks(x, dname, rotation=45)
plt.ylabel("Area (ha)")
plt.tight_layout()
plt.savefig(str(p / f"output/difference_alos-paneu.png"), dpi=300)


#%% to 100m
out_file = str(p / "alos_fnf_100m/alos_2007_fnf_europe_100m.byt")
out_file2 = str(p / "input/alos_2007_fnf_europe_100m.tif")
gdal_expression = (
    f"gdal_translate -ot Float32 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=3 "
    f'-co ZLEVEL=1 -co BIGTIFF=YES -co NUM_THREADS=6 "{out_file}" "{out_file2}"'
)
print(gdal_expression)
subprocess.check_output(gdal_expression, shell=True)

out_file = str(p / "alos_fnf_100m/alos_2015_fnf_07replace_europe_100m.byt")
out_file2 = str(p / "input/alos_2015_fnf_07replace_europe_100m.tif")
gdal_expression = (
    f"gdal_translate -ot Float32 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=3 "
    f'-co ZLEVEL=1 -co BIGTIFF=YES -co NUM_THREADS=6 "{out_file}" "{out_file2}"'
)
print(gdal_expression)
subprocess.check_output(gdal_expression, shell=True)

ref_file = str(p / "input/alos_2007_fnf_europe_100m.tif")
out_file = str(p / "input/pan_eu_fnf_bool_ft.tif")
out_file3 = str(p / "input/pan_eu_fnf_bool_100m.tif")
rt.raster_clip(ref_file, out_file, out_file3, resampling_method="average")


#%% 100m vcf
out_file = str(p / "input/alos_2007_fnf_europe_100m.tif")
out_file2 = str(p / "input/alos_2015_fnf_07replace_europe_100m.tif")
out_file3 = str(p / "input/pan_eu_fnf_bool_100m.tif")

da1 = xr.open_rasterio(out_file3, chunks=(1, 5000, 5000))
da2a = xr.open_rasterio(out_file, chunks=(1, 5000, 5000))
da2b = xr.open_rasterio(out_file2, chunks=(1, 5000, 5000))

da = xr.concat([da1, da2a, da2b], dim="band")
da["band"] = ("band", ["EU_FNF", "ALOS07_FNF", "ALOS15_FNF"])
df_file = str(p / "output/eu_alos_fnf_100m")
adl.get_df_from_da(da, da2a, df_file)


#%% plot vcf
df_file = str(p / "output/eu_alos_fnf_100m")
df = pd.read_parquet(f"{df_file}.pqt")

df0 = df.sample(40000)
plt.figure(figsize=(8,6))
sns.distplot(df0.EU_FNF * 100)
sns.distplot(df0.ALOS07_FNF)
sns.distplot(df0.ALOS15_FNF)
plt.legend(["EU_VCF", "ALOS07_VCF", "ALOS15_VCF"])
plt.tight_layout()
plt.savefig(str(p / f"output/eu_alos_vcf_hist_100m.png"), dpi=300)

adc.density_scatter_plot(
    df0.EU_FNF.values * 100,
    df0.ALOS07_FNF.values,
    x_label="EU FNF (%)",
    y_label="ALOS FNF (%)",
    x_limit=[0, 100],
    file_name=str(p / "output/eu_alos07_scatter_plot.png"),
)
adc.density_scatter_plot(
    df0.EU_FNF.values * 100,
    df0.ALOS15_FNF.values,
    x_label="EU FNF (%)",
    y_label="ALOS FNF (%)",
    x_limit=[0, 100],
    file_name=str(p / "output/eu_alos15_scatter_plot.png"),
)


#%% 1km vcf plot
out_file = str(p / "input/alos_fnf_2007_bool2_1km.tif")
out_file2 = str(p / "input/alos_fnf_2007_bool_1km.tif")
out_file3 = str(p / "input/pan_eu_fnf_bool_1km.tif")

da1 = xr.open_rasterio(out_file3, chunks=(1, 5000, 5000))
da2a = xr.open_rasterio(out_file, chunks=(1, 5000, 5000))
da2b = xr.open_rasterio(out_file2, chunks=(1, 5000, 5000))

da = xr.concat([da1, da2a, da2b], dim="band")
da["band"] = ("band", ["EU_FNF", "ALOS07_FNF", "ALOS07_FNF2"])
df_file = str(p / "output/eu_alos_fnf_1km")
adl.get_df_from_da(da, da2a, df_file)


#% plot vcf
df_file = str(p / "output/eu_alos_fnf_1km")
df = pd.read_parquet(f"{df_file}.pqt")

df0 = df.sample(40000)

plt.figure(figsize=(8,6))
sns.distplot(df.EU_FNF * 100)
sns.distplot(df.ALOS07_FNF * 100)
plt.legend(["EU_VCF", "ALOS07_VCF"])
plt.tight_layout()
plt.savefig(str(p / f"output/eu_alos_vcf_hist_1km.png"), dpi=300)

adc.density_scatter_plot(
    df0.EU_FNF.values * 100,
    df0.ALOS07_FNF.values * 100,
    x_label="EU FNF (%)",
    y_label="ALOS FNF (%)",
    x_limit=[0, 100],
    file_name=str(p / "output/eu_alos07_1km_scatter_plot.png"),
)
adc.density_scatter_plot(
    df0.EU_FNF.values * 100,
    df0.ALOS07_FNF2.values * 100,
    x_label="EU FNF (%)",
    y_label="ALOS FNF (%)",
    x_limit=[0, 100],
    file_name=str(p / "output/eu_alos072_1km_scatter_plot.png"),
)


#%% to 10-km and plot
out_file = str(p / "input/pan_eu_fnf_bool_ft.tif")
out_file2 = str(p / "input/pan_eu_fnf_bool_10km.tif")
gdal_expression = (
    f"gdal_translate -tr 10000 10000 -r average -ot Float32 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=3 "
    f'-co ZLEVEL=1 -co BIGTIFF=YES -co NUM_THREADS=6 "{out_file}" "{out_file2}"'
)
print(gdal_expression)
subprocess.check_output(gdal_expression, shell=True)


ref_file = str(p / "input/pan_eu_fnf_bool_10km.tif")
out_file = str(p / "input/alos_fnf_2007_bool_ft.tif")
out_file3 = str(p / "input/alos_fnf_2007_bool_10km.tif")
rt.raster_clip(ref_file, out_file, out_file3, resampling_method="average")


out_file = str(p / "input/alos_fnf_2007_bool_10km.tif")
out_file2 = str(p / "input/pan_eu_fnf_bool_10km.tif")
out_file3 = str(p / "input/alos_fnf_2007_bool2_10km.tif")
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --overwrite --type=Float32 --co="COMPRESS=LZW" '
    f'--co="PREDICTOR=3" -A "{out_file}" -B "{out_file2}" --outfile="{out_file3}" --calc="A*(B>0)"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)


#% 10km vcf plot
out_file = str(p / "input/alos_fnf_2007_bool2_10km.tif")
out_file2 = str(p / "input/alos_fnf_2007_bool_10km.tif")
out_file3 = str(p / "input/pan_eu_fnf_bool_10km.tif")

da1 = xr.open_rasterio(out_file3, chunks=(1, 5000, 5000))
da2a = xr.open_rasterio(out_file, chunks=(1, 5000, 5000))
da2b = xr.open_rasterio(out_file2, chunks=(1, 5000, 5000))

da = xr.concat([da1, da2a, da2b], dim="band")
da["band"] = ("band", ["EU_FNF", "ALOS07_FNF", "ALOS07_FNF2"])
df_file = str(p / "output/eu_alos_fnf_10km")
adl.get_df_from_da(da, da2a, df_file)


#% plot vcf
df_file = str(p / "output/eu_alos_fnf_10km")
df = pd.read_parquet(f"{df_file}.pqt")

# df0 = df.sample(40000)
plt.figure(figsize=(8,6))
sns.distplot(df.EU_FNF * 100)
sns.distplot(df.ALOS07_FNF2 * 100)
plt.legend(["EU_VCF", "ALOS07_VCF"])
plt.tight_layout()
plt.savefig(str(p / f"output/eu_alos_vcf_hist_10km.png"), dpi=300)

adc.density_scatter_plot(
    df.EU_FNF.values * 100,
    df.ALOS07_FNF.values * 100,
    x_label="EU FNF (%)",
    y_label="ALOS FNF (%)",
    x_limit=[0, 100],
    file_name=str(p / "output/eu_alos07_10km_scatter_plot.png"),
)
adc.density_scatter_plot(
    df.EU_FNF.values * 100,
    df.ALOS07_FNF2.values * 100,
    x_label="EU FNF (%)",
    y_label="ALOS FNF (%)",
    x_limit=[0, 100],
    file_name=str(p / "output/eu_alos072_10km_scatter_plot.png"),
)



#%% to 10-km using LC_VCF and plot
ref_file = str(p / "input/alos_fnf_2007_bool2_10km.tif")
out_file = str(p / "input/global_lc_vcf_10km_euraw.tif")
out_file3 = str(p / "input/global_lc_vcf_10km_eu.tif")
rt.raster_clip(ref_file, out_file, out_file3, resampling_method="average")

out_file = str(p / "input/alos_fnf_2007_bool2_10km.tif")
out_file3 = str(p / "input/global_lc_vcf_10km_eu.tif")

da1 = xr.open_rasterio(out_file3, chunks=(1, 5000, 5000))
da2a = xr.open_rasterio(out_file, chunks=(1, 5000, 5000))

da = xr.concat([da1, da2a], dim="band")
lst_vcf = [f"VCF_{i}" for i in range(2000, 2019)]
da["band"] = ("band", lst_vcf + ["ALOS07_FNF"])
df_file = str(p / "output/lcvcf_alos_fnf_10km")
adl.get_df_from_da(da, da[19:20,:,:], df_file)

#%% plot vcf
df_file = str(p / "output/lcvcf_alos_fnf_10km")
df = pd.read_parquet(f"{df_file}.pqt")

# df0 = df.sample(40000)
for i in range(19):
    print(i)
    plt.figure(figsize=(8,6))
    sns.distplot(df.iloc[:, i] * 100)
    sns.distplot(df.iloc[:, 19] * 100)
    plt.legend([f"LC_VCF_{2000+i}", "ALOS07_VCF"])
    plt.tight_layout()
    plt.savefig(str(p / f"output/lcvcf{2000+i}_alos_hist_10km.png"), dpi=300)

    adc.density_scatter_plot(
        df.iloc[:, i].values * 100,
        df.iloc[:, 19].values * 100,
        x_label="LC VCF (%)",
        y_label="ALOS FNF (%)",
        x_limit=[0, 100],
        file_name=str(p / f"output/lcvcf{2000+i}_alos07_10km_scatter_plot.png"),
    )


#%% ALOS_FNF to global-10km grid

ref_file = "E:\Codes\pcprojects\\a1901_agbts\\tests\data\global_agb\\test10a_agb_pred_corr.tif"
out_file = str(p / "")  # TBD
out_file3 = "E:\Codes\pcprojects\\a1901_agbts\\tests\data\global_agb\\global_alosfnf_2007_10km.tif"
rt.raster_clip(ref_file, out_file, out_file3, resampling_method="average")

