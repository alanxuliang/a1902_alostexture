#%% setup environment
from pathlib import Path
import subprocess
import xarray as xr
import zarr
from zarr import blosc
import a1902alos.data_load as adl
import learn2map.raster_tools as rt

compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)

#%% combine geotiff to vrt
p = Path("E:\data00\\a1902alostexture\data3")

# file_list = list(p.glob("alos2africa_25m/*.tif"))
# txt_file = p / "in_list.txt"
# with open(txt_file, "w") as t:
#     t.write("\n".join(f"{i}" for i in file_list))
out_file = str(p / "alos2africa_1517.vrt")
# gdal_expression = f'gdalbuildvrt "{out_file}" -input_file_list {txt_file}'
# print(gdal_expression)
# subprocess.check_output(gdal_expression, shell=True)

# file_list = list(p.glob("africa25m_srtm/*.tif"))
# txt_file = p / "in_list.txt"
# with open(txt_file, "w") as t:
#     t.write("\n".join(f"{i}" for i in file_list))
# out_file = p / "srtm_africa_1517.vrt"
# gdal_expression = f'gdalbuildvrt "{out_file}" -input_file_list {txt_file}'
# print(gdal_expression)
# subprocess.check_output(gdal_expression, shell=True)

out_file2 = str(p / "africa25m_layers" / "srtm_africa_1517.tif")
# gdal_expression = f'gdal_translate -ot Float32 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=3 ' \
#     f'-co ZLEVEL=1 -co BIGTIFF=YES -co NUM_THREADS=6 "{out_file}" "{out_file2}"'
# out_file2 = p / "africa25m_layers" / "srtm_africa_1517_int.tif"
# gdal_expression = f'gdal_translate -ot Int16 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=2 ' \
#     f'-co ZLEVEL=1 -co BIGTIFF=YES -co NUM_THREADS=12 "{out_file}" "{out_file2}"'
# print(gdal_expression)
# subprocess.check_output(gdal_expression, shell=True)

out_file3 = str(p / "africa25m_layers" / "alos2africa_1517.tif")
rt.raster_clip(out_file2, out_file, out_file3)

# if stack separate bands
# out_file = p / 'alos2africa_1517.vrt'
# rt.build_stack_vrt(str(txt_file), str(out_file))
print(0)


#%% convert geotiff to zarr
p = Path("E:\data00\\a1902alostexture\data3")
out_files = [
    str(p / "africa25m_layers" / "alos2africa_1517.tif"),
    str(p / "africa25m_layers" / "srtm_africa_1517.tif"),
]
out_file2 = str(p / "africa25m_1517.zarr")

band_names = ["HH", "HV", "angle", "srtm"]
vmaxs = [26000, 8100, 78, 2250]
vmins = [0, 0, 0, 0]
da_list = {}
encoding_list = {}
k = 0
for i in out_files:
    da = xr.open_rasterio(i, chunks=[1, 1000, 1000])
    for j in range(len(da.band)):
        var_name = band_names[k]
        da1 = da[j, :, :].drop("band")
        da_list[var_name] = (da1 - vmins[k]) / (vmaxs[k] - vmins[k])
        k = k+1
        encoding_list[var_name] = {"compressor": compressor}
ds2 = xr.Dataset(da_list).chunk(chunks={"y": 1000, "x": 1000})
print(ds2)
ds2.to_zarr(
    out_file2,
    mode="w",
    # synchronizer=zarr.ThreadSynchronizer(),
    encoding=encoding_list,
)

#%% convert vrt to zarr
p = Path("E:\data00\\a1902alostexture\data3")

out_file = str(p / "alos2africa_1517.vrt")
out_file2 = str(p / "alos2africa_1517.zarr")

# da = xr.open_rasterio(out_file, chunks=[3, 1000, 1000])
# ds0 = xr.Dataset({"da": da})
# ds0.to_zarr(
#     out_file2,
#     mode="w",
#     synchronizer=zarr.ThreadSynchronizer(),
#     encoding={"da": {"compressor": compressor}},
# )

in_zarr = str(p / "alos2africa_1517.zarr")
out_zarr = str(p / "alos2africa_ds.zarr")
adl.get_zarr_data_noref(
    in_zarr,
    out_zarr,
    band_names=["HH", "HV", "angle"],
    vmins=[0, 0, 0],
    vmaxs=[26000, 8100, 78],
    istarget=False,
)


#%% convert vrt to geotiff at multiple scales
p = Path("E:\data00\\a1902alostexture\data3")

file_list = list(p.glob("alos2africa_fnf/*.tif"))
txt_file = p / "in_list.txt"
with open(txt_file, "w") as t:
    t.write("\n".join(f"{i}" for i in file_list))
out_file = p / "mask/alos2africa_1517_fnf.vrt"
gdal_expression = f'gdalbuildvrt "{out_file}" -input_file_list {txt_file}'
print(gdal_expression)
subprocess.check_output(gdal_expression, shell=True)


mch_file = str(p / "lidar_data/lidar_plots_mch.tif")

cmmd = f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_edit.py -unsetnodata "{mch_file}"'
print(cmmd)
subprocess.check_output(cmmd, shell=True)
mch_count = f"{mch_file[:-4]}_count.tif"
cmmd = (
    f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_calc.py --type=Byte --co="COMPRESS=LZW" '
    f'-A "{mch_file}" --outfile="{mch_count}" --calc="(A>0)*(A<2000)"'
)
print(cmmd)
subprocess.check_output(cmmd, shell=True)
cmmd = f'python C:\\Users\\xuliang\\Anaconda3\\envs\\py36\\Scripts\\gdal_edit.py -a_nodata "nan" "{mch_file}"'
print(cmmd)
subprocess.check_output(cmmd, shell=True)


#%% get zarr
print(0)
p = Path("E:\data00\\a1902alostexture\data3")

# gdal_expression = (
#     f"gdal_translate -ot Byte -of GTiff -co COMPRESS=LZW -co PREDICTOR=2 -tr {ires} {ires} "
#     f'-r mode "{out_file}" "{out_file2}"'
# )
# gdal_expression = (
#     f'python C:/Users/xuliang/Anaconda3/envs/py36/Scripts/gdal_calc.py --type=Byte --co="COMPRESS=LZW" '
#     f'-A "{out_file2}" --outfile="{out_file3}" --calc="(A>0)*(A<3)"'
# )

mch_files = [str(p / "lidar_data/lidar_plots_mch.tif")]
for mch_file in mch_files:
    for ires in [0.001, 0.003, 0.005, 0.007, 0.01, 0.013, 0.016, 0.02]:  # different resolutions
        out_file3 = f"{mch_file[:-4]}_{ires}deg.tif"
        gdal_expression = (
            f"gdal_translate -ot Float32 -of GTiff -co COMPRESS=DEFLATE -co PREDICTOR=3 -tr {ires} {ires} "
            f'-r average "{mch_file}" "{out_file3}"'
        )
        print(gdal_expression)
        subprocess.check_output(gdal_expression, shell=True)

        mch_count = f"{mch_file[:-4]}_count.tif"
        out_file5 = f"{mch_file[:-4]}_{ires}deg_count.tif"
        rt.raster_clip(
            out_file3,
            mch_count,
            out_file5,
            resampling_method="average",
            srcnodata=None,
            dstnodata=None,
        )

        out_file4z = f"{mch_file[:-4]}_{ires}deg.zarr"
        da = xr.open_rasterio(out_file3, chunks=[1, 1000, 1000])
        da2 = xr.open_rasterio(out_file5, chunks=[1, 1000, 1000])
        da_all = xr.concat([da2, da], dim="band")
        da_all["band"] = ("band", ["count", "mch"])
        ds0 = xr.Dataset({"da": da_all}).chunk({"band": 1, "y": 1000, "x": 1000})
        ds0.to_zarr(
            out_file4z,
            mode="w",
            synchronizer=zarr.ThreadSynchronizer(),
            encoding={"da": {"compressor": compressor}},
        )


#%% get training samples
p = Path("E:\data00\\a1902alostexture\data3")
out_name = str(p / "output" / "africa25m")

for ires in [0.001, 0.003, 0.005, 0.007, 0.01, 0.013, 0.016, 0.02]:  # different resolutions
    da = xr.open_zarr(str(p / f"lidar_data/lidar_plots_mch_{ires}deg.zarr"))["da"]
    out_file1 = f"{out_name}_{ires}deg_training_samples"
    adl.get_df_from_da_with_xy(da, da[:1, :, :], out_file1, valid_min=0.6)


#%% get pred samples
p = Path("E:\data00\\a1902alostexture\data3")
out_name = str(p / "output" / "africa25m")

# out_file = str(p / "alos2africa_1517.zarr")
# daX = xr.open_zarr(out_file)["da"]
# daX["band"] = ("band", ["HH", "HV", "angle"])

# for ires in [0.004, 0.007, 0.01, 0.015]:  # different resolutions
for ires in [0.01]:  # different resolutions
    # da = xr.open_zarr(str(p / f"lidar_data/lidar_plots_mch_{ires}deg.zarr"))["da"]
    da = xr.open_rasterio(str(p / f"mask/mask_{ires}deg.tif"), chunks=(1, 500, 500))
    da["band"] = ("band", ["lc"])
    out_file1 = f"{out_name}_{ires}deg_pred_samples"
    adl.get_df_from_da_with_xy(da, da[:1, :, :], out_file1, valid_min=0)


#%% ipynb
from joblib import Parallel, delayed

from pathlib import Path

import dask.dataframe as dd
import pandas as pd
import torch.nn as nn
import xarray as xr
from fastai import *
from fastai.vision import *

import a1902alos.data_load as adl
import a1902alos.deep_mdl as adm
import a1902alos.grid_models as gridm

import torch

print(torch.cuda.device_count())
print(torch.cuda.get_device_name(0))
# torch.cuda.set_device(1)
torch.cuda.empty_cache()
torch.device("cuda")
p = Path("/media/xuliang/Data01/data00/a1902alostexture/data3")
for ires in [0.015]:  # different resolutions
    in_file = str(p / f"output/africa25m_{ires}deg_training_samples.pqt")
    df0 = pd.read_parquet(in_file)
print(df0)

# for ires in [0.015]:  # different resolutions
ires = 0.015
in_file = str(p / f"output/africa25m_{ires}deg_training_samples.pqt")
df0 = pd.read_parquet(in_file)

imgsize = int(np.ceil(ires / 0.0002246))
print(imgsize)
x_file = str(p / "alos2africa_ds.zarr")
data = adl.get_databunch_from_df(
    df0,
    x_file,
    label="mch",
    test_df=None,
    blocksize=(-ires, ires),
    dbsize=imgsize,
    bs=32,
)

data.show_batch(figsize=(10, 10))
bnd1 = 3


class L1andLossFlat(nn.L1Loss):
    def forward(self, input: Tensor, target: Tensor) -> Rank0Tensor:
        return super().forward(input.view(-1), target.view(-1))


model = nn.Sequential(
    adm.conv1(bnd1, 4),
    res_block(4),
    adm.conv2(4, 16),
    res_block(16),
    adm.conv2(16, 64),
    res_block(64),
    PoolFlatten(),
    nn.Linear(64, 1),
)

model2 = models.WideResNet(
    num_groups=6,
    N=3,
    num_classes=1,
    start_nf=bnd1 * 3,
    k=1,
    n_in_channels=bnd1,
    drop_p=0.2,
)

# learn = cnn_learner(data, models.resnet18, path="../output/")
# learn = Learner(data, model, path="../output/")
# learn = Learner(data, model, loss_func = MSELossFlat(), metrics=[mean_squared_error], path="../output/")
# learn = Learner(data, model2, loss_func = L1LossFlat(), metrics=[mean_squared_error], path="../output/")
learn = Learner(
    data,
    model2,
    loss_func=MSELossFlat(),
    metrics=[mean_squared_error],
    path=str(p / f"output/learn_{ires}deg"),
)

print(learn.loss_func)
# print(learn.summary())
learn.lr_find(end_lr=1000)
