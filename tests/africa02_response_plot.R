# library
library(tidyverse)

# Create dataset
setwd("F:\\ngs_data\\indices")
data <- read.csv(file = 'response_indices.csv')
head(data)

# Set a number of 'empty bar' to add at the end of each group
empty_bar <- 2
to_add <- data.frame( matrix(NA, empty_bar*nlevels(data$group), ncol(data)) )
colnames(to_add) <- colnames(data)
to_add$group <- rep(levels(data$group), each=empty_bar)
data <- rbind(data, to_add)
data <- data %>% arrange(group)
data$id <- seq(1, nrow(data))

# Get the name and the y position of each label
label_data <- data
number_of_bar <- nrow(label_data)
angle <- 90 - 360 * (label_data$id-0.5) /number_of_bar     # I substract 0.5 because the letter must have the angle of the center of the bars. Not extreme right(1) or extreme left (0)
label_data$hjust <- ifelse( angle < -90, 1, 0)
label_data$angle <- ifelse(angle < -90, angle+180, angle)

# prepare a data frame for base lines
base_data <- data %>% 
  group_by(group) %>% 
  summarize(start=min(id), end=max(id) - empty_bar) %>% 
  rowwise() %>% 
  mutate(title=mean(c(start, end)))

# prepare a data frame for grid (scales)
grid_data <- base_data
grid_data$end <- grid_data$end[ c( nrow(grid_data), 1:nrow(grid_data)-1)] + 1
grid_data$start <- grid_data$start - 1
grid_data <- grid_data[-1,]
grid_data <- grid_data[-1,]

# Make the plot
png("rplot.png", width = 5000, height = 5000)
p <- ggplot(data, aes(x=as.factor(id), y=value, fill=group)) +       # Note that id is a factor. If x is numeric, there is some space between the first bar
  
  geom_bar(aes(x=as.factor(id), y=value, fill=group), stat="identity", alpha=0.5) +
  
  # Add a val=100/75/50/25 lines. I do it at the beginning to make sur barplots are OVER it.
  # geom_segment(data=grid_data, aes(x = end, y = 0.8, xend = start, yend = 0.80), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = end, y = -0.1, xend = start, yend = -0.1), colour = "black", alpha=0.8, size=2, inherit.aes = FALSE ) +

  geom_segment(data=grid_data, aes(x = end-14, y = 0.2, xend = start+7, yend = 0.20), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  geom_segment(data=grid_data, aes(x = end-14, y = 0.4, xend = start+7, yend = 0.40), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  geom_segment(data=grid_data, aes(x = end-14, y = 0.6, xend = start+7, yend = 0.60), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = start+8, y = 0.2, xend = end-14, yend = 0.20), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = start+8, y = 0.4, xend = end-14, yend = 0.40), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = start+8, y = 0.6, xend = end-14, yend = 0.60), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = end-7, y = 0.2, xend = start+7, yend = 0.20), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = end-7, y = 0.4, xend = start+7, yend = 0.40), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = end-7, y = 0.6, xend = start+7, yend = 0.60), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  # geom_segment(data=grid_data, aes(x = end, y = 0.8, xend = start, yend = 0.80), colour = "grey", alpha=1, size=1 , inherit.aes = FALSE ) +
  
  # Add text showing the value of each 100/75/50/25 lines
  # annotate("text", x = rep(max(data$id),4), y = c(0.20, 0.40, 0.60, 0.80), label = c("0.2", "0.4", "0.6", "0.8") , color="grey", size=9 , angle=0, fontface="bold", hjust=1) +
  annotate("text", x = rep(max(data$id),3), y = c(0.20, 0.40, 0.60), label = c("0.2", "0.4", "0.6") , color="dimgrey", size=40 , angle=0, fontface="bold", hjust=1) +
  # annotate("text", x = rep(max(data$id),3), y = c(0.40, 0.60, 0.80), label = c("0.4", "0.6", "0.8") , color="grey", size=30, angle=0, fontface="bold", hjust=1) +
  
  geom_bar(aes(x=as.factor(id), y=value, fill=group), stat="identity", alpha=0.5) +

  scale_fill_manual(values=c("#ffa600", "#56aa3f", "#fe0000")) +
  ylim(-1,1.2) +
  theme_minimal() +
  theme(
    legend.position = "none",
    axis.text = element_blank(),
    axis.title = element_blank(),
    panel.grid = element_blank(),
    plot.margin = unit(rep(-1,4), "cm") 
  ) +
  coord_polar() + 
  geom_text(data=label_data, aes(x=id, y=value+0.06, label=individual, hjust=hjust), color="black", fontface="bold",alpha=0.6, size=42, angle= label_data$angle, inherit.aes = FALSE ) +
  
  # Add base line information
  geom_segment(data=base_data, aes(x = start, y = -0.1, xend = end, yend = -0.1), colour = "black", alpha=0.8, size=2,  inherit.aes = FALSE ) 
# geom_text(data=base_data, aes(x = title, y = -0.45, label=group), hjust=c(0,0.5,1), colour = "black", alpha=0.8, size=28, fontface="bold", inherit.aes = FALSE)

p
dev.off()
