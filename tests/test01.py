#%% import modules
import a1902alos.data_load as adl
import a1902alos.deep_mdl as adm
import os
import pandas as pd
import xarray as xr

os.chdir("data/input")


#%% setup zarr data
x_list = ["hhmean.tif", "hvmean.tif", "srtm_mean.tif", "srtm_std.tif"]
adl.get_zarr_data(
    "mask_300m.tif",
    ["lidar_training.tif"] + x_list,
    "train_300_v1.zarr",
    istarget=True,
)
adl.get_zarr_data(
    "mask.tif",
    x_list,
    "train_100_v1.zarr",
    vmins=[0, 0, 0, 0],
    vmaxs=[16000, 5000, 500, 11],
    istarget=False,
)
df = adl.get_train_df("train_300_v1.zarr", "train_300_df1")


#%% setup databunch
df0 = pd.read_csv("train_300_df1_0.csv")
data = adl.get_databunch_from_df(
    df0, "train_100_v1.zarr", test_df=None, blocksize=(-0.0045, 0.0045), dbsize=5
)
data.normalize()
data.show_batch(rows=3, figsize=(10,10))



