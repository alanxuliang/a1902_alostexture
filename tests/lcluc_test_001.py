# import modules (using ALOS/SRTM/L8 layers @ 50m)
import os
import subprocess
import time
import glob
import gdal
import h5py
import hypertools as hyp
import a1902alos.gee_app as ga
import a1902alos.grid_models as ggm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import xarray as xr
from scipy import spatial
import scipy.ndimage as ndimage
import dask
import dask.array as dskda
import dask.dataframe as dd
import seaborn as sns
import learn2map.raster_tools as rt
from scipy.interpolate import UnivariateSpline
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import (
    train_test_split,
)
from sklearn.impute import SimpleImputer
from dask.diagnostics import ProgressBar
import zarr
from zarr import blosc

AUTOTUNE = tf.data.experimental.AUTOTUNE
compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)

# from dask.distributed import Client, progress
# client = Client(threads_per_worker=4, n_workers=1)
# client
print(1)

# %% training data generation (@ 250m)
start = time.time()
os.chdir("D:\Docs\Xu_projects\\for_cc\\202006_lcluc_prop\\figures")

mch_csv_out = f"lcluc_a0"
files = glob.glob(f"a0.tif")
ga.raster_to_csv(files, mch_csv_out, band_list=["LC"], valid_min=0.0)

# %% training layers (CHM & RGB)
rgb = [
    "UTM35S_Plot3_RGB",
    "UTM34S_Plot19_RGB",
    "UTM33S_Plot176_RGB",
    "UTM33S_Plot26_RGB",
    "UTM34S_Plot2_RGB",
    "UTM34S_Plot158_RGB",
    "UTM35S_Plot14_RGB",
    "UTM34S_Plot47_RGB",
    "UTM35S_Plot155_RGB",
    "UTM35S_Plot79_RGB",
    "UTM35N_Plot148_RGB",
    "UTM35N_Plot150_RGB",
    "UTM35N_Plot180_RGB",
    "UTM35N_Plot129_RGB",
    "UTM34N_Plot106_RGB",
    "UTM35S_Plot66_RGB",
    "UTM34S_Plot177_RGB",
    "UTM34N_Plot92_RGB",
    "UTM34N_Plot171_RGB",
    "UTM35S_Plot156_RGB",
    "UTM34S_Plot181_RGB",
]

chm = [
    "UTM35S_Plot3_CHM",
    "UTM34S_Plot19_CHM",
    "UTM33S_Plot176_CHM",
    "UTM33S_Plot26_CHM",
    "UTM34S_Plot2_CHM",
    "UTM34S_Plot158_CHM",
    "UTM35S_Plot14_CHM",
    "UTM34S_Plot47_CHM",
    "UTM35S_Plot155_CHM",
    "UTM35S_Plot79_CHM",
    "UTM35N_Plot148_CHM",
    "UTM35N_Plot150_CHM",
    "UTM35N_Plot180_CHM",
    "UTM35N_Plot129_CHM",
    "UTM34N_Plot106_CHM",
    "UTM35S_Plot66_CHM",
    "UTM34S_Plot177_CHM",
    "UTM34N_Plot92_CHM",
    "UTM34N_Plot171_CHM",
    "UTM35S_Plot156_CHM",
    "UTM34S_Plot181_CHM",
]

for i in rgb:
    new_resolution = 0.000001
    gdal_expression = (
        f"gdalwarp -ot Float32 -of GTiff -overwrite -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-co BIGTIFF=YES -tr {new_resolution} {new_resolution} '
        f'-t_srs "EPSG:4326" '
        f'"G:\\Data\\MyBookDuo\\Archived_Data\\DRC_data\\001_lidar_rasters\\RGB_mosaic\\{i}.tif" '
        f'"F:\\Projects\\a1902alostexture\\lcluc\\{i}_geo.tif"'
    )
    subprocess.check_output(gdal_expression, shell=True)

for i in chm:
    new_resolution = 0.00002
    gdal_expression = (
        f"gdalwarp -ot Float32 -of GTiff -overwrite -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-co BIGTIFF=YES -tr {new_resolution} {new_resolution} '
        f'-t_srs "EPSG:4326" '
        f'"G:\\Data\\MyBookDuo\\Archived_Data\\DRC_data\\001_lidar_rasters\\Lidar_2meter\\CHM\\{i}.tif" '
        f'"F:\\Projects\\a1902alostexture\\lcluc\\{i}_geo.tif"'
    )
    subprocess.check_output(gdal_expression, shell=True)

# %% get training data in tfrecords
rgb = [
    "UTM35S_Plot3_RGB",
    "UTM34S_Plot19_RGB",
    "UTM33S_Plot176_RGB",
    "UTM33S_Plot26_RGB",
    "UTM34S_Plot2_RGB",
    "UTM34S_Plot158_RGB",
    "UTM35S_Plot14_RGB",
    "UTM34S_Plot47_RGB",
    "UTM35S_Plot155_RGB",
    "UTM35S_Plot79_RGB",
    "UTM35N_Plot148_RGB",
    "UTM35N_Plot150_RGB",
    "UTM35N_Plot180_RGB",
    "UTM35N_Plot129_RGB",
    "UTM34N_Plot106_RGB",
    "UTM35S_Plot66_RGB",
    "UTM34S_Plot177_RGB",
    "UTM34N_Plot92_RGB",
    "UTM34N_Plot171_RGB",
    "UTM35S_Plot156_RGB",
    "UTM34S_Plot181_RGB",
]

chm = [
    "UTM35S_Plot3_CHM",
    "UTM34S_Plot19_CHM",
    "UTM33S_Plot176_CHM",
    "UTM33S_Plot26_CHM",
    "UTM34S_Plot2_CHM",
    "UTM34S_Plot158_CHM",
    "UTM35S_Plot14_CHM",
    "UTM34S_Plot47_CHM",
    "UTM35S_Plot155_CHM",
    "UTM35S_Plot79_CHM",
    "UTM35N_Plot148_CHM",
    "UTM35N_Plot150_CHM",
    "UTM35N_Plot180_CHM",
    "UTM35N_Plot129_CHM",
    "UTM34N_Plot106_CHM",
    "UTM35S_Plot66_CHM",
    "UTM34S_Plot177_CHM",
    "UTM34N_Plot92_CHM",
    "UTM34N_Plot171_CHM",
    "UTM35S_Plot156_CHM",
    "UTM34S_Plot181_CHM",
]

os.chdir("F:\\Projects\\a1902alostexture\\lcluc")
df = pd.read_csv("lcluc_a0_0.csv")

for i in rgb:
    new_resolution = 0.000001
    offsets = 0.0000003 + new_resolution * np.array([42.])
    width = np.array([384])
    ga.regression_tfrecord_from_df(
        df,
        f"als/{i}_geo.tif",
        f"tfrecords/VHR_{i}.tfrecord",
        x_cols="xvar",
        response="LC",
        loc=("latitude", "longitude"),
        offset=(-offsets[0], offsets[0]),
        blocksize=(width[0], width[0]),
    )

for i in chm:
    new_resolution = 0.00002
    offsets = 0.000008 + new_resolution * np.array([8.5])
    width = np.array([32])
    ga.regression_tfrecord_from_df(
        df,
        f"als/{i}_geo.tif",
        f"tfrecords/Lidar_{i}.tfrecord",
        x_cols="xvar",
        response="LC",
        loc=("latitude", "longitude"),
        offset=(-offsets[0], offsets[0]),
        blocksize=(width[0], width[0]),
    )


# %% plot histograms of layers
os.chdir("F:\\Projects\\a1902alostexture\\lcluc")
batch_size = 600

new_resolution = 0.000001
offsets = 0.0000003 + new_resolution * np.array([42.])
width = np.array([384])
ds, train_n = ga.load_tf(
    f"tfrecords/VHR_*.tfrecord",
    x_features="xvar",
    y_feature="LC",
    x_width=(width[0], width[0], 3),
    y_width=(1,),
    compression="",
)
ds1 = ds.shuffle(50000).batch(batch_size)
arr = iter(ds1.take(1)).next()

X = arr[0].numpy()
y = arr[1].numpy().ravel()
X1 = X.reshape(batch_size, -1, 3)
lst = []
bnds = ["R", "G", "B"]
for i in range(batch_size):
    for k in range(3):
        value = X1[i, :, k]
        band = [bnds[k]] * X1.shape[1]
        lc = [y[i]] * X1.shape[1]
        id = [i] * X1.shape[1]
        df0 = pd.DataFrame({"ID": id, "Value": value, "Band": band, "LC": lc})
        lst.append(df0)
df1 = pd.concat(lst, ignore_index=True)
df2 = df1[df1["LC"] < 6.5]

flatui = ["red", "green", "blue"]
clst = sns.color_palette(flatui)
fig = plt.figure(figsize=[7, 4])
ax = sns.boxplot(x="LC", y="Value", hue="Band",
                 data=df2, palette=flatui, showfliers=False)
plt.xticks(np.arange(6), ['Crops/Grass', 'Forest Regrowth', 'Tree Plantation',
                          'Wildfire', 'Urbanization', 'Primary Forest'],
           rotation=45)
plt.xlabel("Land Cover")
plt.tight_layout()
plt.savefig("vhr_rgb_boxplot_lc", dpi=600, bbox_inches='tight')

new_resolution = 0.00002
offsets = 0.000008 + new_resolution * np.array([8.5])
width = np.array([32])
ds, train_n = ga.load_tf(
    f"tfrecords/Lidar_*.tfrecord",
    x_features="xvar",
    y_feature="LC",
    x_width=(width[0], width[0], 1),
    y_width=(1,),
    compression="",
)
ds1 = ds.shuffle(50000).batch(batch_size)
arr = iter(ds1.take(1)).next()

X = arr[0].numpy()
y = arr[1].numpy().ravel()
X1 = X.reshape(batch_size, -1, 1)
lst = []
bnds = ["CHM"]
for i in range(batch_size):
    for k in range(1):
        value = X1[i, :, k]
        band = [bnds[k]] * X1.shape[1]
        lc = [y[i]] * X1.shape[1]
        id = [i] * X1.shape[1]
        df0 = pd.DataFrame({"ID": id, "Value": value, "Band": band, "LC": lc})
        lst.append(df0)
df1 = pd.concat(lst, ignore_index=True)
df2 = df1[df1["Value"] < 50]
df2 = df2.dropna()

fig = plt.figure(figsize=[4, 3])
sns.distplot(df2.loc[df2.LC == 1, "Value"], label="Crops/Grass", norm_hist=True)
sns.distplot(df2.loc[df2.LC == 4, "Value"], label="Wildfire", norm_hist=True)
sns.distplot(df2.loc[df2.LC == 5, "Value"], label="Urbanization", norm_hist=True)
plt.ylim(0, 2)
plt.xlim(-5, 15)
plt.legend()
plt.ylabel("Frequency")
plt.xlabel("Lidar Tree Height (m)")
plt.tight_layout()
plt.savefig("lidar_chm_histplot_lc1", dpi=600, bbox_inches='tight')

fig = plt.figure(figsize=[4, 3])
sns.distplot(df2.loc[df2.LC == 2, "Value"], label="Forest Regrowth", norm_hist=True)
sns.distplot(df2.loc[df2.LC == 3, "Value"], label="Tree Plantation", norm_hist=True)
sns.distplot(df2.loc[df2.LC == 6, "Value"], label="Primary Forest", norm_hist=True)
plt.xlim(-5, 50)
plt.legend()
plt.ylabel("Frequency")
plt.xlabel("Lidar Tree Height (m)")
plt.tight_layout()
plt.savefig("lidar_chm_histplot_lc2", dpi=600, bbox_inches='tight')

# %% texture layers of autoencoder
os.chdir("F:\\Projects\\a1902alostexture\\lcluc")
batch_size = 100
nbands = 3
new_resolution = 0.000001
offsets = 0.0000003 + new_resolution * np.array([42.])
width = np.array([384])
ds, train_n = ga.load_tf(
    f"tfrecords/VHR_*.tfrecord",
    x_features="xvar",
    y_feature="LC",
    x_width=(width[0], width[0], nbands),
    y_width=(1,),
    compression="",
)
# ds1 = ds.shuffle(5000).batch(batch_size)
ds1 = ds.batch(batch_size)

nbands2 = 1
new_resolution2 = 0.00002
offsets2 = 0.000008 + new_resolution2 * np.array([8.5])
width2 = np.array([32])
ds, train_n = ga.load_tf(
    f"tfrecords/Lidar_*.tfrecord",
    x_features="xvar",
    y_feature="LC",
    x_width=(width2[0], width2[0], nbands2),
    y_width=(1,),
    compression="",
)
# ds1 = ds.shuffle(5000).batch(batch_size)
ds2 = ds.batch(batch_size)

# autoenc1 = tf.keras.Sequential(
#         [
#           tf.keras.layers.InputLayer(input_shape=(width[0],width[0],3)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.Flatten(),
#           # tf.keras.layers.GlobalAveragePooling2D(),
#           # No activation
#           tf.keras.layers.Dense(72),
#           tf.keras.layers.Dense(units=6*6*60, activation=tf.nn.relu),
#           tf.keras.layers.Reshape(target_shape=(6, 6, 60)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=48,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=36,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=24,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=12,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=6,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           # No activation
#           tf.keras.layers.Conv2DTranspose(
#               filters=3,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME"),
#         ]
#     )
# autoenc1.compile(
#     optimizer=tf.keras.optimizers.Adam(0.001),
#     loss=tf.keras.losses.get("MeanSquaredError"),
#     # loss=tf.keras.losses.get("MeanAbsoluteError"),
#     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
# )
# autoenc2 = tf.keras.Sequential(
#         [
#           tf.keras.layers.InputLayer(input_shape=(width[0],width[0],3)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.Flatten(),
#           # tf.keras.layers.GlobalAveragePooling2D(),
#           # No activation
#           tf.keras.layers.Dense(72),
#           tf.keras.layers.Dense(units=6*6*60, activation=tf.nn.relu),
#           tf.keras.layers.Reshape(target_shape=(6, 6, 60)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=48,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=36,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=24,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=12,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=6,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           # No activation
#           tf.keras.layers.Conv2DTranspose(
#               filters=3,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME"),
#         ]
#     )
# autoenc2.compile(
#     optimizer=tf.keras.optimizers.Adam(0.001),
#     loss=tf.keras.losses.get("MeanSquaredError"),
#     # loss=tf.keras.losses.get("MeanAbsoluteError"),
#     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
# )
# autoenc3 = tf.keras.Sequential(
#         [
#           tf.keras.layers.InputLayer(input_shape=(width[0],width[0],3)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.Flatten(),
#           # tf.keras.layers.GlobalAveragePooling2D(),
#           # No activation
#           tf.keras.layers.Dense(72),
#           tf.keras.layers.Dense(units=6*6*60, activation=tf.nn.relu),
#           tf.keras.layers.Reshape(target_shape=(6, 6, 60)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=48,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=36,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=24,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=12,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=6,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           # No activation
#           tf.keras.layers.Conv2DTranspose(
#               filters=3,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME"),
#         ]
#     )
# autoenc3.compile(
#     optimizer=tf.keras.optimizers.Adam(0.001),
#     loss=tf.keras.losses.get("MeanSquaredError"),
#     # loss=tf.keras.losses.get("MeanAbsoluteError"),
#     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
# )
# autoenc4 = tf.keras.Sequential(
#         [
#           tf.keras.layers.InputLayer(input_shape=(width[0],width[0],3)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.Flatten(),
#           # tf.keras.layers.GlobalAveragePooling2D(),
#           # No activation
#           tf.keras.layers.Dense(72),
#           tf.keras.layers.Dense(units=6*6*60, activation=tf.nn.relu),
#           tf.keras.layers.Reshape(target_shape=(6, 6, 60)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=48,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=36,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=24,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=12,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=6,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           # No activation
#           tf.keras.layers.Conv2DTranspose(
#               filters=3,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME"),
#         ]
#     )
# autoenc4.compile(
#     optimizer=tf.keras.optimizers.Adam(0.001),
#     loss=tf.keras.losses.get("MeanSquaredError"),
#     # loss=tf.keras.losses.get("MeanAbsoluteError"),
#     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
# )
# autoenc5 = tf.keras.Sequential(
#         [
#           tf.keras.layers.InputLayer(input_shape=(width[0],width[0],3)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.Flatten(),
#           # tf.keras.layers.GlobalAveragePooling2D(),
#           # No activation
#           tf.keras.layers.Dense(72),
#           tf.keras.layers.Dense(units=6*6*60, activation=tf.nn.relu),
#           tf.keras.layers.Reshape(target_shape=(6, 6, 60)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=48,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=36,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=24,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=12,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=6,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           # No activation
#           tf.keras.layers.Conv2DTranspose(
#               filters=3,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME"),
#         ]
#     )
# autoenc5.compile(
#     optimizer=tf.keras.optimizers.Adam(0.001),
#     loss=tf.keras.losses.get("MeanSquaredError"),
#     # loss=tf.keras.losses.get("MeanAbsoluteError"),
#     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
# )
# autoenc6 = tf.keras.Sequential(
#         [
#           tf.keras.layers.InputLayer(input_shape=(width[0],width[0],3)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2D(
#               filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
#           tf.keras.layers.Flatten(),
#           # tf.keras.layers.GlobalAveragePooling2D(),
#           # No activation
#           tf.keras.layers.Dense(72),
#           tf.keras.layers.Dense(units=6*6*60, activation=tf.nn.relu),
#           tf.keras.layers.Reshape(target_shape=(6, 6, 60)),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=48,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=36,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=24,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=12,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           tf.keras.layers.BatchNormalization(axis=-1),
#           tf.keras.layers.Conv2DTranspose(
#               filters=6,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME",
#               activation='relu'),
#           # No activation
#           tf.keras.layers.Conv2DTranspose(
#               filters=3,
#               kernel_size=3,
#               strides=(2, 2),
#               padding="SAME"),
#         ]
#     )
# autoenc6.compile(
#     optimizer=tf.keras.optimizers.Adam(0.001),
#     loss=tf.keras.losses.get("MeanSquaredError"),
#     # loss=tf.keras.losses.get("MeanAbsoluteError"),
#     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
# )
autoenc6 = tf.keras.Sequential(
    [
        tf.keras.layers.InputLayer(input_shape=(width[0], width[0], 3)),
        tf.keras.layers.Conv2D(
            filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=72, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Flatten(),
        # tf.keras.layers.GlobalAveragePooling2D(),
        # No activation
        tf.keras.layers.Dense(84),
        tf.keras.layers.Dense(units=3 * 3 * 72, activation=tf.nn.relu),
        tf.keras.layers.Reshape(target_shape=(3, 3, 72)),
        tf.keras.layers.Conv2DTranspose(
            filters=60,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=48,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=36,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=24,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=12,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=6,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        # No activation
        tf.keras.layers.Conv2DTranspose(
            filters=3,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME"),
    ]
)
autoenc6.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.get("MeanSquaredError"),
    # loss=tf.keras.losses.get("MeanAbsoluteError"),
    metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
)

xlst = []
ylst = []
for i in range(6):
    n0 = 0
    n = 40
    for element in ds1.as_numpy_iterator():
        a = np.where(element[1] == i + 1)[0].shape[0]
        if (n0 < n) and (a > 0):
            id1 = np.where(element[1] == i + 1)[0]
            n1 = min(id1.shape[0], n - n0)
            xlst.append(element[0][id1[:n1], :, :, :])
            ylst.append(element[1][id1[:n1], :])
            n0 = n0 + n1
xdata = np.concatenate(xlst)
ydata = np.concatenate(ylst)

xlst2 = []
ylst2 = []
for i in range(6):
    n0 = 0
    n = 40
    for element in ds2.as_numpy_iterator():
        a = np.where(element[1] == i + 1)[0].shape[0]
        if (n0 < n) and (a > 0):
            id1 = np.where(element[1] == i + 1)[0]
            n1 = min(id1.shape[0], n - n0)
            xlst2.append(element[0][id1[:n1], :, :, :])
            ylst2.append(element[1][id1[:n1], :])
            n0 = n0 + n1
xdata2 = np.concatenate(xlst2)
ydata2 = np.concatenate(ylst2)

autoenc6.fit(x=xdata, y=xdata, epochs=3000)
# X_test1 = autoenc6.predict(xdata, verbose=1)

# plt.imshow(xdata[0,:,:,:]/255), plt.savefig("lc1_raw.png", dpi=600, bbox_inches='tight')
# plt.imshow(X_test1[0,:,:,:]/255), plt.savefig("lc1_auto.png", dpi=600, bbox_inches='tight')
# plt.imshow(xdata[40,:,:,:]/255), plt.savefig("lc2_raw.png", dpi=600, bbox_inches='tight')
# plt.imshow(X_test1[40,:,:,:]/255), plt.savefig("lc2_auto.png", dpi=600, bbox_inches='tight')
# plt.imshow(xdata[83,:,:,:]/255), plt.savefig("lc3_raw.png", dpi=600, bbox_inches='tight')
# plt.imshow(X_test1[83,:,:,:]/255), plt.savefig("lc3_auto.png", dpi=600, bbox_inches='tight')
# plt.imshow(xdata[127,:,:,:]/255), plt.savefig("lc4_raw.png", dpi=600, bbox_inches='tight')
# plt.imshow(X_test1[127,:,:,:]/255), plt.savefig("lc4_auto.png", dpi=600, bbox_inches='tight')
# plt.imshow(xdata[160,:,:,:]/255), plt.savefig("lc5_raw.png", dpi=600, bbox_inches='tight')
# plt.imshow(X_test1[160,:,:,:]/255), plt.savefig("lc5_auto.png", dpi=600, bbox_inches='tight')
# plt.imshow(xdata[200,:,:,:]/255), plt.savefig("lc6_raw.png", dpi=600, bbox_inches='tight')
# plt.imshow(X_test1[200,:,:,:]/255), plt.savefig("lc6_auto.png", dpi=600, bbox_inches='tight')

np.save("lc_xdata.npy", xdata)
np.save("lc_ydata.npy", ydata)
np.save("lc_xdata2.npy", xdata2)
np.save("lc_ydata2.npy", ydata2)

mlayer_model = tf.keras.Model(
    inputs=autoenc6.input,
    outputs=autoenc6.get_layer("dense").output
)
X_mlayer = mlayer_model.predict(xdata, verbose=1)
np.save("lc_mlayer.npy", X_mlayer)

xdata = np.load("lc_xdata.npy")
ydata = np.load("lc_ydata.npy")
xdata2 = np.load("lc_xdata2.npy")
ydata2 = np.load("lc_ydata2.npy")
X_mlayer = np.load("lc_mlayer.npy")
geo = hyp.plot(X_mlayer, '.',
               group=ydata.ravel(),
               legend=['Crops/Grass', 'Forest Regrowth', 'Tree Plantation',
                       'Wildfire', 'Urbanization', 'Primary Forest'],
               reduce='FastICA')

# for element in train_dataset.as_numpy_iterator():
#     X1 = element[0][element[1][:, 0]==1,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc1.fit(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==2,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc2.fit(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==3,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc3.fit(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==4,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc4.fit(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==5,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc5.fit(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==6,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc6.fit(x=X1, y=X1, epochs=3)
#
#
# for element in test_dataset.as_numpy_iterator():
#     X1 = element[0][element[1][:, 0]==1,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc1.evaluate(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==2,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc2.evaluate(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==3,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc3.evaluate(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==4,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc4.evaluate(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==5,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc5.evaluate(x=X1, y=X1, epochs=3)
#     X1 = element[0][element[1][:, 0]==6,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc6.evaluate(x=X1, y=X1, epochs=3)


# for element in train_dataset.take(1).as_numpy_iterator():
#     X1 = element[0][element[1][:, 0]==1,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc1.fit(x=X1, y=X1, epochs=30)
#         X_test1 = autoenc1.predict(X1, verbose=1)
#     X1 = element[0][element[1][:, 0]==2,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc2.fit(x=X1, y=X1, epochs=30)
#         X_test2 = autoenc2.predict(X1, verbose=1)
#     X1 = element[0][element[1][:, 0]==3,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc3.fit(x=X1, y=X1, epochs=30)
#         X_test3 = autoenc3.predict(X1, verbose=1)
#     X1 = element[0][element[1][:, 0]==4,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc4.fit(x=X1, y=X1, epochs=30)
#         X_test4 = autoenc4.predict(X1, verbose=1)
#     X1 = element[0][element[1][:, 0]==5,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc5.fit(x=X1, y=X1, epochs=30)
#         X_test5 = autoenc5.predict(X1, verbose=1)
#     X1 = element[0][element[1][:, 0]==6,:,:,:]
#     if X1.shape[0] > 0:
#         autoenc6.fit(x=X1, y=X1, epochs=30)
#         X_test6 = autoenc6.predict(X1, verbose=1)

print(1)

# %% texture layers for CNN learning
os.chdir("F:\\Projects\\a1902alostexture\\lcluc")


def map_x(X, y):
    x_mean = np.load("x_mean.npy")
    x_std = np.load("x_std.npy")
    sample_mean = tf.constant(x_mean.reshape([1, 1, x_mean.shape[0]]))
    sample_std = tf.constant(x_std.reshape([1, 1, x_std.shape[0]]))
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    return X, y


def map_x2(X, y):
    x_mean = np.load("x2_mean.npy")
    x_std = np.load("x2_std.npy")
    sample_mean = tf.constant(x_mean.reshape([1, 1, x_mean.shape[0]]))
    sample_std = tf.constant(x_std.reshape([1, 1, x_std.shape[0]]))
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    return X, y


batch_size = 100
nbands = 3
new_resolution = 0.000001
offsets = 0.0000003 + new_resolution * np.array([42.])
width = np.array([384])
ds, train_n = ga.load_tf(
    f"tfrecords/VHR_UTM*.tfrecord",
    x_features="xvar",
    y_feature="LC",
    x_width=(width[0], width[0], nbands),
    y_width=(1,),
    compression="",
)
# ds1 = ds.shuffle(5000).batch(batch_size)
ds1 = ds.map(map_x).batch(batch_size)

nbands2 = 1
new_resolution2 = 0.00002
offsets2 = 0.000008 + new_resolution2 * np.array([8.5])
width2 = np.array([32])
ds, train_n = ga.load_tf(
    f"tfrecords/Lidar_UTM*.tfrecord",
    x_features="xvar",
    y_feature="LC",
    x_width=(width2[0], width2[0], nbands2),
    y_width=(1,),
    compression="",
)
# ds1 = ds.shuffle(5000).batch(batch_size)
ds2 = ds.map(map_x2).batch(batch_size)

xlst = []
ylst = []
for i in range(6):
    n0 = 0
    n = 120
    for element in ds1.as_numpy_iterator():
        a = np.where(element[1] == i + 1)[0].shape[0]
        if (n0 < n) and (a > 0):
            id1 = np.where(element[1] == i + 1)[0]
            n1 = min(id1.shape[0], n - n0)
            xlst.append(element[0][id1[:n1], :, :, :])
            ylst.append(element[1][id1[:n1], :])
            n0 = n0 + n1
xdata = np.concatenate(xlst)
ydata = np.concatenate(ylst)

xlst2 = []
ylst2 = []
for i in range(6):
    n0 = 0
    n = 120
    for element in ds2.as_numpy_iterator():
        a = np.where(element[1] == i + 1)[0].shape[0]
        if (n0 < n) and (a > 0):
            id1 = np.where(element[1] == i + 1)[0]
            n1 = min(id1.shape[0], n - n0)
            xlst2.append(element[0][id1[:n1], :, :, :])
            ylst2.append(element[1][id1[:n1], :])
            n0 = n0 + n1
xdata2 = np.concatenate(xlst2)
ydata2 = np.concatenate(ylst2)

# x_mean = np.nanmean(xdata.reshape(-1, 3), axis=0)
# x_std = np.nanstd(xdata.reshape(-1, 3), axis=0)
# x2_mean = np.nanmean(xdata2.reshape(-1, 1), axis=0)
# x2_std = np.nanstd(xdata2.reshape(-1, 1), axis=0)
# np.save("x_mean.npy", x_mean)
# np.save("x_std.npy", x_std)
# np.save("x2_mean.npy", x2_mean)
# np.save("x2_std.npy", x2_std)
#
# xdata2[np.isnan(xdata2)] = 0
# xdata2[np.isinf(xdata2)] = 0
# xdata[np.isnan(xdata)] = 0
# x_reshape = (xdata.reshape(-1, 3) - np.mean(xdata.reshape(-1, 3), axis=0)) / np.std(xdata.reshape(-1, 3), axis=0)
# xdata = x_reshape.reshape(720, 384, 384, 3)
# x_reshape = (xdata2.reshape(-1, 1) - np.mean(xdata2.reshape(-1, 1), axis=0)) / np.std(xdata2.reshape(-1, 1), axis=0)
# xdata2 = x_reshape.reshape(720, 32, 32, 1)
ydata = ydata - 1


dn_structure = [
    (6, 12, 18, 24, 18, 12, 6),
    (6, 12, 18, 12, 6),
]
img_input = tf.keras.Input(batch_shape=(None, width[0], width[0], nbands), name='img_input')
img_densenet = ga.DenseNet(
    num_layers_in_each_block=dn_structure[0],
    growth_rate=8,
    dropout_rate=0.2,
    weight_decay=1e-4,
    compression=0.5,
    pool_initial=True,
    bottleneck=True,
    include_top=False,
)(img_input)
chm_input = tf.keras.Input(batch_shape=(None, width2[0], width2[0], nbands2), name='chm_input')
chm_densenet = ga.DenseNet(
    num_layers_in_each_block=dn_structure[1],
    growth_rate=8,
    dropout_rate=0.2,
    weight_decay=1e-4,
    compression=0.5,
    bottleneck=True,
    include_top=False,
)(chm_input)
img_pool = tf.keras.layers.GlobalAveragePooling2D(name="img_pool")(img_densenet)
chm_pool = tf.keras.layers.GlobalAveragePooling2D(name="chm_pool")(chm_densenet)
global_pool = tf.concat([img_pool, chm_pool], axis=-1, name="global_pool")
dense_layer = tf.keras.layers.Dense(1024, activation="relu")(global_pool)
dense_2layers = tf.keras.layers.Dense(2)(dense_layer)
# outputs = tf.keras.layers.Dense(6, activation='softmax', name='lc_output')(dense_layer)
outputs = tf.keras.layers.Dense(6, name='lc_output')(dense_2layers)
mdl = tf.keras.Model(inputs=[img_input, chm_input], outputs=outputs)


ds0 = tf.data.Dataset.from_tensor_slices(
    ({'img_input': xdata, 'chm_input': xdata2},
     {'lc_output': ydata.astype(int).ravel()}))
ds0 = ds0.shuffle(buffer_size=1024).batch(40)
training_data = ds0.take(12)
test_data = ds0.skip(12)

mdl.compile(
    optimizer=tf.keras.optimizers.Adam(),
    # optimizer=tf.keras.optimizers.RMSprop(),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    # loss='sparse_categorical_crossentropy',
    metrics=['sparse_categorical_accuracy'])

# history = mdl.fit([xdata, xdata2], ydata.astype(int).ravel(), epochs=90)

earlystop_callback = tf.keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0.001, verbose=1,
    patience=15, restore_best_weights=True)

history = mdl.fit(training_data, epochs=90,
                  callbacks=[earlystop_callback],
                  # steps_per_epoch=7,
                  validation_data=test_data,
                  # validation_steps=5,
                  )

plt.figure(figsize=(5, 3))
plt.plot(history.history["sparse_categorical_accuracy"], label="Training")
plt.plot(history.history["val_sparse_categorical_accuracy"], label="Validation")
plt.xlabel("Epoch")
plt.ylabel("Categorical Accuracy")
plt.legend(loc="lower right")
plt.tight_layout()
# plt.savefig(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_history_mae.png", dpi=300)
plt.savefig(f"lcluc_accuracy_history2.png", dpi=300, bbox_inches='tight')
plt.show()
mdl.save_weights(f"cnn50m_lcluc_30m_chmvhr_trained_weights.h5")



xlst = []
xlst2 = []
ylst = []
for element in test_data.as_numpy_iterator():
    xlst.append(element[0]['img_input'])
    xlst2.append(element[0]['chm_input'])
    ylst.append(element[1]['lc_output'])
yt = np.concatenate(ylst, axis=0)
xt = np.concatenate(xlst, axis=0)
xt2 = np.concatenate(xlst2, axis=0)
y_pred = mdl.predict([xt, xt2])
yp = np.argmax(y_pred, axis=1)
cnf_matrix = confusion_matrix(yt, yp)
np.set_printoptions(precision=2)
plt.figure()
ggm.plot_confusion_matrix(
    cnf_matrix,
    normalize=True,
    classes=['Crops/Grass', 'Forest Regrowth', 'Tree Plantation',
             'Wildfire', 'Urbanization', 'Primary Forest'],
    title='Confusion Matrix')
plt.savefig('cm_plot1.png', dpi=600, bbox_inches='tight')



mlayer2 = tf.keras.Model(
    inputs=mdl.input,
    outputs=mdl.get_layer("dense_1").output
)
y_m2 = mlayer2.predict([xt, xt2], verbose=1)

fig = plt.figure(figsize=(5,4))
for i in range(6):
    plt.scatter(y_m2[yt==i,0], y_m2[yt==i, 1], s=40, ), \
plt.legend(['Crops/Grass', 'Forest Regrowth', 'Tree Plantation',
    'Wildfire', 'Urbanization', 'Primary Forest'], loc="lowerright")
plt.xlabel("Feature 1")
plt.ylabel("Feature 2")
plt.tight_layout()
plt.savefig('feature_plot1.png', dpi=600, bbox_inches='tight')

print(1)





# %% predict model - CNN learning for texture layers
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\lcluc")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def load_model(dn_structure, width, width2, nbands, nbands2):
    img_input = tf.keras.Input(batch_shape=(None, width, width, nbands), name='img_input')
    img_densenet = ga.DenseNet(
        num_layers_in_each_block=dn_structure[0],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        pool_initial=True,
        bottleneck=True,
        include_top=False,
    )(img_input)
    chm_input = tf.keras.Input(batch_shape=(None, width2, width2, nbands2), name='chm_input')
    chm_densenet = ga.DenseNet(
        num_layers_in_each_block=dn_structure[1],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(chm_input)
    img_pool = tf.keras.layers.GlobalAveragePooling2D(name="img_pool")(img_densenet)
    chm_pool = tf.keras.layers.GlobalAveragePooling2D(name="chm_pool")(chm_densenet)
    global_pool = tf.concat([img_pool, chm_pool], axis=-1, name="global_pool")
    dense_layer = tf.keras.layers.Dense(1024, activation="relu")(global_pool)
    dense_2layers = tf.keras.layers.Dense(2)(dense_layer)
    # outputs = tf.keras.layers.Dense(6, activation='softmax', name='lc_output')(dense_layer)
    outputs = tf.keras.layers.Dense(6, name='lc_output')(dense_2layers)
    mdl = tf.keras.Model(inputs=[img_input, chm_input], outputs=outputs)

    mdl.load_weights(f"cnn50m_lcluc_30m_chmvhr_trained_weights.h5")
    return mdl


def map_x(X, y):
    x_mean = np.load("x_mean.npy")
    x_std = np.load("x_std.npy")
    sample_mean = tf.constant(x_mean.reshape([1, 1, x_mean.shape[0]]))
    sample_std = tf.constant(x_std.reshape([1, 1, x_std.shape[0]]))
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    return X, y


def map_x2(X, y):
    x_mean = np.load("x2_mean.npy")
    x_std = np.load("x2_std.npy")
    sample_mean = tf.constant(x_mean.reshape([1, 1, x_mean.shape[0]]))
    sample_std = tf.constant(x_std.reshape([1, 1, x_std.shape[0]]))
    X = (X - sample_mean) / sample_std
    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    return X, y


batch_size = 100
nbands = 3
new_resolution = 0.000001
offsets = 0.0000003 + new_resolution * np.array([42.])
width = np.array([384])
nbands2 = 1
new_resolution2 = 0.00002
offsets2 = 0.000008 + new_resolution2 * np.array([8.5])
width2 = np.array([32])

epochs = [15, ]
res = [30, ]
buffer = [5, ]
dn_structure = [
    (6, 12, 18, 24, 18, 12, 6),
    (6, 12, 18, 12, 6),
]

rgb = [
    "UTM35S_Plot3_RGB",
    "UTM34S_Plot19_RGB",
    "UTM33S_Plot176_RGB",
    "UTM33S_Plot26_RGB",
    "UTM34S_Plot2_RGB",
    "UTM34S_Plot158_RGB",
    "UTM35S_Plot14_RGB",
    "UTM34S_Plot47_RGB",
    "UTM35S_Plot155_RGB",
    "UTM35S_Plot79_RGB",
    "UTM35N_Plot148_RGB",
    "UTM35N_Plot150_RGB",
    "UTM35N_Plot180_RGB",
    "UTM35N_Plot129_RGB",
    "UTM34N_Plot106_RGB",
    "UTM35S_Plot66_RGB",
    "UTM34S_Plot177_RGB",
    "UTM34N_Plot92_RGB",
    "UTM34N_Plot171_RGB",
    "UTM35S_Plot156_RGB",
    "UTM34S_Plot181_RGB",
]

chm = [
    "UTM35S_Plot3_CHM",
    "UTM34S_Plot19_CHM",
    "UTM33S_Plot176_CHM",
    "UTM33S_Plot26_CHM",
    "UTM34S_Plot2_CHM",
    "UTM34S_Plot158_CHM",
    "UTM35S_Plot14_CHM",
    "UTM34S_Plot47_CHM",
    "UTM35S_Plot155_CHM",
    "UTM35S_Plot79_CHM",
    "UTM35N_Plot148_CHM",
    "UTM35N_Plot150_CHM",
    "UTM35N_Plot180_CHM",
    "UTM35N_Plot129_CHM",
    "UTM34N_Plot106_CHM",
    "UTM35S_Plot66_CHM",
    "UTM34S_Plot177_CHM",
    "UTM34N_Plot92_CHM",
    "UTM34N_Plot171_CHM",
    "UTM35S_Plot156_CHM",
    "UTM34S_Plot181_CHM",
]

for i in range(len(rgb)):
    path1 = f"als/{rgb[i]}_geo.tif"
    path2 = f"als/{chm[i]}_geo.tif"

    da2 = xr.open_rasterio(path2)
    da3 = da2.coarsen(y=15, x=15, boundary="trim").mean()
    out_file1 = f"als/{chm[i]}_geo_30m.nc"
    ds1 = xr.Dataset({"da": da3})
    ds1.to_netcdf(out_file1)
    out_file2 = f"als/{chm[i]}_geo_30m.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    ga.raster_to_csv([out_file2],
                     f"als/{chm[i]}_geo_30m",
                     band_list=["CHM"], valid_min=0.0)
    df = dd.read_csv(f"als/{chm[i]}_geo_30m_*.csv").compute()
    ga.regression_tfrecord_from_df(
        df,
        f"als/{rgb[i]}_geo.tif",
        f"tfrecords/VHR_{i}_30m.tfrecord",
        x_cols="xvar",
        response="CHM",
        loc=("latitude", "longitude"),
        offset=(-offsets[0], offsets[0]),
        blocksize=(width[0], width[0]),
    )
    ga.regression_tfrecord_from_df(
        df,
        f"als/{chm[i]}_geo.tif",
        f"tfrecords/Lidar_{i}_30m.tfrecord",
        x_cols="xvar",
        response="CHM",
        loc=("latitude", "longitude"),
        offset=(-offsets2[0], offsets2[0]),
        blocksize=(width2[0], width2[0]),
    )

    ds, train_n = ga.load_tf(
        f"tfrecords/VHR_{i}_30m.tfrecord",
        x_features="xvar",
        y_feature="CHM",
        x_width=(width[0], width[0], nbands),
        y_width=(1,),
        compression="",
    )
    # ds1 = ds.shuffle(5000).batch(batch_size)
    ds1 = ds.map(map_x).batch(batch_size)
    ds, train_n = ga.load_tf(
        f"tfrecords/Lidar_{i}_30m.tfrecord",
        x_features="xvar",
        y_feature="CHM",
        x_width=(width2[0], width2[0], nbands2),
        y_width=(1,),
        compression="",
    )
    # ds1 = ds.shuffle(5000).batch(batch_size)
    ds2 = ds.map(map_x2).batch(batch_size)

    model = load_model(dn_structure, width[0], width2[0], nbands, nbands2)

    xlst = []
    for element in ds1.as_numpy_iterator():
        xlst.append(element[0])
    xdata = np.concatenate(xlst, axis=0)
    xlst2 = []
    for element in ds2.as_numpy_iterator():
        xlst2.append(element[0])
    xdata2 = np.concatenate(xlst2, axis=0)

    y_pred = model.predict([xdata, xdata2])
    yp = np.argmax(y_pred, axis=1)
    df["LC_hat"] = yp
    df.to_csv(f"als/{chm[i]}_geoLC_30m.csv")
    rt.ogrvrt_to_grid(
        f"als/{chm[i]}_geo_30m.tif",
        f"als/{chm[i]}_geoLC_30m.csv",
        "longitude", "latitude", "LC_hat",
        f"als/{chm[i]}_geoLC_30m"
    )

    print(time.time() - start)





#%% CNN features for tree-structure visualization
os.chdir("F:\\Projects\\a1902alostexture\\lcluc")
batch_size = 100
nbands = 3
new_resolution = 0.000001
offsets = 0.0000003 + new_resolution * np.array([42.])
width = np.array([384])

nbands2 = 1
new_resolution2 = 0.00002
offsets2 = 0.000008 + new_resolution2 * np.array([8.5])
width2 = np.array([32])

autoenc6 = tf.keras.Sequential(
    [
        tf.keras.layers.InputLayer(input_shape=(width[0], width[0], 3)),
        tf.keras.layers.Conv2D(
            filters=6, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=12, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=24, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=36, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=48, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=60, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Conv2D(
            filters=72, kernel_size=3, strides=(2, 2), activation='relu'),
        tf.keras.layers.Flatten(),
        # tf.keras.layers.GlobalAveragePooling2D(),
        # No activation
        tf.keras.layers.Dense(84),
        tf.keras.layers.Dense(units=3 * 3 * 72, activation=tf.nn.relu),
        tf.keras.layers.Reshape(target_shape=(3, 3, 72)),
        tf.keras.layers.Conv2DTranspose(
            filters=60,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=48,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=36,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=24,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=12,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        tf.keras.layers.Conv2DTranspose(
            filters=6,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME",
            activation='relu'),
        # No activation
        tf.keras.layers.Conv2DTranspose(
            filters=3,
            kernel_size=3,
            strides=(2, 2),
            padding="SAME"),
    ]
)
autoenc6.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.get("MeanSquaredError"),
    # loss=tf.keras.losses.get("MeanAbsoluteError"),
    metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
)
xdata = np.load("lc_xdata.npy")
ydata = np.load("lc_ydata.npy")
xdata2 = np.load("lc_xdata2.npy")
ydata2 = np.load("lc_ydata2.npy")
X_mlayer = np.load("lc_mlayer.npy")

xlayer2 = np.concatenate([X_mlayer, xdata2.reshape(240, -1)], axis=1)


from sklearn.decomposition import PCA
pca = PCA(n_components=2, svd_solver='arpack')
x2v = xlayer2[~np.isnan(np.mean(xlayer2, axis=1)) & ((ydata[:,0]==1) | (ydata[:,0]==5)),:]
x_pca = pca.fit_transform(x2v)
y2 = ydata[~np.isnan(np.mean(xlayer2, axis=1)) & ((ydata[:,0]==1) | (ydata[:,0]==5)), :]
from sklearn import tree
clf = tree.DecisionTreeClassifier()
clf = clf.fit(x_pca, y2.ravel())

from dtreeviz.trees import dtreeviz
viz = dtreeviz(clf,
               x_pca,
               y2.ravel(),
               target_name='',
               feature_names=np.array([f'Feature_{i}' for i in range(1,3)]),
               class_names={0: 'Secondary Forest', 1: 'Primary Forest'})
viz.view()




#%% get training from satellite layers
def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2609046e-02, 5.0183419e-02, 1.5364012e-01, 1.6194092e-01,
                    1.3404459e-01, 4.0531594e-02, 5.4019067e+02, 4.6418205e-02,
                    1.6195051e-01, 1.6210890e-01, 8.7572202e-02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    1.8084852e-02, 3.6881823e-02, 8.2259081e-02, 6.7309268e-02,
                    9.6319959e-02, 2.8609153e-02, 3.7470444e+02, 3.5437502e-02,
                    1.0083786e-01, 8.0986813e-02, 6.3853748e-02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std

    X = tf.where(tf.math.is_nan(X), tf.zeros_like(X), X)
    # # X1 = X.numpy().reshape([-1, 7])
    # X1 = X.reshape([-1, 7])
    # imp_0 = SimpleImputer(strategy='constant', fill_value=0)
    # imp_0.fit_transform(X1)
    # X = X1.reshape(X.shape[0], X.shape[1], X.shape[2])
    # # X = tf.convert_to_tensor(X2, np.float32)

    # return X[:, :, 4:], Y
    return X[:, :, :], Y


os.chdir("F:\\Projects\\a1902alostexture\\lcluc")
df = pd.read_csv("lcluc_a0_0.csv")
nbands = 11

pix_size = 0.000449157642
offsets = 0.00005 + pix_size * np.array([1.5])
width = np.array([4])
res = [100, ]

# path_tif = "../input/xlyrs_50m_probaxlc8_2015_v3.vrt"
# ga.regression_tfrecord_from_df(
#     df,
#     path_tif,
#     f"tfrecords/satellite_xlyrs_50m.tfrecord",
#     x_cols="xvar",
#     response="LC",
#     loc=("latitude", "longitude"),
#     offset=(-offsets[0], offsets[0]),
#     blocksize=(width[0], width[0]),
# )

path_training = f"tfrecords/satellite_xlyrs_50m.tfrecord"
ds, train_n = ga.load_tf(
    path_training,
    x_features="xvar",
    y_feature="LC",
    x_width=(width[0], width[0], nbands),
    y_width=(1,),
    compression="",
)

batch_size = 256
ds1 = (
    ds
    .cache()
    .shuffle(25000)
    # .repeat()
    .map(sample_normalize)
    .batch(batch_size)
    .prefetch(AUTOTUNE)
)

xlst = []
ylst = []
for element in ds1.as_numpy_iterator():
    xlst.append(element[0])
    ylst.append(element[1])
xt = np.concatenate(xlst, axis=0)
yt = np.concatenate(ylst, axis=0) - 1
tfbool = yt[:, 0] < 6
xt = xt[tfbool, :, :, :]
yt = yt[tfbool, :]

x_train, x_test, y_train, y_test = train_test_split(xt, yt, test_size=0.2, random_state=777)

dn_structure = [
    (16, 16),
]
img_input = tf.keras.Input(batch_shape=(None, width[0], width[0], nbands), name='img_input')
img_densenet = ga.DenseNet(
    num_layers_in_each_block=dn_structure[0],
    growth_rate=8,
    dropout_rate=0.2,
    weight_decay=1e-4,
    compression=0.5,
    pool_initial=False,
    bottleneck=True,
    include_top=False,
)(img_input)
img_pool = tf.keras.layers.GlobalAveragePooling2D(name="img_pool")(img_densenet)
dense_layer = tf.keras.layers.Dense(1024, activation="relu")(img_pool)
# outputs = tf.keras.layers.Dense(6, activation='softmax', name='lc_output')(dense_layer)
outputs = tf.keras.layers.Dense(6, name='lc_output')(dense_layer)
mdl = tf.keras.Model(inputs=img_input, outputs=outputs)


mdl.compile(
    optimizer=tf.keras.optimizers.Adam(),
    # optimizer=tf.keras.optimizers.RMSprop(),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    # loss='sparse_categorical_crossentropy',
    metrics=['sparse_categorical_accuracy'])

# history = mdl.fit([xdata, xdata2], ydata.astype(int).ravel(), epochs=90)

earlystop_callback = tf.keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0.001, verbose=1,
    patience=15, restore_best_weights=True)

history = mdl.fit(x_train, y_train, epochs=90,
                  callbacks=[earlystop_callback],
                  validation_data=[x_test, y_test],
                  )

plt.figure(figsize=(5, 3))
plt.plot(history.history["sparse_categorical_accuracy"], label="Training")
plt.plot(history.history["val_sparse_categorical_accuracy"], label="Validation")
plt.xlabel("Epoch")
plt.ylabel("Categorical Accuracy")
plt.legend(loc="lower right")
plt.tight_layout()
# plt.savefig(f"../output/cnn50m_output2015_v3_{res[k]}m_width{width[k]}_history_mae.png", dpi=300)
plt.savefig(f"lcluc_satlyrs_accuracy_history.png", dpi=300, bbox_inches='tight')
plt.show()


y_pred = mdl.predict(x_test)
yp = np.argmax(y_pred, axis=1)
cnf_matrix = confusion_matrix(y_test.ravel(), yp)
np.set_printoptions(precision=2)
plt.figure()
ggm.plot_confusion_matrix(
    cnf_matrix,
    normalize=True,
    classes=['Crops/Grass', 'Forest Regrowth', 'Tree Plantation',
             'Wildfire', 'Urbanization', 'Primary Forest'],
    title='Confusion Matrix')
plt.savefig('cm_plot2.png', dpi=600, bbox_inches='tight')

