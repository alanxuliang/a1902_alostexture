#%% setup environment
from pathlib import Path
import subprocess
import xarray as xr
import zarr
from zarr import blosc
import a1902alos.data_load as adl
import learn2map.raster_tools as rt

compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)


#%% convert vrt to zarr
p = Path("E:\data00\\a1902alostexture\data3")

out_file = str(p / "alos2africa_1517.vrt")
out_file2 = str(p / "alos2africa_1517.zarr")

# da = xr.open_rasterio(out_file, chunks=[3, 1000, 1000])
# ds0 = xr.Dataset({"da": da})
# ds0.to_zarr(
#     out_file2,
#     mode="w",
#     synchronizer=zarr.ThreadSynchronizer(),
#     encoding={"da": {"compressor": compressor}},
# )

in_zarr = str(p / "alos2africa_1517.zarr")
out_zarr = str(p / "alos2africa_ds.zarr")
adl.get_zarr_data_noref(
    in_zarr,
    out_zarr,
    band_names=["HH", "HV", "angle"],
    vmins=[0, 0, 0],
    vmaxs=[26000, 8100, 78],
    istarget=False,
)
