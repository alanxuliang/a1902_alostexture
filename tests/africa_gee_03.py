# import modules (using ALOS/SRTM/L8 layers @ 250m)
import os
import subprocess
import time
import glob
import gdal
import a1902alos.gee_app as ga
import a1902alos.grid_models as ggm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import xarray as xr
import dask.array as dskda
import dask as delayed
import learn2map.raster_tools as rt
import dask.dataframe as dd
import seaborn as sns
from scipy.interpolate import UnivariateSpline
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import (
    train_test_split,
)
from dask.diagnostics import ProgressBar
import zarr
from zarr import blosc

AUTOTUNE = tf.data.experimental.AUTOTUNE
compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)

# from dask.distributed import Client, progress
# client = Client(threads_per_worker=4, n_workers=1)
# client
print(1)

# %% training data generation (different resolutions: 100, 250, 500, 750, 1k, 1.5k, 2k, 3k)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\output")
os.chdir("F:\\Projects\\a1902alostexture\\output")


def sample_normalize(X, Y):
    # sample_mean = tf.constant(
    #     [
    #         [
    #             [
    #                 3.2123085e-02,
    #                 3.1446800e-01,
    #                 1.7229147e-01,
    #                 6.1950065e-02,
    #                 2.2465163e-01,
    #                 7.6607764e-02,
    #                 5.6069208e02,
    #             ]
    #         ]
    #     ]
    # )
    # sample_std = tf.constant(
    #     [
    #         [
    #             [
    #                 7.8484304e-03,
    #                 2.8701460e-02,
    #                 2.1761550e-02,
    #                 1.4474887e-02,
    #                 4.6037395e-02,
    #                 1.8193731e-02,
    #                 2.4210707e02,
    #             ]
    #         ]
    #     ]
    # )
    sample_mean = tf.constant([[[6570.302, 3821.262, 563.231]]])
    sample_std = tf.constant([[[1464.3604, 929.6932, 249.8123]]])
    X = (X - sample_mean) / sample_std
    return X, Y


res = [100, 250, 500, 750, 1000, 1500, 2000, 3000]
pix_size = 0.000224578821
offsets = 0.00005 + pix_size * np.array([1.5, 4.5, 9.5, 14.5, 19.5, 29.5, 39.5, 59.5])
width = np.array([1, 2, 3, 4, 6, 8, 12, 20])
width = np.array([4, 10, 20, 30, 40, 60, 80, 120])
dn_structure = [
    (6, 12),
    (6, 12, 12),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 24, 16),
    (6, 12, 24, 32, 20),
]
epochs = [5, 10, 80, 110, 150, 200, 250, 300]

mch_100 = "../input/lidar_data/drc/lidar_plots_mch.tif"
# for k in range(0, 1):
for k in range(len(res)):
    ref_file = f"../input/mask_{res[k]}m_all.tif"
    mch_newres = f"../input/lidar_data/drc/lidar_plots_mch_{res[k]}m.tif"
    mch_count_newres = f"../input/lidar_data/drc/lidar_plots_mch_tf_{res[k]}m.tif"
    mch_newres, mch_count_newres = ga.raster_resample(
        mch_100, ref_file, mch_newres, mch_count_newres
    )
    mch_newres_out = f"../input/lidar_data/drc/lidar_valid_mch_{res[k]}m.tif"
    ga.raster_mask(mch_newres, mch_count_newres, mch_newres_out, valid_min=0.8)

    mch_csv_out = f"../input/csvs/mch_{res[k]}m_geo_v2"
    ga.raster_to_csv(f"{mch_newres_out}", mch_csv_out, valid_min=0.0)

# %% training model - 7 bands from 250m lyrs (different neighboring widths)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


nbands = 7

pix_size = 0.00224578821
# offsets = 0.0005 + pix_size * np.array([0, 0.5, 1.5, 3.5, 7.5, 15.5])
# width = np.array([1, 2, 4, 8, 16, 32])
offsets = 0.0005 + pix_size * np.array([0.])
width = np.array([1])
dn_structure = [
    (12,),
    (12,),
    (12, 18),
    (12, 18),
    (12, 18, 24),
    (12, 18, 24, 16),
]
epochs = [10, 10, 10, 10, 10, 10]
res = [250, ]

for k in range(0, len(width)):
    # for k in range(0, len(width)):

    # df0 = dd.read_csv(f"csvs/mch_{res[0]}m_geo_v2_*.csv").compute()
    # df, df_test = train_test_split(df0, test_size=0.2, random_state=77)
    df = pd.read_csv("csvs/mch_250m_training_v0.csv")
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv")

    path_tif = "xlyrs_250m_median_1317_v0.vrt"
    path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )
    path_tfrecord = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_test,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )

    path_training = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"
    path_test = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds_test.take(1)).next())

    # CNN model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 7))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(32)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    history = m.fit(
        x=ds1,
        epochs=epochs[k],
        steps_per_epoch=int(train_n / batch_size),
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    plt.savefig(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_history.png", dpi=300)
    plt.show()
    m.save_weights(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        file_name=f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_val.png",
    )

    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})

    tf.keras.backend.clear_session()

    # compare with RF model using spatial mean of each band
    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_train = np.nanmean(X_train, axis=(1, 2))
    y_train = y_train.squeeze()
    X_test1 = np.nanmean(X_test, axis=(1, 2))
    y_test = y_test.squeeze()

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test1, y_test)
    # model_regress.save_models(outname=f"xgboost250m_bc_output_v3_{res[0]}m.pkl")
    model_regress.save_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost250m_bc_output_v3_{res[0]}m"),
        file_name="{}_val.png".format(f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}"),
    )

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict(X_test1)
    # model_regress.save_models(outname=f"xgboost250m_output_v3_{res[0]}m.pkl")
    model_regress.save_models(outname=f"../output/rf250m_output_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost250m_output_v3_{res[0]}m"),
        file_name="{}_val.png".format(f"../output/rf250m_output_v3_{res[0]}m_width{width[k]}"),
    )
    dfy['y_rfbc'] = model_y_pred[~np.isnan(y_test)]

    # compare with CNN+linear model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 7))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(32)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_trained_weights.h5")
    mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)

    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_mlayer_train = mlayer_model.predict(X_train, verbose=1)

    # X_list = []
    # y_list = []
    # for X, Y in ds_test.take(int(test_n / batch_size)):
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_test = np.concatenate(X_list)
    # y_test = np.concatenate(y_list)
    X_mlayer_test = mlayer_model.predict(X_test, verbose=1)

    model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [100, 200, 400, 600],
    #         "learn__max_depth": [3, 6, 9, 12],
    #         # "learn__booster": ["gbtree","gblinear", "dart"],
    #         "learn__booster": ["dart"],
    #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    #     }
    # )
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [300],
    #         # "learn__max_depth": [6, 9, 12],
    #         # "learn__min_samples_split": [2, 7, 12],
    #         "learn__max_depth": [9],
    #         "learn__min_samples_split": [2],
    #     }
    # )
    model_regress.grid_opts(model_pipe, params_grid, k=3)
    # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    model_y_pred = model_regress.meta_model_predict(X_mlayer_test)

    # model_regress.save_models(outname=f"../output/ridge250m_mlayer_model_v3_{res[0]}m.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    y_test = y_test.ravel()
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"../output/ridge250m_mlayer_model_v3_{res[0]}m_width{width[k]}"),
    )
    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": model_y_pred[~np.isnan(y_test)]})
    dfy["y_ridgetest"] = y_test[~np.isnan(y_test)]
    dfy.to_csv(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_val_ridge.csv")

    # dfy.to_csv(f"../output/cnn250m_output_v3_{res[0]}m_val_data.csv")
    tf.keras.backend.clear_session()

    print(time.time() - start)

# def get_xys(df):
#     df['y_sum'] = df.y_test.values + df.y_predcnn.values
#     df['y_bin'] = pd.cut(df.y_sum, np.arange(0, 70, 6))
#     df1 = df.groupby('y_bin').mean()
#     x = df1.y_test.values
#     y = df1.y_predcnn.values
#     spl = UnivariateSpline(x, y)
#     spl.set_smoothing_factor(10)
#     # reg = LinearRegression().fit(x[:, None], y)
#     xs = np.linspace(0, 35, 100)
#     ys = spl(xs)
#     # ys = reg.predict(xs[:, None])
#     return x, y, xs, ys
#
#
# l1 = []
# fig, ax = plt.subplots(figsize=(5, 4))
# l1a = plt.plot([-5, 35], [-5, 35], 'k')
# l1.append(l1a[0])
#
# # df = pd.read_csv(f"../output/cnn250m_output_v3_250m_val_data.csv")
# df = pd.read_csv(f"../output/cnn250m_output_v3_250m_val_ridge.csv")
# x, y, xs, ys = get_xys(df)
# p1 = plt.plot(x, y, 'o', alpha=0.7)
# l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
# l1.append(l1a[0])
# # ax = sns.regplot(x=df.y_test, y=df.y_predcnn, x_bins=np.arange(0, 35.1, 3), ci=None, ax=ax, scatter_kws={'alpha': 0.7}, order=3, line_kws={'alpha': 0.7})
# # l1.append(ax.get_lines()[-1])
#
# # df = pd.read_csv(f"../output/cnn250m_output_v3_500m_val_data.csv")
# df = pd.read_csv(f"../output/cnn250m_output_v3_500m_val_ridge.csv")
# x, y, xs, ys = get_xys(df)
# p1 = plt.plot(x, y, 'o', alpha=0.7)
# l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
# l1.append(l1a[0])
#
# # df = pd.read_csv(f"../output/cnn250m_output_v3_1000m_val_data.csv")
# df = pd.read_csv(f"../output/cnn250m_output_v3_1000m_val_ridge.csv")
# x, y, xs, ys = get_xys(df)
# p1 = plt.plot(x, y, 'o', alpha=0.7)
# l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
# l1.append(l1a[0])
#
# # df = pd.read_csv(f"../output/cnn250m_output_v3_2000m_val_data.csv")
# df = pd.read_csv(f"../output/cnn250m_output_v3_2000m_val_ridge.csv")
# x, y, xs, ys = get_xys(df)
# p1 = plt.plot(x, y, 'o', alpha=0.7)
# l1a = plt.plot(xs, ys, alpha=0.7, color=p1[0].get_color())
# l1.append(l1a[0])
#
# ax.set_xlabel('Measurements')
# ax.set_ylabel('Predictions')
# ax.set_xlim([-5, 35])
# ax.set_ylim([-5, 35])
# ax.legend(l1, ['1-to-1 Line', '250m Retrieval', '500m Retrieval', '1km Retrieval', '2km Retrieval'])
# # ax.legend(l1, ['1-to-1 Line', '250m Retrieval', '500m Retrieval', '1km Retrieval'])
# plt.savefig(f"../output/cnn250m_output_v3_multi-scale_scatter3s.png", dpi=300)
# plt.show()

print(1)

# %% training and predicting model - 7 bands from 250m lyrs (3x3 @250m) -not efficient
start = time.time()
# os.chdir("F:\\Projects\\a1902alostexture\\input")
os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def func_predict_rf_0(
        mask_arr,
        block_id=None,
        chunksize=None,
        xdata=None,
        ydata=None,
        model=None,
        path_tif="",
        path_tfrecord="",
        offset=(0, 0),
        blocksize=(1, 1),
        batch_size=40,
):
    if xdata is None:
        xdata = np.arange(mask_arr.shape[1])
        ydata = np.arange(mask_arr.shape[0])

    id = np.argwhere(mask_arr > 0)
    print(id.shape)
    if id.shape[0] > 0:
        x1 = xdata[id[:, 1] + block_id[1] * chunksize]
        y1 = ydata[id[:, 0] + block_id[0] * chunksize]

        # bak
        # y_width = blocksize[0]
        # x_width = blocksize[1]
        # y_offset = y1 - offset[0]
        # x_offset = x1 - offset[1]
        # da1 = xr.open_rasterio(path_tif, chunks=(-1, 2000, 2000))
        # nbands = da1.band.shape[0]
        # da1y = da1.y.values
        # da1x = da1.x.values
        # X_test = np.zeros([id.shape[0], y_width, x_width, nbands], "float32")
        # for i in np.arange(id.shape[0]):
        #     id1y = np.abs(da1y - y_offset[i]).argmin()
        #     id1x = np.abs(da1x - x_offset[i]).argmin()
        #     id2y = id1y + y_width
        #     id2x = id1x + x_width
        #     id1y = max(0, id1y)
        #     id1x = max(0, id1x)
        #     id2y = min(da1y.shape[0], id2y)
        #     id2x = min(da1x.shape[0], id2x)
        #     img = da1.isel(y=slice(id1y, id2y), x=slice(id1x, id2x)).transpose(
        #         "y", "x", "band"
        #     )
        #     X_test[i, : img.shape[0], : img.shape[1], :] = img.values

        df1 = pd.DataFrame({"Y": y1, "X": x1})
        file_tfrecord = f"{path_tfrecord}_{block_id[0]}_{block_id[1]}.tfrecord"
        if os.path.isfile(file_tfrecord):
            pass
        else:
            print(file_tfrecord)
            ga.regression_tfrecord_from_df(df1, path_tif, file_tfrecord, offset=offset, blocksize=blocksize)

        in0 = gdal.Open(path_tif)
        n_bands = in0.RasterCount
        in0 = None
        pred_ds, pred_n = ga.load_tf(
            file_tfrecord,
            x_features="xvar",
            y_feature="NA",  # predictions
            x_width=(blocksize[0], blocksize[1], n_bands),
            y_width=(1,),
            compression="",
        )

        pred_ds = pred_ds.map(sample_normalize)
        pred_lst = []
        for xvar, _ in pred_ds.take(pred_n):
            pred_lst.append(xvar.numpy().ravel())
        pred_X = np.stack(pred_lst, axis=0)
        model_y_pred = model.meta_model_predict_biascorr(pred_X)

        pred_arr = np.full_like(mask_arr.ravel(), np.nan, dtype="float32")
        # pred_arr[0, id[:, 0], id[:, 1]] = model_y_pred.ravel()
        pred_arr[mask_arr.ravel() > 0] = model_y_pred.ravel()
        pred_arr = pred_arr.reshape(mask_arr.shape)
        print(f"pred_shape: {pred_arr.shape}")
    else:
        pred_arr = np.full_like(mask_arr, np.nan, dtype="float32")

    return pred_arr


nbands = 7

pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([1.])
width = np.array([3])
dn_structure = [
    (12,),
]
epochs = [10, ]
res = [250, ]

for k in range(0, len(width)):

    # train

    # df0 = dd.read_csv(f"csvs/mch_{res[0]}m_geo_v2_*.csv").compute()
    # df, df_test = train_test_split(df0, test_size=0.2, random_state=77)
    df = pd.read_csv("csvs/mch_250m_training_v0.csv")
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv")

    path_tif = "xlyrs_250m_median_1317_v0.vrt"
    path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )
    path_tfrecord = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_test,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )

    path_training = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"
    path_test = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds_test.take(1)).next())
    #
    # # CNN model
    # inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 7))
    # mlayer = ga.DenseNet(
    #     num_layers_in_each_block=dn_structure[k],
    #     growth_rate=8,
    #     dropout_rate=0.2,
    #     weight_decay=1e-4,
    #     compression=0.5,
    #     bottleneck=True,
    #     include_top=False,
    # )(inputs)
    # m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    # m2 = tf.keras.layers.Dense(32)(m2)
    # outputs = tf.keras.layers.Dense(1)(m2)
    # m = tf.keras.Model(inputs, outputs)
    #
    # m.compile(
    #     optimizer=tf.keras.optimizers.Adam(0.001),
    #     loss=tf.keras.losses.get("MeanSquaredError"),
    #     metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    # )
    #
    # history = m.fit(
    #     x=ds1,
    #     epochs=epochs[k],
    #     steps_per_epoch=int(train_n / batch_size),
    #     validation_data=ds_test,
    #     validation_steps=int(test_n / batch_size),
    # )
    # plt.figure(figsize=(6, 4))
    # plt.plot(history.history["root_mean_squared_error"], label="Training")
    # plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    # plt.xlabel("Epoch")
    # plt.ylabel("RMSE")
    # plt.legend(loc="upper right")
    # plt.savefig(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_history.png", dpi=300)
    # plt.show()
    # m.save_weights(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)

    # predictions = m.predict(X_test, verbose=1)
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     predictions[~np.isnan(y_test)],
    #     file_name=f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_val.png",
    # )
    #
    # dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})
    #
    # tf.keras.backend.clear_session()

    # compare with RF model using spatial mean of each band
    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    # X_train = np.nanmean(X_train, axis=(1, 2))
    X_train = X_train.reshape(X_train.shape[0], -1)
    y_train = y_train.squeeze()
    # X_test1 = np.nanmean(X_test, axis=(1, 2))
    X_test1 = X_test.reshape(X_test.shape[0], -1)
    y_test = y_test.squeeze()

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test1, y_test)
    # model_regress.save_models(outname=f"xgboost250m_bc_output_v3_{res[0]}m.pkl")
    model_regress.save_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost250m_bc_output_v3_{res[0]}m"),
        file_name="{}_val.png".format(f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}"),
    )

    # model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_y_pred = model_regress.meta_model_predict(X_test1)
    # # model_regress.save_models(outname=f"xgboost250m_output_v3_{res[0]}m.pkl")
    # model_regress.save_models(outname=f"../output/rf250m_output_v3_{res[0]}m_width{width[k]}.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     # file_name="{}_val.png".format(f"xgboost250m_output_v3_{res[0]}m"),
    #     file_name="{}_val.png".format(f"../output/rf250m_output_v3_{res[0]}m_width{width[k]}"),
    # )
    # dfy['y_rfbc'] = model_y_pred[~np.isnan(y_test)]
    #
    # # compare with CNN+linear model
    # inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 7))
    # mlayer = ga.DenseNet(
    #     num_layers_in_each_block=dn_structure[k],
    #     growth_rate=8,
    #     dropout_rate=0.2,
    #     weight_decay=1e-4,
    #     compression=0.5,
    #     bottleneck=True,
    #     include_top=False,
    # )(inputs)
    # m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    # m2 = tf.keras.layers.Dense(32)(m2)
    # outputs = tf.keras.layers.Dense(1)(m2)
    # m = tf.keras.Model(inputs, outputs)
    # m.load_weights(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_trained_weights.h5")
    # mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)
    #
    # X_list = []
    # y_list = []
    # for X, Y in ds1.take(int(train_n / batch_size)):
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_train = np.concatenate(X_list)
    # y_train = np.concatenate(y_list)
    # X_mlayer_train = mlayer_model.predict(X_train, verbose=1)
    #
    # # X_list = []
    # # y_list = []
    # # for X, Y in ds_test.take(int(test_n / batch_size)):
    # #     X_list.append(X.numpy())
    # #     y_list.append(Y.numpy())
    # # X_test = np.concatenate(X_list)
    # # y_test = np.concatenate(y_list)
    # X_mlayer_test = mlayer_model.predict(X_test, verbose=1)
    #
    # model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    # model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # # model_pipe, params_grid = model_regress.setup_xgb_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [100, 200, 400, 600],
    # #         "learn__max_depth": [3, 6, 9, 12],
    # #         # "learn__booster": ["gbtree","gblinear", "dart"],
    # #         "learn__booster": ["dart"],
    # #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    # #     }
    # # )
    # # model_pipe, params_grid = model_regress.setup_rf_model(
    # #     param_grid={
    # #         "pca": [None],
    # #         "learn__n_estimators": [300],
    # #         # "learn__max_depth": [6, 9, 12],
    # #         # "learn__min_samples_split": [2, 7, 12],
    # #         "learn__max_depth": [9],
    # #         "learn__min_samples_split": [2],
    # #     }
    # # )
    # model_regress.grid_opts(model_pipe, params_grid, k=3)
    # # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    # model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict(X_mlayer_test)
    #
    # # model_regress.save_models(outname=f"../output/ridge250m_mlayer_model_v3_{res[0]}m.pkl")
    # if isinstance(y_test, pd.core.series.Series):
    #     y_test = y_test.values
    # y_test = y_test.ravel()
    # r2, rmse, beta0 = ggm.density_scatter_plot(
    #     y_test[~np.isnan(y_test)],
    #     model_y_pred[~np.isnan(y_test)],
    #     file_name="{}_val.png".format(f"../output/ridge250m_mlayer_model_v3_{res[0]}m_width{width[k]}"),
    # )
    # dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": model_y_pred[~np.isnan(y_test)]})
    # dfy["y_ridgetest"] = y_test[~np.isnan(y_test)]
    # dfy.to_csv(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_val_ridge.csv")
    #
    # # dfy.to_csv(f"../output/cnn250m_output_v3_{res[0]}m_val_data.csv")
    # tf.keras.backend.clear_session()

    print(time.time() - start)

    # prediction
    chunk_size = 1000
    mask_file = f"mask_250m_all.tif"
    da_mask = xr.open_rasterio(mask_file, chunks=(1, chunk_size, chunk_size))[0, :, :]
    # mask_file = f"mask_250m_congo.zarr"
    # da_mask = xr.open_zarr(mask_file, synchronizer=zarr.ThreadSynchronizer())["da"]
    path_tif = "xlyrs_250m_median_1317_v0.vrt"

    da_x = da_mask.x.values
    da_y = da_mask.y.values
    path_tfrecord = f"tfrecords/xlyrs_{res[0]}m_from_250m_y2015_v0v3"

    # inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 7))
    # mlayer = ga.DenseNet(
    #     num_layers_in_each_block=dn_structure[k],
    #     growth_rate=8,
    #     dropout_rate=0.2,
    #     weight_decay=1e-4,
    #     compression=0.5,
    #     bottleneck=True,
    #     include_top=False,
    # )(inputs)
    # m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    # m2 = tf.keras.layers.Dense(32)(m2)
    # outputs = tf.keras.layers.Dense(1)(m2)
    # m = tf.keras.Model(inputs, outputs)
    # m.load_weights(f"../output/cnn250m_output_v3_{res[0]}m_width{width[k]}_trained_weights.h5")

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")

    kwargs = {
        "chunksize": chunk_size,
        "xdata": da_x,
        "ydata": da_y,
        "model": model_regress,
        "path_tif": path_tif,
        "path_tfrecord": path_tfrecord,
        "offset": (-offsets[k], offsets[k]),
        "blocksize": (width[k], width[k]),
        "batch_size": 40,
    }
    da_pred = dskda.map_blocks(func_predict_rf_0, da_mask.data, dtype='float32', **kwargs)

    # da_pred = xr.apply_ufunc(
    #     func_predict_cnn_0,
    #     da_mask,
    #     kwargs=kwargs,
    #     dask="parallelized",
    #     # dask="allowed",
    #     output_dtypes=[float],
    # )

    da1 = xr.DataArray(da_pred, coords=(da_y, da_x), dims=("y", "x"))
    out_file1 = f"../output/mch_prediction_{res[0]}m_from_xlyrs_250m_v0v3.nc"
    ds2 = xr.Dataset({"da": da1})
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_{res[0]}m_from_xlyrs_250m_v0v3.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)

# %% training and predicting model - 7 bands from 250m lyrs (1x1 @250m)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    return X


def func_predict_rf_1(
        mask_arr,
        model=None,
):
    arr_table = mask_arr.reshape(mask_arr.shape[0], -1)
    pred_X = arr_table[:, arr_table[-1, :] > -1000].T
    pred_X = sample_normalize_1(pred_X)
    print(pred_X.shape)
    if pred_X.shape[0] > 0:
        model_y_pred = model.meta_model_predict_biascorr(pred_X)

        pred_arr = np.full_like(mask_arr[0, :, :].ravel(), np.nan, dtype="float32")
        pred_arr[arr_table[-1, :] > -1000] = model_y_pred.ravel()
        pred_arr = pred_arr.reshape((mask_arr.shape[1], mask_arr.shape[2]))
        print(f"pred_shape: {pred_arr.shape}")
    else:
        pred_arr = np.full_like(mask_arr[0, :, :], np.nan, dtype="float32")

    return pred_arr


nbands = 7

pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([0.])
width = np.array([1])
dn_structure = [
    (12,),
]
epochs = [10, ]
res = [250, ]

for k in range(0, len(width)):

    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv")

    path_test = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"

    batch_size = 40
    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)

    # X_test1 = np.nanmean(X_test, axis=(1, 2))
    X_test1 = X_test.reshape(X_test.shape[0], -1)
    y_test = y_test.squeeze()

    print(time.time() - start)

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_250m_median_1317_v0.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))
    da_x = da_mask.x.values
    da_y = da_mask.y.values

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")

    kwargs = {
        "model": model_regress,
    }
    da_pred = dskda.map_blocks(func_predict_rf_1, da_mask.data, dtype='float32', drop_axis=0, **kwargs)

    da1 = xr.DataArray(da_pred, coords=(da_y, da_x), dims=("y", "x"))
    out_file1 = f"../output/mch_prediction_{res[0]}m_width{width[k]}_from_xlyrs_250m_v0v3.nc"
    ds2 = xr.Dataset({"da": da1})
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_{res[0]}m_width{width[k]}_from_xlyrs_250m_v0v3.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)

# %% training and predicting model - 7 bands from 250m lyrs (3x3 @250m)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")
# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    return X


def func_predict_rf_1(
        mask_arr,
        block_id=None,
        chunksize=1000,
        xdata=None,
        ydata=None,
        model=None,
        out_name=None
):
    max_x = min((block_id[2] + 1) * chunksize, xdata.shape[0])
    max_y = min((block_id[1] + 1) * chunksize, ydata.shape[0])
    idx = np.arange(block_id[2] * chunksize, max_x)
    idy = np.arange(block_id[1] * chunksize, max_y)
    x1 = xdata[idx]
    y1 = ydata[idy]
    arr_new = np.moveaxis(mask_arr, 0, -1)[None, :, :, :]
    arr_patch = tf.image.extract_patches(images=arr_new,
                                         sizes=[1, 3, 3, 1],
                                         strides=[1, 1, 1, 1],
                                         rates=[1, 1, 1, 1],
                                         padding='VALID')
    arr_table = arr_patch.numpy().reshape(-1, arr_patch.shape[-1])
    pred_X = arr_table[arr_table[:, 34] > -1000, :]
    pred_X = sample_normalize_1(pred_X.reshape(-1, 7)).reshape(-1, arr_table.shape[1])
    print(pred_X.shape)
    if pred_X.shape[0] > 0:
        model_y_pred = model.meta_model_predict_biascorr(pred_X)

        pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy().ravel(), np.nan, dtype="float32")
        pred_arr[arr_table[:, 34] > -1000] = model_y_pred.ravel()
        pred_arr = pred_arr.reshape((arr_patch.shape[1], arr_patch.shape[2]))
        print(f"pred_shape: {pred_arr.shape}")
    else:
        pred_arr = np.full_like(arr_patch[0, :, :, 0].numpy(), np.nan, dtype="float32")

    da = xr.DataArray(data=pred_arr, coords=[y1, x1], dims=['y', 'x'])
    out_file1 = f"{out_name}_{block_id[1]}_{block_id[2]}.nc"
    ds1 = xr.Dataset({"da": da})
    ds1.to_netcdf(out_file1)
    # np.save(out_file, pred_arr)

    return mask_arr


nbands = 7

pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([1.])
width = np.array([3])
dn_structure = [
    (12,),
]
epochs = [10, ]
res = [250, ]

for k in range(0, len(width)):

    path_training = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"
    path_test = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_1317_v0v3.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()


    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)

    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    # X_train = np.nanmean(X_train, axis=(1, 2))
    X_train = X_train.reshape(X_train.shape[0], -1)
    y_train = y_train.squeeze()
    # X_test1 = np.nanmean(X_test, axis=(1, 2))
    X_test1 = X_test.reshape(X_test.shape[0], -1)
    y_test = y_test.squeeze()

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test1, y_test)
    model_regress.save_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost250m_bc_output_v3_{res[0]}m"),
        file_name="{}_val.png".format(f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}"),
    )

    print(time.time() - start)

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_250m_median_1317_v0.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))
    da_x = da_mask.x.values
    da_y = da_mask.y.values

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_regress.load_models(outname=f"../output/rf250m_bc_output_v3_{res[0]}m_width{width[k]}.pkl")

    kwargs = {
        "chunksize": chunk_size,
        "xdata": da_x,
        "ydata": da_y,
        "model": model_regress,
        "out_name": f"../output/tmp/mch_tmpblocks",
    }
    # g = dskda.overlap.overlap(da_mask.data, depth={0: 0, 1: 1, 2: 1},
    #                           boundary={0: 'periodic', 1: 'periodic', 2: 'periodic'})
    # da_pred = g.map_blocks(func_predict_rf_1, dtype='float32', **kwargs)
    da_pred = dskda.map_overlap(da_mask, func_predict_rf_1, depth={0: 0, 1: 1, 2: 1},
                                boundary={0: 'nearest', 1: 'nearest', 2: 'nearest'},
                                dtype='float32', **kwargs)

    out_file1 = f"../output/mch_prediction_dask.zarr"
    delayed_obj = dskda.to_zarr(da_pred, out_file1, overwrite=True, compute=False)
    with ProgressBar():
         delayed_obj.compute()

    ds2 = xr.open_mfdataset(f"../output/tmp/mch_tmpblocks_*.nc",
                            chunks={'y': chunk_size, 'x': chunk_size},
                            combine='by_coords')

    out_file1 = f"../output/mch_prediction_{res[0]}m_width{width[k]}_from_xlyrs_250m_v0v3.nc"
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction_{res[0]}m_width{width[k]}_from_xlyrs_250m_v0v3.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)



# %% build vrt from stack tiff files
os.chdir("F:\\Projects\\a1902alostexture\\input")
file_list = glob.glob("xlyrs_250m_2010/*.tif")
in_file = "xlyrs_250m_2010_files.txt"
with open(in_file, "w") as f:
    for file in file_list:
        print(f"{file}", file=f)

out_file = "xlyrs_250m_median_2010_v0.vrt"
gdal_expression_01 = f'gdalbuildvrt -overwrite -input_file_list "{in_file}" "{out_file}" --config GDAL_CACHEMAX 2000'
subprocess.check_output(gdal_expression_01, shell=True)

# rt.build_stack_vrt(in_file, out_file)

print(1)

# %% training model (2010) - 7 bands from 250m lyrs (different neighboring widths)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


nbands = 7

pix_size = 0.00224578821
# offsets = 0.0005 + pix_size * np.array([0, 0.5, 1.5, 3.5, 7.5, 15.5])
# width = np.array([1, 2, 4, 8, 16, 32])
offsets = 0.0005 + pix_size * np.array([0.])
width = np.array([1])
dn_structure = [
    (12,),
    (12,),
    (12, 18),
    (12, 18),
    (12, 18, 24),
    (12, 18, 24, 16),
]
epochs = [10, 10, 10, 10, 10, 10]
res = [250, ]

for k in range(0, len(width)):
    # for k in range(0, len(width)):

    # df0 = dd.read_csv(f"csvs/mch_{res[0]}m_geo_v2_*.csv").compute()
    # df, df_test = train_test_split(df0, test_size=0.2, random_state=77)
    df = pd.read_csv("csvs/mch_250m_training_v0.csv")
    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv")

    path_tif = "xlyrs_250m_median_2010_v0.vrt"
    path_tfrecord = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2010_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )
    path_tfrecord = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2010_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_test,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )

    path_training = f"tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2010_v0v3.tfrecord"
    path_test = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2010_v0v3.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds = ds.map(sample_normalize)
    ds1 = ds.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds1.take(1)).next())

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()
    # print(iter(ds_test.take(1)).next())

    # CNN model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 7))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(32)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)

    m.compile(
        optimizer=tf.keras.optimizers.Adam(0.001),
        loss=tf.keras.losses.get("MeanSquaredError"),
        metrics=[tf.keras.metrics.get(metric) for metric in ["RootMeanSquaredError"]],
    )

    history = m.fit(
        x=ds1,
        epochs=epochs[k],
        steps_per_epoch=int(train_n / batch_size),
        validation_data=ds_test,
        validation_steps=int(test_n / batch_size),
    )
    plt.figure(figsize=(6, 4))
    plt.plot(history.history["root_mean_squared_error"], label="Training")
    plt.plot(history.history["val_root_mean_squared_error"], label="Validation")
    plt.xlabel("Epoch")
    plt.ylabel("RMSE")
    plt.legend(loc="upper right")
    plt.savefig(f"../output/cnn250m_output2010_v3_{res[0]}m_width{width[k]}_history.png", dpi=300)
    plt.show()
    m.save_weights(f"../output/cnn250m_output2010_v3_{res[0]}m_width{width[k]}_trained_weights.h5")

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)
    predictions = m.predict(X_test, verbose=1)
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        predictions[~np.isnan(y_test)],
        file_name=f"../output/cnn250m_output2010_v3_{res[0]}m_width{width[k]}_val.png",
    )

    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": predictions[~np.isnan(y_test)]})

    tf.keras.backend.clear_session()

    # compare with RF model using spatial mean of each band
    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):  # only take first element of dataset
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_train = np.nanmean(X_train, axis=(1, 2))
    y_train = y_train.squeeze()
    X_test1 = np.nanmean(X_test, axis=(1, 2))
    y_test = y_test.squeeze()

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test1, y_test)
    # model_regress.save_models(outname=f"xgboost250m_bc_output2010_v3_{res[0]}m.pkl")
    model_regress.save_models(outname=f"../output/rf250m_bc_output2010_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost250m_bc_output2010_v3_{res[0]}m"),
        file_name="{}_val.png".format(f"../output/rf250m_bc_output2010_v3_{res[0]}m_width{width[k]}"),
    )

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict(X_test1)
    # model_regress.save_models(outname=f"xgboost250m_output2010_v3_{res[0]}m.pkl")
    model_regress.save_models(outname=f"../output/rf250m_output2010_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        # file_name="{}_val.png".format(f"xgboost250m_output2010_v3_{res[0]}m"),
        file_name="{}_val.png".format(f"../output/rf250m_output2010_v3_{res[0]}m_width{width[k]}"),
    )
    dfy['y_rfbc'] = model_y_pred[~np.isnan(y_test)]

    # compare with CNN+linear model
    inputs = tf.keras.Input(batch_shape=(None, width[k], width[k], 7))
    mlayer = ga.DenseNet(
        num_layers_in_each_block=dn_structure[k],
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(32)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    m.load_weights(f"../output/cnn250m_output2010_v3_{res[0]}m_width{width[k]}_trained_weights.h5")
    mlayer_model = tf.keras.Model(inputs=m.input, outputs=m.get_layer("dense").output)

    X_list = []
    y_list = []
    for X, Y in ds1.take(int(train_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_train = np.concatenate(X_list)
    y_train = np.concatenate(y_list)
    X_mlayer_train = mlayer_model.predict(X_train, verbose=1)

    # X_list = []
    # y_list = []
    # for X, Y in ds_test.take(int(test_n / batch_size)):
    #     X_list.append(X.numpy())
    #     y_list.append(Y.numpy())
    # X_test = np.concatenate(X_list)
    # y_test = np.concatenate(y_list)
    X_mlayer_test = mlayer_model.predict(X_test, verbose=1)

    model_regress = ggm.StackRegressor(X_mlayer_test, y_test.ravel())
    model_pipe, params_grid = model_regress.setup_ridgecv_model()
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [100, 200, 400, 600],
    #         "learn__max_depth": [3, 6, 9, 12],
    #         # "learn__booster": ["gbtree","gblinear", "dart"],
    #         "learn__booster": ["dart"],
    #         "learn__colsample_bytree": [0.3, 0.5, 0.7, 0.9],
    #     }
    # )
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [300],
    #         # "learn__max_depth": [6, 9, 12],
    #         # "learn__min_samples_split": [2, 7, 12],
    #         "learn__max_depth": [9],
    #         "learn__min_samples_split": [2],
    #     }
    # )
    model_regress.grid_opts(model_pipe, params_grid, k=3)
    # model_regress.meta_model_biascorr(X=X_mlayer_train, y=y_train.ravel(), k=3)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_mlayer_test, y_test.ravel())
    model_regress.meta_model(X=X_mlayer_train, y=y_train.ravel(), k=3)
    model_y_pred = model_regress.meta_model_predict(X_mlayer_test)

    # model_regress.save_models(outname=f"../output/ridge250m_mlayer_model_v3_{res[0]}m.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    y_test = y_test.ravel()
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"../output/ridge250m_mlayer_model_v3_{res[0]}m_width{width[k]}"),
    )
    dfy = pd.DataFrame({"y_test": y_test[~np.isnan(y_test)], "y_predcnn": model_y_pred[~np.isnan(y_test)]})
    dfy["y_ridgetest"] = y_test[~np.isnan(y_test)]
    dfy.to_csv(f"../output/cnn250m_output2010_v3_{res[0]}m_width{width[k]}_val_ridge.csv")

    # dfy.to_csv(f"../output/cnn250m_output2010_v3_{res[0]}m_val_data.csv")
    tf.keras.backend.clear_session()

    print(time.time() - start)

print(1)

# %% predicting model (2010) - 7 bands from 250m lyrs (1x1 @250m)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\input")


# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [
                    3.2123085e-02,
                    3.1446800e-01,
                    1.7229147e-01,
                    6.1950065e-02,
                    2.2465163e-01,
                    7.6607764e-02,
                    5.6069208e02,
                ]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [
                    7.8484304e-03,
                    2.8701460e-02,
                    2.1761550e-02,
                    1.4474887e-02,
                    4.6037395e-02,
                    1.8193731e-02,
                    2.4210707e02,
                ]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [
            3.2123085e-02,
            3.1446800e-01,
            1.7229147e-01,
            6.1950065e-02,
            2.2465163e-01,
            7.6607764e-02,
            5.6069208e02,
        ]
    )[None, :]
    sample_std = np.array(
        [
            7.8484304e-03,
            2.8701460e-02,
            2.1761550e-02,
            1.4474887e-02,
            4.6037395e-02,
            1.8193731e-02,
            2.4210707e02,
        ]
    )[None, :]
    X = (X - sample_mean) / sample_std
    return X


def func_predict_rf_1(
        mask_arr,
        model=None,
):
    arr_table = mask_arr.reshape(mask_arr.shape[0], -1)
    pred_X = arr_table[:, arr_table[-1, :] > -1000].T
    pred_X = sample_normalize_1(pred_X)
    print(pred_X.shape)
    if pred_X.shape[0] > 0:
        model_y_pred = model.meta_model_predict_biascorr(pred_X)

        pred_arr = np.full_like(mask_arr[0, :, :].ravel(), np.nan, dtype="float32")
        pred_arr[arr_table[-1, :] > -1000] = model_y_pred.ravel()
        pred_arr = pred_arr.reshape((mask_arr.shape[1], mask_arr.shape[2]))
        print(f"pred_shape: {pred_arr.shape}")
    else:
        pred_arr = np.full_like(mask_arr[0, :, :], np.nan, dtype="float32")

    return pred_arr


nbands = 7

pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([0.])
width = np.array([1])
dn_structure = [
    (12,),
]
epochs = [10, ]
res = [250, ]

for k in range(0, len(width)):

    df_test = pd.read_csv("csvs/mch_250m_test_v0.csv")

    path_test = f"tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2010_v0v3.tfrecord"

    batch_size = 40
    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    dst = dst.map(sample_normalize)
    ds_test = dst.shuffle(100000).batch(batch_size).repeat()

    X_list = []
    y_list = []
    for X, Y in ds_test.take(int(test_n / batch_size)):
        X_list.append(X.numpy())
        y_list.append(Y.numpy())
    X_test = np.concatenate(X_list)
    y_test = np.concatenate(y_list)

    # X_test1 = np.nanmean(X_test, axis=(1, 2))
    X_test1 = X_test.reshape(X_test.shape[0], -1)
    y_test = y_test.squeeze()

    print(time.time() - start)

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_250m_median_2010_v0.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))
    da_x = da_mask.x.values
    da_y = da_mask.y.values

    model_regress = ggm.StackRegressor(X_test1, y_test)
    model_regress.load_models(outname=f"../output/rf250m_bc_output2010_v3_{res[0]}m_width{width[k]}.pkl")

    kwargs = {
        "model": model_regress,
    }
    da_pred = dskda.map_blocks(func_predict_rf_1, da_mask.data, dtype='float32', drop_axis=0, **kwargs)

    da1 = xr.DataArray(da_pred, coords=(da_y, da_x), dims=("y", "x"))
    out_file1 = f"../output/mch_prediction2010_{res[0]}m_width{width[k]}_from_xlyrs_250m_v0v3.nc"
    ds2 = xr.Dataset({"da": da1})
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"../output/mch_prediction2010_{res[0]}m_width{width[k]}_from_xlyrs_250m_v0v3.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)



# %% training model (2018) - 7 bands from 250m lyrs (different neighboring widths)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\drc")


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [14.837413, 71.68285, 57.943977, 24.164152, 6649.3877,
                 3876.6677, 562.8809]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [3.4911506, 6.124829, 6.7662935, 5.6472054, 745.73254,
                 530.1489, 250.08694]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


nbands = 7

pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([0.])
width = np.array([1])
res = [250, ]
for k in range(0, 1):
    df = pd.read_csv("../input/csvs/mch_250m_training_v0.csv")
    df_test = pd.read_csv("../input/csvs/mch_250m_test_v0.csv")

    path_tif = "drc_layers_250m_geo.vrt"
    path_tfrecord = f"../input/tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2018_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )
    path_tfrecord = f"../input/tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2018_v0v3.tfrecord"
    ga.regression_tfrecord_from_df(
        df_test,
        path_tif,
        path_tfrecord,
        x_cols="xvar",
        response="MCH",
        # loc=("Y", "X"),
        loc=("latitude", "longitude"),
        offset=(-offsets[k], offsets[k]),
        blocksize=(width[k], width[k]),
    )

    path_training = f"../input/tfrecords/training_{res[0]}m_width{width[k]}_from_xlyrs_2018_v0v3.tfrecord"
    path_test = f"../input/tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2018_v0v3.tfrecord"

    batch_size = 40
    ds, train_n = ga.load_tf(
        path_training,
        x_features="xvar",
        y_feature="MCH",
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds1 = (
        ds
        .cache()
        .shuffle(25000)
        .map(sample_normalize)
        .batch(batch_size)
        .prefetch(AUTOTUNE)
    )

    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds_test = (
        dst
        .map(sample_normalize)
        .batch(batch_size)
    )

    xlst = []
    ylst = []
    for element in ds_test.as_numpy_iterator():
        xlst.append(element[0])
        ylst.append(element[1])
    X_test = np.concatenate(xlst, axis=0)
    y_test = np.concatenate(ylst, axis=0)

    xlst = []
    ylst = []
    for element in ds1.as_numpy_iterator():
        xlst.append(element[0])
        ylst.append(element[1])
    X_train = np.concatenate(xlst, axis=0)
    y_train = np.concatenate(ylst, axis=0)

    X_train = np.nanmean(X_train, axis=(1, 2))
    y_train = y_train.squeeze()
    X_test = np.nanmean(X_test, axis=(1, 2))
    y_test = y_test.squeeze()

    model_regress = ggm.StackRegressor(X_test, y_test)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    model_regress.save_models(outname=f"rf250m_bc_output2018_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"rf250m_bc_output2018_v3_{res[0]}m_width{width[k]}"),
    )

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_y_pred = model_regress.meta_model_predict(X_test)
    model_regress.save_models(outname=f"rf250m_output2018_v3_{res[0]}m_width{width[k]}.pkl")
    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values
    r2, rmse, beta0 = ggm.density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(f"rf250m_output2018_v3_{res[0]}m_width{width[k]}"),
    )

    print(time.time() - start)

print(1)




# %% predicting model (2018) - 7 bands from 250m lyrs (1x1 @250m)
start = time.time()
os.chdir("F:\\Projects\\a1902alostexture\\gabon")


# os.chdir('/Users/xuliang/Documents/data/a1902_texture/input')


def sample_normalize(X, Y):
    sample_mean = tf.constant(
        [
            [
                [14.837413, 71.68285, 57.943977, 24.164152, 6649.3877,
                 3876.6677, 562.8809]
            ]
        ]
    )
    sample_std = tf.constant(
        [
            [
                [3.4911506, 6.124829, 6.7662935, 5.6472054, 745.73254,
                 530.1489, 250.08694]
            ]
        ]
    )
    X = (X - sample_mean) / sample_std
    # return X[:, :, 4:], Y
    return X[:, :, :], Y


def sample_normalize_1(X):
    sample_mean = np.array(
        [14.837413, 71.68285, 57.943977, 24.164152, 6649.3877,
         3876.6677, 562.8809]
    )[None, :]
    sample_std = np.array(
        [3.4911506, 6.124829, 6.7662935, 5.6472054, 745.73254,
         530.1489, 250.08694]
    )[None, :]
    X = (X - sample_mean) / sample_std
    return X


def func_predict_rf_1(
        mask_arr,
        model=None,
):
    arr_table = mask_arr.reshape(mask_arr.shape[0], -1)
    pred_X = arr_table[:, arr_table[-1, :] > -1000].T
    pred_X = sample_normalize_1(pred_X)
    print(pred_X.shape)
    if pred_X.shape[0] > 0:
        model_y_pred = model.meta_model_predict_biascorr(pred_X)

        pred_arr = np.full_like(mask_arr[0, :, :].ravel(), np.nan, dtype="float32")
        pred_arr[arr_table[-1, :] > -1000] = model_y_pred.ravel()
        pred_arr = pred_arr.reshape((mask_arr.shape[1], mask_arr.shape[2]))
        print(f"pred_shape: {pred_arr.shape}")
    else:
        pred_arr = np.full_like(mask_arr[0, :, :], np.nan, dtype="float32")

    return pred_arr


nbands = 7

pix_size = 0.00224578821
offsets = 0.0005 + pix_size * np.array([0.])
width = np.array([1])
dn_structure = [
    (12,),
]
epochs = [10, ]
res = [250, ]

for k in range(0, len(width)):

    df_test = pd.read_csv("../input/csvs/mch_250m_test_v0.csv")

    path_test = f"../input/tfrecords/test_{res[0]}m_width{width[k]}_from_xlyrs_2018_v0v3.tfrecord"

    batch_size = 40
    dst, test_n = ga.load_tf(
        path_test,
        x_features="xvar",
        y_feature="MCH",  # for v0, v2
        x_width=(width[k], width[k], nbands),
        y_width=(1,),
        compression="",
    )
    ds_test = (
        dst
        .map(sample_normalize)
        .batch(batch_size)
    )

    xlst = []
    ylst = []
    for element in ds_test.as_numpy_iterator():
        xlst.append(element[0])
        ylst.append(element[1])
    X_test = np.concatenate(xlst, axis=0)
    y_test = np.concatenate(ylst, axis=0)
    X_test = np.nanmean(X_test, axis=(1, 2))
    y_test = y_test.squeeze()

    print(time.time() - start)

    # prediction
    chunk_size = 1000
    path_tif = "xlyrs_2019/gabon_layers_250m_geo.vrt"
    da_mask = xr.open_rasterio(path_tif, chunks=(-1, chunk_size, chunk_size))
    da_x = da_mask.x.values
    da_y = da_mask.y.values

    model_regress = ggm.StackRegressor(X_test, y_test)
    model_regress.load_models(outname=f"../drc/rf250m_bc_output2018_v3_{res[0]}m_width{width[k]}.pkl")

    kwargs = {
        "model": model_regress,
    }
    da_pred = dskda.map_blocks(func_predict_rf_1, da_mask.data, dtype='float32', drop_axis=0, **kwargs)

    da1 = xr.DataArray(da_pred, coords=(da_y, da_x), dims=("y", "x"))
    out_file1 = f"mch_prediction2018_gabon_250m_v0v3.nc"
    ds2 = xr.Dataset({"da": da1})
    # ds2.to_netcdf(out_file1)
    delayed_obj = ds2.to_netcdf(out_file1, compute=False)
    with ProgressBar():
        results = delayed_obj.compute()

    out_file2 = f"mch_prediction2018_gabon_250m_v0v3.tif"
    gdal_cmmd = (
        f"gdal_translate -of GTiff -ot Float32 -co COMPRESS=LZW -co PREDICTOR=2 "
        f'-a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'NETCDF:"{out_file1}":da "{out_file2}"'
    )
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(out_file1)

    print(time.time() - start)
