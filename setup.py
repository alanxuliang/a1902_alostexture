"""
Setup a1902_alostexture package.
"""

from setuptools import setup, find_packages

setup(
    name='a1902alos',
    version='0.1.1',
    description='ALOS Texture Learning',
    url='https://gitlab.com/alanxuliang/',

    author='Alan Xu',
    author_email='bireme@gmail.com',

    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: GIS',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='ALOS',
    # package_dir={'': 'src'},
    # packages=find_packages(where='src', exclude=['data', 'docs', 'tests']),
    packages=find_packages(exclude=['data', 'tests']),

    install_requires=[
        'numpy',
        'scipy',
        'pandas',
        'scikit-learn',
        'dask',
        'xarray',
        'zarr',
        'learn2map',
        'lightgbm',
        'cartopy',
    ],

)
