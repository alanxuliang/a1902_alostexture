# import modules
import os
import ee
import sys
import osr
import time
import gdal
import h5py
import pyproj
import subprocess
from concurrent import futures
from pathlib import Path
import numpy as np
import xarray as xr
import dask.array as dskda
import folium
import dask.dataframe as dd
import tensorflow as tf
import learn2map.raster_tools as rt
import zarr
from zarr import blosc
compressor = blosc.Blosc(cname="lz4", clevel=6, shuffle=0)


def raster_resize(in_file, new_resolution):
    in_noext = os.path.splitext(in_file)[0]
    in_newres = f"{in_noext}_res{new_resolution}.tif"
    gdal_expression = (
        f"gdalwarp -ot Float32 -of GTiff -overwrite -co COMPRESS=LZW -co PREDICTOR=3 "
        f'-tr {new_resolution} {new_resolution} "{in_file}" "{in_newres}"'
    )
    subprocess.check_output(gdal_expression, shell=True)


def raster_resample(mch_file, ref_file, mch_newres, mch_count_newres, v_min=0, v_max=9999):
    if os.name == "nt":
        prefix = (
            subprocess.check_output("where python", shell=True)
            .decode("utf-8")
            .split("python")
        )
        cmmd_prefix = "python " + prefix[0] + "Scripts\\"
    elif os.name == "posix":
        cmmd_prefix = ""
    else:
        cmmd_prefix = ""

    mch_noext = os.path.splitext(mch_file)[0]

    cmmd = f'{cmmd_prefix}gdal_edit.py -unsetnodata "{mch_file}"'
    subprocess.check_output(cmmd, shell=True)

    mch_count = f"{mch_noext}_tf.tif"
    cmmd = (
        f'{cmmd_prefix}gdal_calc.py --type=Float32 --co="COMPRESS=LZW" -A "{mch_file}" '
        f'--overwrite --outfile="{mch_count}" --calc="(A>{v_min})*(A<{v_max})*1.0"'
    )
    subprocess.check_output(cmmd, shell=True)

    cmmd = f'{cmmd_prefix}gdal_edit.py -a_nodata "nan" "{mch_file}"'
    subprocess.check_output(cmmd, shell=True)

    # mch_count_newres = f"{mch_noext}_tf_reg.tif"
    rt.raster_clip(
        ref_file,
        mch_count,
        mch_count_newres,
        resampling_method="average",
        srcnodata=None,
        dstnodata=None,
    )
    os.remove(mch_count)

    # mch_newres = f"{mch_noext}_reg.tif"
    rt.raster_clip(ref_file, mch_file, mch_newres, resampling_method="average")
    return mch_newres, mch_count_newres


def raster_mask(mch_file, mch_count, mch_out, valid_min=0.8):
    if os.name == "nt":
        prefix = (
            subprocess.check_output("where python", shell=True)
            .decode("utf-8")
            .split("python")
        )
        cmmd_prefix = "python " + prefix[0] + "Scripts\\"
    elif os.name == "posix":
        cmmd_prefix = ""
    else:
        cmmd_prefix = ""

    cmmd = (
        f'{cmmd_prefix}gdal_calc.py --type=Float32 --co="COMPRESS=LZW" '
        f' -A "{mch_file}" -B "{mch_count}" --overwrite '
        f'--outfile="{mch_out}" --calc="(B>{valid_min})*A"'
    )
    subprocess.check_output(cmmd, shell=True)


def batch_prediction(buffer, model=None, valid_min=0.8):
    dim0 = buffer.shape
    arr0 = buffer.reshape(dim0[0]*dim0[1], dim0[2])
    id0 = arr0[:, 0] > valid_min
    y0 = np.full_like(arr0[:, 0], np.nan)
    X = arr0[id0, 1:]
    if model.best_models is not None:
        y1 = model.meta_model_predict(X)
    else:
        y1 = np.zeros_like(arr0[:, 0])
    y0[id0] = y1
    Y = y0.reshape(dim0[0], dim0[1])
    return Y


def raster_predict(path_tif, path_mask, out_name, model, depth=3, valid_min=0.8):
    daX = xr.open_rasterio(path_tif, chunks=[-1, 4096, 4096])
    da_mask = xr.open_rasterio(path_mask, chunks=[1, 4096, 4096])
    da_all = xr.concat([da_mask, daX], dim='band')
    da_all = da_all.transpose('y', 'x', 'band')
    y = da_all.data.map_overlap(lambda buffer: batch_prediction(buffer, model=model, valid_min=valid_min), depth=depth, boundary=np.nan)
    da_y = xr.DataArray(y, coords=da_mask.coords, dims=da_mask.dims)
    ds2 = xr.Dataset({"da": da_y})
    ds2.to_netcdf(f"{out_name}.nc", mode="w")
    gdal_cmmd = (
        f'gdal_translate -of GTiff -ot Float32 -a_srs "+proj=longlat +datum=WGS84 +no_defs" '
        f'-co "COMPRESS=LZW" -co "PREDICTOR=2" NETCDF:"{out_name}.nc":da "{out_name}.tif"')
    subprocess.check_output(gdal_cmmd, shell=True)
    os.remove(f"{out_name}.nc")


def raster_to_csv(mch_files, out_name, band_list=["MCH"], stack_axis=0, valid_min=0.0):
    da_lst = []
    for i in range(len(mch_files)):
        daX = xr.open_rasterio(mch_files[i], chunks=[1, 3000, 3000])
        daX_trans = daX.data.reshape([daX.data.shape[0], -1]).transpose()
        if i==0:
            da_mask = daX_trans[:, 0] > valid_min
            z_mask = daX.data[0, :, :] > valid_min
            mask0 = da_mask
            z0 = z_mask
        else:
            da_mask = mask0
            z_mask = z0
        data1 = daX_trans[da_mask, :]
        # data1 = daX.data.ravel()[daX.data.ravel() > valid_min]
        data1.compute_chunk_sizes()
        data1 = data1.rechunk((5000000, 1))
        id = dskda.argwhere(z_mask)
        id.compute_chunk_sizes()
        id = id.rechunk((5000000, 2))
        X1 = dskda.from_array(daX.x.data, (5000000,))[id[:, 1]].reshape(-1, 1)
        Y1 = dskda.from_array(daX.y.data, (5000000,))[id[:, 0]].reshape(-1, 1)
        if (stack_axis==0) or (i==0):
            # da_all = dskda.stack([Y1, X1, data1], axis=-1)
            da_all = dskda.concatenate([Y1, X1, data1], axis=-1)
        else:
            da_all = data1
        da_lst.append(da_all)
    da_array = dskda.concatenate(da_lst, axis=stack_axis)

    col_names = ["latitude", "longitude"] + band_list
    df_valid = dd.from_dask_array(da_array, columns=col_names)
    df_valid.to_csv('{}_*.csv'.format(out_name))

    return None


def raster_to_vector(mch_file, mch_count, out_name, band_list=["MCH"], valid_min=0.8):
    daX = xr.open_rasterio(mch_file, chunks=[-1, 2048, 2048])
    daX["band"] = ("band", band_list)
    da_mask = xr.open_rasterio(mch_count, chunks=[1, 2048, 2048])
    da_mask["band"] = ("band", ["mask"])

    shape0 = daX.shape
    da_y = dskda.from_array(daX.y.data, (50000,)).reshape(-1, 1)
    da_x = dskda.from_array(daX.x.data, (50000,)).reshape(1, -1)
    # da_y2 = dskda.repeat(da_y, shape0[2], axis=1).reshape(-1, 1).rechunk((50000, -1))
    # da_x2 = dskda.repeat(da_x, shape0[1], axis=0).reshape(-1, 1).rechunk((50000, -1))
    # da_data = daX.data.reshape(daX.shape[0], -1).transpose().rechunk((50000, -1))
    # da_data2 = da_mask.data.reshape(da_mask.shape[0], -1).transpose().rechunk((50000, -1))
    # da_all = dskda.concatenate([da_y2, da_x2, da_data, da_data2], axis=1).rechunk((50000, -1))
    da_y2 = dskda.repeat(da_y, shape0[2], axis=1).reshape(-1, 1)
    da_x2 = dskda.repeat(da_x, shape0[1], axis=0).reshape(-1, 1)
    da_data = daX.data.reshape(daX.shape[0], -1).transpose()
    da_data2 = da_mask.data.reshape(da_mask.shape[0], -1).transpose()
    da_all = dskda.concatenate([da_y2, da_x2, da_data, da_data2], axis=1)
    col_names = ["y", "x"] + daX["band"].values.tolist() + da_mask["band"].values.tolist()
    print(da_all.shape)

    df1 = dd.from_dask_array(da_all, columns=col_names)
    # df1 = da_all.to_dask_dataframe(columns=col_names).repartition(partition_size="200MB")
    df2 = df1[df1[col_names[-1]] > valid_min].iloc[:, :-1].repartition(partition_size="200MB")

    # df2 = df2.compute()
    # in0 = gdal.Open(mch_file)
    # prj0 = in0.GetProjection()
    # in0 = None
    # srs0 = osr.SpatialReference()
    # srs0.ImportFromWkt(prj0)
    # srs0_str = srs0.ExportToProj4()
    # utm_s = pyproj.Proj(srs0_str)
    # lon, lat = utm_s(df2.x.values, df2.y.values, inverse=True)
    # # wgs84 = pyproj.Proj("epsg:4326")
    # # transformer = pyproj.Transformer.from_proj(utm_s, wgs84)
    # # lon, lat = transformer(df2.x.values, df2.y.values)
    # df2["longitude"] = lon
    # df2["latitude"] = lat

    # df2.to_csv(f"{out_name}_*.csv", index=False)
    dd.to_parquet(df2, f"{out_name}.pqt", write_index=False)
    daX.close()
    da_mask.close()
    return df2


def raster_to_vector_2(mch_file, mch_count, out_name, band_list=["MCH"], valid_min=0.8):
    daX = xr.open_rasterio(mch_file, chunks=[-1, 2048, 2048])
    daX["band"] = ("band", band_list)
    da_mask = xr.open_rasterio(mch_count, chunks=[1, 2048, 2048])
    da_mask["band"] = ("band", ["mask"])

    shape0 = daX.shape
    da_data = daX.data.reshape(daX.shape[0], -1).transpose().rechunk((500000, -1))
    da_data2 = da_mask.data.reshape(-1, ).rechunk((500000,))
    da_y = dskda.from_array(daX.y.data, (500000,)).reshape(-1, 1)
    da_x = dskda.from_array(daX.x.data, (500000,)).reshape(1, -1)
    da_y2 = dskda.repeat(da_y, shape0[2], axis=1).reshape(-1, 1).rechunk((500000, -1))
    da_x2 = dskda.repeat(da_x, shape0[1], axis=0).reshape(-1, 1).rechunk((500000, -1))
    da_all = dskda.concatenate([da_y2, da_x2, da_data], axis=1).rechunk((500000, -1))
    col_names = ["y", "x"] + daX["band"].values.tolist()
    print(da_all)

    x_valid = da_all[da_data2 > valid_min, :]
    # x_valid.compute_chunk_sizes()
    x_valid.to_hdf5(f'{out_name}.hdf5', '/da')
    fid = h5py.File(f'{out_name}.hdf5')['/da']
    x_valid = dskda.from_array(fid, chunks=(500000, -1))
    # x_valid.to_zarr(
    #     f'{out_name}.zarr',
    #     mode="w",
    #     synchronizer=zarr.ThreadSynchronizer(),
    #     compressor=compressor,
    # )
    # x_valid = dskda.from_zarr(f'{out_name}.zarr')

    df2 = x_valid.to_dask_dataframe(columns=col_names)

    df2.to_csv(f"{out_name}_*.csv", index=False)
    daX.close()
    da_mask.close()
    return df2


def extract_xytable_func(x, valid_min=0):
    x1 = x.reshape(-1, x.shape[-1])
    x_valid = x1[x1[:, -1] > valid_min, :-1]
    return x_valid


def raster_to_vector_map(mch_file, mch_count, out_name, band_list=["MCH"], valid_min=0.8):
    da_mask = xr.open_zarr(mch_count)['da']
    daX = xr.open_zarr(mch_file)['da']
    b1 = xr.ones_like(daX.band)
    x1 = xr.ones_like(daX.x)
    y1 = xr.ones_like(daX.y)
    y2 = b1 * daX.y * x1.chunk(40, )
    y2 = y2.chunk((1, 3000, 3000))
    x2 = b1 * y1.chunk(40, ) * daX.x
    x2 = x2.chunk((1, 3000, 3000))
    da_all = xr.concat([y2, x2, daX, da_mask], dim="band").chunk((-1, 3000, 3000)).transpose('y', 'x', 'band')
    col_names = ["Y", "X"] + band_list
    da_all["band"] = ("band", col_names + ["mask"])

    print(da_all)

    da2 = dskda.map_blocks(
        lambda inputx: extract_xytable_func(inputx, valid_min=valid_min),
        da_all.data,
        dtype=np.float_,
        chunks=(5000, da_all.shape[2]-1),
        drop_axis=(0, 1),
        new_axis=0,
    )

    df2 = da2.to_dask_dataframe(columns=col_names)

    df2.to_csv(f"{out_name}_*.csv", index=False)
    daX.close()
    da_mask.close()
    return df2


class ParallelList:
    def __init__(self):
        self.flist = []

    def __call__(self, r):
        self.flist.append(r.result())


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _float_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def serialize_example(
    multi_array, response_data, x_cols="X", y_col="Y", ml_type="regression"
):
    features = {x_cols: _float_feature(multi_array.ravel())}
    if ml_type == "regression":
        features[y_col] = _float_feature([response_data])
    else:
        features[y_col] = _int64_feature([response_data])
    # print(features)

    example_proto = tf.train.Example(features=tf.train.Features(feature=features))
    return example_proto.SerializeToString()


def regression_tfrecord_from_df(
    df,
    path_tif,
    path_tfrecord,
    bands=[3,4,6,7],
    x_cols="xvar",
    response="NA",
    loc=("Y", "X"),
    offset=(0, 0),
    blocksize=(1, 1),
):
    y1 = df[loc[0]]  # loc[0]: y or lat
    x1 = df[loc[1]]  # loc[1]: x or lon
    if response == "NA":
        y_data = np.full(y1.shape, np.nan)
    else:
        y_data = df[response].values
    y_width = blocksize[0]
    x_width = blocksize[1]
    y_offset = y1.values - offset[0]
    x_offset = x1.values - offset[1]

    if (os.path.splitext(path_tif)[1]==".tif") or (os.path.splitext(path_tif)[1]==".vrt"):
        da1 = xr.open_rasterio(path_tif, chunks=(-1, 3000, 3000))
    else:
        da1 = xr.open_zarr(path_tif)['da']
    da1y = da1.y.values
    da1x = da1.x.values
    nbands = len(bands)

    with tf.io.TFRecordWriter(path_tfrecord) as writer:
        for i in np.arange(df.shape[0]):
            dy0 = np.abs(da1y - y1.values[i]).min()
            dx0 = np.abs(da1x - x1.values[i]).min()
            if (dy0 > np.abs(offset[0])) or (dx0 > np.abs(offset[1])):
                # print(f"Record {i} not in file: {path_tif}")
                continue
            else:
                id1y = np.abs(da1y - y_offset[i]).argmin()
                id1x = np.abs(da1x - x_offset[i]).argmin()
                id2y = id1y + y_width
                id2x = id1x + x_width
                id1y = max(0, id1y)
                id1x = max(0, id1x)
                id2y = min(da1y.shape[0], id2y)
                id2x = min(da1x.shape[0], id2x)
                img = (
                    da1.isel(y=slice(id1y, id2y), x=slice(id1x, id2x)).transpose(
                        "y", "x", "band"
                    )
                    # .chunk((-1, -1, -1))
                )
                multi_array = np.zeros([y_width, x_width, nbands], 'float32')
                multi_array[: img.shape[0], : img.shape[1], :] = img.values[:, :, bands]
                # if i in [0, 1000, 10000, 30000]:
                #     print(f"{y_data[i, ]}")
                #     print(f"{img.values}")
                #     print(f"X shape: {img.data.shape}")
                example = serialize_example(multi_array, y_data[i], x_cols, response)
                writer.write(example)

    return None


def to_tuple(inputs, x_features, y_feature):
    # def to_tuple(inputs, x_features, y_feature):
    """
    col 0: X
    col 1: Y
    :param inputs: 
    :return: 
    """
    # keys = list(inputs.keys())
    # print(keys)
    X = inputs.get(x_features)
    Y = inputs.get(y_feature)
    # X = inputs.get("X")
    # Y = inputs.get("MCH")
    return X, Y


def load_tf(
    path_tfrecord,
    x_features="X",
    y_feature="Y",
    x_width=(1, 1),
    y_width=(1,),
    compression="",
):
    glob = tf.io.gfile.glob(path_tfrecord)
    dataset = tf.data.TFRecordDataset(glob, compression_type=compression)
    feature_columns = {
        x_features: tf.io.FixedLenFeature(x_width, dtype=tf.float32),
        y_feature: tf.io.FixedLenFeature(y_width, dtype=tf.float32),
    }
    # features = x_features + y_feature
    # columns = [
    #     tf.io.FixedLenFeature(x_width, dtype=tf.float32),
    #     tf.io.FixedLenFeature(y_width, dtype=tf.float32),
    # ]
    # feature_columns = dict(zip(features, columns))
    parsed = dataset.map(
        lambda example: tf.io.parse_single_example(example, feature_columns),
        num_parallel_calls=4,
    )
    parsed = parsed.map(
        lambda example: (example.get(x_features), example.get(y_feature)),
        num_parallel_calls=4,
    )
    # parsed = parsed.map(
    #     lambda example: to_tuple(example, x_features, y_feature), num_parallel_calls=4
    # )
    # parsed = dataset.map(parse_tfrecord, num_parallel_calls=4)
    # parsed = parsed.map(to_tuple, num_parallel_calls=4)

    # print(parsed.take(0))

    records_n = sum(1 for _ in dataset)
    print("records_n = {}".format(records_n))
    return parsed, records_n


def densenet_model(dn_structure, width, nbands):
    inputs = tf.keras.Input(batch_shape=(None, width, width, nbands))
    mlayer = DenseNet(
        num_layers_in_each_block=dn_structure,
        growth_rate=8,
        dropout_rate=0.2,
        weight_decay=1e-4,
        compression=0.5,
        bottleneck=True,
        include_top=False,
    )(inputs)
    m2 = tf.keras.layers.GlobalAveragePooling2D()(mlayer)
    m2 = tf.keras.layers.Dense(64)(m2)
    outputs = tf.keras.layers.Dense(1)(m2)
    m = tf.keras.Model(inputs, outputs)
    return m


def trainreg_from_tfrecord(path_tfrecord):
    ds = load_tf(
        path_tfrecord,
        x_features="X",
        y_feature="MCH",
        x_width=(1, 1, 7),
        y_width=(1,),
        compression="",
    )
    ds = ds.shuffle(100000).batch(50).repeat()
    return None


EE_TILES = (
    "https://earthengine.googleapis.com/map/{mapid}/{{z}}/{{x}}/{{y}}?token={token}"
)


def map_display(center, geometry, tiles="OpenStreetMap", zoom_start=10):
    """
    Display ee.Features and ee.Images using folium.
    :param center: Center of the map (Latitude and Longitude).
    :param geometry: Earth Engine Geometries
    :param tiles: Map tileset to use.
    :param zoom_start: Initial zoom level for the map.
    :return: A folium.Map object.
    """
    mapViz = folium.Map(location=center, tiles=tiles, zoom_start=zoom_start)
    for k, v in geometry.items():
        if ee.image.Image in [type(x) for x in v.values()]:
            folium.TileLayer(
                tiles=EE_TILES.format(**v),
                attr="Google Earth Engine",
                overlay=True,
                name=k,
            ).add_to(mapViz)
        else:
            folium.GeoJson(data=v, name=k).add_to(mapViz)
    mapViz.add_child(folium.LayerControl())
    return mapViz


def l8sr_mask_clouds(image):
    flag1 = ee.Number(2).pow(3).int()
    flag2 = ee.Number(2).pow(5).int()
    flag3 = ee.Number(2).pow(7).int()
    flag4 = ee.Number(2).pow(9).int()
    img = image.updateMask(
        image.select(["pixel_qa"]).bitwiseAnd(flag1).eq(0)
        .And(image.select(["pixel_qa"]).bitwiseAnd(flag2).eq(0))
        .And(image.select(["pixel_qa"]).bitwiseAnd(flag3).eq(0))
        .And(image.select(["pixel_qa"]).bitwiseAnd(flag4).eq(0))
    )
    return img


def l7sr_mask_clouds(image):
    flag1 = ee.Number(2).pow(3).int()
    flag2 = ee.Number(2).pow(5).int()
    flag3 = ee.Number(2).pow(7).int()
    img = image.updateMask(
        image.select(["pixel_qa"]).bitwiseAnd(flag1).eq(0)
        .And(image.select(["pixel_qa"]).bitwiseAnd(flag2).eq(0))
        .And(image.select(["pixel_qa"]).bitwiseAnd(flag3).eq(0))
    )
    return img


def l8toa_mask_clouds(image):
    scored = ee.Algorithms.Landsat.simpleCloudScore(image)
    img = image.updateMask(
        scored.select(["cloud"]).lt(20).And(image.select(["B5"]).gt(0))
    )
    return img


def m43_mask_clouds(image):
    img = image.updateMask(image.select(["Nadir_Reflectance_Band2"]).gt(0))
    # flag1 = ee.Number(2).pow(0).int()
    # img = image.updateMask(
    #     image.select(["BRDF_Albedo_Band_Mandatory_Quality_Band2"])
    #     .bitwiseAnd(flag1)
    #     .eq(0)
    #     # .And(
    #     #     image.select(["BRDF_Albedo_Band_Mandatory_Quality_Band1"])
    #     #     .bitwiseAnd(flag1)
    #     #     .eq(0)
    #     # )
    # )
    return img


def m09_mask_clouds(image):
    flag1 = ee.Number(2).pow(0).int()
    flag2 = ee.Number(2).pow(1).int()
    img = image.updateMask(
        image.select(["QC_500m"]).bitwiseAnd(flag1).eq(0)
        .And(image.select(["QC_500m"]).bitwiseAnd(flag2).eq(0))
        .And(image.select(["SensorZenith"]).lt(1000))
    )
    return img


def proba_mask_clouds(image):
    flag1 = ee.Number(2).pow(0).int()
    flag2 = ee.Number(2).pow(1).int()
    flag3 = ee.Number(2).pow(2).int()
    img = image.updateMask(
        image.select(['SM']).bitwiseAnd(flag1).eq(0)
        .And(
            image.select(['SM']).bitwiseAnd(flag2).eq(0)
        ).And(
            image.select(['SM']).bitwiseAnd(flag3).eq(0)
        # ).And(
        #     image.select(['VNIRVZA']).lt(50)
        )
    )
    return img


class GeeCase:
    def __init__(self, feature_ref, feature_train, feature_test=None):
        try:
            ee.Initialize()
            print("The Earth Engine package initialized successfully!")
        except ee.EEException as e:
            print("The Earth Engine package failed to initialize!")
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        self.ee_ref = ee.Image(feature_ref)
        self.ee_train = ee.FeatureCollection(feature_train)
        if feature_test is not None:
            self.ee_test = ee.FeatureCollection(feature_test)
            self.ee_all = self.ee_train.merge(self.ee_test)
        else:
            self.ee_test = None
            self.ee_all = self.ee_train

        pts = self.ee_ref.getInfo()["properties"]["system:footprint"]["coordinates"]
        self.bounds = ee.Geometry.Polygon(pts)

        self.center = self.ee_train.geometry().centroid().getInfo()["coordinates"]
        self.center.reverse()
        print(f"Lat: {self.center[0]:.2f}; Lon: {self.center[1]:.2f}")

    def get_feature_tiles(self, tile_scale=4):
        feature_bounds = self.ee_all.geometry().bounds()
        fb_coords = np.array(feature_bounds.getInfo()["coordinates"])[0, :, :]
        dim_lon = np.array([fb_coords[:, 0].min(), fb_coords[:, 0].max()])
        dim_lon = dim_lon + [-np.ptp(dim_lon) * 0.02, np.ptp(dim_lon) * 0.02]
        dim_lat = np.array([fb_coords[:, 1].min(), fb_coords[:, 1].max()])
        dim_lat = dim_lat + [-np.ptp(dim_lat) * 0.02, np.ptp(dim_lat) * 0.02]
        print(dim_lat)
        print(dim_lon)

        grid_x, grid_y = np.meshgrid(
            np.linspace(dim_lon[0], dim_lon[1], tile_scale + 1),
            np.linspace(dim_lat[0], dim_lat[1], tile_scale + 1),
        )
        tile_list = []
        for i in range(tile_scale):
            for j in range(tile_scale):
                sub_coords = [
                    grid_x[i, j],
                    grid_y[i, j],
                    grid_x[i + 1, j + 1],
                    grid_y[i + 1, j + 1],
                ]
                # sub_bounds = ee.Geometry.Rectangle(sub_coords)
                # tile_list.append(sub_bounds)
                tile_list.append(sub_coords)
        return tile_list

    def get_l8sr(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ["B4", "B5", "B6", "B7"]
        m43_bands = [
            "Nadir_Reflectance_Band1",
            "Nadir_Reflectance_Band2",
            "Nadir_Reflectance_Band6",
            "Nadir_Reflectance_Band7",
        ]
        lc8 = (
            ee.ImageCollection("LANDSAT/LC08/C01/T1_SR")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(l8sr_mask_clouds)
            .median()
            .select(l8_bands)
            .multiply(0.0001)
        )
        nbar = (
            ee.ImageCollection("MODIS/006/MCD43A4")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(m43_mask_clouds)
            .median()
            .select(m43_bands)
            .rename(l8_bands)
            .multiply(0.0001)
        )
        lc8 = lc8.unmask(nbar)
        nbar_fmean = nbar.focal_mean(radius=1500, units="meters")
        lc8_fmean = lc8.focal_mean(radius=1500, units="meters")
        lc8_nbar = lc8.expression(
            "TOA / A * B", {"TOA": lc8, "A": lc8_fmean, "B": nbar_fmean}
        ).toFloat()

        return lc8_nbar

    def get_l7sr(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ["B3", "B4", "B5", "B7"]
        m43_bands = [
            "Nadir_Reflectance_Band1",
            "Nadir_Reflectance_Band2",
            "Nadir_Reflectance_Band6",
            "Nadir_Reflectance_Band7",
        ]
        lc8 = (
            ee.ImageCollection("LANDSAT/LE07/C01/T1_SR")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(l7sr_mask_clouds)
            .median()
            .select(l8_bands)
            .multiply(0.0001)
        )
        nbar = (
            ee.ImageCollection("MODIS/006/MCD43A4")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(m43_mask_clouds)
            .median()
            .select(m43_bands)
            .rename(l8_bands)
            .multiply(0.0001)
        )
        lc8 = lc8.unmask(nbar)
        nbar_fmean = nbar.focal_mean(radius=2500, units="meters")
        lc8_fmean = lc8.focal_mean(radius=2500, units="meters")
        lc8_nbar = lc8.expression(
            "TOA / A * B", {"TOA": lc8, "A": lc8_fmean, "B": nbar_fmean}
        ).toFloat()

        return lc8_nbar

    def get_l5sr(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ["B3", "B4", "B5", "B7"]
        m43_bands = [
            "Nadir_Reflectance_Band1",
            "Nadir_Reflectance_Band2",
            "Nadir_Reflectance_Band6",
            "Nadir_Reflectance_Band7",
        ]
        lc8 = (
            ee.ImageCollection("LANDSAT/LT05/C01/T1_SR")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(l7sr_mask_clouds)
            .median()
            .select(l8_bands)
            .multiply(0.0001)
        )
        nbar = (
            ee.ImageCollection("MODIS/006/MCD43A4")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(m43_mask_clouds)
            .median()
            .select(m43_bands)
            .rename(l8_bands)
            .multiply(0.0001)
        )
        # lc8 = lc8.unmask(nbar)
        nbar_fmean = nbar.focal_mean(radius=2500, units="meters")
        lc8_fmean = lc8.focal_mean(radius=2500, units="meters")
        lc8_nbar = lc8.expression(
            "TOA / A * B", {"TOA": lc8, "A": lc8_fmean, "B": nbar_fmean}
        ).toFloat()

        return lc8_nbar

    def get_nbar(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ["B3", "B4", "B5", "B7"]
        m43_bands = [
            "Nadir_Reflectance_Band1",
            "Nadir_Reflectance_Band2",
            "Nadir_Reflectance_Band6",
            "Nadir_Reflectance_Band7",
        ]
        nbar = (
            ee.ImageCollection("MODIS/006/MCD43A4")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(m43_mask_clouds)
            .median()
            .select(m43_bands)
            .rename(l8_bands)
            .multiply(0.0001)
        )
        lc8_nbar = nbar.toFloat()

        return lc8_nbar

    def get_mod09(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ["B3", "B4", "B5", "B7"]
        m43_bands = [
            "sur_refl_b01",
            "sur_refl_b02",
            "sur_refl_b06",
            "sur_refl_b07",
        ]
        nbar = (
            ee.ImageCollection("MODIS/006/MOD09GA")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(m09_mask_clouds)
            .median()
            .select(m43_bands)
            .rename(l8_bands)
            .multiply(0.0001)
        )
        lc8_nbar = nbar.toFloat()

        return lc8_nbar

    def get_probav(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ['BLUE', 'RED', 'NIR', 'SWIR']
        nbar = (
            ee.ImageCollection('VITO/PROBAV/C1/S1_TOC_100M')
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(proba_mask_clouds)
            .median()
            .select(l8_bands)
            .multiply(0.0005)
        )
        lc8_nbar = nbar.toFloat()

        return lc8_nbar

    def get_l8toa(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ["B4", "B5", "B6", "B7"]
        m43_bands = [
            "Nadir_Reflectance_Band1",
            "Nadir_Reflectance_Band2",
            "Nadir_Reflectance_Band6",
            "Nadir_Reflectance_Band7",
        ]
        lc8 = (
            ee.ImageCollection("LANDSAT/LC08/C01/T1_TOA")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(l8toa_mask_clouds)
            .median()
            .select(l8_bands)
            .multiply(0.0001)
        )
        nbar = (
            ee.ImageCollection("MODIS/006/MCD43A4")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .map(m43_mask_clouds)
            .median()
            .select(m43_bands)
            .rename(l8_bands)
            .multiply(0.0001)
        )
        nbar_fmean = nbar.focal_mean(radius=2500, units="meters")
        lc8_fmean = lc8.focal_mean(radius=2500, units="meters")
        lc8_nbar = lc8.expression(
            "TOA / A * B", {"TOA": lc8, "A": lc8_fmean, "B": nbar_fmean}
        ).toFloat()

        return lc8_nbar

    def get_alos(self, start_date, end_date, bounds=None):
        if bounds is not None:
            a_bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            a_bounds = self.bounds
        a_bands = ["HH", "HV"]
        a = (
            ee.ImageCollection("JAXA/ALOS/PALSAR/YEARLY/SAR")
            .filterBounds(a_bounds)
            .filterDate(start_date, end_date)
            .median()
            .select(a_bands)
            .toDouble()
            .pow(2)
            .divide(199526231)
            .toFloat()
        )
        return a

    def get_srtm(self, bounds=None):
        if bounds is not None:
            a_bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            a_bounds = self.bounds
        a_bands = ["elevation"]
        a = ee.Image("USGS/SRTMGL1_003").clip(a_bounds).select(a_bands).toFloat()
        return a

    def get_cgls_vcf(self, start_date, end_date, bounds=None):
        if bounds is not None:
            l8bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            l8bounds = self.bounds
        l8_bands = ['tree-coverfraction']
        nbar = (
            ee.ImageCollection("COPERNICUS/Landcover/100m/Proba-V/Global")
            .filterBounds(l8bounds)
            .filterDate(start_date, end_date)
            .median()
            .select(l8_bands)
        )
        lc8_nbar = nbar.toFloat()
        return lc8_nbar

    def save_batches(
        self,
        image,
        train_features,
        feature_columns,
        img_bands,
        bounds=None,
        kernel_size=10,
        scale=30,
        folder="gee_001",
        name_prefix="train",
        size_limit=500,
        bucket="gee_001_bucket_1",
    ):
        image_neighborhood = image.neighborhoodToArray(
            ee.Kernel.rectangle(kernel_size, kernel_size, "pixels", normalize=False)
        )
        if bounds is not None:
            a_bounds = ee.Geometry.Rectangle(bounds)  # [xmin, ymin, xmax, ymax]
        else:
            a_bounds = self.bounds
        train_feat0 = train_features.filterBounds(a_bounds)
        size_all = train_feat0.size().getInfo()
        filenames = []
        if size_all > size_limit:
            num_parts = int(np.ceil(size_all / size_limit))
            print(f"Large dataset, splitting to {num_parts} batches.")
        else:
            num_parts = 1

        # for i in range(1):
        for i in range(num_parts):
            offset = i * size_limit
            count = min(size_limit, size_all - offset)
            print(count)
            subset_features = ee.FeatureCollection(train_feat0.toList(count, offset))
            train_db = image_neighborhood.sampleRegions(
                collection=subset_features,
                properties=feature_columns,
                scale=scale,
                tileScale=12,
            )
            # print(feature_columns)
            # print(train_db)
            # print(train_db.first().toDictionary())

            print(f"batch {i} ...")
            filename = f"{folder}/{name_prefix}_{i}"
            task = ee.batch.Export.table.toCloudStorage(
                collection=train_db,
                description=f"{name_prefix}Batch{i}",
                bucket=bucket,
                fileNamePrefix=filename,
                fileFormat="TFRecord",
                selectors=img_bands + feature_columns,
            )
            task.start()
            filenames.append(filename)
            while task.active():
                # print(f"processing task id: {task.id}.")
                time.sleep(60)

        return filenames


# Define custom DenseNet model.

l2 = tf.keras.regularizers.l2


class ConvBlock(tf.keras.Model):
    """Convolutional Block consisting of (batchnorm->relu->conv).
  Arguments:
    num_filters: number of filters passed to a convolutional layer.
    data_format: "channels_first" or "channels_last"
    bottleneck: if True, then a 1x1 Conv is performed followed by 3x3 Conv.
    weight_decay: weight decay
    dropout_rate: dropout rate.
  """

    def __init__(
        self, num_filters, data_format, bottleneck, weight_decay=1e-4, dropout_rate=0.2
    ):
        super(ConvBlock, self).__init__()
        axis = -1 if data_format == "channels_last" else 1
        inter_filter = num_filters * 4
        self.bottleneck = bottleneck
        self.bn1 = tf.keras.layers.BatchNormalization(axis=axis)
        self.relu1 = tf.keras.layers.ReLU()
        self.dropout = tf.keras.layers.Dropout(dropout_rate)
        self.conv2 = tf.keras.layers.Conv2D(
            num_filters,
            3,
            padding="same",
            use_bias=False,
            kernel_initializer="he_normal",
            kernel_regularizer=l2(weight_decay),
        )

        if self.bottleneck:
            self.conv1 = tf.keras.layers.Conv2D(
                inter_filter,
                1,
                padding="same",
                use_bias=False,
                kernel_initializer="he_normal",
                kernel_regularizer=l2(weight_decay),
            )
            self.bn2 = tf.keras.layers.BatchNormalization(axis=axis)
            self.relu2 = tf.keras.layers.ReLU()

    def call(self, x, training=True):
        output = self.bn1(x, training=training)
        output = self.relu1(output)
        if self.bottleneck:
            output = self.conv1(output)
            output = self.bn2(output, training=training)
            output = self.relu2(output)
        output = self.conv2(output)
        output = self.dropout(output, training=training)
        return output


class TransitionBlock(tf.keras.Model):
    """Transition Block to reduce the number of features.
  Arguments:
    num_filters: number of filters passed to a convolutional layer.
    data_format: "channels_first" or "channels_last"
    weight_decay: weight decay
    dropout_rate: dropout rate.
  """

    def __init__(self, num_filters, data_format, weight_decay=1e-4, dropout_rate=0):
        super(TransitionBlock, self).__init__()
        axis = -1 if data_format == "channels_last" else 1

        self.batchnorm = tf.keras.layers.BatchNormalization(axis=axis)
        self.conv = tf.keras.layers.Conv2D(
            num_filters,
            (1, 1),
            padding="same",
            use_bias=False,
            data_format=data_format,
            kernel_initializer="he_normal",
            kernel_regularizer=l2(weight_decay),
        )
        self.avg_pool = tf.keras.layers.AveragePooling2D(data_format=data_format)

    def call(self, x, training=True):
        output = self.batchnorm(x, training=training)
        output = self.conv(tf.nn.relu(output))
        output = self.avg_pool(output)
        return output


class DenseBlock(tf.keras.Model):
    """Dense Block.
  It consists of ConvBlocks where each block's output is concatenated
  with its input.
  Arguments:
    num_layers: Number of layers in each block.
    growth_rate: number of filters to add per conv block.
    data_format: "channels_first" or "channels_last"
    bottleneck: boolean, that decides which part of ConvBlock to call.
    weight_decay: weight decay
    dropout_rate: dropout rate.
  """

    def __init__(
        self,
        num_layers,
        growth_rate,
        data_format,
        bottleneck,
        weight_decay=1e-4,
        dropout_rate=0,
    ):
        super(DenseBlock, self).__init__()
        self.num_layers = num_layers
        self.axis = -1 if data_format == "channels_last" else 1

        self.blocks = []
        for _ in range(int(self.num_layers)):
            self.blocks.append(
                ConvBlock(
                    growth_rate, data_format, bottleneck, weight_decay, dropout_rate
                )
            )

    def call(self, x, training=True):
        for i in range(int(self.num_layers)):
            output = self.blocks[i](x, training=training)
            x = tf.concat([x, output], axis=self.axis)

        return x


class DenseNet(tf.keras.Model):
    """Creating the Densenet Architecture.
  Arguments:
    growth_rate: number of filters to add per conv block.
    output_classes: number of output classes.
    num_layers_in_each_block: number of layers in each block.
                              If -1, then we calculate this by (depth-3)/4.
                              If positive integer, then it is used as the
                                number of layers per block.
                              If list or tuple, then this list is used directly.
    data_format: "channels_first" or "channels_last"
    bottleneck: boolean, to decide which part of conv block to call.
    compression: reducing the number of inputs(filters) to the transition block.
    weight_decay: weight decay
    dropout_rate: dropout rate.
    pool_initial: If True add a 7x7 conv with stride 2 followed by 3x3 maxpool
                  else, do a 3x3 conv with stride 1.
    include_top: If true, GlobalAveragePooling Layer and Dense layer are
                 included.
  """

    def __init__(
        self,
        growth_rate=12,
        output_classes=1,
        num_layers_in_each_block=[4, 4, 4],
        data_format="channels_last",
        bottleneck=True,
        compression=0.5,
        weight_decay=1e-4,
        dropout_rate=0.2,
        pool_initial=False,
        include_top=True,
    ):
        super(DenseNet, self).__init__()
        self.growth_rate = growth_rate
        self.output_classes = output_classes
        self.num_layers_in_each_block = num_layers_in_each_block
        self.data_format = data_format
        self.bottleneck = bottleneck
        self.compression = compression
        self.weight_decay = weight_decay
        self.dropout_rate = dropout_rate
        self.pool_initial = pool_initial
        self.include_top = include_top

        axis = -1 if self.data_format == "channels_last" else 1
        self.num_of_blocks = len(num_layers_in_each_block)

        # setting the filters and stride of the initial covn layer.
        if self.pool_initial:
            init_filters = (7, 7)
            stride = (2, 2)
        else:
            init_filters = (3, 3)
            stride = (1, 1)

        self.num_filters = 2 * self.growth_rate

        # first conv and pool layer
        self.conv1 = tf.keras.layers.Conv2D(
            self.num_filters,
            init_filters,
            strides=stride,
            padding="same",
            use_bias=False,
            data_format=self.data_format,
            kernel_initializer="he_normal",
            kernel_regularizer=l2(self.weight_decay),
        )
        if self.pool_initial:
            self.pool1 = tf.keras.layers.MaxPooling2D(
                pool_size=(3, 3),
                strides=(2, 2),
                padding="same",
                data_format=self.data_format,
            )
            self.batchnorm1 = tf.keras.layers.BatchNormalization(axis=axis)

        self.batchnorm2 = tf.keras.layers.BatchNormalization(axis=axis)

        # calculating the number of filters after each block
        num_filters_after_each_block = [self.num_filters]
        for i in range(1, self.num_of_blocks):
            temp_num_filters = num_filters_after_each_block[i - 1] + (
                self.growth_rate * self.num_layers_in_each_block[i - 1]
            )
            # using compression to reduce the number of inputs to the
            # transition block
            temp_num_filters = int(temp_num_filters * compression)
            num_filters_after_each_block.append(temp_num_filters)

        # dense block initialization
        self.dense_blocks = []
        self.transition_blocks = []
        for i in range(self.num_of_blocks):
            self.dense_blocks.append(
                DenseBlock(
                    self.num_layers_in_each_block[i],
                    self.growth_rate,
                    self.data_format,
                    self.bottleneck,
                    self.weight_decay,
                    self.dropout_rate,
                )
            )
            if i + 1 < self.num_of_blocks:
                self.transition_blocks.append(
                    TransitionBlock(
                        num_filters_after_each_block[i + 1],
                        self.data_format,
                        self.weight_decay,
                        self.dropout_rate,
                    )
                )

        # last pooling and fc layer
        if self.include_top:
            self.last_pool = tf.keras.layers.GlobalAveragePooling2D(
                data_format=self.data_format
            )
            self.last_dense = tf.keras.layers.Dense(1024, activation="relu")
            self.last_dropout = tf.keras.layers.Dropout(self.dropout_rate)
            self.classifier = tf.keras.layers.Dense(self.output_classes)

    def call(self, x, training=True):
        output = self.conv1(x)

        if self.pool_initial:
            output = self.batchnorm1(output, training=training)
            output = tf.nn.relu(output)
            output = self.pool1(output)

        for i in range(self.num_of_blocks - 1):
            output = self.dense_blocks[i](output, training=training)
            output = self.transition_blocks[i](output, training=training)

        output = self.dense_blocks[self.num_of_blocks - 1](output, training=training)
        output = self.batchnorm2(output, training=training)
        output = tf.nn.relu(output)

        if self.include_top:
            output = self.last_pool(output)
            output = self.classifier(output)

        return output


from tensorflow import keras
from tensorflow.keras import layers


def toy_resnet():

    inputs = keras.Input(shape=(1, 1, 7), name='img')

    # resblk1 = ConvBlock(num_filters=48, data_format="channels_last", bottleneck=True)
    # resblk2 = ConvBlock(num_filters=96, data_format="channels_last", bottleneck=True)
    # resblk3 = ConvBlock(num_filters=192, data_format="channels_last", bottleneck=True)
    # x = resblk1(inputs)
    # block_1_output = tf.concat([x, inputs], axis=-1)
    # x = resblk2(block_1_output)
    # block_2_output = tf.concat([x, block_1_output], axis=-1)
    # x = resblk3(block_2_output)
    # block_3_output = tf.concat([x, block_2_output], axis=-1)

    x = tf.keras.layers.BatchNormalization(axis=-1)(inputs)
    x = tf.nn.relu(x)
    # x = layers.Conv2D(48*4, 1, use_bias=False, kernel_initializer="he_normal", kernel_regularizer=l2(1e-4), padding="same")(x)
    # x = tf.keras.layers.BatchNormalization(axis=-1)(x)
    # x = tf.nn.relu(x)
    x = layers.Conv2D(48, 3, use_bias=False, kernel_initializer="he_normal", kernel_regularizer=l2(1e-4), padding="same")(x)
    # x = layers.Dropout(0.2)(x)
    block_1_output = tf.concat([x, inputs], axis=-1)

    x = tf.keras.layers.BatchNormalization(axis=-1)(block_1_output)
    x = tf.nn.relu(x)
    # x = layers.Conv2D(96*4, 1, use_bias=False, kernel_initializer="he_normal", kernel_regularizer=l2(1e-4), padding="same")(x)
    # x = tf.keras.layers.BatchNormalization(axis=-1)(x)
    # x = tf.nn.relu(x)
    x = layers.Conv2D(96, 3, use_bias=False, kernel_initializer="he_normal", kernel_regularizer=l2(1e-4), padding="same")(x)
    # x = layers.Dropout(0.2)(x)
    block_3_output = tf.concat([x, block_1_output], axis=-1)
    # block_2_output = tf.concat([x, inputs], axis=-1)

    # x = tf.keras.layers.BatchNormalization(axis=-1)(block_2_output)
    # x = tf.nn.relu(x)
    # x = layers.Conv2D(192*4, 1, use_bias=False, kernel_initializer="he_normal", kernel_regularizer=l2(1e-4), padding="same")(x)
    # x = tf.keras.layers.BatchNormalization(axis=-1)(x)
    # x = tf.nn.relu(x)
    # x = layers.Conv2D(192, 3, use_bias=False, kernel_initializer="he_normal", kernel_regularizer=l2(1e-4), padding="same")(x)
    # x = layers.Dropout(0.2)(x)
    # block_3_output = tf.concat([x, block_2_output], axis=-1)
    # # block_3_output = tf.concat([x, inputs], axis=-1)

    x = tf.keras.layers.BatchNormalization(axis=-1)(block_3_output)
    x = tf.nn.relu(x)
    # x = layers.Conv2D(256, 3, padding="same")(x)
    x = layers.GlobalAveragePooling2D()(x)
    # x = layers.Dense(512, activation="relu")(x)
    # x = layers.Dropout(0.2)(x)
    outputs = layers.Dense(1)(x)

    model = keras.Model(inputs, outputs, name='toy_resnet')
    return model
