# import modules
import numpy as np
import pandas as pd
import json
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import (
    StandardScaler,
    RobustScaler,
    PowerTransformer,
    QuantileTransformer,
)
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsRegressor, KNeighborsClassifier
from sklearn.neural_network import MLPRegressor, MLPClassifier
from sklearn.linear_model import (
    LassoCV,
    ElasticNetCV,
    LogisticRegressionCV,
    RidgeClassifierCV,
    RidgeCV,
)
from sklearn.svm import SVR, SVC
from sklearn.ensemble import (
    RandomForestRegressor,
    ExtraTreesRegressor,
    RandomForestClassifier,
    ExtraTreesClassifier,
)
from sklearn.model_selection import (
    GridSearchCV,
    KFold,
    cross_val_score,
    cross_val_predict,
    train_test_split,
)
from xgboost import XGBRegressor, XGBClassifier
from lightgbm import LGBMRegressor, LGBMClassifier
from hyperopt import fmin, tpe, hp, space_eval


RANDNUM = 425


def data_clean_regressor(X, y, noise_threshold=10, iter=5):
    """
    INFFC noise filtering (https://doi.org/10.1016/j.inffus.2015.04.002)
    adjusted for regression models
    :param X: Input X
    :param y: Input y
    :param noise_threshold: noise threshold
    :return: cleaned X and y and indices of the original
    """

    mdl1 = RandomForestRegressor(random_state=RANDNUM + 1)
    mdl2 = LGBMRegressor(random_state=RANDNUM + 321)
    mdl3 = ElasticNetCV(random_state=RANDNUM + 2)

    mdl_list = [mdl1, mdl2, mdl3]
    noise_list = []
    for mdl in mdl_list:
        estimators = [("scale", RobustScaler()), ("learn", mdl)]
        ppl = Pipeline(estimators)
        y_hat = cross_val_predict(ppl, X, y, cv=5)
        #         print(y_hat.shape)
        #         print(y.shape)
        y_c = np.int8(np.abs(y - y_hat) > noise_threshold)
        noise_list.append(y_c)

    noise0 = np.sum(np.stack(noise_list), axis=0)

    # post-preliminary, iteration
    noise = noise0
    noise_pool = []
    for i in range(iter):
        y_renew = np.copy(y)
        Xtr = X[noise < 2, :]
        ytr = y[noise < 2]
        Xtt = X[noise >= 2, :]
        ytt = y[noise >= 2]
        noise_list = []
        for mdl in mdl_list:
            estimators = [("scale", RobustScaler()), ("learn", mdl)]
            ppl = Pipeline(estimators)
            y_hatr = cross_val_predict(ppl, Xtr, ytr, cv=5)
            y_hatt = ppl.fit(Xtr, ytr).predict(Xtt)
            y_renew[noise < 2] = y_hatr
            y_renew[noise >= 2] = y_hatt
            y_c = np.int8(np.abs(y - y_renew) > noise_threshold)
            print(
                "Fraction of noise (iter #{} in models): {:.3f}%".format(
                    i, np.sum(y_c > 0) / y_c.size * 100
                )
            )
            noise_list.append(y_c)
            noise_pool.append(y_c)
        noise = np.sum(np.stack(noise_list), axis=0)
        print(
            "Fraction of valid noise (iter #{}): {:.3f}%".format(
                i, np.sum(noise > 1) / y.size * 100
            )
        )

    noise = np.sum(np.stack(noise_pool), axis=0)
    idx = noise < 2 * iter
    X_clean = X[noise < 2 * iter, :]
    y_clean = y[noise < 2 * iter]
    print(
        "Fraction of FINAL valid noise: {:.3f}%".format(
            np.sum(noise > 2 * iter - 1) / y.size * 100
        )
    )
    plt.figure()
    n, bins, p1 = plt.hist(
        y, 20, alpha=0.5, edgecolor="gray", range=(np.min(y), np.max(y))
    )
    n, bins, p2 = plt.hist(
        y_clean, 20, alpha=0.5, edgecolor="gray", range=(np.min(y), np.max(y))
    )
    plt.xlabel("Target")
    plt.ylabel("Frequency")
    plt.legend((p1[0], p2[0]), ("Original", "Filtered"))
    plt.show()

    # save results
    return X_clean, y_clean, idx


class StackClassifier:
    """
    Build machine learning object that can find the best parameters for final run.

    """

    def __init__(self, X, y, score="accuracy"):
        self.X = X
        self.y = y
        self.best_params = []
        self.best_models = []
        self.score = score
        self.meta_mdl = LogisticRegressionCV(
            scoring=self.score, random_state=RANDNUM + 41
        )
        # self.meta_mdl = LGBMClassifier()

    def meta_model(self, models=None, X=None, y=None, k=5):
        """
        Meta model for stacking Classifiers
        :param models: list of selected models
        :return:
        """
        if models is None:
            models = self.best_models
        if X is None:
            X = self.X
        if y is None:
            y = self.y

        y_pred_list = []
        for mdl in models:
            y_pred = cross_val_predict(mdl, X, y, cv=k)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)

        cv_score = cross_val_score(
            self.meta_mdl, y_pred_stacked, y, scoring=self.score, cv=k
        )
        print("cross-validated score: {}".format(cv_score))
        print("mean cv score: {}".format(cv_score.mean()))

        self.meta_mdl.fit(y_pred_stacked, y)
        print(y_pred_stacked.shape)
        plt.figure(figsize=(12, 8))
        cm = np.corrcoef(y_pred_stacked, rowvar=False)
        sns.set(font_scale=1)
        sns.heatmap(cm, center=0, annot=True, square=True, fmt=".2f")
        plt.show()
        return self.meta_mdl

    def meta_model_predict(self, X_test, y_test=None, models=None, meta_model=None):
        """

        :param X_test: test set of X
        :param y_test: test set of y
        :param models: model list of trained pipelines
        :param meta_model: trained meta model for final output
        :return: final prediction y
        """
        if models is None:
            models = self.best_models
        if meta_model is None:
            meta_model = self.meta_mdl

        y_pred_list = []
        for mdl in models:
            y_pred = mdl.predict(X_test)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        y_final = meta_model.predict(y_pred_stacked)

        if y_test is not None:
            score = meta_model.score(y_pred_stacked, y_test)
            print("test score: {}".format(score))

        return y_final

    def meta_model_predict_proba(
        self, X_test, y_test=None, models=None, meta_model=None
    ):
        """

        :param X_test: test set of X
        :param y_test: test set of y
        :param models: model list of trained pipelines
        :param meta_model: trained meta model for final output
        :return: final prediction y
        """
        if models is None:
            models = self.best_models
        if meta_model is None:
            meta_model = self.meta_mdl

        y_pred_list = []
        for mdl in models:
            y_pred = mdl.predict(X_test)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        y_final = meta_model.predict_proba(y_pred_stacked)

        if y_test is not None:
            score = meta_model.score(y_pred_stacked, y_test)
            print("test score: {}".format(score))

        return y_final

    def baysian_opts(self, pipe, param_grid, k=3, iter=10):
        """
        baysian optimazation of hyper parameters
        :param pipe:
        :param param_grid: should be the range of parameters
        :param k:
        """

        def cv_score(param):
            disp_param = param.copy()
            for key in param:
                if (isinstance(param[key], (float, int))) and (param[key] >= 1):
                    param[key] = int(param[key])
                disp_param[key] = str(disp_param[key]).split("(")[0]
            pipe.set_params(**param)
            score1 = cross_val_score(
                pipe, self.X, self.y, cv=k, scoring=self.score
            ).mean()
            print("Params: {}".format(disp_param))
            print("CV Score: {}".format(score1))
            return 1 - score1

        print(
            "+-----------------------------------------------------------------------------+"
        )
        print(
            "Baysian Optimazation for pipeline: {}...".format(
                str(pipe.steps[-1][1]).split("(")[0]
            )
        )
        best_vals = fmin(
            fn=cv_score, space=param_grid, algo=tpe.suggest, max_evals=iter
        )
        best_params = space_eval(param_grid, best_vals)
        disp_param = best_params.copy()
        for key in best_params:
            if (isinstance(best_params[key], (float, int))) and (best_params[key] >= 1):
                best_params[key] = int(best_params[key])
            disp_param[key] = str(disp_param[key]).split("(")[0]
        print("BEST PARAMS: ")
        print(json.dumps(disp_param, indent=2))

        self.best_params.append(best_params)
        pipe.set_params(**best_params)
        pipe.fit(self.X, self.y)
        self.best_models.append(pipe)

    def setup_rf_model(self, param_grid=None):
        """
        setup rf model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 500, 50),
                "learn__max_depth": hp.quniform("learn__max_depth", 1, 10, 1),
                "learn__class_weight": hp.choice(
                    "learn__class_weight", [None, "balanced"]
                ),
            }

        mdl = RandomForestClassifier(random_state=RANDNUM + 43)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_et_model(self, param_grid=None):
        """
        setup et model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 1000, 50),
                "learn__max_depth": hp.quniform("learn__max_depth", 1, 10, 1),
                "learn__class_weight": hp.choice(
                    "learn__class_weight", [None, "balanced"]
                ),
            }

        mdl = ExtraTreesClassifier(random_state=RANDNUM + 45)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_xgb_model(self, param_grid=None):
        """
        setup xgboost model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 500, 50),
                "learn__max_depth": hp.quniform("learn__max_depth", 1, 10, 1),
                "learn__booster": hp.choice(
                    "learn__booster", ["gbtree", "gblinear", "dart"]
                ),
                "learn__learning_rate": hp.loguniform(
                    "learn__learning_rate", np.log(0.01), np.log(0.2)
                ),
                "learn__colsample_bytree": hp.uniform(
                    "learn__colsample_bytree", 0.3, 1.0
                ),
            }

        mdl = XGBClassifier(random_state=RANDNUM + 46)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_lgb_model(self, param_grid=None):
        """
        setup lightgbm model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 500, 50),
                "learn__num_leaves": hp.quniform("learn__num_leaves", 30, 150, 1),
                "learn__min_child_samples": hp.quniform(
                    "learn__min_child_samples", 5, 500, 5
                ),
                "learn__boosting_type": hp.choice(
                    "learn__boosting_type", ["gbdt", "dart"]
                ),
                "learn__learning_rate": hp.loguniform(
                    "learn__learning_rate", np.log(0.01), np.log(0.2)
                ),
                "learn__class_weight": hp.choice(
                    "learn__class_weight", [None, "balanced"]
                ),
            }

        mdl = LGBMClassifier(min_data=1, min_data_in_bin=1, random_state=RANDNUM + 47)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_logisticcv_model(self, param_grid=None):
        """
        setup LogisticRegressionCV model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "scale": hp.choice(
                    "scale",
                    [
                        StandardScaler(),
                        RobustScaler(),
                        PowerTransformer(),
                        QuantileTransformer(),
                    ],
                ),
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__class_weight": hp.choice(
                    "learn__class_weight", [None, "balanced"]
                ),
            }

        mdl = LogisticRegressionCV(scoring=self.score, random_state=RANDNUM + 410)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_ridgecv_model(self, param_grid=None):
        """
        setup RidgeClassifierCV model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "scale": hp.choice(
                    "scale",
                    [
                        StandardScaler(),
                        RobustScaler(),
                        PowerTransformer(),
                        QuantileTransformer(),
                    ],
                ),
                "pca": hp.choice("pca", [None, PCA(0.95)]),
            }

        mdl = RidgeClassifierCV(scoring=self.score)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_svc_model(self, param_grid=None):
        """
        setup svc model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__kernel": hp.choice(
                    "learn_kernel", ["linear", "rbf", "poly", "sigmoid"]
                ),
                "learn__C": hp.loguniform("learn__C", np.log(0.01), np.log(100)),
                "learn__gamma": hp.loguniform(
                    "learn__gamma", np.log(0.0001), np.log(0.2)
                ),
                "learn__class_weight": hp.choice(
                    "learn__class_weight", [None, "balanced"]
                ),
            }

        mdl = SVC(random_state=RANDNUM + 413)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_knn_model(self, param_grid=None):
        """
        setup knn model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "scale": hp.choice(
                    "scale",
                    [
                        StandardScaler(),
                        RobustScaler(),
                        PowerTransformer(),
                        QuantileTransformer(),
                    ],
                ),
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_neighbors": hp.quniform("learn__n_neighbors", 1, 100, 1),
                "learn__weights": hp.choice("learn__weights", ["uniform", "distance"]),
                "learn__p": hp.choice("learn__p", [1, 2]),
            }

        mdl = KNeighborsClassifier(n_neighbors=5, weights="distance")

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid


class StackRegressor:
    """
    Build machine learning object that can find the best parameters for final run.

    """

    def __init__(self, X, y, score="neg_mean_squared_error"):
        self.X = X
        self.y = y
        self.best_params = []
        self.best_models = []
        self.score = score
        # self.meta_mdl = RidgeCV(scoring=self.score)
        self.meta_mdl = XGBRegressor()

    def save_models(self, outname="saved_models.pkl"):
        pickle.dump([self.meta_mdl, self.best_models], open(outname, "wb"))

    def load_models(self, outname="saved_models.pkl"):
        self.meta_mdl, self.best_models = pickle.load(open(outname, "rb"))

    def meta_model(self, X=None, y=None, k=5):
        """
        Meta model for stacking Regressors
        :return:
        """
        if X is None:
            X = self.X
        if y is None:
            y = self.y

        models = []
        y_pred_list = []
        for mdl in self.best_models:
            mdl.fit(self.X, self.y)
            models.append(mdl)
            y_pred = cross_val_predict(mdl, X, y, cv=k)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        self.best_models = models

        cv_score = cross_val_score(
            self.meta_mdl, y_pred_stacked, y, scoring=self.score, cv=k
        )
        print("cross-validated score: {}".format(cv_score))
        print("mean cv score: {}".format(cv_score.mean()))

        self.meta_mdl.fit(y_pred_stacked, y)
        print(y_pred_stacked.shape)
        plt.figure(figsize=(12, 8))
        cm = np.corrcoef(y_pred_stacked, rowvar=False)
        sns.set(font_scale=1)
        sns.heatmap(cm, center=0, annot=True, square=True, fmt=".2f")
        plt.show()
        return self.meta_mdl

    def meta_model_predict(self, X_test, y_test=None, models=None, meta_model=None):
        """

        :param X_test: test set of X
        :param y_test: test set of y
        :param models: model list of trained pipelines
        :param meta_model: trained meta model for final output
        :return: final prediction y
        """
        if models is None:
            models = self.best_models
        if meta_model is None:
            meta_model = self.meta_mdl

        y_pred_list = []
        for mdl in models:
            y_pred = mdl.predict(X_test)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        y_final = meta_model.predict(y_pred_stacked)

        if y_test is not None:
            score = meta_model.score(y_pred_stacked, y_test)
            print("test score: {}".format(score))

        return y_final

    def baysian_opts(self, pipe, param_grid, k=3, iter=10):
        """
        baysian optimazation of hyper parameters
        :param pipe:
        :param param_grid: should be the range of parameters
        :param k:
        """

        def cv_score(param):
            disp_param = param.copy()
            for key in param:
                if (isinstance(param[key], (float, int))) and (param[key] >= 1):
                    param[key] = int(param[key])
                disp_param[key] = str(disp_param[key]).split("(")[0]
            pipe.set_params(**param)
            score1 = cross_val_score(
                pipe, self.X, self.y, cv=k, scoring=self.score
            ).mean()
            print("Params: {}".format(disp_param))
            print("CV Score: {}".format(score1))
            return 1 - score1

        print(
            "+-----------------------------------------------------------------------------+"
        )
        print(
            "Baysian Optimazation for pipeline: {}...".format(
                str(pipe.steps[-1][1]).split("(")[0]
            )
        )
        best_vals = fmin(
            fn=cv_score, space=param_grid, algo=tpe.suggest, max_evals=iter, verbose=1
        )
        best_params = space_eval(param_grid, best_vals)
        disp_param = best_params.copy()
        for key in best_params:
            if (isinstance(best_params[key], (float, int))) and (best_params[key] >= 1):
                best_params[key] = int(best_params[key])
            disp_param[key] = str(disp_param[key]).split("(")[0]
        print("BEST PARAMS: ")
        print(json.dumps(disp_param, indent=2))

        self.best_params.append(best_params)
        pipe.set_params(**best_params)
        pipe.fit(self.X, self.y)
        self.best_models.append(pipe)

    def setup_rf_model(self, param_grid=None):
        """
        setup rf model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 500, 50),
                "learn__max_depth": hp.quniform("learn__max_depth", 1, 10, 1),
            }

        mdl = RandomForestRegressor(random_state=RANDNUM + 43)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_et_model(self, param_grid=None):
        """
        setup et model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 1000, 50),
                "learn__max_depth": hp.quniform("learn__max_depth", 1, 10, 1),
            }

        mdl = ExtraTreesRegressor(random_state=RANDNUM + 45)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_xgb_model(self, param_grid=None):
        """
        setup xgboost model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 500, 50),
                "learn__max_depth": hp.quniform("learn__max_depth", 1, 10, 1),
                "learn__booster": hp.choice(
                    "learn__booster", ["gbtree", "gblinear", "dart"]
                ),
                "learn__learning_rate": hp.loguniform(
                    "learn__learning_rate", np.log(0.01), np.log(0.2)
                ),
                "learn__colsample_bytree": hp.uniform(
                    "learn__colsample_bytree", 0.3, 1.0
                ),
            }

        mdl = XGBRegressor(random_state=RANDNUM + 46)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_lgb_model(self, param_grid=None):
        """
        setup lightgbm model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_estimators": hp.quniform("learn__n_estimators", 50, 500, 50),
                "learn__num_leaves": hp.quniform("learn__num_leaves", 30, 150, 1),
                "learn__min_child_samples": hp.quniform(
                    "learn__min_child_samples", 5, 500, 5
                ),
                "learn__boosting_type": hp.choice(
                    "learn__boosting_type", ["gbdt", "dart"]
                ),
                "learn__learning_rate": hp.loguniform(
                    "learn__learning_rate", np.log(0.01), np.log(0.2)
                ),
            }

        mdl = LGBMRegressor(min_data=1, min_data_in_bin=1, random_state=RANDNUM + 47)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_lassocv_model(self, param_grid=None):
        """
        setup LassoCV model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "scale": hp.choice(
                    "scale",
                    [
                        StandardScaler(),
                        RobustScaler(),
                        PowerTransformer(),
                        QuantileTransformer(),
                    ],
                ),
                "pca": hp.choice("pca", [None, PCA(0.95)]),
            }

        mdl = LassoCV(random_state=RANDNUM + 410)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_elascv_model(self, param_grid=None):
        """
        setup ElasticNetCV model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "scale": hp.choice(
                    "scale",
                    [
                        StandardScaler(),
                        RobustScaler(),
                        PowerTransformer(),
                        QuantileTransformer(),
                    ],
                ),
                "pca": hp.choice("pca", [None, PCA(0.95)]),
            }

        mdl = ElasticNetCV(l1_ratio=[0.1, 0.5, 0.9, 0.95], random_state=RANDNUM + 48)

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_svr_model(self, param_grid=None):
        """
        setup svr model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__kernel": hp.choice(
                    "learn_kernel", ["linear", "rbf", "poly", "sigmoid"]
                ),
                "learn__C": hp.loguniform("learn__C", np.log(0.01), np.log(100)),
                "learn__gamma": hp.loguniform(
                    "learn__gamma", np.log(0.0001), np.log(0.2)
                ),
                "learn__epsilon": hp.uniform("learn__epsilon", 0.001, 0.5),
            }

        mdl = SVR()

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_knn_model(self, param_grid=None):
        """
        setup knn model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "scale": hp.choice(
                    "scale",
                    [
                        StandardScaler(),
                        RobustScaler(),
                        PowerTransformer(),
                        QuantileTransformer(),
                    ],
                ),
                "pca": hp.choice("pca", [None, PCA(0.95)]),
                "learn__n_neighbors": hp.quniform("learn__n_neighbors", 1, 100, 1),
                "learn__weights": hp.choice("learn__weights", ["uniform", "distance"]),
                "learn__p": hp.choice("learn__p", [1, 2]),
            }

        mdl = KNeighborsRegressor()

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid
