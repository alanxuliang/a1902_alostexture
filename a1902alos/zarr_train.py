# import modules
from pathlib import Path
import numpy as np
import pandas as pd
import dask.array as dskda
import dask.dataframe as dd
import xarray as xr
import learn2map.raster_tools as rt
import zarr
from zarr import blosc
from concurrent import futures
# import torch
# from fastai import *
# from fastai.vision import *
# import torch.nn as nn
import a1902alos.grid_models as gridm

compressor = blosc.Blosc(cname="lz4", clevel=7, shuffle=0)


def get_zarr_from_gdal(in_tiff, out_zarr, band_names=None, istarget=False):
    """
    convert in_tiff file to out_zarr
    :param in_tiff: (band, y, x)
    :param out_zarr:
    :return:
    """
    encoding_list = {"da": {"compressor": compressor}}
    da = xr.open_rasterio(in_tiff, chunks=[1, 3000, 3000])
    if band_names is not None:
        if istarget:
            band_names[0] = "target"
        da["band"] = ("band", band_names)
    print(da)
    ds2 = xr.Dataset({"da": da})
    print(ds2)
    ds2.to_zarr(
        out_zarr,
        mode="w",
        synchronizer=zarr.ThreadSynchronizer(),
        encoding=encoding_list,
    )


def append_zarr_from_gdal(in_tiff, out_zarr, band_names=None):
    """
    append in_tiff file to out_zarr
    :param in_tiff:
    :param out_zarr:
    :param band_names:
    :return:
    """
    da = xr.open_rasterio(in_tiff, chunks=[1, 1000, 1000])
    if band_names is not None:
        da["band"] = ("band", band_names)
    print(da)
    ds2 = xr.Dataset({"da": da})
    ds2.to_zarr(out_zarr, mode="a", append_dim="band")


def get_df_from_da_with_xy(daX, da_mask, out_fname, valid_min=0):
    """
    create dataframe from xarray data_array
    :param da_all: array with dims (band, y, x)
    :param out_fname: output filename
    :return: df
    """
    shape0 = daX.shape
    print(shape0)
    da_data = daX.data.reshape(daX.shape[0], -1).transpose()
    da_y = dskda.from_array(daX.y.data, (1000,)).reshape(-1, 1)
    da_x = dskda.from_array(daX.x.data, (1000,)).reshape(1, -1)
    da_y2 = dskda.repeat(da_y, shape0[2], axis=1).reshape(-1, 1)
    da_x2 = dskda.repeat(da_x, shape0[1], axis=0).reshape(-1, 1)
    da_all = dskda.concatenate([da_y2, da_x2, da_data], axis=1)
    col_names = ["y", "x"] + daX["band"].values.tolist()

    df = da_all.to_dask_dataframe(columns=col_names)
    da_data = da_mask.data.reshape(da_mask.shape[0], -1).transpose()
    df_lc = da_data.to_dask_dataframe(columns=da_mask["band"].values.tolist())
    df = df[df_lc.iloc[:, 0] > valid_min]
    # df.to_csv(f"{out_fname}_*.csv", index=False)
    df.to_parquet(f"{out_fname}.pqt", compression="snappy", engine="fastparquet")


class ParallelList:
    def __init__(self):
        self.flist = []

    def __call__(self, r):
        self.flist.append(r.result())


def write_img_files(s1, y2a, y2b, x2a, x2b, path_zarr, path_img):
    ds = xr.open_zarr(path_zarr)
    da1 = ds["da"]
    img = (
        da1.sel(y=slice(y2a, y2b), x=slice(x2a, x2b))
        .rechunk((-1, -1, -1))
        .transpose("y", "x", "band")
    )
    da3 = img.data
    y2 = (y2a + y2b) / 2
    x2 = (x2a + x2b) / 2
    da3.to_zarr(path_img + f"/{y2}_{x2}", overwrite=True, compressor=compressor)
    s1["x_files"] = f"/{y2}_{x2}"
    return s1


def get_image_from_df(df, path_zarr, path_img, cols=("y", "x"), blocksize=(-1, 1)):
    p = Path(path_img)
    p.mkdir(parents=True, exist_ok=True)
    y1 = df[cols[0]]
    x1 = df[cols[1]]
    y_width = blocksize[0]
    x_width = blocksize[1]
    y1a = y1.values - float(y_width) / 2
    y1b = y1.values + float(y_width) / 2
    x1a = x1.values - float(x_width) / 2
    x1b = x1.values + float(x_width) / 2
    # print(x1a)

    file_list = ParallelList()
    with futures.ProcessPoolExecutor() as pool:
        for k in range(df.shape[0]):
            future_result = pool.submit(
                write_img_files,
                df.iloc[k, :],
                y1a[k],
                y1b[k],
                x1a[k],
                x1b[k],
                path_zarr,
                path_img,
            )
            future_result.add_done_callback(file_list)
    df1 = pd.DataFrame(file_list.flist)

    # file_list = []
    # for k in range(df.shape[0]):
    #     file_list.append(write_img_files(y1a[k], y1b[k], x1a[k], x1b[k], path_zarr, path_img))
    # df["x_files"] = file_list

    print("Done")
    return df1


def parallel_predict_from_array(path_zarr, core_dim, learn_obj, blocksize=(-1, 1)):
    def predict_func(X):
        pred = learn_obj.predict(X)

    da = xr.open_zarr(path_zarr)["da"]
    daX = da.chunk({"band": -1, "y": blocksize[0], "x": blocksize[1]}).transpose(
        "band", "y", "x"
    )
    daX.data.map_overlap(
        predict_func, depth=1, boundary="reflect", trim=False, chunks=(500, 500)
    ).compute()

    y = daX.data.map_overlap(derivative, depth=1, boundary=0)

    xr.apply_ufunc(
        predict_func,
        daX,
        input_core_dims=[[core_dim]],
        dask="parallelized",
        output_dtypes=[float],
    )
