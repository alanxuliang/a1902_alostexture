# import modules
from pathlib import Path
import numpy as np
import pandas as pd
import dask.array as dskda
import dask.dataframe as dd
import xarray as xr
import learn2map.raster_tools as rt
import zarr
from zarr import blosc
from concurrent import futures
import torch
from fastai import *
from fastai.vision import *
import torch.nn as nn
import a1902alos.grid_models as gridm

compressor = blosc.Blosc(cname="lz4", clevel=6, shuffle=0)

# m=models.resnet34()

def conv3x3(in_planes, out_planes, stride=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)

def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(Bottleneck, self).__init__()
        self.conv1 = conv1x1(inplanes, planes)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = conv3x3(planes, planes, stride)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = conv1x1(planes, planes * self.expansion)
        self.bn3 = nn.BatchNorm2d(planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class ResNet(nn.Module):

    def __init__(self, block, layers, band_num=3, num_classes=1000, zero_init_residual=False):
        super(ResNet, self).__init__()
        self.inplanes = 64
        self.conv1 = nn.Conv2d(band_num, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)

        return x


def resnet34(**kwargs):
    """Constructs a ResNet-34 model.

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet(BasicBlock, [3, 4, 6, 3], **kwargs)
    return model


def get_zarr_data_noref(
    in_zarr, out_zarr, band_names=None, vmins=None, vmaxs=None, istarget=False
):
    """
    convert in_zarr file to out_zarr without using ref
    :param in_zarr: (band, y, x
    :param out_zarr:
    :return:
    """
    da_list = {}
    encoding_list = {}
    da = xr.open_zarr(in_zarr)["da"]
    if band_names is not None:
        da["band"] = ("band", band_names)
    print(da)
    for k in range(len(da.band)):
        if (k == 0) and istarget:
            var_name = "target"
        else:
            var_name = da.band.values.tolist()[k]
        da1 = da[k, :, :].drop("band")
        if vmins is not None:
            da_list[var_name] = (da1 - vmins[k]) / (vmaxs[k] - vmins[k])
        else:
            da_list[var_name] = da1
        encoding_list[var_name] = {"compressor": compressor}
    ds2 = xr.Dataset(da_list)
    print(ds2)
    ds2.to_zarr(
        out_zarr,
        mode="w",
        synchronizer=zarr.ThreadSynchronizer(),
        encoding=encoding_list,
    )


def get_zarr_data(
    ref_file, tif_fnames, out_zarr, vmins=None, vmaxs=None, istarget=False
):
    """
    1st file in tif_fnames is the target variable
    :param ref_file:
    :param tif_fnames:
    :param out_zarr:
    :return:
    """
    da_list = {}
    encoding_list = {}
    for i, fn in enumerate(tif_fnames):
        print(f"{i}: {fn}")
        out_fn = fn.replace(".tif", "_r.tif")
        rt.raster_clip(ref_file, fn, out_fn, resampling_method="average")
        da = xr.open_rasterio(out_fn, chunks=(1, 1000, 1000))
        for k in range(len(da.band)):
            if (i == 0) and (k == 0) and istarget:
                var_name = "target"
            else:
                var_name = f"{Path(fn).stem}_{k}"
            if vmins is not None:
                da_list[var_name] = (da[k, :, :] - vmins[k]) / (vmaxs[k] - vmins[k])
            else:
                da_list[var_name] = da[k, :, :]
            encoding_list[var_name] = {"compressor": compressor}
    ds2 = xr.Dataset(da_list)
    ds2 = ds2.transpose("y", "x").reset_coords(drop=True)
    ds2 = ds2.chunk(chunks={"y": 1000, "x": 1000})
    print(ds2)
    ds2.to_zarr(
        out_zarr,
        mode="w",
        synchronizer=zarr.ThreadSynchronizer(),
        encoding=encoding_list,
    )


def get_df_from_da_with_xy(daX, da_mask, out_fname, valid_min=0):
    """
    create dataframe from xarray data_array
    :param da_all: array with dims (band, y, x)
    :param out_fname: output filename
    :return: df
    """
    shape0 = daX.shape
    print(shape0)
    da_data = daX.data.reshape(daX.shape[0], -1).transpose()
    da_y = dskda.from_array(daX.y.data, (500,)).reshape(-1, 1)
    da_x = dskda.from_array(daX.x.data, (500,)).reshape(1, -1)
    da_y2 = dskda.repeat(da_y, shape0[2], axis=1).reshape(-1, 1)
    da_x2 = dskda.repeat(da_x, shape0[1], axis=0).reshape(-1, 1)
    da_all = dskda.concatenate([da_y2, da_x2, da_data], axis=1)
    col_names = ["y", "x"] + daX["band"].values.tolist()

    df = da_all.to_dask_dataframe(columns=col_names)
    da_data = da_mask.data.reshape(da_mask.shape[0], -1).transpose()
    df_lc = da_data.to_dask_dataframe(columns=da_mask["band"].values.tolist())
    df = df[df_lc.iloc[:, 0] > valid_min]
    # df.to_csv(f"{out_fname}_*.csv", index=False)
    df.to_parquet(f"{out_fname}.pqt", compression="snappy", engine="fastparquet")


class ParallelList:
    def __init__(self):
        self.flist = []

    def __call__(self, r):
        self.flist.append(r.result())


def write_img_files(s1, y2a, y2b, x2a, x2b, path_zarr, path_img):
    ds = xr.open_zarr(path_zarr)
    da_mat = []
    for i, fp in enumerate(ds.data_vars):
        da1 = ds[fp]
        img = da1.sel(y=slice(y2a, y2b), x=slice(x2a, x2b))
        # img = da1.loc[dict(y=slice(y2a, y2b), x=slice(x2a, x2b))]
        da_mat.append(img.data)
    da3 = dskda.stack(da_mat, axis=-1).rechunk((-1, -1, -1))
    # print(da3)
    y2 = (y2a + y2b) / 2
    x2 = (x2a + x2b) / 2
    da3.to_zarr(path_img + f"/{y2}_{x2}", overwrite=True, compressor=compressor)
    s1["x_files"] = f"/{y2}_{x2}"
    return s1


def get_image_from_df(df, path_zarr, path_img, cols=("y", "x"), blocksize=(-1, 1)):
    p = Path(path_img)
    p.mkdir(parents=True, exist_ok=True)
    y1 = df[cols[0]]
    x1 = df[cols[1]]
    y_width = blocksize[0]
    x_width = blocksize[1]
    y1a = y1.values - float(y_width) / 2
    y1b = y1.values + float(y_width) / 2
    x1a = x1.values - float(x_width) / 2
    x1b = x1.values + float(x_width) / 2
    # print(x1a)

    file_list = ParallelList()
    with futures.ProcessPoolExecutor() as pool:
        for k in range(df.shape[0]):
            future_result = pool.submit(write_img_files, df.iloc[k, :], y1a[k], y1b[k], x1a[k], x1b[k], path_zarr, path_img)
            future_result.add_done_callback(file_list)
    df1 = pd.DataFrame(file_list.flist)

    # file_list = []
    # for k in range(df.shape[0]):
    #     file_list.append(write_img_files(y1a[k], y1b[k], x1a[k], x1b[k], path_zarr, path_img))
    # df["x_files"] = file_list

    print("Done")
    return df1


def get_df_from_da(daX, da_mask, out_fname):
    """
    create dataframe from xarray data_array
    :param da_all: array with dims (band, y, x)
    :param out_fname: output filename
    :return: df
    """
    print(daX.shape)
    da_data = daX.data.reshape(daX.shape[0], -1).transpose()
    df = da_data.to_dask_dataframe(columns=daX["band"].values.tolist())
    da_data = da_mask.data.reshape(da_mask.shape[0], -1).transpose()
    df_lc = da_data.to_dask_dataframe(columns=da_mask["band"].values.tolist())
    df = df[df_lc.iloc[:, 0] > 0]
    # df.to_csv(f"{out_fname}_*.csv", index=False)
    df.to_parquet(f"{out_fname}.pqt", compression="snappy", engine="fastparquet")


def get_da_from_df(df, da_lc, out_fname):
    """
    create dataarray from df using da_lc as reference
    :param df: 1 column (y_hat) dataframe
    :param da_lc: lc_mask data_array
    :param out_fname:
    :return:
    """
    assert len(da_lc.shape) == 2
    data_lc = da_lc.data.reshape(-1, 1)
    df_lc = data_lc.to_dask_dataframe(columns=["lc_mask"])
    df_all = df_lc.assign(y_hat=np.nan)
    y_series = df_all["y_hat"]
    y_series[df_all.lc_mask > 0] = df["y_hat"]
    da_y0 = y_series.to_dask_array(length=True)
    da_y_arr = da_y0.reshape(da_lc.shape[0], da_lc.shape[1])
    da_y = xr.DataArray(da_y_arr, coords={"y": da_lc.y, "x": da_lc.x}, dims=["y", "x"])
    return da_y


def get_train_df(ds_fname, out_fn, valid_min=0):
    """
    target variable with the column name: "target"
    y_x as the name indicating y and x locations
    :param ds_fname: name of the zarr file containing target variable
    :param out_fn: output filename prefix for csv files
    :param valid_min: valid minimum values
    :return:
    """
    ds = xr.open_zarr(ds_fname)
    ds1 = ds.stack(z=["y", "x"]).reset_index("z")
    print(ds1)
    # da1 = ds1["target"].load()
    df = ds1.where(ds1["target"] > valid_min, drop=True).to_dask_dataframe()
    print(df)
    # df['y_x'] = df["y"].astype(str) + "_" + df["x"].astype(str)
    print(out_fn)
    df.to_csv(f"{out_fn}_*.csv", index=False)
    df.to_parquet(f"{out_fn}.pqt", compression="snappy", write_index=False)
    return df


def open_zarr_xy(fpArr):
    """
    open zarr data at xy locations
    :param fpArr:
    :return:
    """
    fn, y0, blk_all = fpArr[0].split("@#$")
    _, x0, _ = fpArr[1].split("@#$")
    y1 = float(y0.strip("\\/"))
    x1 = float(x0.strip("\\/"))
    y_width, x_width = blk_all.split("_")
    print(x1 - float(x_width) / 2, x1 + float(x_width) / 2)
    print(y1 - float(y_width) / 2, y1 + float(y_width) / 2)

    blosc.use_threads = False
    # ds = xr.open_zarr(fn)
    # print(ds.data_vars)
    # # ds1 = ds.sel(
    # #     y=slice(y1 - float(y_width) / 2, y1 + float(y_width) / 2),
    # #     x=slice(x1 - float(x_width) / 2, x1 + float(x_width) / 2),
    # # ).load()
    # # print(ds1.data_vars["HH"].shape)
    # ds.close()
    mat = None
    # for i, fp in enumerate(ds.data_vars):
    #     da1 = ds[fp]
    for i in range(3):
        da1 = dskda.from_zarr(fn, "HH")
        img = da1[:67, :67].compute()
        # img = da1.loc[dict(
        #     y=slice(y1 - float(y_width) / 2, y1 + float(y_width) / 2),
        #     x=slice(x1 - float(x_width) / 2, x1 + float(x_width) / 2),
        # )].load()
        print(img.shape)
        chan = pil2tensor(img, np.float32)
        # print(chan.shape)
        if mat is None:
            # mat = torch.zeros((len(ds.data_vars), chan.shape[1], chan.shape[2]))
            mat = torch.zeros((3, chan.shape[1], chan.shape[2]))
        mat[i, :, :] = chan[0, :, :]
    # print(mat.shape)
    # print(mat[1, 0:3, 0:3])
    return Image(mat)


def open_tiff_xy(fpArr):
    """
    open zarr data at xy locations
    :param fpArr:
    :return:
    """
    fn, y0, blk_all = fpArr[0].split("@#$")
    _, x0, _ = fpArr[1].split("@#$")
    y1 = float(y0.strip("\\/"))
    x1 = float(x0.strip("\\/"))
    y_width, x_width = blk_all.split("_")
    print(x1 - float(x_width) / 2, x1 + float(x_width) / 2)
    print(y1 - float(y_width) / 2, y1 + float(y_width) / 2)

    blosc.use_threads = False
    # ds = xr.open_zarr(fn)
    # print(ds.data_vars)
    # # ds1 = ds.sel(
    # #     y=slice(y1 - float(y_width) / 2, y1 + float(y_width) / 2),
    # #     x=slice(x1 - float(x_width) / 2, x1 + float(x_width) / 2),
    # # ).load()
    # # print(ds1.data_vars["HH"].shape)
    # ds.close()
    mat = None
    # for i, fp in enumerate(ds.data_vars):
    #     da1 = ds[fp]
    for i in range(3):
        da1 = dskda.from_zarr(fn, "HH")
        img = da1[:67, :67].compute()
        # img = da1.loc[dict(
        #     y=slice(y1 - float(y_width) / 2, y1 + float(y_width) / 2),
        #     x=slice(x1 - float(x_width) / 2, x1 + float(x_width) / 2),
        # )].load()
        print(img.shape)
        chan = pil2tensor(img, np.float32)
        # print(chan.shape)
        if mat is None:
            # mat = torch.zeros((len(ds.data_vars), chan.shape[1], chan.shape[2]))
            mat = torch.zeros((3, chan.shape[1], chan.shape[2]))
        mat[i, :, :] = chan[0, :, :]
    # print(mat.shape)
    # print(mat[1, 0:3, 0:3])
    return Image(mat)


# class MultiChannelImageList(ImageList):
#     def open(self, fn):
#         return open_zarr_xy(fn)
#
#
# class MultiChannelImageListNew(ImageList):
#     """
#     load items first
#     """
#
#     def open(self, da):
#         mat = pil2tensor(da.compute(), np.float32).float()
#         return Image(mat)
#
#     @classmethod
#     def from_df(cls, df, path, cols=0, blocksize=(-1, 1), **kwargs):
#         res = super().from_df(df, path=path, cols=0, **kwargs)
#         print(res.items)
#         y1 = df[cols[0]]
#         x1 = df[cols[1]]
#         y_width = blocksize[0]
#         x_width = blocksize[1]
#         y1a = y1.values - float(y_width) / 2
#         y1b = y1.values + float(y_width) / 2
#         x1a = x1.values - float(x_width) / 2
#         x1b = x1.values + float(x_width) / 2
#         print(x1a)
#         ds = xr.open_zarr(path)
#         print(ds.data_vars)
#         da_list = []
#         for k in range(df.shape[0]):
#             # ds1 = ds.sel(
#             #     y=slice(y1a[k], y1b[k]),
#             #     x=slice(x1a[k], x1b[k]),
#             # )
#             # print(ds1.data_vars["HH"].shape)
#
#             da_mat = []
#             for i, fp in enumerate(ds.data_vars):
#                 da1 = ds[fp]
#                 img = da1.loc[dict(y=slice(y1a[k], y1b[k]), x=slice(x1a[k], x1b[k]))]
#                 da_mat.append(img.data)
#             da3 = dskda.stack(da_mat, axis=-1)
#             da_list.append(da3)
#         res.items = np.array(da_list, dtype=object)
#         # print(da_list)
#         return res
#
#
# class MultiChannelImageListFolder(ImageList):
#     """
#     load items first
#     """
#     def open(self, fn):
#         # print(fn)
#         da = dskda.from_zarr(fn)
#         mat = pil2tensor(da.compute(), np.float32)
#         return Image(mat)


def get_databunch_from_df(
    df0,
    zarr_name,
    label="target",
    test_df=None,
    blocksize=(-1, 1),
    dbsize=(3, 3),
    bs=12,
):
    """

    get fastai data bunch
    :param df0: dataframe containing target variable and location information
    :param zarr_name: zarr file containing all layers
    :param blocksize: size of block in (dy, dx)
    :return: databunch
    """
    # il = MultiChannelImageList.from_df(
    #     path=f"{zarr_name}@#$",
    #     df=df0,
    #     cols=["y", "x"],
    #     suffix=f"@#${blocksize[0]}_{blocksize[1]}",
    # )
    il = MultiChannelImageListNew.from_df(
        path=f"{zarr_name}", df=df0, cols=["y", "x"], blocksize=blocksize
    )
    print(il)
    tfms = get_transforms(
        flip_vert=True,
        do_flip=True,
        max_rotate=30,
        max_zoom=1,
        max_lighting=None,
        max_warp=None,
        p_affine=0.0,
        p_lighting=0.0,
    )
    ils = il.split_by_rand_pct()
    ils2 = ils.label_from_df(cols=label)
    if test_df is not None:
        # test_items = MultiChannelImageList.from_df(
        #     path=f"{zarr_name}@#$",
        #     df=test_df,
        #     cols=["y", "x"],
        #     suffix=f"@#${blocksize[0]}_{blocksize[1]}",
        # )
        test_items = MultiChannelImageListNew.from_df(
            path=f"{zarr_name}", df=test_df, cols=["y", "x"], blocksize=blocksize
        )
        ils2 = ils2.add_test(items=test_items)
    ils4 = ils2.transform(tfms, size=dbsize)  # add tfms & size
    ils5 = ils4.databunch(bs=bs)
    return ils5


def get_databunch_from_df_training(
    df0,
    x_path,
    label="target",
    test_df=None,
    dbsize=(3, 3),
    bs=12,
):
    """

    get fastai data bunch
    :param df0: dataframe containing target variable, x_files location information
    :param zarr_name: zarr file containing all layers
    :param blocksize: size of block in (dy, dx)
    :return: databunch
    """
    il = MultiChannelImageListFolder.from_df(
        path=f"{x_path}", df=df0, cols=["x_files"]
    )
    print(il)
    tfms = get_transforms(
        flip_vert=True,
        do_flip=True,
        max_rotate=30,
        max_zoom=1,
        max_lighting=None,
        max_warp=None,
        p_affine=0.0,
        p_lighting=0.0,
    )
    ils = il.split_by_rand_pct()
    ils2 = ils.label_from_df(cols=label)
    if test_df is not None:
        test_items = MultiChannelImageListFolder.from_df(
            path=f"{x_path}", df=test_df, cols=["x_files"]
        )
        ils2 = ils2.add_test(items=test_items)
    ils4 = ils2.transform(tfms, size=dbsize)  # add tfms & size
    ils5 = ils4.databunch(bs=bs)
    return ils5
