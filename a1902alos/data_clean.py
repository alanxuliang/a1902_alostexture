# import modules
import os
import numpy as np
import pandas as pd
import xarray as xr
import subprocess
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
import seaborn as sns
from shapely.geometry.polygon import Polygon
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from cartopy.io.img_tiles import Stamen
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import LinearRegression


sns.set(
    style="whitegrid", font_scale=2, rc={"figure.figsize": (8, 6), "figure.dpi": 100}
)

# preprocessing classes
class XarrayEDA:
    def __init__(self, in_data):
        self.da = in_data

    def xrview_box(
        self,
        x_bounds=(-180, 180),
        y_bounds=(-30, 25),
        vminmax=(None, None),
        robust=True,
        out_name="colormap",
    ):
        box_ts = self.da.where(
            (self.da.x > x_bounds[0])
            & (self.da.x < x_bounds[1])
            & (self.da.y > y_bounds[0])
            & (self.da.y < y_bounds[1]), drop=True
        )

        for i in range(box_ts.time.shape[0]):
            ts1 = box_ts.isel(time=i)
            # ts1 = box_ts.isel(time=i).coarsen(y=5, x=5).mean()
            plt.figure(figsize=(12, 6))
            ax = plt.axes(projection=ccrs.PlateCarree())
            ts1.plot(
                ax=ax,
                robust=robust,
                vmin=vminmax[0],
                vmax=vminmax[1],
                cmap="rainbow",
                cbar_kwargs={
                    "orientation": "horizontal",
                    "pad": 0.05,
                    "shrink": 0.8,
                    "aspect": 30,
                },
            )
            ax.coastlines(linewidth=1)

            # ax = sns.heatmap(
            #     box_ts.isel(time=i),
            #     vmin=vminmax[0],
            #     vmax=vminmax[1],
            #     square=True,
            #     xticklabels=False,
            #     yticklabels=False,
            #     cbar_kws={
            #         "orientation": "horizontal",
            #         "pad": 0.05,
            #         "shrink": 0.8,
            #         "aspect": 30,
            #     },
            #     cmap="rainbow",
            #     ax=ax,
            # )
            plt.title(
                "Colormap ({})".format(
                    box_ts["time"][i].values
                    # box_ts["time.year"][i].values, box_ts["time.month"][i].values
                )
            )
            plt.tight_layout()
            plt.savefig("{}_{:03d}.png".format(out_name, i), dpi=300)
            plt.close()

        cmmd = "ffmpeg -r 3 -i {}_%03d.png -vcodec mpeg4 -y {}.mp4".format(
            out_name, out_name
        )
        subprocess.check_output(cmmd, shell=True)

        for i in range(box_ts.time.shape[0]):
            os.remove("{}_{:03d}.png".format(out_name, i))

    def xrview_monthly_box(
        self,
        x_bounds=(-80, -50),
        y_bounds=(-20, 10),
        vminmax=(-20, 20),
        out_name="colormap",
    ):
        box_ts = self.da.where(
            (self.da.x > x_bounds[0])
            & (self.da.x < x_bounds[1])
            & (self.da.y > y_bounds[0])
            & (self.da.y < y_bounds[1])
        )

        for i in range(box_ts.band.shape[0]):
            for j in range(box_ts.month.shape[0]):
                plt.figure()
                ax = plt.axes(projection=ccrs.PlateCarree())
                box_ts.isel(month=j, band=i).plot(
                    ax=ax,
                    vmin=vminmax[0],
                    vmax=vminmax[1],
                    cmap="rainbow",
                    cbar_kwargs={
                        "orientation": "horizontal",
                        "pad": 0.05,
                        "shrink": 0.8,
                        "aspect": 30,
                    },
                )
                ax.coastlines(linewidth=1)
                plt.title(
                    "Colormap ({}-{})".format(
                        box_ts["band"][i].values, box_ts["month"][i].values
                    )
                )
                plt.savefig("{}_{:03d}.png".format(out_name, i))
                plt.close()

        cmmd = "ffmpeg -r 3 -i {}_%03d.png -vcodec mpeg4 -y {}.mp4".format(
            out_name, out_name
        )
        subprocess.check_output(cmmd, shell=True)

    def xrview_ts(
        self, x_bounds=(-80, -40), y_bounds=(-20, 10), out_name="regional", robust=True
    ):
        box_ts = self.da.where(
            (self.da.x > x_bounds[0])
            & (self.da.x < x_bounds[1])
            & (self.da.y > y_bounds[0])
            & (self.da.y < y_bounds[1])
        )
        box_mean = box_ts.mean("time")
        plt.figure()
        ax = plt.axes(projection=ccrs.PlateCarree())
        box_mean.plot(
            ax=ax,
            cmap="rainbow",
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.05,
                "shrink": 0.8,
                "aspect": 30,
            },
        )
        ax.coastlines(linewidth=1)
        plt.title("Colormap (Long-term Mean)")
        plt.tight_layout()
        plt.savefig("{}_mean_map.png".format(out_name))
        plt.show()
        plt.close()

        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=(6, 3))
        plt.plot(ts1.time, ts1.values, "ro-")
        # regressor = LinearRegression()
        # regressor.fit(np.arange(ts1.shape[0])[:, None], ts1.values)
        # print(regressor.intercept_)
        # print(regressor.coef_)
        # slope_y = regressor.predict(np.arange(ts1.shape[0])[:, None])
        # plt.plot(ts1.time, slope_y, "--k", linewidth=0.5)

        plt.xlabel("Time")
        plt.ylabel("Monthly Time Series")
        # plt.title("Original time series")
        plt.tight_layout()
        plt.savefig("{}_ts_original.png".format(out_name), dpi=300)
        plt.show()
        plt.close()

        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=(6, 2.6))
        plt.plot(ts2.time, ts2.values, "o-")

        plt.xlabel("Time")
        plt.title("Annual Time Series")
        plt.tight_layout()
        plt.savefig("{}_ts_annual.png".format(out_name), dpi=300)
        plt.show()
        plt.close()


    def xrview_mask_ts(self, y_label="", lwmask=None, out_name="masked", robust=True):
        plt.style.use(["seaborn-whitegrid"])
        if lwmask is None:
            lwmask = self.da.lwmask
        box_ts = self.da.where(lwmask == 1, drop=True)
        box_mean = box_ts.mean("time")
        plt.figure()
        ax = plt.axes(projection=ccrs.PlateCarree())
        box_mean.plot(
            ax=ax,
            cmap="rainbow",
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.12,
                "shrink": 0.8,
                "aspect": 30,
            },
        )
        ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        ax.coastlines(linewidth=0.5)
        ax.background_patch.set_visible(False)
        ax.outline_patch.set_visible(False)
        # gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        # gl.xlabels_top = False
        # gl.ylabels_left = False
        # gl.xformatter = LONGITUDE_FORMATTER
        # gl.yformatter = LATITUDE_FORMATTER
        # plt.title("Colormap (Long-term Mean)")
        plt.title("")
        plt.savefig("{}_mean_map.png".format(out_name), dpi=300)
        # plt.show()
        # plt.close()

        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=(12, 5))
        plt.plot(ts1.time, ts1.values, "o-", alpha=0.7)
        plt.xlabel("Time")
        plt.ylabel(y_label)
        # plt.title("Original time series")
        plt.tight_layout()
        plt.savefig("{}_ts_mean.png".format(out_name), dpi=300, bbox_inches='tight')
        # plt.show()
        # plt.close()
        df = pd.DataFrame({'regional_mean': ts1.values}, index=np.datetime_as_string(ts1.time.values, unit='D'))

        # ts1 = box_ts.std(["x", "y"])
        # plt.figure(figsize=(12, 5))
        # plt.plot(ts1.time, ts1.values, "o-", alpha=0.7)
        # plt.xlabel("Time")
        # plt.ylabel(y_label)
        # # plt.title("Original time series")
        # plt.savefig("{}_ts_std.png".format(out_name))
        # plt.show()
        # plt.close()
        # df['regional_std'] = ts1.values

        df.to_csv("{}_data.csv".format(out_name))

        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=(12, 5))
        plt.plot(ts2.time, ts2.values, "o-", alpha=0.7)
        plt.xlabel("Time")
        plt.ylabel(y_label)
        # plt.title("Annual Mean time series")
        plt.tight_layout()
        plt.savefig("{}_ts_annual.png".format(out_name), dpi=300, bbox_inches='tight')
        # plt.show()
        # plt.close()

        df = pd.DataFrame({'regional_mean': ts2.values},
                          index=np.datetime_as_string(ts2.time.values, unit='D'))
        df.to_csv("{}_ann.csv".format(out_name))


        # ts2 = ts1.resample(time="1YS").std("time")
        # plt.figure(figsize=(12, 5))
        # plt.plot(ts2.time, ts2.values, "o-")
        # plt.xlabel("Time")
        # plt.title("Annual Std time series")
        # plt.savefig("{}_ts_annualstd.png".format(out_name))
        # plt.show()
        # plt.close()
        #
        # ts2 = ts1.resample(time="1YS").mean("time")[1:]
        # plt.figure(figsize=(12, 5))
        # plt.plot(ts2.time, ts2.values, "o-")
        # plt.xlabel("Time")
        # plt.title("Annual Mean time series")
        # plt.savefig("{}_ts_annual2.png".format(out_name))
        # plt.show()
        # plt.close()
        #
        # ts2 = ts1.resample(time="1YS").std("time")[1:]
        # plt.figure(figsize=(12, 5))
        # plt.plot(ts2.time, ts2.values, "o-")
        # plt.xlabel("Time")
        # plt.title("Annual Std time series")
        # plt.savefig("{}_ts_annualstd2.png".format(out_name))
        # plt.show()
        # plt.close()

        # ts3 = ts1.chunk({"time": -1}).interpolate_na("time", method="cubic")
        # plt.figure(figsize=(12, 6))
        # plt.plot(ts3.time, ts3.values, "o-")
        # plt.xlabel("Time")
        # plt.title("Interpolated time series")
        # plt.savefig("{}_ts_interp.png".format(out_name))
        # plt.show()
        # plt.close()
        #
        # ts4 = ts3.resample(time="1YS").mean("time")
        # plt.figure(figsize=(12, 6))
        # plt.plot(ts4.time, ts4.values, "o-")
        # plt.xlabel("Time")
        # plt.title("Annual time series")
        # plt.savefig("{}_ts_annual_interp.png".format(out_name))
        # plt.show()
        # plt.close()

    def xrview_agb_ts(self, vmax=300, lwmask=None, out_name="masked", robust=True):
        plt.style.use(["seaborn-whitegrid"])
        if lwmask is None:
            lwmask = self.da.lwmask
        box_ts = self.da.where((lwmask > 0) & (lwmask < 15), drop=True)
        box_mean = box_ts.mean("time")
        plt.figure(figsize=(12, 8))
        # ax = plt.axes(projection=ccrs.PlateCarree())
        ax = plt.axes(projection=ccrs.Robinson())
        box_mean.plot(
            ax=ax,
            cmap="gist_earth_r",
            vmax=vmax,
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.12,
                "shrink": 0.8,
                "aspect": 30,
            },
            transform=ccrs.PlateCarree(),
        )
        ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        ax.coastlines(linewidth=0.5)
        ax.background_patch.set_visible(False)
        ax.outline_patch.set_visible(False)
        # gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        # gl.xlabels_top = False
        # gl.ylabels_left = False
        # gl.xformatter = LONGITUDE_FORMATTER
        # gl.yformatter = LATITUDE_FORMATTER
        # plt.title("Colormap (Long-term Mean)")
        plt.title("")
        plt.savefig(f"{out_name}_mean_map.png", dpi=300)
        # plt.show()
        plt.close()

        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=(12, 5))
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.ylabel("AGB (Mg/ha)")
        plt.savefig("{}_ts_agb.png".format(out_name))
        # plt.show()
        plt.close()

    def xrview_carbon_ts(self, vmax=0.002, lwmask=None, out_name="masked", robust=True):
        plt.style.use(["seaborn-whitegrid"])
        if lwmask is None:
            lwmask = self.da.lwmask
        box_ts = self.da.where((lwmask > 0) & (lwmask < 15), drop=True)
        box_mean = box_ts.mean("time")
        # print(box_mean)
        print(xr.where(box_mean>0,1,0).sum(['x','y']))
        plt.figure(figsize=(12, 8))
        ax = plt.axes(projection=ccrs.PlateCarree())
        # ax = plt.axes(projection=ccrs.Robinson())
        box_mean.plot(
            ax=ax,
            cmap="gist_earth_r",
            vmax=vmax,
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.12,
                "shrink": 0.8,
                "aspect": 30,
            },
            transform=ccrs.PlateCarree(),
        )
        ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        ax.coastlines(linewidth=0.5)
        ax.background_patch.set_visible(False)
        ax.outline_patch.set_visible(False)
        # gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        # gl.xlabels_top = False
        # gl.ylabels_left = False
        # gl.xformatter = LONGITUDE_FORMATTER
        # gl.yformatter = LATITUDE_FORMATTER
        # # plt.title("Colormap (Long-term Mean)")
        plt.title("")
        plt.savefig(f"{out_name}_mean_map.png", dpi=300)
        # plt.show()
        plt.close()

        ts1 = box_ts.sum(["x", "y"])
        ts1.to_series().to_csv(f'{out_name}_ts_carbon.csv')
        plt.figure(figsize=(12, 5))
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.ylabel("Carbon (Pg C)")
        plt.savefig("{}_ts_carbon.png".format(out_name))
        # plt.show()
        plt.close()

    def xrview_maskvalue_ts(self, lwmask=None, out_name="masked", robust=True):
        # plt.style.use("classic")
        plt.style.use(["classic"])
        if lwmask is None:
            lwmask = self.da.lwmask

        # box_mean = lwmask.where(lwmask > 0, drop=True)
        # plt.figure()
        # ax = plt.axes(projection=ccrs.PlateCarree())
        # box_mean.plot(
        #     ax=ax,
        #     cmap="summer_r",
        #     robust=robust,
        #     add_colorbar=False,
        # )
        # ax.add_feature(cfeature.BORDERS, linewidth=1, edgecolor="gray")
        # ax.coastlines(linewidth=1)
        # gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        # gl.xlabels_top = False
        # gl.ylabels_left = False
        # gl.xformatter = LONGITUDE_FORMATTER
        # gl.yformatter = LATITUDE_FORMATTER
        # plt.title("Amazon North-South Region")
        # plt.savefig("{}_ns_map.png".format(out_name))
        # # plt.show()
        # plt.close()

        size1 = (9, 3)
        size2 = (6, 3)
        box_ts = self.da.where(lwmask == 1, drop=True)
        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=size1)
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon South time series")
        plt.tight_layout()
        plt.savefig("{}_ts_south.png".format(out_name), dpi=200)
        # plt.show()
        plt.close()
        df_monthly = pd.DataFrame({'South': ts1.values}, index=ts1.time)

        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts2.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon South Annual series")
        plt.tight_layout()
        plt.savefig("{}_ts_south_annual.png".format(out_name), dpi=200)
        plt.close()
        df_annual = pd.DataFrame({'South': ts2.values}, index=ts2.time)

        box_ts = self.da.where(lwmask == 2, drop=True)
        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=size1)
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon North time series")
        plt.tight_layout()
        plt.savefig("{}_ts_north.png".format(out_name), dpi=200)
        # plt.show()
        plt.close()
        df_monthly['North'] = ts1.values

        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts2.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon North Annual series")
        plt.tight_layout()
        plt.savefig("{}_ts_north_annual.png".format(out_name), dpi=200)
        plt.show()
        plt.close()
        df_annual['North'] = ts2.values

        box_ts = self.da.where(lwmask > 0, drop=True)
        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=size1)
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon time series")
        plt.tight_layout()
        plt.savefig("{}_ts_all.png".format(out_name), dpi=200)
        # plt.show()
        plt.close()
        df_monthly['Amazon'] = ts1.values

        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts2.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon Annual series")
        plt.tight_layout()
        plt.savefig("{}_ts_all_annual.png".format(out_name), dpi=200)
        plt.show()
        plt.close()
        df_annual['Amazon'] = ts2.values

        df_monthly.to_csv("{}_monthly.csv".format(out_name))
        df_annual.to_csv("{}_annual.csv".format(out_name))

    def xrview_maskvalue_ts_seasonal(self, lwmask=None, out_name="masked", robust=True):
        # plt.style.use("classic")
        plt.style.use(["classic"])
        if lwmask is None:
            lwmask = self.da.lwmask

        size1 = (9, 3)
        size2 = (6, 3)
        box_ts = self.da.where(lwmask == 1, drop=True)
        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=size2)
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon Original series")
        plt.tight_layout()
        plt.savefig("{}_ts_south_monthly.png".format(out_name), dpi=200)
        plt.close()
        df_monthly = pd.DataFrame({'South': ts1.values}, index=ts1.time)

        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts2.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon South Annual series")
        plt.tight_layout()
        plt.savefig("{}_ts_south_annual_mean.png".format(out_name), dpi=200)
        plt.close()

        # ts2 = ts1.resample(time="1YS").sum("time")
        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts2.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon South Annual series")
        plt.tight_layout()
        plt.savefig("{}_ts_south_annual.png".format(out_name), dpi=200)
        plt.close()
        df_annual = pd.DataFrame({'South Total': ts2.values}, index=ts2.time)

        # ts3 = ts1.resample(time="QS-JAN").sum("time")
        ts3 = ts1.resample(time="QS-JAN").mean("time")
        ts3s = ts3.values.reshape((19, 4))
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts3s, "o-")
        plt.xlabel("Time")
        plt.title("Amazon South Seasonal series")
        plt.tight_layout()
        plt.savefig("{}_ts_south_seasonal.png".format(out_name), dpi=200)
        plt.close()
        df_annual['South JFM'] = ts3s[:, 0]
        df_annual['South AMJ'] = ts3s[:, 1]
        df_annual['South JAS'] = ts3s[:, 2]
        df_annual['South OND'] = ts3s[:, 3]


        box_ts = self.da.where(lwmask == 2, drop=True)
        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=size2)
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon Original series")
        plt.tight_layout()
        plt.savefig("{}_ts_north_monthly.png".format(out_name), dpi=200)
        plt.close()
        df_monthly['North'] = ts1.values

        # ts2 = ts1.resample(time="1YS").sum("time")
        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts2.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon North Annual series")
        plt.tight_layout()
        plt.savefig("{}_ts_north_annual.png".format(out_name), dpi=200)
        plt.show()
        plt.close()
        df_annual['North Total'] = ts2.values

        # ts3 = ts1.resample(time="QS-JAN").sum("time")
        ts3 = ts1.resample(time="QS-JAN").mean("time")
        ts3s = ts3.values.reshape((19, 4))
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts3s, "o-")
        plt.xlabel("Time")
        plt.title("Amazon North Seasonal series")
        plt.tight_layout()
        plt.savefig("{}_ts_North_seasonal.png".format(out_name), dpi=200)
        plt.close()
        df_annual['North JFM'] = ts3s[:, 0]
        df_annual['North AMJ'] = ts3s[:, 1]
        df_annual['North JAS'] = ts3s[:, 2]
        df_annual['North OND'] = ts3s[:, 3]


        box_ts = self.da.where(lwmask > 0, drop=True)
        ts1 = box_ts.mean(["x", "y"])
        plt.figure(figsize=size2)
        plt.plot(ts1.time, ts1.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon Original series")
        plt.tight_layout()
        plt.savefig("{}_ts_all_monthly.png".format(out_name), dpi=200)
        plt.close()
        df_monthly['Amazon'] = ts1.values

        # ts2 = ts1.resample(time="1YS").sum("time")
        ts2 = ts1.resample(time="1YS").mean("time")
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts2.values, "o-")
        plt.xlabel("Time")
        plt.title("Amazon Annual series")
        plt.tight_layout()
        plt.savefig("{}_ts_all_annual.png".format(out_name), dpi=200)
        plt.close()
        df_annual['All Amazon'] = ts2.values

        # ts3 = ts1.resample(time="QS-JAN").sum("time")
        ts3 = ts1.resample(time="QS-JAN").mean("time")
        ts3s = ts3.values.reshape((19, 4))
        plt.figure(figsize=size2)
        plt.plot(ts2.time, ts3s, "o-")
        plt.xlabel("Time")
        plt.title("Amazon All Seasonal series")
        plt.tight_layout()
        plt.savefig("{}_ts_All_seasonal.png".format(out_name), dpi=200)
        plt.close()
        df_annual['All JFM'] = ts3s[:, 0]
        df_annual['All AMJ'] = ts3s[:, 1]
        df_annual['All JAS'] = ts3s[:, 2]
        df_annual['All OND'] = ts3s[:, 3]


        df_monthly.to_csv("{}_monthly.csv".format(out_name))
        df_annual.to_csv("{}_annual.csv".format(out_name))

    def xrview_mask_ts_compare(self, da_comp, lwmask=None, out_name="masked", robust=True):
        sns.set(
            style="whitegrid", font_scale=1.5, rc={"figure.figsize": (8, 6), "figure.dpi": 100}
        )

        if lwmask is None:
            lwmask = self.da.lwmask.load()
        box_ts = self.da.where(lwmask == 1, drop=True)
        box_ts2 = da_comp.where(lwmask == 1, drop=True)
        box_mean = box_ts.mean("time")
        plt.figure()
        ax = plt.axes(projection=ccrs.PlateCarree())
        box_mean.plot(
            ax=ax,
            cmap="rainbow",
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.12,
                "shrink": 0.8,
                "aspect": 30,
            },
        )
        ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        ax.coastlines(linewidth=1)
        gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        gl.xlabels_top = False
        gl.ylabels_left = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        plt.title("Colormap (Long-term Mean)")
        plt.savefig("{}_mean_map.png".format(out_name))
        plt.show()
        plt.close()

        box_mean = box_ts2.mean("time")
        plt.figure()
        ax = plt.axes(projection=ccrs.PlateCarree())
        box_mean.plot(
            ax=ax,
            cmap="rainbow",
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.12,
                "shrink": 0.8,
                "aspect": 30,
            },
        )
        ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        ax.coastlines(linewidth=1)
        gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        gl.xlabels_top = False
        gl.ylabels_left = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        plt.title("Colormap (Long-term Mean)")
        plt.savefig("{}_mean_map_compared.png".format(out_name))
        plt.show()
        plt.close()

        ts1 = box_ts.mean(["x", "y"])
        ts1b = box_ts2.mean(["x", "y"])
        plt.figure(figsize=(12, 5))
        plt.plot(ts1.time, ts1.values, "o-")
        plt.plot(ts1b.time, ts1b.values, "+-")
        plt.xlabel("Time")
        plt.title("Original time series")
        plt.savefig("{}_ts_original.png".format(out_name))
        plt.show()
        plt.close()

        ts2 = ts1.resample(time="1YS").mean("time")
        ts2b = ts1b.resample(time="1YS").mean("time")
        plt.figure(figsize=(12, 5))
        plt.plot(ts2.time, ts2.values, "o-")
        plt.plot(ts2b.time, ts2b.values, "o-")
        plt.xlabel("Time")
        plt.title("Annual Mean time series")
        plt.savefig("{}_ts_annual.png".format(out_name))
        plt.show()
        plt.close()

        ts2 = ts1.resample(time="1YS").std("time")
        ts2b = ts1b.resample(time="1YS").std("time")
        plt.figure(figsize=(12, 5))
        plt.plot(ts2.time, ts2.values, "o-")
        plt.plot(ts2b.time, ts2b.values, "o-")
        plt.xlabel("Time")
        plt.title("Annual Std time series")
        plt.savefig("{}_ts_annualstd.png".format(out_name))
        plt.show()
        plt.close()

    def xrview_mask_annualts_compare(self, da_comp, lwmask=None, out_name="masked", robust=True):
        if lwmask is None:
            lwmask = self.da.lwmask.load()
        box_ts = self.da.where(lwmask == 1, drop=True)
        box_ts2 = da_comp.where(lwmask == 1, drop=True)
        box_mean = box_ts.mean("time")
        plt.figure()
        ax = plt.axes(projection=ccrs.PlateCarree())
        box_mean.plot(
            ax=ax,
            cmap="rainbow",
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.12,
                "shrink": 0.8,
                "aspect": 30,
            },
        )
        ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        ax.coastlines(linewidth=1)
        gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        gl.xlabels_top = False
        gl.ylabels_left = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        plt.title("Colormap (Long-term Mean)")
        plt.savefig("{}_mean_map.png".format(out_name), dpi=300)
        # plt.show()
        plt.close()

        box_mean = box_ts2.mean("time")
        plt.figure()
        ax = plt.axes(projection=ccrs.PlateCarree())
        box_mean.plot(
            ax=ax,
            cmap="rainbow",
            robust=robust,
            cbar_kwargs={
                "orientation": "horizontal",
                "pad": 0.12,
                "shrink": 0.8,
                "aspect": 30,
            },
        )
        ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        ax.coastlines(linewidth=1)
        gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        gl.xlabels_top = False
        gl.ylabels_left = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        plt.title("Colormap (Long-term Mean)")
        plt.savefig("{}_mean_map_compared.png".format(out_name), dpi=300)
        # plt.show()
        plt.close()

        # box_mean = box_ts2.isel(time=2)
        # plt.figure()
        # ax = plt.axes(projection=ccrs.PlateCarree())
        # box_mean.plot(
        #     ax=ax,
        #     cmap="rainbow",
        #     robust=robust,
        #     cbar_kwargs={
        #         "orientation": "horizontal",
        #         "pad": 0.12,
        #         "shrink": 0.8,
        #         "aspect": 30,
        #     },
        # )
        # ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        # ax.coastlines(linewidth=1)
        # gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        # gl.xlabels_top = False
        # gl.ylabels_left = False
        # gl.xformatter = LONGITUDE_FORMATTER
        # gl.yformatter = LATITUDE_FORMATTER
        # plt.title("Colormap (Long-term Mean)")
        # plt.savefig("{}_map2_compared.png".format(out_name), dpi=300)
        # # plt.show()
        # plt.close()
        #
        # box_mean = box_ts2.isel(time=8)
        # plt.figure()
        # ax = plt.axes(projection=ccrs.PlateCarree())
        # box_mean.plot(
        #     ax=ax,
        #     cmap="rainbow",
        #     robust=robust,
        #     cbar_kwargs={
        #         "orientation": "horizontal",
        #         "pad": 0.12,
        #         "shrink": 0.8,
        #         "aspect": 30,
        #     },
        # )
        # ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        # ax.coastlines(linewidth=1)
        # gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        # gl.xlabels_top = False
        # gl.ylabels_left = False
        # gl.xformatter = LONGITUDE_FORMATTER
        # gl.yformatter = LATITUDE_FORMATTER
        # plt.title("Colormap (Long-term Mean)")
        # plt.savefig("{}_map22_compared.png".format(out_name), dpi=300)
        # # plt.show()
        # plt.close()
        #
        # box_mean = box_ts2.isel(time=16)
        # plt.figure()
        # ax = plt.axes(projection=ccrs.PlateCarree())
        # box_mean.plot(
        #     ax=ax,
        #     cmap="rainbow",
        #     robust=robust,
        #     cbar_kwargs={
        #         "orientation": "horizontal",
        #         "pad": 0.12,
        #         "shrink": 0.8,
        #         "aspect": 30,
        #     },
        # )
        # ax.add_feature(cfeature.BORDERS, linewidth=0.5, edgecolor="gray")
        # ax.coastlines(linewidth=1)
        # gl = ax.gridlines(draw_labels=True, color="gray", alpha=0.5, linestyle="--")
        # gl.xlabels_top = False
        # gl.ylabels_left = False
        # gl.xformatter = LONGITUDE_FORMATTER
        # gl.yformatter = LATITUDE_FORMATTER
        # plt.title("Colormap (Long-term Mean)")
        # plt.savefig("{}_map222_compared.png".format(out_name), dpi=300)
        # # plt.show()
        # plt.close()

        ts1 = box_ts.mean(["x", "y"]).resample(time="1YS").mean("time")
        ts1b = box_ts2.mean(["x", "y"])
        plt.figure(figsize=(12, 5))
        plt.plot(ts1.time, ts1.values, "o-")
        plt.plot(ts1b.time, ts1b.values, "+-")
        plt.xlabel("Time")
        plt.title("Annual time series")
        plt.tight_layout()
        plt.savefig("{}_ts_annual.png".format(out_name))
        plt.show()
        plt.close()


def get_rect_polygon(xll, yll, xur, yur):
    pgon = Polygon(((xll, yll),
                    (xll, yur),
                    (xur, yur),
                    (xur, yll),
                    (xll, yll)))
    return pgon


def plot_map_trend(da0, da1m, vmin=None, vmax=None, outname=None):
    # da1m = da1m.where((da0>0) & (da0<4), np.nan)
    da1m = da1m.where(da1m.x>-120, drop=True)
    # da1m = da1m.where(da1m.y>-30, drop=True)
    # da1m = da1m.where(da1m.y<30, drop=True)
    sns.set(style="ticks", context="notebook", rc={"figure.figsize": (6, 4)})
    plt.figure(figsize=(8, 4))
    ax = plt.axes(projection=ccrs.PlateCarree())
    # ax = plt.axes(projection=ccrs.Robinson())
    da1m.plot(
        ax=ax,
        cmap="RdYlBu_r",
        # robust=True,
        ylim=[-30, 30],
        vmin=vmin,
        vmax=vmax,
        # add_colorbar=False,
        cbar_kwargs={"orientation": "horizontal", "pad": 0.15, "shrink": 0.8, "aspect": 30},
        transform=ccrs.PlateCarree(),
    )
    fname = 'lc/boundaries/wwf_rainforest.shp'
    shape_feature = ShapelyFeature(Reader(fname).geometries(),
                                   ccrs.PlateCarree(), edgecolor='black', facecolor='none')
    ax.add_feature(shape_feature, linewidth=0.5)
    ax.coastlines(linewidth=0.5)
    ax.background_patch.set_visible(False)
    ax.outline_patch.set_linewidth(1)
    ax.outline_patch.set_visible(True)
    # ax.gridlines(linewidth=0.5)
    ax.set_yticks(np.arange(-20, 21, 20), crs=ccrs.PlateCarree())
    ax.set_xticks(np.arange(-120, 151, 60), crs=ccrs.PlateCarree())
    lon_formatter = LongitudeFormatter(degree_symbol='')
    lat_formatter = LatitudeFormatter(degree_symbol='')
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    # ax.coastlines(linewidth=0.5)
    # ax.background_patch.set_visible(False)
    # ax.outline_patch.set_visible(False)
    plt.title("")
    ax.set_ylabel("")
    ax.set_xlabel("")
    plt.tight_layout()
    plt.savefig(outname, dpi=600, bbox_inches='tight')
    plt.close()


def plot_xy_on_map(lon, lat, out_name="xy_map"):
    plt.figure(figsize=(20, 15))
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.stock_img()
    plt.plot(lon, lat, "r.", transform=ccrs.PlateCarree())
    plt.savefig("{}.png".format(out_name))
    plt.show()
    plt.close()


def density_scatter_plot(
    x0,
    y0,
    x_label="Measured",
    y_label="Predicted",
    x_limit=None,
    file_name="fig_00.png",
):
    plt.style.use(["seaborn-whitegrid"])
    # Calculate the point density
    xy = np.vstack([x0, y0])
    z0 = gaussian_kde(xy)(xy)
    idx = z0.argsort()
    x0, y0, z0 = x0[idx], y0[idx], z0[idx]

    # plotting
    if x_limit is None:
        x_limit = [min(x0) - abs(np.ptp(x0))*0.05, max(x0) + abs(np.ptp(x0))*0.05]
        # print(x_limit)
    y_limit = x_limit
    fig0 = plt.figure(figsize=(8,6))
    plt.scatter(x0, y0, c=z0, cmap="jet", s=5, edgecolor="")
    # plt.scatter(x0, y0, s=15, edgecolor="")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.grid(True)
    x1 = plt.xlim(x_limit)
    y1 = plt.ylim(y_limit)
    plt.plot(x1, y1, "k-")

    r2 = r2_score(x0[:, None], y0[:, None])
    rmse = np.sqrt(mean_squared_error(x0[:, None], y0[:, None]))
    print(f'R2: {r2}; RMSE: {rmse}')
    plt.annotate(
        "$RMSE = {:1.3f}$".format(rmse) + "\n" + r"$R^2 = {:1.3f}$".format(r2),
        (0.04, 0.85),
        xycoords='axes fraction',
    )
    plt.tight_layout()
    plt.savefig(file_name, dpi=300)
    plt.close(fig0)

    # return r2, rmse
