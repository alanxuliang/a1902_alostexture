# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
# from eli5.sklearn import PermutationImportance
from sklearn.feature_selection import SelectFromModel
from eli5.permutation_importance import get_score_importances
from scipy.stats import gaussian_kde
import itertools
from sklearn.metrics import confusion_matrix
from sklearn.base import clone
from sklearn.impute import SimpleImputer, KNNImputer
from sklearn.metrics import mean_squared_error, r2_score, precision_score
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import (
    PolynomialFeatures,
    StandardScaler,
    RobustScaler,
    PowerTransformer,
    QuantileTransformer,
)
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsRegressor, KNeighborsClassifier
from sklearn.neural_network import MLPRegressor, MLPClassifier
from sklearn.linear_model import (
    LassoCV,
    RidgeCV,
    ElasticNetCV,
    LogisticRegressionCV,
    RidgeClassifierCV,
    LinearRegression,
)
from sklearn.svm import SVR, SVC
from sklearn.ensemble import (
    RandomForestRegressor,
    ExtraTreesRegressor,
    RandomForestClassifier,
    ExtraTreesClassifier,
)
from sklearn.model_selection import (
    GridSearchCV,
    KFold,
    cross_val_score,
    cross_val_predict,
    train_test_split,
)
from sklearn.multioutput import MultiOutputRegressor
from xgboost import XGBRegressor, XGBClassifier
from lightgbm import LGBMRegressor, LGBMClassifier
from sklearn.utils import compute_sample_weight
import xarray as xr
import warnings

RANDNUM = 425
warnings.filterwarnings("ignore")


def density_scatter_plot(
    x0,
    y0,
    x_label="Measured",
    y_label="Predicted",
    x_limit=None,
    y_limit=None,
    oto_line=True,
    file_name="fig_00.png",
):
    # plt.style.use(["seaborn-whitegrid"])
    # Calculate the point density
    xy = np.vstack([x0, y0])
    z0 = gaussian_kde(xy)(xy)
    idx = z0.argsort()
    x0, y0, z0 = x0[idx], y0[idx], z0[idx]

    # plotting
    if x_limit is None:
        x_limit = [min(x0) - abs(np.ptp(x0)) * 0.05, max(x0) + abs(np.ptp(x0)) * 0.05]
    if y_limit is None:
        y_limit = x_limit
    fig0 = plt.figure(figsize=(5, 4))
    plt.scatter(x0, y0, c=z0, cmap="jet", s=5)
    # plt.scatter(x0, y0, c=z0, cmap="jet", s=5, edgecolor="")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.grid(True)
    x1 = plt.xlim(x_limit)
    y1 = plt.ylim(y_limit)
    if oto_line is True:
        plt.plot(x1, y1, "k-")

    r2 = r2_score(x0[:, None], y0[:, None])
    reg1 = LinearRegression().fit(x0[:, None], y0[:, None])
    # reg2 = LinearRegression().fit(y0[:, None], x0[:, None])
    # beta0 = (reg1.coef_[0] + 1 / reg2.coef_[0]) / 2
    beta0 = reg1.coef_[0]
    rmse = np.sqrt(mean_squared_error(x0[:, None], y0[:, None]))
    plt.annotate(
        r"$R^2 = {:1.3f}$".format(r2) + "\n" + "$RMSE = {:1.3f}$".format(rmse),
        (0.04, 0.82),
        xycoords="axes fraction",
    )
    plt.tight_layout()
    plt.savefig(file_name, dpi=300, bbox_inches='tight')
    # plt.show()
    plt.close(fig0)

    return r2, rmse, beta0


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("\nNormalized confusion matrix")
    else:
        print('\nConfusion matrix, without normalization')

    print(cm)
    print()

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('Actual Class')
    plt.xlabel('Predicted Class')


def data_clean_regressor(X, y, noise_threshold=10, iter=5):
    """
    INFFC noise filtering (https://doi.org/10.1016/j.inffus.2015.04.002)
    adjusted for regression models
    :param X: Input X
    :param y: Input y
    :param noise_threshold: noise threshold
    :return: cleaned X and y and indices of the original
    """

    mdl1 = RandomForestRegressor(random_state=RANDNUM + 1)
    mdl2 = LGBMRegressor(random_state=RANDNUM + 321)
    mdl3 = ElasticNetCV(random_state=RANDNUM + 2)

    mdl_list = [mdl1, mdl2, mdl3]
    noise_list = []
    for mdl in mdl_list:
        estimators = [("scale", RobustScaler()), ("learn", mdl)]
        ppl = Pipeline(estimators)
        y_hat = cross_val_predict(ppl, X, y, cv=5)
        #         print(y_hat.shape)
        #         print(y.shape)
        y_c = np.int8(np.abs(y - y_hat) > noise_threshold)
        noise_list.append(y_c)

    noise0 = np.sum(np.stack(noise_list), axis=0)
    print(noise0.shape)

    # post-preliminary, iteration
    noise = noise0
    noise_pool = []
    for i in range(iter):
        y_renew = np.copy(y)
        Xtr = X[noise < 2, :]
        ytr = y[noise < 2]
        Xtt = X[noise >= 2, :]
        ytt = y[noise >= 2]
        noise_list = []
        for mdl in mdl_list:
            estimators = [("scale", RobustScaler()), ("learn", mdl)]
            ppl = Pipeline(estimators)
            y_hatr = cross_val_predict(ppl, Xtr, ytr, cv=5)
            y_hatt = ppl.fit(Xtr, ytr).predict(Xtt)
            y_renew[noise < 2] = y_hatr
            y_renew[noise >= 2] = y_hatt
            y_c = np.int8(np.abs(y - y_renew) > noise_threshold)
            print(
                "Fraction of noise (iter #{} in models): {:.3f}%".format(
                    i, np.sum(y_c > 0) / y_c.size * 100
                )
            )
            noise_list.append(y_c)
            noise_pool.append(y_c)
        noise = np.sum(np.stack(noise_list), axis=0)
        print(
            "Fraction of valid noise (iter #{}): {:.3f}%".format(
                i, np.sum(noise > 1) / y.size * 100
            )
        )

    noise = np.sum(np.stack(noise_pool), axis=0)
    idx = noise < 2 * iter
    X_clean = X[noise < 2 * iter, :]
    y_clean = y[noise < 2 * iter]
    print(
        "Fraction of FINAL valid noise: {:.3f}%".format(
            np.sum(noise > 2 * iter - 1) / y.size * 100
        )
    )
    plt.figure()
    n, bins, p1 = plt.hist(
        y, 20, alpha=0.5, edgecolor="gray", range=(np.min(y), np.max(y))
    )
    n, bins, p2 = plt.hist(
        y_clean, 20, alpha=0.5, edgecolor="gray", range=(np.min(y), np.max(y))
    )
    plt.xlabel("Target")
    plt.ylabel("Frequency")
    plt.legend((p1[0], p2[0]), ("Original", "Filtered"))
    plt.show()

    # save results
    return X_clean, y_clean, idx


class StackRegressor:
    """
    Build machine learning object that can find the best parameters for final run.

    """

    def __init__(self, X, y, score="neg_mean_squared_error"):
        self.X = X
        self.y = y
        self.best_params = []
        self.best_models = []
        self.bc_models = []
        self.score = score
        self.meta_mdl = RidgeCV(scoring=self.score)
        # self.meta_mdl = Pipeline([
        #     ("pf", PolynomialFeatures()),
        #     ("learn", RidgeCV(scoring=self.score)),
        # ])
        # self.meta_mdl = XGBRegressor(n_jobs=5)
        # self.meta_mdl = RandomForestRegressor(n_estimators=100, min_samples_split=5, n_jobs=5)

    def save_models(self, outname="saved_models.pkl"):
        pickle.dump(
            [self.meta_mdl, self.best_models, self.bc_models], open(outname, "wb")
        )

    def load_models(self, outname="saved_models.pkl"):
        self.meta_mdl, self.best_models, self.bc_models = pickle.load(
            open(outname, "rb")
        )

    def meta_model(self, X=None, y=None, weights=None, k=5):
        """
        Meta model for stacking Regressors
        :return:
        """
        if X is None:
            X = self.X
        if y is None:
            y = self.y
        if weights is None:
            weights = np.ones_like(y)

        models = []
        y_pred_list = []
        for mdl in self.best_models:
            # mdl.fit(X, y, learn__sample_weight=weights)
            mdl.fit(X, y)
            models.append(mdl)
            y_pred = cross_val_predict(mdl, X, y, cv=k, n_jobs=5)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        self.best_models = models

        cv_score = cross_val_score(
            self.meta_mdl, y_pred_stacked, y, scoring=self.score, cv=k, n_jobs=5
        )
        print("cross-validated score: {}".format(cv_score))
        print("mean cv score: {}".format(cv_score.mean()))

        # self.meta_mdl.fit(y_pred_stacked, y, sample_weight=weights)
        self.meta_mdl.fit(y_pred_stacked, y)
        print(y_pred_stacked.shape)
        if y_pred_stacked.shape[1] > 1:
            plt.figure(figsize=(12, 8))
            cm = np.corrcoef(y_pred_stacked, rowvar=False)
            sns.set(font_scale=1)
            sns.heatmap(cm, center=0, annot=True, square=True, fmt=".2f")
            plt.show()
        return self.meta_mdl

    def meta_model_biascorr(self, X=None, y=None, k=5):
        """
        Meta model for stacking Regressors
        :return:
        """
        if X is None:
            X = self.X
        if y is None:
            y = self.y

        models = []
        bc_models = []
        y_pred_list = []
        for mdl in self.best_models:
            mdl.fit(X, y)
            models.append(mdl)
            y_hat = cross_val_predict(mdl, X, y, cv=k, n_jobs=5)
            if len(y_hat.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            eps = y - y_hat
            yb = y_hat - eps
            mdl2 = clone(mdl)
            mdl2.fit(X, yb)
            bc_models.append(mdl2)
            y_pred = y_hat + (y_hat - cross_val_predict(mdl2, X, y, cv=k, n_jobs=5))
            # y_pred = y_hat + (y_hat - mdl2.predict(X))
            print(mean_squared_error(y_pred[:, None], y[:, None]))
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        self.best_models = models
        self.bc_models = bc_models

        cv_score = cross_val_score(
            self.meta_mdl, y_pred_stacked, y, scoring=self.score, cv=k, n_jobs=5
        )
        print("cross-validated score: {}".format(cv_score))
        print("mean cv score: {}".format(cv_score.mean()))

        self.meta_mdl.fit(y_pred_stacked, y)
        print(y_pred_stacked.shape)
        if y_pred_stacked.shape[1] > 1:
            plt.figure(figsize=(12, 8))
            cm = np.corrcoef(y_pred_stacked, rowvar=False)
            sns.set(font_scale=1)
            sns.heatmap(cm, center=0, annot=True, square=True, fmt=".2f")
            plt.show()
        return self.meta_mdl, self.best_models, self.bc_models

    def meta_model_predict(self, X_test, y_test=None, models=None, meta_model=None):
        """

        :param X_test: test set of X
        :param y_test: test set of y
        :param models: model list of trained pipelines
        :param meta_model: trained meta model for final output
        :return: final prediction y
        """
        if models is None:
            models = self.best_models
        if meta_model is None:
            meta_model = self.meta_mdl

        y_pred_list = []
        for mdl in models:
            y_pred = mdl.predict(X_test)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        y_final = meta_model.predict(y_pred_stacked)

        if y_test is not None:
            score = meta_model.score(y_pred_stacked, y_test)
            print("test score: {}".format(score))

        return y_final

    def meta_model_predict_biascorr(self, X_test, y_test=None):
        """

        :param X_test: test set of X
        :param y_test: test set of y
        :return: final prediction y
        """
        y_pred_list = []
        for imdl in range(len(self.best_models)):
            y_hat1 = self.best_models[imdl].predict(X_test)
            y_hat2 = self.bc_models[imdl].predict(X_test)
            y_pred = y_hat1 * 2 - y_hat2
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        if y_test is not None:
            score = mean_squared_error(y_pred_stacked[:, 0], y_test[:, None])
            print("test score 0: {}".format(score))
        y_final = self.meta_mdl.predict(y_pred_stacked)

        return y_final

    def rf_model_feature_importance(
        self,
        X=None,
        y=None,
        thresh=0.0001,
        figsize=(4, 8),
        n_feature=10,
        outname="feature_importance_plot.png",
    ):
        if X is not None:
            # model = PermutationImportance(self.best_models[0].steps[-1][1]).fit(X, y)
            model = self.best_models[0].steps[-1][1]
            if model.feature_importances_ is not None:
                clist = [X.columns.values, np.arange(len(X.columns))]
                tuples = list(zip(*clist))
                col_names = pd.MultiIndex.from_tuples(tuples, names=["name", "id"])
                # col_names = [f'{X.columns[i]}_{i}' for i in range(len(X.columns))]
                feat_importances = pd.Series(
                    model.feature_importances_, index=col_names
                )
                finl = feat_importances.nlargest(n_feature)
                sns.set(font_scale=1, rc={"figure.figsize": figsize})
                sns.barplot(x=finl, y=finl.index)
                plt.xlabel("Feature Importance Score")
                plt.title("Important Features")
                plt.tight_layout()
                plt.savefig(outname, dpi=300)

    def meta_model_predict_biascorr_trees(
        self, X_test, y_test, X_pred, n_estimator=200
    ):
        """
        RF estimations for each tree
        assuming the first model to be the RFBC model and no other models
        :param X_test: test set of X
        :param X_pred: set of X for prediction
        :param y_test: test set of y
        :return: final prediction y
        """
        y_cv_lst = []
        xtest_1 = Pipeline(self.best_models[0].steps[:-1]).transform(X_test)
        xtest_2 = Pipeline(self.bc_models[0].steps[:-1]).transform(X_test)
        # for i in range(len(self.bc_models[0].steps[-1][1].estimators_)):
        for i in range(n_estimator):
            # y_cv1 = cross_val_predict(self.best_models[0].steps[-1][1].estimators_[i], xtest_1, y_test, cv=5, n_jobs=5)
            # y_cv2 = cross_val_predict(self.bc_models[0].steps[-1][1].estimators_[i], xtest_2, y_cv1 * 2 - y_test, cv=5, n_jobs=5)
            y_cv1 = (
                self.best_models[0]
                .steps[-1][1]
                .estimators_[i]
                .predict(xtest_1.astype("float32"), check_input=False)
            )
            y_cv2 = (
                self.bc_models[0]
                .steps[-1][1]
                .estimators_[i]
                .predict(xtest_2.astype("float32"), check_input=False)
            )
            y_cvbc = y_cv1 * 2 - y_cv2
            y_cv_lst.append(y_cvbc)

        # y_cv_var = np.nanvar(np.stack(y_cv_lst, axis=0), axis=0)
        # y_cv_mean = self.meta_model_predict_biascorr(X_test)
        y_cv_all = np.stack(y_cv_lst, axis=0)
        y_cv_lst = []
        for i in range(100):
            id = np.random.choice(np.arange(n_estimator), size=n_estimator)
            if i == 1:
                print(y_cv_all[id, :].shape)
            y_cv_selected = np.nanmean(y_cv_all[id, :], axis=0)
            y_cv_var = np.nanvar(y_cv_all[id, :], axis=0)
            y_cv_lst.append(np.sqrt((y_test - y_cv_selected) ** 2 / y_cv_var))
        taus = np.stack(y_cv_lst, axis=0)
        print(taus.shape)
        # y_cv_mean = self.meta_model_predict_biascorr(X_test)
        #
        # taus = np.sqrt((y_test - y_cv_mean) ** 2 / y_cv_var)
        plt.hist(taus.flatten(), bins=50, range=[0, 10])
        plt.show()
        tau95 = np.percentile(taus.flatten(), 95)
        print("tau95 = {}".format(tau95))

        xpred_1 = Pipeline(self.best_models[0].steps[:-1]).transform(X_pred)
        xpred_2 = Pipeline(self.bc_models[0].steps[:-1]).transform(X_pred)
        y_pred = np.zeros((X_pred.shape[0], n_estimator), dtype=np.float64)
        # y_pred = np.zeros((X_pred.shape[0], len(self.bc_models[0].steps[-1][1].estimators_)), dtype=np.float64)
        # for i in range(len(self.bc_models[0].steps[-1][1].estimators_)):
        for i in range(n_estimator):
            prediction1 = (
                self.best_models[0]
                .steps[-1][1]
                .estimators_[i]
                .predict(xpred_1.astype("float32"), check_input=False)
            )
            prediction2 = (
                self.bc_models[0]
                .steps[-1][1]
                .estimators_[i]
                .predict(xpred_2.astype("float32"), check_input=False)
            )
            y_pred[:, i] = prediction1 * 2 - prediction2
        return y_pred, tau95

    def grid_opts(self, pipe, param_grid, k=5):
        """
        grid search of hyper parameters
        :param pipe:
        :param param_grid:
        """
        print("Grid Search for pipeline of {}...".format(pipe.steps[-1][1]))
        grid = GridSearchCV(
            pipe, cv=k, n_jobs=2, param_grid=param_grid, verbose=1, scoring=self.score
        )
        grid.fit(self.X, self.y)
        print(grid.best_params_)
        # print(np.sqrt(-grid.best_score_))
        print(grid.best_score_)
        self.best_params.append(grid.best_params_)
        self.best_models.append(grid.best_estimator_)

    def setup_rf_model(self, param_grid=None):
        """
        setup rf model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": [None],
                "learn__n_estimators": [100, 300],
                "learn__min_samples_split": [5, 30],
            }

        mdl = RandomForestRegressor(n_estimators=100, min_samples_split=5, n_jobs=-1)

        estimators = [
            ("scale", StandardScaler()),
            ("impute", SimpleImputer()),
            ("pca", PCA()),
            ("learn", mdl),
        ]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_rf_feature_model(self, param_grid=None):
        """
        setup rf model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": [None],
                "learn__n_estimators": [100, 300],
                "learn__min_samples_split": [5, 30],
            }

        mdl = RandomForestRegressor(n_estimators=400, min_samples_split=2, n_jobs=-1)

        estimators = [
            ("scale", StandardScaler()),
            ("impute", SimpleImputer()),
            ("pca", PCA()),
            ("feature", SelectFromModel(mdl, threshold=0.01)),
            ("learn", mdl),
        ]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_et_model(self, param_grid=None):
        """
        setup et model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__n_estimators": [100, 300],
                "learn__min_samples_split": [5, 30],
            }

        mdl = ExtraTreesRegressor(
            n_estimators=100, min_samples_split=5, random_state=RANDNUM + 5
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_xgb_model(self, param_grid=None):
        """
        setup xgboost model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": [None],
                "learn__n_estimators": [400],
                "learn__max_depth": [9],
                # "learn__booster": ["gbtree","gblinear", "dart"],
                "learn__booster": ["gbtree"],
                "learn__colsample_bytree": [0.8, 1],
            }

        mdl = XGBRegressor(learning_rate=0.1, n_jobs=-1, random_state=RANDNUM + 6)

        estimators = [
            ("scale", StandardScaler()),
            ("impute", SimpleImputer()),
            ("pca", PCA()),
            ("learn", mdl),
        ]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_xgb_multi_model(self, param_grid=None):
        """
        setup xgboost multi-output regression model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "pca": [None],
                "learn__estimator__n_estimators": [400],
                "learn__estimator__max_depth": [9],
                # "learn__estimator__booster": ["gbtree","gblinear", "dart"],
                "learn__estimator__booster": ["dart"],
                "learn__estimator__colsample_bytree": [0.8, 1],
            }

        mdl = MultiOutputRegressor(
            XGBRegressor(learning_rate=0.1, n_jobs=5, random_state=RANDNUM + 6),
            n_jobs=5,
        )

        estimators = [("scale", StandardScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_lgb_model(self, param_grid=None):
        """
        setup lightgbm model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__n_estimators": [100, 300],
                "learn__num_leaves": [10, 400],
                "learn__learning_rate": [0.01, 0.1],
            }

        mdl = LGBMRegressor(
            learning_rate=0.1, n_estimators=50, num_leaves=30, random_state=RANDNUM + 7
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_mlp_model(self, param_grid=None):
        """
        setup mlp model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__hidden_layer_sizes": [5, 300],
                "learn__learning_rate_init": [0.01, 0.1],
            }

        mdl = MLPRegressor(
            learning_rate_init=0.1, hidden_layer_sizes=(50,), random_state=RANDNUM + 9
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_lassocv_model(self, param_grid=None):
        """
        setup lassocv model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {}

        mdl = LassoCV(random_state=RANDNUM + 10)

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_ridgecv_model(self, param_grid=None):
        """
        setup lassocv model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "scale": [
                    StandardScaler(),
                    RobustScaler(),
                    PowerTransformer(),
                    QuantileTransformer(),
                ],
                "poly": [PolynomialFeatures(1), PolynomialFeatures(2)],
            }

        mdl = RidgeCV(alphas=[1e-3, 1e-2, 1e-1, 1])

        estimators = [
            ("scale", RobustScaler()),
            ("poly", PolynomialFeatures()),
            ("learn", mdl),
        ]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_elascv_model(self, param_grid=None):
        """
        setup ElasticNetCV model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {}

        mdl = ElasticNetCV(
            l1_ratio=[0.01, 0.1, 0.3, 0.5, 0.7, 0.9, 0.95, 0.99], random_state=RANDNUM + 12, n_jobs=-1
        )

        estimators = [
            ("scale", RobustScaler()),
            ("impute", KNNImputer()),
            ("pca", PCA()),
            ("poly", PolynomialFeatures(1)),
            ("learn", mdl),
        ]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_svr_model(self, param_grid=None):
        """
        setup svr model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {"learn__C": [1, 100], "learn__epsilon": [0.01, 0.1]}

        mdl = SVR(C=1, epsilon=0.01)

        estimators = [
            ("scale", RobustScaler()),
            ("impute", SimpleImputer()),
            ("pca", PCA()),
            ("learn", mdl),
        ]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_knn_model(self, param_grid=None):
        """
        setup knn model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {"pca__n_components": [2, 10, 30], "learn__n_neighbors": [3, 30]}

        mdl = KNeighborsRegressor(n_neighbors=5, weights="distance", n_jobs=-1)

        estimators = [
            ("scale", RobustScaler()),
            ("impute", SimpleImputer()),
            ("pca", PCA()),
            ("learn", mdl),
        ]
        pipe = Pipeline(estimators)

        return pipe, param_grid


class StackClassifier:
    """
    Build machine learning object that can find the best parameters for final run.

    """

    def __init__(self, X, y, score="accuracy"):
        self.X = X
        self.y = y
        self.best_params = []
        self.best_models = []
        self.score = score
        self.meta_mdl = LogisticRegressionCV(
            scoring=self.score, random_state=RANDNUM + 41
        )
        # self.meta_mdl = LGBMClassifier()

    def save_models(self, outname="saved_models.pkl"):
        pickle.dump(
            [self.meta_mdl, self.best_models], open(outname, "wb")
        )

    def load_models(self, outname="saved_models.pkl"):
        self.meta_mdl, self.best_models = pickle.load(
            open(outname, "rb")
        )

    def meta_model(self, X=None, y=None, weights=None, k=5):
        """
        Meta model for stacking classifiers
        :return:
        """
        if X is None:
            X = self.X
        if y is None:
            y = self.y
        if weights is None:
            weights = np.ones_like(y)

        models = []
        y_pred_list = []
        for mdl in self.best_models:
            mdl.fit(X, y, learn__sample_weight=weights)
            # mdl.fit(X, y)
            models.append(mdl)
            y_pred = cross_val_predict(mdl, X, y, cv=k, n_jobs=5)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        self.best_models = models

        cv_score = cross_val_score(
            self.meta_mdl, y_pred_stacked, y, scoring=self.score, cv=k, n_jobs=5
        )
        print("cross-validated score: {}".format(cv_score))
        print("mean cv score: {}".format(cv_score.mean()))

        self.meta_mdl.fit(y_pred_stacked, y, sample_weight=weights)
        # self.meta_mdl.fit(y_pred_stacked, y)
        print(y_pred_stacked.shape)
        if y_pred_stacked.shape[1] > 1:
            plt.figure(figsize=(12, 8))
            cm = np.corrcoef(y_pred_stacked, rowvar=False)
            sns.set(font_scale=1)
            sns.heatmap(cm, center=0, annot=True, square=True, fmt=".2f")
            plt.show()
        return self.meta_mdl

    def meta_model_predict(self, X_test, y_test=None, models=None, meta_model=None):
        """

        :param X_test: test set of X
        :param y_test: test set of y
        :param models: model list of trained pipelines
        :param meta_model: trained meta model for final output
        :return: final prediction y
        """
        if models is None:
            models = self.best_models
        if meta_model is None:
            meta_model = self.meta_mdl

        y_pred_list = []
        for mdl in models:
            y_pred = mdl.predict(X_test)
            if len(y_pred.shape) != 1:
                msg = "Model output is not in the correct array shape (n, )..."
                raise ValueError(msg)
            y_pred_list.append(y_pred)
        y_pred_stacked = np.stack(y_pred_list, axis=-1)
        y_final = meta_model.predict(y_pred_stacked)

        if y_test is not None:
            score = meta_model.score(y_pred_stacked, y_test)
            print("test score: {}".format(score))

        return y_final

    def grid_opts(self, pipe, param_grid, k=5):
        """
        grid search of hyper parameters
        :param pipe:
        :param param_grid:
        """
        print("Grid Search for pipeline of {}...".format(pipe.steps[-1][1]))
        grid = GridSearchCV(
            pipe, cv=k, n_jobs=-1, param_grid=param_grid, verbose=1, scoring=self.score
        )
        grid.fit(self.X, self.y)
        print(grid.best_params_)
        print(grid.best_score_)
        self.best_params.append(grid.best_params_)
        self.best_models.append(grid.best_estimator_)

    def setup_rf_model(self, param_grid=None):
        """
        setup rf model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__n_estimators": [100, 300],
                "learn__min_samples_split": [5, 30],
            }

        mdl = RandomForestClassifier(
            n_estimators=100, min_samples_split=5, random_state=RANDNUM + 43
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_et_model(self, param_grid=None):
        """
        setup et model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__n_estimators": [100, 300],
                "learn__min_samples_split": [5, 30],
            }

        mdl = ExtraTreesClassifier(
            n_estimators=100, min_samples_split=5, random_state=RANDNUM + 45
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]

        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_xgb_model(self, param_grid=None):
        """
        setup xgboost model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__n_estimators": [100, 300],
                "learn__max_depth": [3, 8],
                "learn__learning_rate": [0.01, 0.1],
            }

        mdl = XGBClassifier(
            learning_rate=0.1, n_estimators=50, max_depth=5, random_state=RANDNUM + 46
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_lgb_model(self, param_grid=None):
        """
        setup lightgbm model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__n_estimators": [100, 300],
                "learn__num_leaves": [10, 400],
                "learn__learning_rate": [0.01, 0.1],
            }

        mdl = LGBMClassifier(
            learning_rate=0.1, n_estimators=50, num_leaves=30, random_state=RANDNUM + 47
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_mlp_model(self, param_grid=None):
        """
        setup mlp model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {
                "learn__hidden_layer_sizes": [5, 300],
                "learn__learning_rate_init": [0.01, 0.1],
            }

        mdl = MLPClassifier(
            learning_rate_init=0.1, hidden_layer_sizes=(50,), random_state=RANDNUM + 49
        )

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_logisticcv_model(self, param_grid=None):
        """
        setup LogisticRegressionCV model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {}

        mdl = LogisticRegressionCV(scoring=self.score, random_state=RANDNUM + 410)

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_ridgecv_model(self, param_grid=None):
        """
        setup RidgeClassifierCV model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {}

        mdl = RidgeClassifierCV(scoring=self.score)

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_svc_model(self, param_grid=None):
        """
        setup svc model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {"learn__C": [1, 100], "learn__gamma": [0.01, 0.1]}

        mdl = SVC(C=1, gamma=0.01, random_state=RANDNUM + 413)

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid

    def setup_knn_model(self, param_grid=None):
        """
        setup knn model
        :return: pipe, param_grid
        """
        if param_grid is None:
            param_grid = {"pca__n_components": [2, 30], "learn__n_neighbors": [3, 30]}

        mdl = KNeighborsClassifier(n_neighbors=5, weights="distance")

        estimators = [("scale", RobustScaler()), ("pca", PCA()), ("learn", mdl)]
        pipe = Pipeline(estimators)

        return pipe, param_grid


def data_train(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=777
    )
    if X_train.shape[0] > 3000:
        _, X_cv, _, y_cv = train_test_split(
            X_train, y_train, test_size=3000, random_state=77
        )
    else:
        X_cv = X_train
        y_cv = y_train

    model_regress = StackRegressor(X_cv, y_cv)
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [500],
    #         # "learn__max_depth": [12],
    #         # "learn__min_samples_split": [2],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__min_samples_split": [2, 7, 12],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_pipe, params_grid = model_regress.setup_xgb_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [250, 500, 750],
            "learn__max_depth": [6, 9, 12],
            "learn__booster": ["gbtree"],
            "learn__colsample_bytree": [0.6, 1],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=3)

    # pipe, pgrid = model_regress.setup_elascv_model(
    #     {
    #         "pca": [None],
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer(), QuantileTransformer()],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # pipe, pgrid = model_regress.setup_svr_model(
    #     {
    #         "pca": [None],
    #         "learn__C": [1, 10, 30, 50],
    #         "learn__epsilon": [0.1, 0.01, 0.001],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # model_pipe, params_grid = model_regress.setup_knn_model(
    #     param_grid={
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer()],
    #         "pca": [None],
    #         "learn__n_neighbors": [3, 6, 10, 15, 20, 30],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_y_pred = model_regress.meta_model_predict(X_test)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # model_y_pred[model_y_pred < 0] = 0

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )
    return model_regress, r2, rmse, beta0


def data_train_svr(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=777
    )
    if X_train.shape[0] > 3000:
        _, X_cv, _, y_cv = train_test_split(
            X_train, y_train, test_size=3000, random_state=77
        )
    else:
        X_cv = X_train
        y_cv = y_train

    model_regress = StackRegressor(X_cv, y_cv)

    pipe, pgrid = model_regress.setup_svr_model(
        {
            "pca": [None],
            "learn__C": [1, 10, 30, 50, 80, 120],
            "learn__epsilon": [0.5, 0.2, 0.1, 0.01, 0.001],
        }
    )
    model_regress.grid_opts(pipe, pgrid, k=5)

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_y_pred = model_regress.meta_model_predict(X_test)

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )
    return model_regress, r2, rmse, beta0


def data_train_knn(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=777
    )
    if X_train.shape[0] > 3000:
        _, X_cv, _, y_cv = train_test_split(
            X_train, y_train, test_size=3000, random_state=77
        )
    else:
        X_cv = X_train
        y_cv = y_train

    model_regress = StackRegressor(X_cv, y_cv)

    model_pipe, params_grid = model_regress.setup_knn_model(
        param_grid={
            "scale": [StandardScaler(), RobustScaler(), PowerTransformer()],
            "pca": [None],
            "learn__n_neighbors": [3, 6, 10, 15, 20, 30],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_y_pred = model_regress.meta_model_predict(X_test)

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )
    return model_regress, r2, rmse, beta0


def data_train_rfbc(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=777
    )
    if X_train.shape[0] > 3000:
        _, X_cv, _, y_cv = train_test_split(
            X_train, y_train, test_size=3000, random_state=77
        )
    else:
        X_cv = X_train
        y_cv = y_train

    model_regress = StackRegressor(X_cv, y_cv)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    # model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    # model_y_pred = model_regress.meta_model_predict(X_test)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    model_y_pred[model_y_pred < 0] = 0

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )
    return model_regress, r2, rmse, beta0


def data_train_xgb(X, y, outname="test07_agb"):
    X_train = X[:-3200, :]
    y_train = y[:-3200, ]
    X_test = X[-3000:, :]
    y_test = y[-3000:, ]
    _, X_cv, _, y_cv = train_test_split(
        X_train, y_train, test_size=2000, random_state=77
    )

    model_regress = StackRegressor(X_cv, y_cv)
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [500],
    #         # "learn__max_depth": [12],
    #         # "learn__min_samples_split": [2],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__min_samples_split": [2, 7, 12],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_pipe, params_grid = model_regress.setup_xgb_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [250, 500, 750],
            "learn__max_depth": [6, 9, 12],
            "learn__booster": ["gbtree"],
            "learn__colsample_bytree": [0.6, 1],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=3)

    # pipe, pgrid = model_regress.setup_elascv_model(
    #     {
    #         "pca": [None],
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer(), QuantileTransformer()],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # pipe, pgrid = model_regress.setup_svr_model(
    #     {
    #         "pca": [None],
    #         "learn__C": [1, 10, 30, 50],
    #         "learn__epsilon": [0.1, 0.01, 0.001],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # model_pipe, params_grid = model_regress.setup_knn_model(
    #     param_grid={
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer()],
    #         "pca": [None],
    #         "learn__n_neighbors": [3, 6, 10, 15, 20, 30],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_y_pred = model_regress.meta_model_predict(X_test)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # model_y_pred[model_y_pred < 0] = 0

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )
    return model_regress, r2, rmse, beta0


def data_train_lc(X, y, outname="test07_agb"):
    # X_train, X_test, y_train, y_test = train_test_split(
    #     X, y, test_size=0.2, random_state=777
    # )
    # if X_train.shape[0] > 3000:
    #     _, X_cv, _, y_cv = train_test_split(
    #         X_train, y_train, test_size=3000, random_state=77
    #     )
    # else:
    #     X_cv = X_train
    #     y_cv = y_train
    X_train = X[:-4000, :]
    y_train = y[:-4000, ]
    X_test = X[-3000:, :]
    y_test = y[-3000:, ]
    _, X_cv, _, y_cv = train_test_split(
        X_train, y_train, test_size=2000, random_state=77
    )

    model_regress = StackClassifier(X_cv, y_cv)
    # model_pipe, params_grid = model_regress.setup_rf_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [500],
    #         # "learn__max_depth": [12],
    #         # "learn__min_samples_split": [2],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__min_samples_split": [2, 7, 12],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_pipe, params_grid = model_regress.setup_xgb_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [250, 500, 750],
            "learn__max_depth": [6, 9, 12],
            "learn__booster": ["gbtree"],
            "learn__colsample_bytree": [0.6, 1],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=3)

    # pipe, pgrid = model_regress.setup_elascv_model(
    #     {
    #         "pca": [None],
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer(), QuantileTransformer()],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # pipe, pgrid = model_regress.setup_svr_model(
    #     {
    #         "pca": [None],
    #         "learn__C": [1, 10, 30, 50],
    #         "learn__epsilon": [0.1, 0.01, 0.001],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # model_pipe, params_grid = model_regress.setup_knn_model(
    #     param_grid={
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer()],
    #         "pca": [None],
    #         "learn__n_neighbors": [3, 6, 10, 15, 20, 30],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    sample_weight = compute_sample_weight('balanced', y_train)
    model_regress.meta_model(X=X_train, y=y_train, weights=sample_weight, k=3)
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_y_pred = model_regress.meta_model_predict(X_test)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # model_y_pred[model_y_pred < 0] = 0

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    x0 = y_test[~np.isnan(y_test)]
    y0 = model_y_pred[~np.isnan(y_test)]
    print(f"precision score: {precision_score(x0, y0)}")

    fig0 = plt.figure(figsize=(4, 3))
    plt.scatter(x0, y0, s=1)
    plt.xlabel("Measured")
    plt.ylabel("Predicted")
    plt.grid(True)
    r2 = r2_score(x0[:, None], y0[:, None])
    reg1 = LinearRegression().fit(x0[:, None], y0[:, None])
    beta0 = reg1.coef_[0]
    rmse = np.sqrt(mean_squared_error(x0[:, None], y0[:, None]))
    plt.annotate(
        r"$R^2 = {:1.3f}$".format(r2) + "\n" + "$RMSE = {:1.3f}$".format(rmse),
        (0.04, 0.82),
        xycoords="axes fraction",
    )
    plt.tight_layout()
    plt.savefig("{}_val.png".format(outname), dpi=300)
    plt.show()
    plt.close(fig0)

    return model_regress, r2, rmse, beta0


def data_train_ensemble(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=666
    )
    if X_train.shape[0] > 2000:
        _, X_cv, _, y_cv = train_test_split(
            X_train, y_train, test_size=2000, random_state=77
        )
    else:
        X_cv = X_train
        y_cv = y_train

    model_regress = StackRegressor(X_cv, y_cv)
    model_pipe, params_grid = model_regress.setup_rf_model(
        param_grid={
            "pca": [None],
            "learn__n_estimators": [100, 300, 500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [3, 6, 9, 12],
            "learn__min_samples_split": [2, 5, 8, 13],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_pipe, params_grid = model_regress.setup_xgb_model(
        param_grid={
            "impute": [None],
            "pca": [None],
            # "pca": [None],
            "learn__n_estimators": [50, 100, 250, 600],
            "learn__max_depth": [3, 6, 9, 12],
            "learn__booster": ["gbtree"],
            "learn__colsample_bytree": [0.3, 0.6, 0.9, 1],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    # pipe, pgrid = model_regress.setup_elascv_model(
    #     {
    #         "pca": [None],
    #         "scale": [
    #             StandardScaler(),
    #             RobustScaler(),
    #             PowerTransformer(),
    #             QuantileTransformer(),
    #         ],
    #         "poly": [PolynomialFeatures(1), PolynomialFeatures(2)],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)

    pipe, pgrid = model_regress.setup_svr_model(
        {
            "pca": [None],
            "learn__C": [1, 10, 30, 50, 100],
            "learn__epsilon": [0.3, 0.1, 0.01, 0.001],
        }
    )
    model_regress.grid_opts(pipe, pgrid, k=5)

    model_pipe, params_grid = model_regress.setup_knn_model(
        param_grid={
            "scale": [StandardScaler(), RobustScaler(), PowerTransformer(), QuantileTransformer()],
            "pca": [None],
            "learn__n_neighbors": [3, 6, 10, 20, 30, 40],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    # model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_y_pred = model_regress.meta_model_predict(X_test)
    # model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    # model_y_pred[model_y_pred < 0] = 0

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )

    return model_regress, r2, rmse, beta0


def data_train_nfeatrue(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=777
    )
    _, X_cv, _, y_cv = train_test_split(
        X_train, y_train, test_size=1200, random_state=42
    )

    model_regress = StackRegressor(X_cv, y_cv)
    model_pipe, params_grid = model_regress.setup_rf_feature_model(
        param_grid={
            "pca": [None],
            "feature__threshold": [0.001],
            "learn__n_estimators": [500],
            # "learn__max_depth": [12],
            # "learn__min_samples_split": [2],
            "learn__max_depth": [6, 9, 12],
            "learn__min_samples_split": [2, 7, 12],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=5)
    # model_pipe, params_grid = model_regress.setup_xgb_model(
    #     param_grid={
    #         "pca": [None],
    #         "learn__n_estimators": [250, 500, 750],
    #         "learn__max_depth": [6, 9, 12],
    #         "learn__booster": ["gbtree"],
    #         "learn__colsample_bytree": [0.6, 1],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    # pipe, pgrid = model_regress.setup_elascv_model(
    #     {
    #         "pca": [None],
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer(), QuantileTransformer()],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # pipe, pgrid = model_regress.setup_svr_model(
    #     {
    #         "pca": [None],
    #         "learn__C": [1, 10, 30, 50],
    #         "learn__epsilon": [0.1, 0.01, 0.001],
    #     }
    # )
    # model_regress.grid_opts(pipe, pgrid, k=5)
    #
    # model_pipe, params_grid = model_regress.setup_knn_model(
    #     param_grid={
    #         "scale": [StandardScaler(), RobustScaler(), PowerTransformer()],
    #         "pca": [None],
    #         "learn__n_neighbors": [3, 6, 10, 15, 20, 30],
    #     }
    # )
    # model_regress.grid_opts(model_pipe, params_grid, k=5)

    # model_regress.meta_model(X=X_train, y=y_train, k=3)

    print(model_regress.best_models[0].steps[-2][1].transform(X_cv).shape)
    model_regress.meta_model_biascorr(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))

    # model_y_pred = model_regress.meta_model_predict(X_test)
    model_y_pred = model_regress.meta_model_predict_biascorr(X_test, y_test)
    model_y_pred[model_y_pred < 0] = 0

    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )

    return model_regress, r2, rmse, beta0


def data_train_elas(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=777
    )
    if X_train.shape[0] > 2000:
        _, X_cv, _, y_cv = train_test_split(
            X_train, y_train, test_size=2000, random_state=77
        )
    else:
        X_cv = X_train
        y_cv = y_train

    model_regress = StackRegressor(X_cv, y_cv)
    model_pipe, params_grid = model_regress.setup_elascv_model(
        param_grid={
            # "scale": [StandardScaler(), RobustScaler(), PowerTransformer(), QuantileTransformer()],
            "scale": [RobustScaler()],
            "pca": [None],
            "poly": [PolynomialFeatures(1),
                     PolynomialFeatures(2),
                     PolynomialFeatures(3),
                     PolynomialFeatures(4),
                     PolynomialFeatures(5)],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=3)

    # model_regress.best_models[0].fit(X_train, y_train)
    # model_y_pred = model_regress.best_models[0].predict(X_test)
    # model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))
    model_y_pred = model_regress.meta_model_predict(X_test)

    # c1 = model_regress.best_models[0].named_steps["learn"].intercept_
    # b1 = model_regress.best_models[0].named_steps["learn"].coef_
    # if X.shape[1] == 2:
    #     B1 = ["1", "x1", "x2", "x1^2", "x1x2", "x2^2"]
    #     b1s = [f"{b1[i]:+.2f}*{B1[i]}" for i in range(1, len(b1))]
    #     s1 = f"Fitted Model:  y = {c1:.2f} " + " ".join(b1s)
    #     print(s1)
    # elif X.shape[1] == 1:
    #     B1 = ["1", "x", "x^2"]
    #     b1s = [f"{b1[i]:+.2f}*{B1[i]}" for i in range(1, len(b1))]
    #     s1 = f"Fitted Model:  y = {c1:.2f} " + " ".join(b1s)
    #     print(s1)


    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )

    return model_regress, r2, rmse, beta0


def data_train_ridge(X, y, outname="test07_agb"):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=777
    )
    if X_train.shape[0] > 2000:
        _, X_cv, _, y_cv = train_test_split(
            X_train, y_train, test_size=2000, random_state=77
        )
    else:
        X_cv = X_train
        y_cv = y_train

    model_regress = StackRegressor(X_cv, y_cv)
    model_pipe, params_grid = model_regress.setup_ridgecv_model(
        param_grid={
            # "scale": [StandardScaler(), RobustScaler(), PowerTransformer(), QuantileTransformer()],
            "scale": [RobustScaler()],
            "poly": [PolynomialFeatures(1),
                    ],
        }
    )
    model_regress.grid_opts(model_pipe, params_grid, k=3)

    # model_regress.best_models[0].fit(X_train, y_train)
    # model_y_pred = model_regress.best_models[0].predict(X_test)
    # model_regress.save_models(outname="{}_training.pkl".format(outname))

    model_regress.meta_model(X=X_train, y=y_train, k=3)
    model_regress.save_models(outname="{}_training.pkl".format(outname))
    model_y_pred = model_regress.meta_model_predict(X_test)

    # c1 = model_regress.best_models[0].named_steps["learn"].intercept_
    # b1 = model_regress.best_models[0].named_steps["learn"].coef_
    # if X.shape[1] == 2:
    #     B1 = ["1", "x1", "x2", "x1^2", "x1x2", "x2^2"]
    #     b1s = [f"{b1[i]:+.2f}*{B1[i]}" for i in range(1, len(b1))]
    #     s1 = f"Fitted Model:  y = {c1:.2f} " + " ".join(b1s)
    #     print(s1)
    # elif X.shape[1] == 1:
    #     B1 = ["1", "x", "x^2"]
    #     b1s = [f"{b1[i]:+.2f}*{B1[i]}" for i in range(1, len(b1))]
    #     s1 = f"Fitted Model:  y = {c1:.2f} " + " ".join(b1s)
    #     print(s1)


    if isinstance(y_test, pd.core.series.Series):
        y_test = y_test.values

    r2, rmse, beta0 = density_scatter_plot(
        y_test[~np.isnan(y_test)],
        model_y_pred[~np.isnan(y_test)],
        file_name="{}_val.png".format(outname),
    )

    return model_regress, r2, rmse, beta0
