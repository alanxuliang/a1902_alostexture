# import modules (using ALOS/SRTM/L8 layers @ 50m)
import os
import subprocess
import glob
import h5py
import requests
import numpy as np
import pandas as pd
from joblib import Parallel, delayed
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

print(1)


# %% GEDI preprocessing

class gedi_process():
    def __init__(self, region, lat0, lon0, out_name):
        self.region = region
        self.out_name = out_name
        self.lat0 = lat0
        self.lon0 = lon0

    def gedi_extraction(self, path_in):
        files = glob.glob(path_in)
        out_list = []
        for k in range(len(files)):
            print(f"{k}: {files[k]}")
            basename = os.path.basename(files[k])
            keyname = basename.split("_", 3)
            keyname = "".join(keyname[:3])
            out_csv = f"{self.out_name}_{keyname}.csv"
            if os.path.exists(out_csv):
                out_list.append(out_csv)
            else:
                try:
                    with h5py.File(files[k], 'r') as f:
                        h_lst = []
                        gtx = ['BEAM0000', 'BEAM0001', 'BEAM0010', 'BEAM0011',
                               'BEAM0101', 'BEAM0110', 'BEAM1000', 'BEAM1011']
                        for i in range(len(gtx)):
                            # print(i)
                            gti = gtx[i]
                            lati = f[f'/{gti}/lat_lowestmode'].value[:, None]
                            loni = f[f'/{gti}/lon_lowestmode'].value[:, None]
                            qualityflag = f[f'/{gti}/quality_flag'].value
                            id_bound = (qualityflag == 1) & (lati.ravel() > self.lat0[0]) & (
                                    lati.ravel() < self.lat0[1]) & (loni.ravel() > self.lon0[0]) & (
                                               loni.ravel() < self.lon0[1])
                            if np.sum(id_bound) > 0:
                                lati = f[f'/{gti}/lat_lowestmode'].value[id_bound, None]
                                loni = f[f'/{gti}/lon_lowestmode'].value[id_bound, None]
                                hmetric1 = f[f'/{gti}/rh'].value[id_bound, :]
                                hmetric2 = f[f'/{gti}/elev_lowestmode'].value[id_bound, None]
                                num_detectedmodes = f[f'/{gti}/num_detectedmodes'].value[id_bound, None]
                                sensitivity = f[f'/{gti}/sensitivity'].value[id_bound, None]
                                elevation_bin0_error = f[f'/{gti}/elevation_bin0_error'].value[id_bound, None]
                                tandem_x = f[f'/{gti}/digital_elevation_model'].value[id_bound, None]
                                elevation_bias_flag = f[f'/{gti}/elevation_bias_flag'].value[id_bound, None]
                                beam = f[f'/{gti}/beam'].value[id_bound, None]
                                channel = f[f'/{gti}/channel'].value[id_bound, None]
                                delta_time = f[f'/{gti}/delta_time'].value[id_bound, None]
                                shot_number = f[f'/{gti}/shot_number'].value[id_bound, None]
                                solar_elevation = f[f'/{gti}/solar_elevation'].value[id_bound, None]
                                degrade_flag = f[f'/{gti}/degrade_flag'].value[id_bound, None]
                                surface_flag = f[f'/{gti}/surface_flag'].value[id_bound, None]
                                vcf1 = f[f'/{gti}/land_cover_data/landsat_treecover'].value[id_bound, None]
                                vcf2 = f[f'/{gti}/land_cover_data/modis_treecover'].value[id_bound, None]
                                vcf3 = f[f'/{gti}/land_cover_data/modis_nonvegetated'].value[id_bound, None]
                                energy_total = f[f'/{gti}/energy_total'].value[id_bound, None]
                                energy_lowest = f[f'/{gti}/geolocation/energy_lowestmode_a1'].value[id_bound, None]
                                selected_algo = f[f'/{gti}/selected_algorithm'].value[id_bound, None]
                                h_all = np.concatenate([lati, loni, hmetric1, hmetric2, num_detectedmodes,
                                                        sensitivity, elevation_bin0_error, tandem_x,
                                                        elevation_bias_flag, beam, channel, delta_time,
                                                        shot_number, solar_elevation, degrade_flag, surface_flag,
                                                        vcf1, vcf2, vcf3, energy_total, energy_lowest, selected_algo
                                                        ], axis=1)
                                h_lst.append(h_all)
                        if len(h_lst) > 0:
                            h_arr = np.concatenate(h_lst, axis=0)
                        else:
                            h_arr = []
                        df0 = pd.DataFrame(h_arr, columns=['lat', 'lon'] + [f'rh{i}' for i in range(0, 101, 1)] +
                                                          ['elev_lowest', 'num_modes', 'sensitivity', 'elev_err',
                                                           'elev_tandem', 'elev_bias', 'beam', 'channel',
                                                           'delta_time', 'shot_number', 'solar_elev', 'degrade_flag',
                                                           'surface_flag', 'landsat_vcf', 'modis_vcf', 'modis_nvf',
                                                           'energy_total', 'energy_lowest', 'selected_algo'])
                        df0.to_csv(out_csv, index=False)
                    out_list.append(out_csv)
                except:
                    print("------------------- h5 file corrupted -------------------")
                    print(out_csv)
        return out_list

    def parallel_dl(self, link_i, path_in):
        basename = os.path.basename(link_i)
        file_path = os.path.join(path_in, basename)
        keyname = basename.split("_", 3)
        keyname = "".join(keyname[:3])
        out_csv = f"{self.out_name}_{keyname}.csv"
        if os.path.exists(out_csv):
            print(out_csv)
            csv_lst = [out_csv]
        else:
            if ~os.path.exists(file_path):
                cmmd = f"python D:/Codes/pcprojects/a1902_alostexture/a1902alos/DAACDataDownload.py " \
                       f"-dir {path_in} -f {link_i}"
                subprocess.check_output(cmmd, shell=True)
                csv_lst = self.gedi_extraction(file_path)
            else:
                csv_lst = self.gedi_extraction(file_path)
            os.remove(file_path)
        return csv_lst

    def gedi_download(self, path_in, keyname="GEDI02_A", n_jobs=4):
        bbox = f"[{self.lat0[1]},{self.lon0[0]},{self.lat0[0]},{self.lon0[1]}]"
        url = f"https://lpdaacsvc.cr.usgs.gov/services/gedifinder?product={keyname}&version=001&bbox={bbox}"
        response = requests.get(url, verify=False)
        if response.status_code == 200:
            resp_data = response.json()
            gedi_lst = resp_data['data']
            s = pd.Series(gedi_lst, name="url")
            s.to_csv(os.path.join(path_in, "url.csv"))
            out_lst = Parallel(n_jobs=n_jobs)(delayed(self.parallel_dl)(i, path_in)
                                              for i in gedi_lst)
            return out_lst
        else:
            print(f"Download Error (Code: {response.status_code})")
            return None
